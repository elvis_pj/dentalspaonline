package com.online.dentalspa.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;

public class BasicDataSourcePool {
	private static Logger log = Logger.getLogger(BasicDataSourcePool.class);
	private Map<String, BasicDataSource> dataSources = new HashMap<String, BasicDataSource>();
	private static BasicDataSourcePool pool;
	
	private BasicDataSourcePool(){
		BasicDataSource dataSource=null;
		Properties prop=null;
		try {
			prop = new Properties();
			prop.load(this.getClass().getClassLoader().getResourceAsStream("DataBaseConfig.properties"));

			dataSource = new BasicDataSource();
			dataSource.setDriverClassName(prop.getProperty("driver"));
			dataSource.setUrl(prop.getProperty("url"));
			dataSource.setUsername(prop.getProperty("username"));
			dataSource.setPassword(prop.getProperty("password"));
			dataSource.setDefaultAutoCommit(false);
			dataSource.setInitialSize(1);
			dataSource.setMinIdle(1);
			dataSource.setMaxIdle(4);
			dataSource.setMaxTotal(16);
			dataSource.setMaxWaitMillis(10000);
			dataSource.setRemoveAbandonedTimeout(5);
			dataSource.setLogAbandoned(true);

			dataSources.put(prop.getProperty("databaseid"), dataSource);
			
			log.info("Recurso Agregado["+prop.getProperty("databaseid")+"]");
		} catch (Exception e) {
			e.printStackTrace();
			log.fatal("No se logro inicializar el pool de conexiones", e);
		}
	}
	
	public static BasicDataSourcePool getInstance() {
		if(pool==null)
			pool = new BasicDataSourcePool();
		return pool;
	}
	
	public BasicDataSource getBasicDataSource(String dataBaseName)throws Exception
	{
		try
		{
			if(this.dataSources==null || this.dataSources.size()<1)
				throw new Exception("No se logro inicializar el pool de conexiones");
			
			if(this.dataSources.containsKey(dataBaseName))
				return this.dataSources.get(dataBaseName);
			
			log.error("No existe el recurso solicitado["+dataBaseName+"]");
			throw new Exception("No existe el recurso solicitado");
		}catch(Exception ex) {
			ex.printStackTrace();
			log.error("Error al recuperar el recurso["+dataBaseName+"] :: "+ex.getMessage());
			throw new Exception("Se genero un error al recuperar el recurso de base de datos",ex);
		}
	}
}
