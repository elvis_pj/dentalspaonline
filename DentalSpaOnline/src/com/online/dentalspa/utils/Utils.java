package com.online.dentalspa.utils;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection; 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import com.online.dentalspa.dao.BitacoraDao;
import com.online.dentalspa.dao.BitacoraDaoImpl;
import com.online.dentalspa.model.Antecedentes;
import com.online.dentalspa.model.Bitacora;
import com.online.dentalspa.model.Citas;
import com.online.dentalspa.model.Contactosemergencia;
import com.online.dentalspa.model.Datosfiscales;
import com.online.dentalspa.model.Estados;
import com.online.dentalspa.model.Examendental;
import com.online.dentalspa.model.Formapago;
import com.online.dentalspa.model.Odontogramas;
import com.online.dentalspa.model.Pacientes;
import com.online.dentalspa.model.Pagos;
import com.online.dentalspa.model.Pagosdetalle;
import com.online.dentalspa.model.Pagosdetalletipo;
import com.online.dentalspa.model.Parentescos;
import com.online.dentalspa.model.Perfil;
import com.online.dentalspa.model.Permisos;
import com.online.dentalspa.model.Plataforma;
import com.online.dentalspa.model.Procesos;
import com.online.dentalspa.model.Tipopago;
import com.online.dentalspa.model.Tipotratamiento;
import com.online.dentalspa.model.Tratamientodetalle;
import com.online.dentalspa.model.Tratamientos;
import com.online.dentalspa.model.Usuario;

public class Utils {
	public static final String pathFiles="/Users/lelvispj/Downloads/cargasWeb";
	private Logger log = Logger.getLogger(Utils.class);
	private String DetailError;

	public boolean sessionActive(HttpServletRequest request) {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuariosession");
		
		if((usuario==null || usuario.getUsuarioid()<1) 
				|| (usuario.getPerfil().getPermisos()==null 
					|| usuario.getPerfil().getPermisos().size()<1))
		{
			DetailError="Tu session ha expirado, por favor inicia sesion nuevamente.";
			return false;
		}
		return true;
	}

	public Bitacora isSessionActive(HttpServletRequest request) {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuariosession");
		
		if((usuario==null || usuario.getUsuarioid()<1) 
				|| (usuario.getPerfil().getPermisos()==null || usuario.getPerfil().getPermisos().size()<1))
		{
			DetailError="Tu session ha expirado, por favor inicia sesion nuevamente.";
			return null;
		}
		return validaPermiso(request, usuario);
	}
	
	private Bitacora validaPermiso(HttpServletRequest request, Usuario usuario) {
		BitacoraDao bitacoraDao = new BitacoraDaoImpl();
		Bitacora bitacora = new Bitacora();
		String pathRequest = request.getRequestURI();
		if(pathRequest.indexOf("DentalSpaOnline")>=0)
			pathRequest = pathRequest.substring(16, pathRequest.length());
		for(Permisos permiso: usuario.getPerfil().getPermisos())
		{
			if(pathRequest.equals(permiso.getProcesos().getProcesopath()))
			{
				bitacora.setPermiso(permiso);
				break;
			}
		}
		if(bitacora.getPermiso()==null)
		{
			log.error("No cuentas con permisos para ingresar a esta opcion:: "+pathRequest);
			DetailError="No cuentas con permisos para ingresar a esta opcion "+pathRequest;
			return null;
		}
		bitacora.setPlataformaid(bitacora.getPermiso().getProcesos().getPlataformaid());
		bitacora.setProcesoid(bitacora.getPermiso().getProcesoid());
		bitacora.setUsuarioid(usuario.getUsuarioid());
		bitacora.setBitacoraerror("");
		bitacora.setBitacoramensaje("");
		bitacora.setBitacorapeticion("");
		bitacora.setBitacorarespuesta("");
		bitacora.setBitacoraip(request.getRemoteAddr());
		bitacora.setBitacorafecha(new Timestamp(System.currentTimeMillis()));
		if(!bitacoraDao.createBitacora(bitacora))
		{
			DetailError = bitacoraDao.getDetailError();
			return null;
		}
		return bitacora;
	}

	public void updateBitacora(Bitacora bitacora) {
		BitacoraDao bitacoraDao = new BitacoraDaoImpl();
		if(!bitacoraDao.updateBitacora(bitacora))
			log.error("No se logro actualizar la bitacora");
	}

//	public Connection getConnection(String database) throws Exception
//	{
//		Connection myConn=null;
//		try {
//			InitialContext ctx = new InitialContext();
//			Context env = (Context) ctx.lookup("java:comp/env");
//			DataSource ds = (DataSource) env.lookup("jdbc/"+database);
//			myConn=ds.getConnection();
//		}catch (Exception ex) {
//			ex.printStackTrace();
//			log.fatal(ex);
//			throw new Exception("Se genero un error al solicitar la conexion a la base de datos ["+database+"]. "+ex.getMessage());
//		}
//		finally
//		{
//			if(myConn==null)
//				throw new Exception("No se logro conectar con la base de datos ["+database+"]");
//		}
//		return myConn;
//	}

	public Connection getConnection(String database) throws Exception
	{
		Connection connection=null;
		try {
			BasicDataSource basicDataSource = BasicDataSourcePool.getInstance().getBasicDataSource(database);
			connection=basicDataSource.getConnection();
		}catch (Exception ex) {
			ex.printStackTrace();
			log.fatal("Se genero un error al solicitar la conexion a la base de datos ["+database+"]. ", ex);
			throw new Exception("Se genero un error al solicitar la conexion a la base de datos. "+ex.toString());
		}
		finally
		{
			if(connection==null)
				throw new Exception("No se logro conectar con la base de datos ["+database+"]");
		}
		return connection;
	}

	public void closeConnection(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet) 
	{
		try
		{
			if( resultSet != null )
				resultSet.close();
			
			if( preparedStatement != null)
				preparedStatement.close();
			
			if(connection!=null && !connection.isClosed())
				connection.close();
		}catch(Exception sql)
		{
			log.fatal(sql);
		}		
	}

	public Usuario getObjectUsuario(ResultSet resultSet) throws Exception {
		Usuario usuario;
		try {
			usuario = new Usuario();
			usuario.setUsuarioid(resultSet.getInt("usuarioid"));
			usuario.setPerfilid(resultSet.getInt("perfilid"));
			usuario.setUsuarioactivo(resultSet.getBoolean("usuarioactivo"));
			usuario.setUsuariocorreo(resultSet.getString("usuariocorreo"));
			usuario.setUsuariopwd(resultSet.getString("usuariopwd"));
			usuario.setUsuarionombre(resultSet.getString("usuarionombre"));
			usuario.setUsuarioapellidopaterno(resultSet.getString("usuarioapellidopaterno"));
			usuario.setUsuarioapellidomaterno(resultSet.getString("usuarioapellidomaterno"));
			usuario.setUsuariofechacreacion(resultSet.getTimestamp("usuariofechacreacion"));
			usuario.setUsuariofechamodificacion(resultSet.getTimestamp("usuariofechamodificacion"));
			usuario.setUsuarioultimoacceso(resultSet.getTimestamp("usuarioultimoacceso"));
			usuario.setUsuariokey(resultSet.getString("usuariokey"));
			usuario.setUsuarioimage(resultSet.getBinaryStream("usuarioimage"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Usuario:: "+ex.getMessage());
		}
		return usuario;
	}

	public Perfil getObjectPerfil(ResultSet resultSet) throws Exception{
		Perfil perfil;
		try {
			perfil = new Perfil();
			perfil.setPerfilid(resultSet.getInt("perfilid"));
			perfil.setPerfilactivo(resultSet.getBoolean("perfilactivo"));
			perfil.setPerfilnombre(resultSet.getString("perfilnombre"));
			perfil.setPerfildescripcion(resultSet.getString("perfildescripcion"));
			perfil.setPerfilfechacreacion(resultSet.getTimestamp("perfilfechacreacion"));
			perfil.setPerfilfechamodificacion(resultSet.getTimestamp("perfilfechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Perfil:: "+ex.getMessage());
		}
		return perfil;
	}

	public Permisos getObjectPermisos(ResultSet resultSet) throws Exception{
		Permisos permiso;
		try {
			permiso = new Permisos();
			permiso.setPermisoid(resultSet.getInt("permisoid"));
			permiso.setPermisoactivo(resultSet.getBoolean("permisoactivo"));
			permiso.setPerfilid(resultSet.getInt("perfilid"));
			permiso.setProcesoid(resultSet.getInt("procesoid"));
			permiso.setPermisofechacreacion(resultSet.getTimestamp("permisofechacreacion"));
			permiso.setPermisofechamodificacion(resultSet.getTimestamp("permisofechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Permisos:: "+ex.getMessage());
		}
		return permiso;
	}

	public Procesos getObjectProcesos(ResultSet resultSet) throws Exception{
		Procesos proceso;
		try {
			proceso = new Procesos();
			proceso.setProcesoid(resultSet.getInt("procesoid"));
			proceso.setPlataformaid(resultSet.getInt("plataformaid"));
			proceso.setProcesoactivo(resultSet.getBoolean("procesoactivo"));
			proceso.setProcesonombre(resultSet.getString("procesonombre"));
			proceso.setProcesodescripcion(resultSet.getString("procesodescripcion"));
			proceso.setProcesofechacreacion(resultSet.getTimestamp("procesofechacreacion"));
			proceso.setProcesofechamodificacion(resultSet.getTimestamp("procesofechamodificacion"));
			proceso.setProcesopath(resultSet.getString("procesopath"));
			proceso.setProcesopadreid(resultSet.getInt("procesopadreid"));
			proceso.setProcesoprincipal(resultSet.getBoolean("procesoprincipal"));
			proceso.setProcesoorden(resultSet.getInt("procesoorden"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Procesos:: "+ex.getMessage());
		}
		return proceso;
	}

	public Plataforma getObjectPlataforma(ResultSet resultSet) throws Exception{
		Plataforma plataforma;
		try {
			plataforma = new Plataforma();
			plataforma.setPlataformaid(resultSet.getInt("plataformaid"));
			plataforma.setPlataformaactivo(resultSet.getBoolean("plataformaactivo"));
			plataforma.setPlataformanombre(resultSet.getString("plataformanombre"));
			plataforma.setPlataformadescripcion(resultSet.getString("plataformadescripcion"));
			plataforma.setPlataformafechacreacion(resultSet.getTimestamp("plataformafechacreacion"));
			plataforma.setPlataformafechamodificacion(resultSet.getTimestamp("plataformafechamodificacion"));
			plataforma.setPlataformarutaprincipal(resultSet.getString("plataformarutaprincipal"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Plataforma:: "+ex.getMessage());
		}
		return plataforma;
	}

	public Estados getObjectEstados(ResultSet resultSet) throws Exception{
		Estados estado;
		try {
			estado = new Estados();
			estado.setEstadoid(resultSet.getInt("estadoid"));
			estado.setEstadonombre(resultSet.getString("estadonombre"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Estados:: "+ex.getMessage());
		}
		return estado;
	}

	public Pacientes getObjectPacientes(ResultSet resultSet) throws Exception{
		Pacientes paciente;
		try {
			paciente = new Pacientes();
			paciente.setPacienteid(resultSet.getInt("pacienteid"));
			paciente.setUsuarioid(resultSet.getInt("usuarioid"));
			paciente.setDatosfiscalid(resultSet.getInt("datofiscalid"));
			paciente.setPacienteactivo(resultSet.getBoolean("pacienteactivo"));
			paciente.setPacientenombre(resultSet.getString("pacientenombre"));
			paciente.setPacienteapellidopaterno(resultSet.getString("pacienteapellidopaterno"));
			paciente.setPacienteapellidomaterno(resultSet.getString("pacienteapellidomaterno"));
			paciente.setPacientesexo(resultSet.getString("pacientesexo"));
			paciente.setPacienteedad(resultSet.getInt("pacienteedad"));
			paciente.setPacienteestadocivil(resultSet.getString("pacienteestadocivil"));
			paciente.setPacientelugarnacimiento(resultSet.getString("pacientelugarnacimiento"));
			paciente.setPacientefechanacimiento(resultSet.getDate("pacientefechanacimiento"));
			paciente.setPacientedomicilio(resultSet.getString("pacientedomicilio"));
			paciente.setPacienteciudad(resultSet.getString("pacienteciudad"));
			paciente.setPacientecodigopostal(resultSet.getString("pacientecodigopostal"));
			paciente.setEstadoid(resultSet.getInt("estadoid"));
			paciente.setPacientecorreo(resultSet.getString("pacientecorreo"));
			paciente.setPacientetelefono(resultSet.getString("pacientetelefono"));
			paciente.setPacientecelular(resultSet.getString("pacientecelular"));
			paciente.setPacientefechacreacion(resultSet.getTimestamp("pacientefechacreacion"));
			paciente.setPacientefechamodificacion(resultSet.getTimestamp("pacientefechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Pacientes:: "+ex.getMessage());
		}
		return paciente;
	}

	public Datosfiscales getObjectDatosfiscales(ResultSet resultSet) throws Exception{
		Datosfiscales datofiscal;
		try {
			datofiscal = new Datosfiscales();
			datofiscal.setDatofiscalid(resultSet.getInt("datofiscalid"));
			datofiscal.setDatofiscalnombre(resultSet.getString("datofiscalnombre"));
			datofiscal.setDatofiscalrfc(resultSet.getString("datofiscalrfc"));
			datofiscal.setDatofiscaldireccion(resultSet.getString("datofiscaldireccion"));
			datofiscal.setDatofiscalcorreo(resultSet.getString("datofiscalcorreo"));
			datofiscal.setDatofiscalfechacreacion(resultSet.getTimestamp("datofiscalfechacreacion"));
			datofiscal.setDatofiscalfechamodificacion(resultSet.getTimestamp("datofiscalfechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Datosfiscales:: "+ex.getMessage());
		}
		return datofiscal;
	}

	public Antecedentes getObjectAntecedentes(ResultSet resultSet)throws Exception{
		Antecedentes antecedentes=null;
		try {
			antecedentes = new Antecedentes();
			antecedentes.setAntecedentelid(resultSet.getInt("antecedentelid"));
			antecedentes.setPacienteid(resultSet.getInt("pacienteid"));
			antecedentes.setAntecedenteactivo(resultSet.getBoolean("antecedenteactivo"));
			antecedentes.setAntecedentetratamientomedico(resultSet.getString("antecedentetratamientomedico"));
			antecedentes.setAntecedenteembarazo(resultSet.getString("antecedenteembarazo"));
			antecedentes.setAntecedentemedicamentos(resultSet.getString("antecedentemedicamentos"));
			antecedentes.setAntecedentegsaq(resultSet.getString("antecedentegsaq"));
			antecedentes.setAntecedentealergias(resultSet.getString("antecedentealergias"));
			antecedentes.setAntecedenteetsvih(resultSet.getString("antecedenteetsvih"));
			antecedentes.setAntecedentecoagulacion(resultSet.getString("antecedentecoagulacion"));
			antecedentes.setAntecedentecancer(resultSet.getString("antecedentecancer"));
			antecedentes.setAntecedenteccrn(resultSet.getString("antecedenteccrn"));
			antecedentes.setAntecedentepdtf(resultSet.getString("antecedentepdtf"));
			antecedentes.setAntecedentefechacreacion(resultSet.getTimestamp("antecedentefechacreacion"));
			antecedentes.setAntecedentefechamodificacion(resultSet.getTimestamp("antecedentefechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Antecedentes:: "+ex.getMessage());
		}
		return antecedentes;
	}

	public Contactosemergencia getObjectContactosemergencia(ResultSet resultSet)throws Exception {
		Contactosemergencia contacto=null;
		try {
			contacto = new Contactosemergencia();
			contacto.setContactosemergenciaid(resultSet.getInt("contactosemergenciaid"));
			contacto.setPacienteid(resultSet.getInt("pacienteid"));
			contacto.setParentescoid(resultSet.getInt("parentescoid"));
			contacto.setContactosemergencianombre(resultSet.getString("contactosemergencianombre"));
			contacto.setContactosemergenciatelefono(resultSet.getString("contactosemergenciatelefono"));
			contacto.setContactosemergenciacelular(resultSet.getString("contactosemergenciacelular"));
			contacto.setContactosemergenciacorreo(resultSet.getString("contactosemergenciacorreo"));
			contacto.setContactosemergenciafechacreacion(resultSet.getTimestamp("contactosemergenciafechacreacion"));
			contacto.setContactosemergenciafechamodificacion(resultSet.getTimestamp("contactosemergenciafechamodificacion"));
			contacto.setContactosemergenciaactivo(resultSet.getBoolean("contactosemergenciaactivo"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Antecedentes:: "+ex.getMessage());
		}
		return contacto;
	}

	public Parentescos getObjectParentescos(ResultSet resultSet)throws Exception {
		Parentescos parentesco=null;
		try {
			parentesco = new Parentescos();
			parentesco.setParentescoid(resultSet.getInt("parentescoid"));
			parentesco.setParentesconombre(resultSet.getString("parentesconombre"));
			parentesco.setParentescodescripcion(resultSet.getString("parentescodescripcion"));
			parentesco.setParentescoactivo(resultSet.getBoolean("parentescoactivo"));
			parentesco.setParentescofechacreacion(resultSet.getTimestamp("parentescofechacreacion"));
			parentesco.setParentescofechamodificacion(resultSet.getTimestamp("parentescofechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Parentescos:: "+ex.getMessage());
		}
		return parentesco;
	}

	public Tratamientos getObjectTratamientos(ResultSet resultSet)throws Exception {
		Tratamientos tratamiento = null;
		try {
			tratamiento = new Tratamientos();
			tratamiento.setTratamientoid(resultSet.getInt("tratamientoid"));
			tratamiento.setPacienteid(resultSet.getInt("pacienteid"));
			tratamiento.setTipotratamientoid(resultSet.getInt("tipotratamientoid"));
			tratamiento.setTratamientocomentariosiniciales(resultSet.getString("tratamientocomentariosiniciales"));
			tratamiento.setTratamientoplan(resultSet.getString("tratamientoplan"));
			tratamiento.setTratamientoactivo(resultSet.getBoolean("tratamientoactivo"));
			tratamiento.setTratamientoterminado(resultSet.getBoolean("tratamientoterminado"));
			tratamiento.setTratamientofechacreacion(resultSet.getTimestamp("tratamientofechacreacion"));
			tratamiento.setTratamientofechamodificacion(resultSet.getTimestamp("tratamientofechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Tratamientos:: "+ex.getMessage());
		}
		return tratamiento;
	}

	public Tipotratamiento getObjectTipotratamiento(ResultSet resultSet)throws Exception {
		Tipotratamiento tipotratamiento = null;
		try {
			tipotratamiento = new Tipotratamiento();
			tipotratamiento.setTipotratamientoid(resultSet.getInt("tipotratamientoid"));
			tipotratamiento.setTipotratamientoactivo(resultSet.getBoolean("tipotratamientoactivo"));
			tipotratamiento.setTipotratamientonombre(resultSet.getString("tipotratamientonombre"));
			tipotratamiento.setTipotratamientodescripcion(resultSet.getString("tipotratamientodescripcion"));
			tipotratamiento.setTipotratamientofechacreacion(resultSet.getTimestamp("tipotratamientofechacreacion"));
			tipotratamiento.setTipotratamientofechamodifcacion(resultSet.getTimestamp("tipotratamientofechamodifcacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Tipotratamiento:: "+ex.getMessage());
		}
		return tipotratamiento;
	}

	public Examendental getObjectExamendental(ResultSet resultSet)throws Exception {
		Examendental examendental=null;
		try {
			examendental = new Examendental();
			examendental.setExamendentalid(resultSet.getInt("examendentalid"));
			examendental.setPacienteid(resultSet.getInt("pacienteid"));
			examendental.setExamendentalactivo(resultSet.getBoolean("examendentalactivo"));
			examendental.setExamendentalhigiene(resultSet.getString("examendentalhigiene"));
			examendental.setExamendentalencia(resultSet.getString("examendentalencia"));
			examendental.setExamendentalpisoboca(resultSet.getString("examendentalpisoboca"));
			examendental.setExamendentallabios(resultSet.getString("examendentallabios"));
			examendental.setExamendentalcarillos(resultSet.getString("examendentalcarillos"));
			examendental.setExamendentalbruxismo(resultSet.getString("examendentalbruxismo"));
			examendental.setExamendentalchasquidos(resultSet.getString("examendentalchasquidos"));
			examendental.setExamendentallengua(resultSet.getString("examendentallengua"));
			examendental.setExamendentalpaladar(resultSet.getString("examendentalpaladar"));
			examendental.setExamendentalsupernumerarios(resultSet.getString("examendentalsupernumerarios"));
			examendental.setExamendentalsangrado(resultSet.getString("examendentalsangrado"));
			examendental.setExamendentalplacasarro(resultSet.getString("examendentalplacasarro"));
			examendental.setExamendentalalteraciones(resultSet.getString("examendentalalteraciones"));
			examendental.setExamendentalnota(resultSet.getString("examendentalnota"));
			examendental.setExamendentalfechacreacion(resultSet.getTimestamp("examendentalfechacreacion"));
			examendental.setExamendentalfechamodificacion(resultSet.getTimestamp("examendentalfechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Examendental:: "+ex.getMessage());
		}
		return examendental;
	}

	public Citas getObjectCitas(ResultSet resultSet)throws Exception {
		Citas cita=null;
		try {
			cita = new Citas();
			cita.setCitaid(resultSet.getInt("citaid"));
			cita.setUsuarioid(resultSet.getInt("usuarioid"));
			cita.setPacienteid(resultSet.getInt("pacienteid"));
			cita.setCitaestatusid(resultSet.getInt("citaestatusid"));
			cita.setCitanombre(resultSet.getString("citanombre"));
			cita.setCitacomentarios(resultSet.getString("citacomentarios"));
			cita.setCitafechainicio(resultSet.getTimestamp("citafechainicio"));
			cita.setCitafechafin(resultSet.getTimestamp("citafechafin"));
			cita.setCitafechacreacion(resultSet.getTimestamp("citafechacreacion"));
			cita.setCitafechamodificacion(resultSet.getTimestamp("citafechamodificacion"));
			cita.setTratamientoid(resultSet.getInt("tratamientoid"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Citas:: "+ex.getMessage());
		}
		return cita;
	}

	public Odontogramas getObjectOdontograma(ResultSet resultSet)throws Exception {
		Odontogramas odontrograma=null;
		try {
			odontrograma = new Odontogramas();
			odontrograma.setOdontogramaid(resultSet.getInt("odontogramaid"));
			odontrograma.setTratamientoid(resultSet.getInt("tratamientoid"));
			odontrograma.setOdontogramaactivo(resultSet.getBoolean("odontogramaactivo"));
			odontrograma.setOdontogramacomentarios(resultSet.getString("odontogramacomentarios"));
			odontrograma.setOdontogramafechacreacion(resultSet.getTimestamp("odontogramafechacreacion"));
			odontrograma.setOdontogramafechamodificacion(resultSet.getTimestamp("odontogramafechamodificacion"));
			odontrograma.setOdontograma_11(resultSet.getString("odontograma_11"));
			odontrograma.setOdontograma_12(resultSet.getString("odontograma_12"));
			odontrograma.setOdontograma_13(resultSet.getString("odontograma_13"));
			odontrograma.setOdontograma_14(resultSet.getString("odontograma_14"));
			odontrograma.setOdontograma_15(resultSet.getString("odontograma_15"));
			odontrograma.setOdontograma_16(resultSet.getString("odontograma_16"));
			odontrograma.setOdontograma_17(resultSet.getString("odontograma_17"));
			odontrograma.setOdontograma_18(resultSet.getString("odontograma_18"));
			odontrograma.setOdontograma_21(resultSet.getString("odontograma_21"));
			odontrograma.setOdontograma_22(resultSet.getString("odontograma_22"));
			odontrograma.setOdontograma_23(resultSet.getString("odontograma_23"));
			odontrograma.setOdontograma_24(resultSet.getString("odontograma_24"));
			odontrograma.setOdontograma_25(resultSet.getString("odontograma_25"));
			odontrograma.setOdontograma_26(resultSet.getString("odontograma_26"));
			odontrograma.setOdontograma_27(resultSet.getString("odontograma_27"));
			odontrograma.setOdontograma_28(resultSet.getString("odontograma_28"));
			odontrograma.setOdontograma_31(resultSet.getString("odontograma_31"));
			odontrograma.setOdontograma_32(resultSet.getString("odontograma_32"));
			odontrograma.setOdontograma_33(resultSet.getString("odontograma_33"));
			odontrograma.setOdontograma_34(resultSet.getString("odontograma_34"));
			odontrograma.setOdontograma_35(resultSet.getString("odontograma_35"));
			odontrograma.setOdontograma_36(resultSet.getString("odontograma_36"));
			odontrograma.setOdontograma_37(resultSet.getString("odontograma_37"));
			odontrograma.setOdontograma_38(resultSet.getString("odontograma_38"));
			odontrograma.setOdontograma_41(resultSet.getString("odontograma_41"));
			odontrograma.setOdontograma_42(resultSet.getString("odontograma_42"));
			odontrograma.setOdontograma_43(resultSet.getString("odontograma_43"));
			odontrograma.setOdontograma_44(resultSet.getString("odontograma_44"));
			odontrograma.setOdontograma_45(resultSet.getString("odontograma_45"));
			odontrograma.setOdontograma_46(resultSet.getString("odontograma_46"));
			odontrograma.setOdontograma_47(resultSet.getString("odontograma_47"));
			odontrograma.setOdontograma_48(resultSet.getString("odontograma_48"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Odontogramas:: "+ex.getMessage());
		}
		return odontrograma;
	}

	public Tratamientodetalle getObjectTratamientodetalle(ResultSet resultSet)throws Exception {
		Tratamientodetalle tratamientodetalle=null;
		try {
			tratamientodetalle = new Tratamientodetalle();
			tratamientodetalle.setTratamientodetalleid(resultSet.getInt("tratamientodetalleid"));
			tratamientodetalle.setTratamientoid(resultSet.getInt("tratamientoid"));
			tratamientodetalle.setTratamientodetalleactivo(resultSet.getBoolean("tratamientodetalleactivo"));
			tratamientodetalle.setTratamientodetallenombre(resultSet.getString("tratamientodetallenombre"));
			tratamientodetalle.setTratamientodetallecomentarios(resultSet.getString("tratamientodetallecomentarios"));
			tratamientodetalle.setTratamientodetallefechacreacion(resultSet.getTimestamp("tratamientodetallefechacreacion"));
			tratamientodetalle.setTratamientodetallefechamodificacion(resultSet.getTimestamp("tratamientodetallefechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Tratamientodetalle:: "+ex.getMessage());
		}
		return tratamientodetalle;
	}

	public Pagos getObjectPagos(ResultSet resultSet)throws Exception {
		Pagos pagos=null;
		try {
			pagos = new Pagos();
			pagos.setPagoid(resultSet.getInt("pagoid"));
			pagos.setTratamientoid(resultSet.getInt("tratamientoid"));
			pagos.setTipopagoid(resultSet.getInt("tipopagoid"));
			pagos.setPagoactivo(resultSet.getBoolean("pagoactivo"));
			pagos.setPagosaldado(resultSet.getBoolean("pagosaldado"));
			pagos.setPagoimportetotal(resultSet.getDouble("pagoimportetotal"));
			pagos.setPagoimportependiente(resultSet.getDouble("pagoimportependiente"));
			pagos.setPagocomentarios(resultSet.getString("pagocomentarios"));
			pagos.setPagofechacreacion(resultSet.getTimestamp("pagofechacreacion"));
			pagos.setPagofechamodificacion(resultSet.getTimestamp("pagofechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Pagos:: "+ex.getMessage());
		}
		return pagos;
	}

	public Tipopago getObjectTipopago(ResultSet resultSet)throws Exception {
		Tipopago tipopago=null;
		try {
			tipopago = new Tipopago();
			tipopago.setTipopagoid(resultSet.getInt("tipopagoid"));
			tipopago.setTipopagoactivo(resultSet.getBoolean("tipopagoactivo"));
			tipopago.setTipopagonombre(resultSet.getString("tipopagonombre"));
			tipopago.setTipopagodescripcion(resultSet.getString("tipopagodescripcion"));
			tipopago.setTipopagofechacreacion(resultSet.getTimestamp("tipopagofechacreacion"));
			tipopago.setTipopagofechamodificacion(resultSet.getTimestamp("tipopagofechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Tipopago:: "+ex.getMessage());
		}
		return tipopago;
	}

	public Pagosdetalle getObjectPagosdetalle(ResultSet resultSet)throws Exception {
		Pagosdetalle pagosdetalle=null;
		try {
			pagosdetalle = new Pagosdetalle();
			pagosdetalle.setPagosdetalleid(resultSet.getInt("pagosdetalleid"));
			pagosdetalle.setPagoid(resultSet.getInt("pagoid"));
			pagosdetalle.setPagosdetalletipoid(resultSet.getString("pagosdetalletipoid"));
			pagosdetalle.setFormapagoid(resultSet.getInt("formapagoid"));
			pagosdetalle.setPagosdetalleactivo(resultSet.getBoolean("pagosdetalleactivo"));
			pagosdetalle.setPagosdetalleliquidado(resultSet.getBoolean("pagosdetalleliquidado"));
			pagosdetalle.setPagosdetalleimporte(resultSet.getDouble("pagosdetalleimporte"));
			pagosdetalle.setPagosdetalleimportependiente(resultSet.getDouble("pagosdetalleimportependiente"));
			pagosdetalle.setPagosdetallefactura(resultSet.getString("pagosdetallefactura"));
			pagosdetalle.setPagosdetallefechacreacion(resultSet.getTimestamp("pagosdetallefechacreacion"));
			pagosdetalle.setPagosdetallefechamodificacion(resultSet.getTimestamp("pagosdetallefechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Pagosdetalle:: "+ex.getMessage());
		}
		return pagosdetalle;
	}

	public Formapago getObjectFormapago(ResultSet resultSet)throws Exception {
		Formapago formapago=null;
		try {
			formapago = new Formapago();
			formapago.setFormapagoid(resultSet.getInt("formapagoid"));
			formapago.setFormapagoactivo(resultSet.getBoolean("formapagoactivo"));
			formapago.setFormapagonombre(resultSet.getString("formapagonombre"));
			formapago.setFormapagodescripcion(resultSet.getString("formapagodescripcion"));
			formapago.setFormapagofechacreacion(resultSet.getTimestamp("formapagofechacreacion"));
			formapago.setFormapagofechamodificacion(resultSet.getTimestamp("formapagofechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Formapago:: "+ex.getMessage());
		}
		return formapago;
	}

	public Pagosdetalletipo getObjectPagosdetalletipo(ResultSet resultSet)throws Exception {
		Pagosdetalletipo pagosdetalletipo=null;
		try {
			pagosdetalletipo = new Pagosdetalletipo();
			pagosdetalletipo.setPagosdetalletipoid(resultSet.getString("pagosdetalletipoid"));
			pagosdetalletipo.setPagosdetalletipoactivo(resultSet.getBoolean("pagosdetalletipoactivo"));
			pagosdetalletipo.setPagosdetalletiponombre(resultSet.getString("pagosdetalletiponombre"));
			pagosdetalletipo.setPagosdetalletipodescripcion(resultSet.getString("pagosdetalletipodescripcion"));
			pagosdetalletipo.setPagosdetalletipofechacreacion(resultSet.getTimestamp("pagosdetalletipofechacreacion"));
			pagosdetalletipo.setPagosdetalletipofechamodificacion(resultSet.getTimestamp("pagosdetalletipofechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Pagosdetalletipo:: "+ex.getMessage());
		}
		return pagosdetalletipo;
	}

	public boolean isDigito(String cadena) {		
		try
		{
			Pattern pat = null;
			Matcher mat = null;        
			pat = Pattern.compile("^([0-9]*)$");
			mat = pat.matcher(cadena);
			if (!mat.find())
				return false;
		}catch(Exception e)
		{
			log.error(e);
			return false;
		}
		return true;
	}

	public String generaPassword()
	{
		String NUMEROS = "0123456789";
		String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";
		String ESPECIALES = "_#@!&*";
		String password = "";
		String key = MINUSCULAS + ESPECIALES + NUMEROS + MAYUSCULAS;
		for (int i = 0; i < 12; i++)
			password += (key.charAt((int) (Math.random() * key.length())));
		log.info("Password[" + password + "]");
		return password;

	}
	
	public boolean saveImage(MultipartFile file, String filename)
	{
		InputStream initialStream=null;
		OutputStream outStream=null;
		byte[] buffer=null;
		File targetFile=null;
    	int scaledWidth=150; 
    	int scaledHeight=160;
		try
		{
			filename=filename + ".png";
			initialStream = new BufferedInputStream(resizeImage(file, filename, scaledWidth, scaledHeight));
			buffer = new byte[initialStream.available()];

			initialStream.read(buffer);
			
			targetFile = new File(pathFiles+"/"+filename);
			
			outStream = new FileOutputStream(targetFile);
			outStream.write(buffer);
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error guardando la imagen",ex);
			return false;
		}
		finally
		{
			try {
				if(outStream!=null)
					outStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	private InputStream resizeImage(MultipartFile uploadedInputStream, String fileName, int width, int height)throws Exception {
        BufferedImage image = ImageIO.read(uploadedInputStream.getInputStream());
        Image originalImage= image.getScaledInstance(width, height, Image.SCALE_DEFAULT);

        int type = ((image.getType() == 0) ? BufferedImage.TYPE_INT_ARGB : image.getType());
        BufferedImage resizedImage = new BufferedImage(width, height, type);

        Graphics2D g2d = resizedImage.createGraphics();
        g2d.drawImage(originalImage, 0, 0, width, height, null);
        g2d.dispose();
        g2d.setComposite(AlphaComposite.Src);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        ImageIO.write(resizedImage, fileName.split("\\.")[1], byteArrayOutputStream);
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

	public String convertirNumeroLetra(double cifra) throws Exception {		
		return new convertirNumerosLetras().Convertir(cifra);
	}

	public void recuperaImagen(InputStream inputStream, HttpServletResponse response) {
		try{
			int i=0;
			OutputStream outputStream=response.getOutputStream();
			BufferedInputStream bufferedInputStream=new BufferedInputStream(inputStream);
			BufferedOutputStream bufferedOutputStream=new BufferedOutputStream(outputStream);
			while((i=bufferedInputStream.read())!=-1) {
				bufferedOutputStream.write(i);
			}
			inputStream.reset();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	class convertirNumerosLetras
	{
	    private final String[] UNIDADES = {"", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve "};
	    private final String[] DECENAS = {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ",
	        "diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ",
	        "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "};
	    private final String[] CENTENAS = {"", "ciento ", "doscientos ", "trecientos ", "cuatrocientos ", "quinientos ", "seiscientos ",
	        "setecientos ", "ochocientos ", "novecientos "};

	    private String Convertir(double cifra) throws Exception {
	        String literal = "";
	        String parte_decimal;
	        //si el numero utiliza (.) en lugar de (,) -> se reemplaza
	        String numero = new DecimalFormat("########0.00").format(cifra);
	        numero = numero.replace(".", ",");
	        //si el numero no tiene parte decimal, se le agrega ,00
	        if (numero.indexOf(",") == -1) {
	            numero = numero + ",00";
	        }
	        //se valida formato de entrada -> 0,00 y 999 999 999,00
	        if (Pattern.matches("\\d{1,9},\\d{1,2}", numero)) {
	            //se divide el numero 0000000,00 -> entero y decimal
	            String Num[] = numero.split(",");
	            //de da formato al numero decimal
	            parte_decimal = "y " + Num[1] + "/100";
	            //se convierte el numero a literal
	            if (Integer.parseInt(Num[0]) == 0) {//si el valor es cero
	                literal = "cero ";
	            } else if (Integer.parseInt(Num[0]) > 999999) {//si es millon
	                literal = getMillones(Num[0]);
	            } else if (Integer.parseInt(Num[0]) > 999) {//si es miles
	                literal = getMiles(Num[0]);
	            } else if (Integer.parseInt(Num[0]) > 99) {//si es centena
	                literal = getCentenas(Num[0]);
	            } else if (Integer.parseInt(Num[0]) > 9) {//si es decena
	                literal = getDecenas(Num[0]);
	            } else {//sino unidades -> 9
	                literal = getUnidades(Num[0]);
	            }
	            //devuelve el resultado
	            return (literal + parte_decimal);
	        } else {//error, no se puede convertir
	            return literal = null;
	        }
	    }

	    /* funciones para convertir los numeros a literales */
	    private String getUnidades(String numero) {// 1 - 9
	        //si tuviera algun 0 antes se lo quita -> 09 = 9 o 009=9
	        String num = numero.substring(numero.length() - 1);
	        return UNIDADES[Integer.parseInt(num)];
	    }

	    private String getDecenas(String num) {// 99                        
	        int n = Integer.parseInt(num);
	        if (n < 10) {//para casos como -> 01 - 09
	            return getUnidades(num);
	        } else if (n > 19) {//para 20...99
	            String u = getUnidades(num);
	            if (u.equals("")) { //para 20,30,40,50,60,70,80,90
	                return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8];
	            } else {
	                return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8] + "y " + u;
	            }
	        } else {//numeros entre 11 y 19
	            return DECENAS[n - 10];
	        }
	    }

	    private String getCentenas(String num) {// 999 o 099
	        if (Integer.parseInt(num) > 99) {//es centena
	            if (Integer.parseInt(num) == 100) {//caso especial
	                return " cien ";
	            } else {
	                return CENTENAS[Integer.parseInt(num.substring(0, 1))] + getDecenas(num.substring(1));
	            }
	        } else {//por Ej. 099 
	            //se quita el 0 antes de convertir a decenas
	            return getDecenas(Integer.parseInt(num) + "");
	        }
	    }

	    private String getMiles(String numero) {// 999 999
	        //obtiene las centenas
	        String c = numero.substring(numero.length() - 3);
	        //obtiene los miles
	        String m = numero.substring(0, numero.length() - 3);
	        String n = "";
	        //se comprueba que miles tenga valor entero
	        if (Integer.parseInt(m) > 0) {
	            n = getCentenas(m);
	            return n + "mil " + getCentenas(c);
	        } else {
	            return "" + getCentenas(c);
	        }

	    }

	    private String getMillones(String numero) { //000 000 000        
	        //se obtiene los miles
	        String miles = numero.substring(numero.length() - 6);
	        //se obtiene los millones
	        String millon = numero.substring(0, numero.length() - 6);
	        String n = "";
	        if (millon.length() > 1) {
	            n = getCentenas(millon) + "millones ";
	        } else {
	            n = getUnidades(millon) + "millon ";
	        }
	        return n + getMiles(miles);
	    }
	}

	public String getDetailError() {
		return DetailError;
	}
}
