package com.online.dentalspa.utils;

public class Scripts {

	public String getSPvalidateUsuario() {
		return "select * from usuarios_online_validate_session(?,?,?)";
	}

	public String getInfoUsuario() {
		return "select * from usuarios u join perfil p using(perfilid) "
				+" join permisos pe using(perfilid) join procesos pr using(procesoid) "
				+" join plataforma using(plataformaid) "
				+" where p.perfilactivo is true and pe.permisoactivo is true "
				+" and pr.procesoactivo is true and u.usuarioid=? order by pr.procesoorden";
	}

	public String getSPcreateBitacora() {
		return "select * from bitacora_online_create(?, ?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public String getSPcreatePaciente() {
		return "select * from pacientes_online_create(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	}

	public String getListaEstados() {
		return "select * from estados order by 2;";
	}

	public String getSPUpdateBitacora() {
		return "select * from bitacora_online_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	}

	public String getInfoPacientes() {
		return "select p.pacienteid,* from pacientes p "
				+"join datosfiscales using(datofiscalid) " 
				+"join usuarios using(usuarioid) "
				+"join estados using(estadoid) "
				+"left join antecedentes a on(p.pacienteid=a.pacienteid and a.antecedenteactivo=true) "
				+"left join examendental e on(p.pacienteid=e.pacienteid and e.examendentalactivo=true) "
				+"where p.pacienteid=? or (trim(upper(pacientenombre||pacienteapellidopaterno||pacienteapellidomaterno)) like upper(?))";
	}

	public String getSPCambiarContrasenia() {
		return "select * from usuarios_online_cambia_contrasenia(?,?,?);";
	}

	public String getContactosemergencia() {
		return "select * from contactosemergencia c join parentescos p using(parentescoid) where pacienteid=? and contactosemergenciaactivo is true order by 1";
	}

	public String getListaParentescos() {
		return "select * from parentescos where parentescoactivo is true order by 1;";
	}

	public String getSPcreateContactoEmergencia() {
		return "select * from contactosemergencia_online_create(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	}

	public String getEliminarContactoEmergencia() {
		return "update contactosemergencia set contactosemergenciaactivo=?, contactosemergenciafechamodificacion=now() where pacienteid=? and contactosemergenciaid=? and contactosemergenciaactivo is true";
	}

	public String getSPupdatePaciente() {
		return "select * from pacientes_online_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public String getSPupdateDatosfiscales() {
		return "select * from datosfiscales_online_update(?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public String getSPupdateAntecedentes() {
		return "select * from antecedentes_online_create(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public String getListaTratamientos() {
		return "select * from tratamientos join tipotratamiento using(tipotratamientoid) join odontogramas using(tratamientoid) "
				+"where tratamientoactivo is true and odontogramaactivo is true and pacienteid=? order by tratamientoid;";
	}

	public String getListaTiposTratamientos() {
		return "select * from tipotratamiento where tipotratamientoactivo is true order by tipotratamientonombre;";
	}

	public String getSPcreateExamendental() {
		return "select * from examendental_online_create(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public String getSPcreateTratamiento() {
		return "select * from tratamientos_online_create(?, ?, ?, ?, ?, ?, ?, ?, ?::jsonb);";
	}

	public String getSPcreateCita() {
		return "select * from citas_online_create(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public String getListaCitas() {
		return "select * from citas c left join usuarios u using(usuarioid) where citaestatusid=? and citafechainicio > current_date - interval '6 month'  order by citafechainicio;";
	}

	public String getListaTratamientosdetalle() {
		return "SELECT * FROM tratamientodetalle where tratamientodetalleactivo is true and tratamientoid=? order by 1 asc";
	}

	public String getSPcreateTratamientodetalle() {
		return "select * from tratamientodetalle_online_create(?, ?, ?, ?, ?, ?);";
	}

	public String getSPUpdateUsuario() {
		return "select * from usuarios_online_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public String getViewTratamientosPagos() {
		return "select * from pagos_tratamientos where pacienteid=?";
	}

	public String getListaTipopago() {
		return "select * from tipopago where tipopagoactivo is true order by tipopagonombre;";
	}

	public String getListaPagosdetalletipo() {
		return "select * from pagosdetalletipo where pagosdetalletipoactivo is true order by pagosdetalletiponombre;";
	}

	public String getListaFormapago() {
		return "select * from formapago where formapagoactivo is true order by formapagonombre;";
	}

	public String getSPcreatePago() {
		return "select * from pagos_online_create(?, ?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public String getSPcreatePagosdetalle() {
		return "select * from pagosdetalle_online_create(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public String getSPcreateTipotratamiento() {
		return "select * from tipotratamiento_online_create(?, ?, ?, ?, ?);";
	}

	public String getSPdeleteTipotratamiento() {
		return "select * from tipotratamiento_online_delete(?);";
	}

}
