package com.online.dentalspa.model;

import java.sql.Timestamp;
import java.util.ArrayList;

public class Pagos {
	private int pagoid;
	private int tratamientoid;
	private int tipopagoid;
	private boolean pagoactivo;
	private boolean pagosaldado;
	private double pagoimportetotal;
	private double pagoimportependiente;
	private String pagocomentarios;
	private Timestamp pagofechacreacion;
	private Timestamp pagofechamodificacion;
	private Tipopago tipopago;
	private ArrayList<Pagosdetalle> pagosdetalle;
	public int getPagoid() {
		return pagoid;
	}
	public void setPagoid(int pagoid) {
		this.pagoid = pagoid;
	}
	public int getTratamientoid() {
		return tratamientoid;
	}
	public void setTratamientoid(int tratamientoid) {
		this.tratamientoid = tratamientoid;
	}
	public int getTipopagoid() {
		return tipopagoid;
	}
	public void setTipopagoid(int tipopagoid) {
		this.tipopagoid = tipopagoid;
	}
	public boolean isPagoactivo() {
		return pagoactivo;
	}
	public void setPagoactivo(boolean pagoactivo) {
		this.pagoactivo = pagoactivo;
	}
	public boolean isPagosaldado() {
		return pagosaldado;
	}
	public void setPagosaldado(boolean pagosaldado) {
		this.pagosaldado = pagosaldado;
	}
	public double getPagoimportetotal() {
		return pagoimportetotal;
	}
	public void setPagoimportetotal(double pagoimportetotal) {
		this.pagoimportetotal = pagoimportetotal;
	}
	public double getPagoimportependiente() {
		return pagoimportependiente;
	}
	public void setPagoimportependiente(double pagoimportependiente) {
		this.pagoimportependiente = pagoimportependiente;
	}
	public String getPagocomentarios() {
		return pagocomentarios;
	}
	public void setPagocomentarios(String pagocomentarios) {
		this.pagocomentarios = pagocomentarios;
	}
	public Timestamp getPagofechacreacion() {
		return pagofechacreacion;
	}
	public void setPagofechacreacion(Timestamp pagofechacreacion) {
		this.pagofechacreacion = pagofechacreacion;
	}
	public Timestamp getPagofechamodificacion() {
		return pagofechamodificacion;
	}
	public void setPagofechamodificacion(Timestamp pagofechamodificacion) {
		this.pagofechamodificacion = pagofechamodificacion;
	}
	public Tipopago getTipopago() {
		return tipopago;
	}
	public void setTipopago(Tipopago tipopago) {
		this.tipopago = tipopago;
	}
	public ArrayList<Pagosdetalle> getPagosdetalle() {
		return pagosdetalle;
	}
	public void setPagosdetalle(ArrayList<Pagosdetalle> pagosdetalle) {
		this.pagosdetalle = pagosdetalle;
	}
	@Override
	public String toString() {
		return "Pagos [pagoid=" + pagoid + ", tratamientoid=" + tratamientoid + ", tipopagoid=" + tipopagoid
				+ ", pagoactivo=" + pagoactivo + ", pagosaldado=" + pagosaldado + ", pagoimportetotal="
				+ pagoimportetotal + ", pagoimportependiente=" + pagoimportependiente + ", pagocomentarios="
				+ pagocomentarios + ", pagofechacreacion=" + pagofechacreacion + ", pagofechamodificacion="
				+ pagofechamodificacion + "]";
	}
}
