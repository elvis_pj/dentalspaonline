package com.online.dentalspa.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Pacientes {
	private int pacienteid;
	private int usuarioid;
	private int datosfiscalid;
	private boolean pacienteactivo;
	private String pacientenombre;
	private String pacienteapellidopaterno;
	private String pacienteapellidomaterno;
	private String pacientesexo;
	private int pacienteedad;
	private String pacienteestadocivil;
	private String pacientelugarnacimiento;
	private Date pacientefechanacimiento;
	private String pacientecurp;
	private String pacientedomicilio;
	private String pacienteciudad;
	private String pacientecodigopostal;
	private int estadoid;
	private String pacientecorreo;
	private String pacientetelefono;
	private String pacientecelular;
	private Timestamp pacientefechacreacion;
	private Timestamp pacientefechamodificacion;
	private Usuario usuario;
	private Datosfiscales datosfiscales;
	private Estados estados;
	private Antecedentes antecedentes;
	private Examendental examendental;
	private ArrayList<Contactosemergencia> contactosemergencia;
	private ArrayList<Citas> citas;
	private ArrayList<Tratamientos> tratamientos;
	private ArrayList<Pagos> pagos;
	public int getPacienteid() {
		return pacienteid;
	}
	public void setPacienteid(int pacienteid) {
		this.pacienteid = pacienteid;
	}
	public int getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(int usuarioid) {
		this.usuarioid = usuarioid;
	}
	public int getDatosfiscalid() {
		return datosfiscalid;
	}
	public void setDatosfiscalid(int datosfiscalid) {
		this.datosfiscalid = datosfiscalid;
	}
	public boolean isPacienteactivo() {
		return pacienteactivo;
	}
	public void setPacienteactivo(boolean pacienteactivo) {
		this.pacienteactivo = pacienteactivo;
	}
	public String getPacientenombre() {
		return pacientenombre;
	}
	public void setPacientenombre(String pacientenombre) {
		this.pacientenombre = pacientenombre;
	}
	public String getPacienteapellidopaterno() {
		return pacienteapellidopaterno;
	}
	public void setPacienteapellidopaterno(String pacienteapellidopaterno) {
		this.pacienteapellidopaterno = pacienteapellidopaterno;
	}
	public String getPacienteapellidomaterno() {
		return pacienteapellidomaterno;
	}
	public void setPacienteapellidomaterno(String pacienteapellidomaterno) {
		this.pacienteapellidomaterno = pacienteapellidomaterno;
	}
	public String getPacientesexo() {
		return pacientesexo;
	}
	public void setPacientesexo(String pacientesexo) {
		this.pacientesexo = pacientesexo;
	}
	public int getPacienteedad() {
		return pacienteedad;
	}
	public void setPacienteedad(int pacienteedad) {
		this.pacienteedad = pacienteedad;
	}
	public String getPacienteestadocivil() {
		return pacienteestadocivil;
	}
	public void setPacienteestadocivil(String pacienteestadocivil) {
		this.pacienteestadocivil = pacienteestadocivil;
	}
	public String getPacientelugarnacimiento() {
		return pacientelugarnacimiento;
	}
	public void setPacientelugarnacimiento(String pacientelugarnacimiento) {
		this.pacientelugarnacimiento = pacientelugarnacimiento;
	}
	public Date getPacientefechanacimiento() {
		return pacientefechanacimiento;
	}
	public void setPacientefechanacimiento(Date pacientefechanacimiento) {
		this.pacientefechanacimiento = pacientefechanacimiento;
	}
	public String getPacientecurp() {
		return pacientecurp;
	}
	public void setPacientecurp(String pacientecurp) {
		this.pacientecurp = pacientecurp;
	}
	public String getPacientedomicilio() {
		return pacientedomicilio;
	}
	public void setPacientedomicilio(String pacientedomicilio) {
		this.pacientedomicilio = pacientedomicilio;
	}
	public String getPacienteciudad() {
		return pacienteciudad;
	}
	public void setPacienteciudad(String pacienteciudad) {
		this.pacienteciudad = pacienteciudad;
	}
	public String getPacientecodigopostal() {
		return pacientecodigopostal;
	}
	public void setPacientecodigopostal(String pacientecodigopostal) {
		this.pacientecodigopostal = pacientecodigopostal;
	}
	public int getEstadoid() {
		return estadoid;
	}
	public void setEstadoid(int estadoid) {
		this.estadoid = estadoid;
	}
	public String getPacientecorreo() {
		return pacientecorreo;
	}
	public void setPacientecorreo(String pacientecorreo) {
		this.pacientecorreo = pacientecorreo;
	}
	public String getPacientetelefono() {
		return pacientetelefono;
	}
	public void setPacientetelefono(String pacientetelefono) {
		this.pacientetelefono = pacientetelefono;
	}
	public String getPacientecelular() {
		return pacientecelular;
	}
	public void setPacientecelular(String pacientecelular) {
		this.pacientecelular = pacientecelular;
	}
	public Timestamp getPacientefechacreacion() {
		return pacientefechacreacion;
	}
	public void setPacientefechacreacion(Timestamp pacientefechacreacion) {
		this.pacientefechacreacion = pacientefechacreacion;
	}
	public Timestamp getPacientefechamodificacion() {
		return pacientefechamodificacion;
	}
	public void setPacientefechamodificacion(Timestamp pacientefechamodificacion) {
		this.pacientefechamodificacion = pacientefechamodificacion;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Datosfiscales getDatosfiscales() {
		return datosfiscales;
	}
	public void setDatosfiscales(Datosfiscales datosfiscales) {
		this.datosfiscales = datosfiscales;
	}
	public Estados getEstados() {
		return estados;
	}
	public void setEstados(Estados estados) {
		this.estados = estados;
	}
	public Antecedentes getAntecedentes() {
		return antecedentes;
	}
	public void setAntecedentes(Antecedentes antecedentes) {
		this.antecedentes = antecedentes;
	}
	public Examendental getExamendental() {
		return examendental;
	}
	public void setExamendental(Examendental examendental) {
		this.examendental = examendental;
	}
	public ArrayList<Contactosemergencia> getContactosemergencia() {
		return contactosemergencia;
	}
	public void setContactosemergencia(ArrayList<Contactosemergencia> contactosemergencia) {
		this.contactosemergencia = contactosemergencia;
	}
	public ArrayList<Citas> getCitas() {
		return citas;
	}
	public void setCitas(ArrayList<Citas> citas) {
		this.citas = citas;
	}
	public ArrayList<Tratamientos> getTratamientos() {
		return tratamientos;
	}
	public void setTratamientos(ArrayList<Tratamientos> tratamientos) {
		this.tratamientos = tratamientos;
	}
	public ArrayList<Pagos> getPagos() {
		return pagos;
	}
	public void setPagos(ArrayList<Pagos> pagos) {
		this.pagos = pagos;
	}
	@Override
	public String toString() {
		return "Pacientes [pacienteid=" + pacienteid + ", usuarioid=" + usuarioid + ", datosfiscalid=" + datosfiscalid
				+ ", pacienteactivo=" + pacienteactivo + ", pacientenombre=" + pacientenombre
				+ ", pacienteapellidopaterno=" + pacienteapellidopaterno + ", pacienteapellidomaterno="
				+ pacienteapellidomaterno + ", pacientesexo=" + pacientesexo + ", pacienteedad=" + pacienteedad
				+ ", pacienteestadocivil=" + pacienteestadocivil + ", pacientelugarnacimiento="
				+ pacientelugarnacimiento + ", pacientefechanacimiento=" + pacientefechanacimiento + ", pacientecurp="
				+ pacientecurp + ", pacientedomicilio=" + pacientedomicilio + ", pacienteciudad=" + pacienteciudad
				+ ", pacientecodigopostal=" + pacientecodigopostal + ", estadoid=" + estadoid + ", pacientecorreo="
				+ pacientecorreo + ", pacientetelefono=" + pacientetelefono + ", pacientecelular=" + pacientecelular
				+ ", pacientefechacreacion=" + pacientefechacreacion + ", pacientefechamodificacion="
				+ pacientefechamodificacion + "]";
	}
}
