package com.online.dentalspa.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Plataforma implements Serializable {
	private static final long serialVersionUID = 3082032196064399454L;
	private int plataformaid;
	private boolean plataformaactivo;
	private String plataformanombre;
	private String plataformadescripcion;
	private Timestamp plataformafechacreacion;
	private Timestamp plataformafechamodificacion;
	private String plataformarutaprincipal;
	public int getPlataformaid() {
		return plataformaid;
	}
	public void setPlataformaid(int plataformaid) {
		this.plataformaid = plataformaid;
	}
	public boolean isPlataformaactivo() {
		return plataformaactivo;
	}
	public void setPlataformaactivo(boolean plataformaactivo) {
		this.plataformaactivo = plataformaactivo;
	}
	public String getPlataformanombre() {
		return plataformanombre;
	}
	public void setPlataformanombre(String plataformanombre) {
		this.plataformanombre = plataformanombre;
	}
	public String getPlataformadescripcion() {
		return plataformadescripcion;
	}
	public void setPlataformadescripcion(String plataformadescripcion) {
		this.plataformadescripcion = plataformadescripcion;
	}
	public Timestamp getPlataformafechacreacion() {
		return plataformafechacreacion;
	}
	public void setPlataformafechacreacion(Timestamp plataformafechacreacion) {
		this.plataformafechacreacion = plataformafechacreacion;
	}
	public Timestamp getPlataformafechamodificacion() {
		return plataformafechamodificacion;
	}
	public void setPlataformafechamodificacion(Timestamp plataformafechamodificacion) {
		this.plataformafechamodificacion = plataformafechamodificacion;
	}
	public String getPlataformarutaprincipal() {
		return plataformarutaprincipal;
	}
	public void setPlataformarutaprincipal(String plataformarutaprincipal) {
		this.plataformarutaprincipal = plataformarutaprincipal;
	}
	@Override
	public String toString() {
		return "Plataforma [plataformaid=" + plataformaid + ", plataformaactivo=" + plataformaactivo
				+ ", plataformanombre=" + plataformanombre + ", plataformadescripcion=" + plataformadescripcion
				+ ", plataformafechacreacion=" + plataformafechacreacion + ", plataformafechamodificacion="
				+ plataformafechamodificacion + ", plataformarutaprincipal=" + plataformarutaprincipal + "]";
	}
}
