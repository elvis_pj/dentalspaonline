package com.online.dentalspa.model;

public class Estados {
	private int estadoid;
	private String estadonombre;
	public int getEstadoid() {
		return estadoid;
	}
	public void setEstadoid(int estadoid) {
		this.estadoid = estadoid;
	}
	public String getEstadonombre() {
		return estadonombre;
	}
	public void setEstadonombre(String estadonombre) {
		this.estadonombre = estadonombre;
	}
}
