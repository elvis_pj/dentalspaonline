package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Tipotratamiento {
	private int tipotratamientoid;
	private boolean tipotratamientoactivo;
	private String tipotratamientonombre;
	private String tipotratamientodescripcion;
	private Timestamp tipotratamientofechacreacion;
	private Timestamp tipotratamientofechamodifcacion;
	public int getTipotratamientoid() {
		return tipotratamientoid;
	}
	public void setTipotratamientoid(int tipotratamientoid) {
		this.tipotratamientoid = tipotratamientoid;
	}
	public boolean isTipotratamientoactivo() {
		return tipotratamientoactivo;
	}
	public void setTipotratamientoactivo(boolean tipotratamientoactivo) {
		this.tipotratamientoactivo = tipotratamientoactivo;
	}
	public String getTipotratamientonombre() {
		return tipotratamientonombre;
	}
	public void setTipotratamientonombre(String tipotratamientonombre) {
		this.tipotratamientonombre = tipotratamientonombre;
	}
	public String getTipotratamientodescripcion() {
		return tipotratamientodescripcion;
	}
	public void setTipotratamientodescripcion(String tipotratamientodescripcion) {
		this.tipotratamientodescripcion = tipotratamientodescripcion;
	}
	public Timestamp getTipotratamientofechacreacion() {
		return tipotratamientofechacreacion;
	}
	public void setTipotratamientofechacreacion(Timestamp tipotratamientofechacreacion) {
		this.tipotratamientofechacreacion = tipotratamientofechacreacion;
	}
	public Timestamp getTipotratamientofechamodifcacion() {
		return tipotratamientofechamodifcacion;
	}
	public void setTipotratamientofechamodifcacion(Timestamp tipotratamientofechamodifcacion) {
		this.tipotratamientofechamodifcacion = tipotratamientofechamodifcacion;
	}
	@Override
	public String toString() {
		return "Tipotratamiento [tipotratamientoid=" + tipotratamientoid + ", tipotratamientoactivo="
				+ tipotratamientoactivo + ", tipotratamientonombre=" + tipotratamientonombre
				+ ", tipotratamientodescripcion=" + tipotratamientodescripcion + ", tipotratamientofechacreacion="
				+ tipotratamientofechacreacion + ", tipotratamientofechamodifcacion=" + tipotratamientofechamodifcacion
				+ "]";
	}
}
