package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Contactosemergencia {
	private int contactosemergenciaid;
	private int pacienteid;
	private int parentescoid;
	private String contactosemergencianombre;
	private String contactosemergenciatelefono;
	private String contactosemergenciacelular;
	private String contactosemergenciacorreo;
	private Timestamp contactosemergenciafechacreacion;
	private Timestamp contactosemergenciafechamodificacion;
	private boolean contactosemergenciaactivo;
	private Pacientes pacientes;
	private Parentescos parentescos;
	public int getContactosemergenciaid() {
		return contactosemergenciaid;
	}
	public void setContactosemergenciaid(int contactosemergenciaid) {
		this.contactosemergenciaid = contactosemergenciaid;
	}
	public int getPacienteid() {
		return pacienteid;
	}
	public void setPacienteid(int pacienteid) {
		this.pacienteid = pacienteid;
	}
	public int getParentescoid() {
		return parentescoid;
	}
	public void setParentescoid(int parentescoid) {
		this.parentescoid = parentescoid;
	}
	public String getContactosemergencianombre() {
		return contactosemergencianombre;
	}
	public void setContactosemergencianombre(String contactosemergencianombre) {
		this.contactosemergencianombre = contactosemergencianombre;
	}
	public String getContactosemergenciatelefono() {
		return contactosemergenciatelefono;
	}
	public void setContactosemergenciatelefono(String contactosemergenciatelefono) {
		this.contactosemergenciatelefono = contactosemergenciatelefono;
	}
	public String getContactosemergenciacelular() {
		return contactosemergenciacelular;
	}
	public void setContactosemergenciacelular(String contactosemergenciacelular) {
		this.contactosemergenciacelular = contactosemergenciacelular;
	}
	public String getContactosemergenciacorreo() {
		return contactosemergenciacorreo;
	}
	public void setContactosemergenciacorreo(String contactosemergenciacorreo) {
		this.contactosemergenciacorreo = contactosemergenciacorreo;
	}
	public Timestamp getContactosemergenciafechacreacion() {
		return contactosemergenciafechacreacion;
	}
	public void setContactosemergenciafechacreacion(Timestamp contactosemergenciafechacreacion) {
		this.contactosemergenciafechacreacion = contactosemergenciafechacreacion;
	}
	public Timestamp getContactosemergenciafechamodificacion() {
		return contactosemergenciafechamodificacion;
	}
	public void setContactosemergenciafechamodificacion(Timestamp contactosemergenciafechamodificacion) {
		this.contactosemergenciafechamodificacion = contactosemergenciafechamodificacion;
	}
	public boolean isContactosemergenciaactivo() {
		return contactosemergenciaactivo;
	}
	public void setContactosemergenciaactivo(boolean contactosemergenciaactivo) {
		this.contactosemergenciaactivo = contactosemergenciaactivo;
	}
	public Pacientes getPacientes() {
		return pacientes;
	}
	public void setPacientes(Pacientes pacientes) {
		this.pacientes = pacientes;
	}
	public Parentescos getParentescos() {
		return parentescos;
	}
	public void setParentescos(Parentescos parentescos) {
		this.parentescos = parentescos;
	}
	@Override
	public String toString() {
		return "Contactosemergencia [contactosemergenciaid=" + contactosemergenciaid + ", pacienteid=" + pacienteid
				+ ", parentescoid=" + parentescoid + ", contactosemergencianombre=" + contactosemergencianombre
				+ ", contactosemergenciatelefono=" + contactosemergenciatelefono + ", contactosemergenciacelular="
				+ contactosemergenciacelular + ", contactosemergenciacorreo=" + contactosemergenciacorreo
				+ ", contactosemergenciafechacreacion=" + contactosemergenciafechacreacion
				+ ", contactosemergenciafechamodificacion=" + contactosemergenciafechamodificacion
				+ ", contactosemergenciaactivo=" + contactosemergenciaactivo + ", pacientes=" + pacientes
				+ ", parentescos=" + parentescos + "]";
	}
}
