package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Pagosdetalletipo {
	private String pagosdetalletipoid;
	private boolean pagosdetalletipoactivo;
	private String pagosdetalletiponombre;
	private String pagosdetalletipodescripcion;
	private Timestamp pagosdetalletipofechacreacion;
	private Timestamp pagosdetalletipofechamodificacion;
	public String getPagosdetalletipoid() {
		return pagosdetalletipoid;
	}
	public void setPagosdetalletipoid(String pagosdetalletipoid) {
		this.pagosdetalletipoid = pagosdetalletipoid;
	}
	public boolean isPagosdetalletipoactivo() {
		return pagosdetalletipoactivo;
	}
	public void setPagosdetalletipoactivo(boolean pagosdetalletipoactivo) {
		this.pagosdetalletipoactivo = pagosdetalletipoactivo;
	}
	public String getPagosdetalletiponombre() {
		return pagosdetalletiponombre;
	}
	public void setPagosdetalletiponombre(String pagosdetalletiponombre) {
		this.pagosdetalletiponombre = pagosdetalletiponombre;
	}
	public String getPagosdetalletipodescripcion() {
		return pagosdetalletipodescripcion;
	}
	public void setPagosdetalletipodescripcion(String pagosdetalletipodescripcion) {
		this.pagosdetalletipodescripcion = pagosdetalletipodescripcion;
	}
	public Timestamp getPagosdetalletipofechacreacion() {
		return pagosdetalletipofechacreacion;
	}
	public void setPagosdetalletipofechacreacion(Timestamp pagosdetalletipofechacreacion) {
		this.pagosdetalletipofechacreacion = pagosdetalletipofechacreacion;
	}
	public Timestamp getPagosdetalletipofechamodificacion() {
		return pagosdetalletipofechamodificacion;
	}
	public void setPagosdetalletipofechamodificacion(Timestamp pagosdetalletipofechamodificacion) {
		this.pagosdetalletipofechamodificacion = pagosdetalletipofechamodificacion;
	}
	@Override
	public String toString() {
		return "Pagosdetalletipo [pagosdetalletipoid=" + pagosdetalletipoid + ", pagosdetalletipoactivo="
				+ pagosdetalletipoactivo + ", pagosdetalletiponombre=" + pagosdetalletiponombre
				+ ", pagosdetalletipodescripcion=" + pagosdetalletipodescripcion + ", pagosdetalletipofechacreacion="
				+ pagosdetalletipofechacreacion + ", pagosdetalletipofechamodificacion="
				+ pagosdetalletipofechamodificacion + "]";
	}
}
