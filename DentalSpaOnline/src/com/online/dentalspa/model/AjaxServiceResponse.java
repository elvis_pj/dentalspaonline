package com.online.dentalspa.model;

public class AjaxServiceResponse {
	private String _State;
	private String _Message;
	private String _ObjectJson;
	public String get_State() {
		return _State;
	}
	public void set_State(String _State) {
		this._State = _State;
	}
	public String get_Message() {
		return _Message;
	}
	public void set_Message(String _Message) {
		this._Message = _Message;
	}
	public String get_ObjectJson() {
		return _ObjectJson;
	}
	public void set_ObjectJson(String _ObjectJson) {
		this._ObjectJson = _ObjectJson;
	}
	@Override
	public String toString() {
		return "AjaxServiceResponse [_State=" + _State + ", _Message=" + _Message + ", _ObjectJson=" + _ObjectJson
				+ "]";
	}
}
