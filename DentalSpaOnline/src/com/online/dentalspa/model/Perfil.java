package com.online.dentalspa.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Perfil implements Serializable {
	private static final long serialVersionUID = 8645067924519948142L;
	private int perfilid;
	private boolean perfilactivo;
	private String perfilnombre;
	private String perfildescripcion;
	private Timestamp perfilfechacreacion;
	private Timestamp perfilfechamodificacion;
	private ArrayList<Permisos> permisos;
	public int getPerfilid() {
		return perfilid;
	}
	public void setPerfilid(int perfilid) {
		this.perfilid = perfilid;
	}
	public boolean isPerfilactivo() {
		return perfilactivo;
	}
	public void setPerfilactivo(boolean perfilactivo) {
		this.perfilactivo = perfilactivo;
	}
	public String getPerfilnombre() {
		return perfilnombre;
	}
	public void setPerfilnombre(String perfilnombre) {
		this.perfilnombre = perfilnombre;
	}
	public String getPerfildescripcion() {
		return perfildescripcion;
	}
	public void setPerfildescripcion(String perfildescripcion) {
		this.perfildescripcion = perfildescripcion;
	}
	public Timestamp getPerfilfechacreacion() {
		return perfilfechacreacion;
	}
	public void setPerfilfechacreacion(Timestamp perfilfechacreacion) {
		this.perfilfechacreacion = perfilfechacreacion;
	}
	public Timestamp getPerfilfechamodificacion() {
		return perfilfechamodificacion;
	}
	public void setPerfilfechamodificacion(Timestamp perfilfechamodificacion) {
		this.perfilfechamodificacion = perfilfechamodificacion;
	}
	public ArrayList<Permisos> getPermisos() {
		return permisos;
	}
	public void setPermisos(ArrayList<Permisos> permisos) {
		this.permisos = permisos;
	}
	@Override
	public String toString() {
		return "Perfil [perfilid=" + perfilid + ", perfilactivo=" + perfilactivo + ", perfilnombre=" + perfilnombre
				+ ", perfildescripcion=" + perfildescripcion + ", perfilfechacreacion=" + perfilfechacreacion
				+ ", perfilfechamodificacion=" + perfilfechamodificacion + "]";
	}
}
