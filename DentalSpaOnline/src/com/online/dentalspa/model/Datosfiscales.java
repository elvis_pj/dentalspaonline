package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Datosfiscales {
	private int datofiscalid;
	private String datofiscalnombre;
	private String datofiscalrfc;
	private String datofiscaldireccion;
	private String datofiscalcorreo;
	private Timestamp datofiscalfechacreacion;
	private Timestamp datofiscalfechamodificacion;
	private Pacientes paciente;
	public int getDatofiscalid() {
		return datofiscalid;
	}
	public void setDatofiscalid(int datofiscalid) {
		this.datofiscalid = datofiscalid;
	}
	public String getDatofiscalnombre() {
		return datofiscalnombre;
	}
	public void setDatofiscalnombre(String datofiscalnombre) {
		this.datofiscalnombre = datofiscalnombre;
	}
	public String getDatofiscalrfc() {
		return datofiscalrfc;
	}
	public void setDatofiscalrfc(String datofiscalrfc) {
		this.datofiscalrfc = datofiscalrfc;
	}
	public String getDatofiscaldireccion() {
		return datofiscaldireccion;
	}
	public void setDatofiscaldireccion(String datofiscaldireccion) {
		this.datofiscaldireccion = datofiscaldireccion;
	}
	public String getDatofiscalcorreo() {
		return datofiscalcorreo;
	}
	public void setDatofiscalcorreo(String datofiscalcorreo) {
		this.datofiscalcorreo = datofiscalcorreo;
	}
	public Timestamp getDatofiscalfechacreacion() {
		return datofiscalfechacreacion;
	}
	public void setDatofiscalfechacreacion(Timestamp datofiscalfechacreacion) {
		this.datofiscalfechacreacion = datofiscalfechacreacion;
	}
	public Timestamp getDatofiscalfechamodificacion() {
		return datofiscalfechamodificacion;
	}
	public void setDatofiscalfechamodificacion(Timestamp datofiscalfechamodificacion) {
		this.datofiscalfechamodificacion = datofiscalfechamodificacion;
	}
	public Pacientes getPaciente() {
		return paciente;
	}
	public void setPaciente(Pacientes paciente) {
		this.paciente = paciente;
	}
	@Override
	public String toString() {
		return "Datosfiscales [datofiscalid=" + datofiscalid + ", datofiscalnombre=" + datofiscalnombre
				+ ", datofiscalrfc=" + datofiscalrfc + ", datofiscaldireccion=" + datofiscaldireccion
				+ ", datofiscalcorreo=" + datofiscalcorreo + ", datofiscalfechacreacion=" + datofiscalfechacreacion
				+ ", datofiscalfechamodificacion=" + datofiscalfechamodificacion + "]";
	}
}
