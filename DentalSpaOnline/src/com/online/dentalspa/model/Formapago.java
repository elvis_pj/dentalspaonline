package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Formapago {
	private int formapagoid;
	private boolean formapagoactivo;
	private String formapagonombre;
	private String formapagodescripcion;
	private Timestamp formapagofechacreacion;
	private Timestamp formapagofechamodificacion;
	public int getFormapagoid() {
		return formapagoid;
	}
	public void setFormapagoid(int formapagoid) {
		this.formapagoid = formapagoid;
	}
	public boolean isFormapagoactivo() {
		return formapagoactivo;
	}
	public void setFormapagoactivo(boolean formapagoactivo) {
		this.formapagoactivo = formapagoactivo;
	}
	public String getFormapagonombre() {
		return formapagonombre;
	}
	public void setFormapagonombre(String formapagonombre) {
		this.formapagonombre = formapagonombre;
	}
	public String getFormapagodescripcion() {
		return formapagodescripcion;
	}
	public void setFormapagodescripcion(String formapagodescripcion) {
		this.formapagodescripcion = formapagodescripcion;
	}
	public Timestamp getFormapagofechacreacion() {
		return formapagofechacreacion;
	}
	public void setFormapagofechacreacion(Timestamp formapagofechacreacion) {
		this.formapagofechacreacion = formapagofechacreacion;
	}
	public Timestamp getFormapagofechamodificacion() {
		return formapagofechamodificacion;
	}
	public void setFormapagofechamodificacion(Timestamp formapagofechamodificacion) {
		this.formapagofechamodificacion = formapagofechamodificacion;
	}
	@Override
	public String toString() {
		return "Formapago [formapagoid=" + formapagoid + ", formapagoactivo=" + formapagoactivo + ", formapagonombre="
				+ formapagonombre + ", formapagodescripcion=" + formapagodescripcion + ", formapagofechacreacion="
				+ formapagofechacreacion + ", formapagofechamodificacion=" + formapagofechamodificacion + "]";
	}
}
