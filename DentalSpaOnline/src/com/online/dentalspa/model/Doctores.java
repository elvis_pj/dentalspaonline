package com.online.dentalspa.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Doctores implements Serializable {
	private static final long serialVersionUID = -7352329997703446882L;
	private int doctorid;
	private int usuarioid;
	private int especialidadid;
	private boolean doctoractivo;
	private String doctornombre;
	private String doctorcedula;
	private String doctorcelular;
	private Timestamp doctorfechacreacion;
	private Timestamp doctorfechamodificacion;
	private Usuario usuario;
	private Especialidades especialidad;
	public int getDoctorid() {
		return doctorid;
	}
	public void setDoctorid(int doctorid) {
		this.doctorid = doctorid;
	}
	public int getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(int usuarioid) {
		this.usuarioid = usuarioid;
	}
	public int getEspecialidadid() {
		return especialidadid;
	}
	public void setEspecialidadid(int especialidadid) {
		this.especialidadid = especialidadid;
	}
	public boolean isDoctoractivo() {
		return doctoractivo;
	}
	public void setDoctoractivo(boolean doctoractivo) {
		this.doctoractivo = doctoractivo;
	}
	public String getDoctornombre() {
		return doctornombre;
	}
	public void setDoctornombre(String doctornombre) {
		this.doctornombre = doctornombre;
	}
	public String getDoctorcedula() {
		return doctorcedula;
	}
	public void setDoctorcedula(String doctorcedula) {
		this.doctorcedula = doctorcedula;
	}
	public String getDoctorcelular() {
		return doctorcelular;
	}
	public void setDoctorcelular(String doctorcelular) {
		this.doctorcelular = doctorcelular;
	}
	public Timestamp getDoctorfechacreacion() {
		return doctorfechacreacion;
	}
	public void setDoctorfechacreacion(Timestamp doctorfechacreacion) {
		this.doctorfechacreacion = doctorfechacreacion;
	}
	public Timestamp getDoctorfechamodificacion() {
		return doctorfechamodificacion;
	}
	public void setDoctorfechamodificacion(Timestamp doctorfechamodificacion) {
		this.doctorfechamodificacion = doctorfechamodificacion;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Especialidades getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(Especialidades especialidad) {
		this.especialidad = especialidad;
	}
	@Override
	public String toString() {
		return "Doctores [doctorid=" + doctorid + ", usuarioid=" + usuarioid + ", especialidadid=" + especialidadid
				+ ", doctoractivo=" + doctoractivo + ", doctornombre=" + doctornombre + ", doctorcedula=" + doctorcedula
				+ ", doctorcelular=" + doctorcelular + ", doctorfechacreacion=" + doctorfechacreacion
				+ ", doctorfechamodificacion=" + doctorfechamodificacion + ", usuario=" + usuario + "]";
	}
}
