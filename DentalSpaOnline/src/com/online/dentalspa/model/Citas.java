package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Citas {
	private int citaid;
	private int usuarioid;
	private int pacienteid;
	private int citaestatusid;
	private String citanombre;
	private String citacomentarios;
	private Timestamp citafechainicio;
	private Timestamp citafechafin;
	private Timestamp citafechacreacion;
	private Timestamp citafechamodificacion;
	private int tratamientoid;
	private Usuario usuario;
	private Catalogocitasestatus citaestatus;
	public int getCitaid() {
		return citaid;
	}
	public void setCitaid(int citaid) {
		this.citaid = citaid;
	}
	public int getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(int usuarioid) {
		this.usuarioid = usuarioid;
	}
	public int getPacienteid() {
		return pacienteid;
	}
	public void setPacienteid(int pacienteid) {
		this.pacienteid = pacienteid;
	}
	public int getCitaestatusid() {
		return citaestatusid;
	}
	public void setCitaestatusid(int citaestatusid) {
		this.citaestatusid = citaestatusid;
	}
	public String getCitanombre() {
		return citanombre;
	}
	public void setCitanombre(String citanombre) {
		this.citanombre = citanombre;
	}
	public String getCitacomentarios() {
		return citacomentarios;
	}
	public void setCitacomentarios(String citacomentarios) {
		this.citacomentarios = citacomentarios;
	}
	public Timestamp getCitafechainicio() {
		return citafechainicio;
	}
	public void setCitafechainicio(Timestamp citafechainicio) {
		this.citafechainicio = citafechainicio;
	}
	public Timestamp getCitafechafin() {
		return citafechafin;
	}
	public void setCitafechafin(Timestamp citafechafin) {
		this.citafechafin = citafechafin;
	}
	public Timestamp getCitafechacreacion() {
		return citafechacreacion;
	}
	public void setCitafechacreacion(Timestamp citafechacreacion) {
		this.citafechacreacion = citafechacreacion;
	}
	public Timestamp getCitafechamodificacion() {
		return citafechamodificacion;
	}
	public void setCitafechamodificacion(Timestamp citafechamodificacion) {
		this.citafechamodificacion = citafechamodificacion;
	}
	public int getTratamientoid() {
		return tratamientoid;
	}
	public void setTratamientoid(int tratamientoid) {
		this.tratamientoid = tratamientoid;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Catalogocitasestatus getCitaestatus() {
		return citaestatus;
	}
	public void setCitaestatus(Catalogocitasestatus citaestatus) {
		this.citaestatus = citaestatus;
	}
	@Override
	public String toString() {
		return "Citas [citaid=" + citaid + ", usuarioid=" + usuarioid + ", pacienteid=" + pacienteid
				+ ", citaestatusid=" + citaestatusid + ", citanombre=" + citanombre + ", citacomentarios="
				+ citacomentarios + ", citafechainicio=" + citafechainicio + ", citafechafin=" + citafechafin
				+ ", citafechacreacion=" + citafechacreacion + ", citafechamodificacion=" + citafechamodificacion
				+ ", tratamientoid=" + tratamientoid + ", citaestatus=" + citaestatus + "]";
	}
}
