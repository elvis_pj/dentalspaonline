package com.online.dentalspa.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Permisos implements Serializable {
	private static final long serialVersionUID = -9049106755167974980L;
	private int permisoid;
	private boolean permisoactivo;
	private int perfilid;
	private int procesoid;
	private Timestamp permisofechacreacion;
	private Timestamp permisofechamodificacion;
	private Perfil perfil;
	private Procesos procesos;
	public int getPermisoid() {
		return permisoid;
	}
	public void setPermisoid(int permisoid) {
		this.permisoid = permisoid;
	}
	public boolean isPermisoactivo() {
		return permisoactivo;
	}
	public void setPermisoactivo(boolean permisoactivo) {
		this.permisoactivo = permisoactivo;
	}
	public int getPerfilid() {
		return perfilid;
	}
	public void setPerfilid(int perfilid) {
		this.perfilid = perfilid;
	}
	public int getProcesoid() {
		return procesoid;
	}
	public void setProcesoid(int procesoid) {
		this.procesoid = procesoid;
	}
	public Timestamp getPermisofechacreacion() {
		return permisofechacreacion;
	}
	public void setPermisofechacreacion(Timestamp permisofechacreacion) {
		this.permisofechacreacion = permisofechacreacion;
	}
	public Timestamp getPermisofechamodificacion() {
		return permisofechamodificacion;
	}
	public void setPermisofechamodificacion(Timestamp permisofechamodificacion) {
		this.permisofechamodificacion = permisofechamodificacion;
	}
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	public Procesos getProcesos() {
		return procesos;
	}
	public void setProcesos(Procesos procesos) {
		this.procesos = procesos;
	}
	@Override
	public String toString() {
		return "Permisos [permisoid=" + permisoid + ", permisoactivo=" + permisoactivo + ", perfilid=" + perfilid
				+ ", procesoid=" + procesoid + ", permisofechacreacion=" + permisofechacreacion
				+ ", permisofechamodificacion=" + permisofechamodificacion + ", perfil=" + perfil + ", procesos="
				+ procesos + "]";
	}
}
