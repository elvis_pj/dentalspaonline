package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Parentescos {
	private int parentescoid;
	private String parentesconombre;
	private String parentescodescripcion;
	private boolean parentescoactivo;
	private Timestamp parentescofechacreacion;
	private Timestamp parentescofechamodificacion;
	public int getParentescoid() {
		return parentescoid;
	}
	public void setParentescoid(int parentescoid) {
		this.parentescoid = parentescoid;
	}
	public String getParentesconombre() {
		return parentesconombre;
	}
	public void setParentesconombre(String parentesconombre) {
		this.parentesconombre = parentesconombre;
	}
	public String getParentescodescripcion() {
		return parentescodescripcion;
	}
	public void setParentescodescripcion(String parentescodescripcion) {
		this.parentescodescripcion = parentescodescripcion;
	}
	public boolean isParentescoactivo() {
		return parentescoactivo;
	}
	public void setParentescoactivo(boolean parentescoactivo) {
		this.parentescoactivo = parentescoactivo;
	}
	public Timestamp getParentescofechacreacion() {
		return parentescofechacreacion;
	}
	public void setParentescofechacreacion(Timestamp parentescofechacreacion) {
		this.parentescofechacreacion = parentescofechacreacion;
	}
	public Timestamp getParentescofechamodificacion() {
		return parentescofechamodificacion;
	}
	public void setParentescofechamodificacion(Timestamp parentescofechamodificacion) {
		this.parentescofechamodificacion = parentescofechamodificacion;
	}
	@Override
	public String toString() {
		return "Parentescos [parentescoid=" + parentescoid + ", parentesconombre=" + parentesconombre
				+ ", parentescodescripcion=" + parentescodescripcion + ", parentescoactivo=" + parentescoactivo
				+ ", parentescofechacreacion=" + parentescofechacreacion + ", parentescofechamodificacion="
				+ parentescofechamodificacion + "]";
	}
}
