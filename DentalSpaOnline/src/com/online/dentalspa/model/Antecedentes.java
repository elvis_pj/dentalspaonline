package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Antecedentes {
	private int antecedentelid;
	private int pacienteid;
	private boolean antecedenteactivo;
	private String antecedentetratamientomedico;
	private String antecedenteembarazo;
	private String antecedentemedicamentos;
	private String antecedentegsaq;
	private String antecedentealergias;
	private String antecedenteetsvih;
	private String antecedentecoagulacion;
	private String antecedentecancer;
	private String antecedenteccrn;
	private String antecedentepdtf;
	private Timestamp antecedentefechacreacion;
	private Timestamp antecedentefechamodificacion;
	private Pacientes pacientes;
	public int getAntecedentelid() {
		return antecedentelid;
	}
	public void setAntecedentelid(int antecedentelid) {
		this.antecedentelid = antecedentelid;
	}
	public int getPacienteid() {
		return pacienteid;
	}
	public void setPacienteid(int pacienteid) {
		this.pacienteid = pacienteid;
	}
	public boolean isAntecedenteactivo() {
		return antecedenteactivo;
	}
	public void setAntecedenteactivo(boolean antecedenteactivo) {
		this.antecedenteactivo = antecedenteactivo;
	}
	public String getAntecedentetratamientomedico() {
		return antecedentetratamientomedico;
	}
	public void setAntecedentetratamientomedico(String antecedentetratamientomedico) {
		this.antecedentetratamientomedico = antecedentetratamientomedico;
	}
	public String getAntecedenteembarazo() {
		return antecedenteembarazo;
	}
	public void setAntecedenteembarazo(String antecedenteembarazo) {
		this.antecedenteembarazo = antecedenteembarazo;
	}
	public String getAntecedentemedicamentos() {
		return antecedentemedicamentos;
	}
	public void setAntecedentemedicamentos(String antecedentemedicamentos) {
		this.antecedentemedicamentos = antecedentemedicamentos;
	}
	public String getAntecedentegsaq() {
		return antecedentegsaq;
	}
	public void setAntecedentegsaq(String antecedentegsaq) {
		this.antecedentegsaq = antecedentegsaq;
	}
	public String getAntecedentealergias() {
		return antecedentealergias;
	}
	public void setAntecedentealergias(String antecedentealergias) {
		this.antecedentealergias = antecedentealergias;
	}
	public String getAntecedenteetsvih() {
		return antecedenteetsvih;
	}
	public void setAntecedenteetsvih(String antecedenteetsvih) {
		this.antecedenteetsvih = antecedenteetsvih;
	}
	public String getAntecedentecoagulacion() {
		return antecedentecoagulacion;
	}
	public void setAntecedentecoagulacion(String antecedentecoagulacion) {
		this.antecedentecoagulacion = antecedentecoagulacion;
	}
	public String getAntecedentecancer() {
		return antecedentecancer;
	}
	public void setAntecedentecancer(String antecedentecancer) {
		this.antecedentecancer = antecedentecancer;
	}
	public String getAntecedenteccrn() {
		return antecedenteccrn;
	}
	public void setAntecedenteccrn(String antecedenteccrn) {
		this.antecedenteccrn = antecedenteccrn;
	}
	public String getAntecedentepdtf() {
		return antecedentepdtf;
	}
	public void setAntecedentepdtf(String antecedentepdtf) {
		this.antecedentepdtf = antecedentepdtf;
	}
	public Timestamp getAntecedentefechacreacion() {
		return antecedentefechacreacion;
	}
	public void setAntecedentefechacreacion(Timestamp antecedentefechacreacion) {
		this.antecedentefechacreacion = antecedentefechacreacion;
	}
	public Timestamp getAntecedentefechamodificacion() {
		return antecedentefechamodificacion;
	}
	public void setAntecedentefechamodificacion(Timestamp antecedentefechamodificacion) {
		this.antecedentefechamodificacion = antecedentefechamodificacion;
	}
	public Pacientes getPacientes() {
		return pacientes;
	}
	public void setPacientes(Pacientes pacientes) {
		this.pacientes = pacientes;
	}
	@Override
	public String toString() {
		return "Antecedentes [antecedentelid=" + antecedentelid + ", pacienteid=" + pacienteid + ", antecedenteactivo="
				+ antecedenteactivo + ", antecedentetratamientomedico=" + antecedentetratamientomedico
				+ ", antecedenteembarazo=" + antecedenteembarazo + ", antecedentemedicamentos="
				+ antecedentemedicamentos + ", antecedentegsaq=" + antecedentegsaq + ", antecedentealergias="
				+ antecedentealergias + ", antecedenteetsvih=" + antecedenteetsvih + ", antecedentecoagulacion="
				+ antecedentecoagulacion + ", antecedentecancer=" + antecedentecancer + ", antecedenteccrn="
				+ antecedenteccrn + ", antecedentepdtf=" + antecedentepdtf + ", antecedentefechacreacion="
				+ antecedentefechacreacion + ", antecedentefechamodificacion=" + antecedentefechamodificacion + "]";
	}
}
