package com.online.dentalspa.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;

public class Usuario implements Serializable {
	private static final long serialVersionUID = 6546972622622975139L;
	private int usuarioid;
	private int perfilid;
	private boolean usuarioactivo;
	private String usuariocorreo;
	private String usuariopwd;
	private String usuarionombre;
	private String usuarioapellidopaterno;
	private String usuarioapellidomaterno;
	private Timestamp usuariofechacreacion;
	private Timestamp usuariofechamodificacion;
	private Timestamp usuarioultimoacceso;
	private String usuariokey;
	private transient InputStream usuarioimage;
	private Perfil perfil;
	public int getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(int usuarioid) {
		this.usuarioid = usuarioid;
	}
	public int getPerfilid() {
		return perfilid;
	}
	public void setPerfilid(int perfilid) {
		this.perfilid = perfilid;
	}
	public boolean isUsuarioactivo() {
		return usuarioactivo;
	}
	public void setUsuarioactivo(boolean usuarioactivo) {
		this.usuarioactivo = usuarioactivo;
	}
	public String getUsuariocorreo() {
		return usuariocorreo;
	}
	public void setUsuariocorreo(String usuariocorreo) {
		this.usuariocorreo = usuariocorreo;
	}
	public String getUsuariopwd() {
		return usuariopwd;
	}
	public void setUsuariopwd(String usuariopwd) {
		this.usuariopwd = usuariopwd;
	}
	public String getUsuarionombre() {
		return usuarionombre;
	}
	public void setUsuarionombre(String usuarionombre) {
		this.usuarionombre = usuarionombre;
	}
	public String getUsuarioapellidopaterno() {
		return usuarioapellidopaterno;
	}
	public void setUsuarioapellidopaterno(String usuarioapellidopaterno) {
		this.usuarioapellidopaterno = usuarioapellidopaterno;
	}
	public String getUsuarioapellidomaterno() {
		return usuarioapellidomaterno;
	}
	public void setUsuarioapellidomaterno(String usuarioapellidomaterno) {
		this.usuarioapellidomaterno = usuarioapellidomaterno;
	}
	public Timestamp getUsuariofechacreacion() {
		return usuariofechacreacion;
	}
	public void setUsuariofechacreacion(Timestamp usuariofechacreacion) {
		this.usuariofechacreacion = usuariofechacreacion;
	}
	public Timestamp getUsuariofechamodificacion() {
		return usuariofechamodificacion;
	}
	public void setUsuariofechamodificacion(Timestamp usuariofechamodificacion) {
		this.usuariofechamodificacion = usuariofechamodificacion;
	}
	public Timestamp getUsuarioultimoacceso() {
		return usuarioultimoacceso;
	}
	public void setUsuarioultimoacceso(Timestamp usuarioultimoacceso) {
		this.usuarioultimoacceso = usuarioultimoacceso;
	}
	public String getUsuariokey() {
		return usuariokey;
	}
	public void setUsuariokey(String usuariokey) {
		this.usuariokey = usuariokey;
	}
	public InputStream getUsuarioimage() {
		return usuarioimage;
	}
	public void setUsuarioimage(InputStream usuarioimage) throws IOException {
		this.usuarioimage = usuarioimage;
	}
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	@Override
	public String toString() {
		return "Usuario [usuarioid=" + usuarioid + ", perfilid=" + perfilid + ", usuarioactivo=" + usuarioactivo
				+ ", usuariocorreo=" + usuariocorreo + ", usuariopwd=" + usuariopwd + ", usuarionombre=" + usuarionombre
				+ ", usuarioapellidopaterno=" + usuarioapellidopaterno + ", usuarioapellidomaterno="
				+ usuarioapellidomaterno + ", usuariofechacreacion=" + usuariofechacreacion
				+ ", usuariofechamodificacion=" + usuariofechamodificacion + ", usuarioultimoacceso="
				+ usuarioultimoacceso + "]";
	}
}
