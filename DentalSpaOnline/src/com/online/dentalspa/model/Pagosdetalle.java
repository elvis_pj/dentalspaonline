package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Pagosdetalle {
	private int pagosdetalleid;
	private int pagoid;
	private String pagosdetalletipoid;
	private int formapagoid;
	private boolean pagosdetalleactivo;
	private boolean pagosdetalleliquidado;
	private double pagosdetalleimporte;
	private double pagosdetalleimportependiente;
	private String pagosdetallefactura;
	private Timestamp pagosdetallefechacreacion;
	private Timestamp pagosdetallefechamodificacion;
	private Pagos pagos;
	private Pagosdetalletipo pagosdetalletipo;
	private Formapago formapago;
	public int getPagosdetalleid() {
		return pagosdetalleid;
	}
	public void setPagosdetalleid(int pagosdetalleid) {
		this.pagosdetalleid = pagosdetalleid;
	}
	public int getPagoid() {
		return pagoid;
	}
	public void setPagoid(int pagoid) {
		this.pagoid = pagoid;
	}
	public String getPagosdetalletipoid() {
		return pagosdetalletipoid;
	}
	public void setPagosdetalletipoid(String pagosdetalletipoid) {
		this.pagosdetalletipoid = pagosdetalletipoid;
	}
	public int getFormapagoid() {
		return formapagoid;
	}
	public void setFormapagoid(int formapagoid) {
		this.formapagoid = formapagoid;
	}
	public boolean isPagosdetalleactivo() {
		return pagosdetalleactivo;
	}
	public void setPagosdetalleactivo(boolean pagosdetalleactivo) {
		this.pagosdetalleactivo = pagosdetalleactivo;
	}
	public boolean isPagosdetalleliquidado() {
		return pagosdetalleliquidado;
	}
	public void setPagosdetalleliquidado(boolean pagosdetalleliquidado) {
		this.pagosdetalleliquidado = pagosdetalleliquidado;
	}
	public double getPagosdetalleimporte() {
		return pagosdetalleimporte;
	}
	public double getPagosdetalleimportependiente() {
		return pagosdetalleimportependiente;
	}
	public void setPagosdetalleimportependiente(double pagosdetalleimportependiente) {
		this.pagosdetalleimportependiente = pagosdetalleimportependiente;
	}
	public void setPagosdetalleimporte(double pagosdetalleimporte) {
		this.pagosdetalleimporte = pagosdetalleimporte;
	}
	public String getPagosdetallefactura() {
		return pagosdetallefactura;
	}
	public void setPagosdetallefactura(String pagosdetallefactura) {
		this.pagosdetallefactura = pagosdetallefactura;
	}
	public Timestamp getPagosdetallefechacreacion() {
		return pagosdetallefechacreacion;
	}
	public void setPagosdetallefechacreacion(Timestamp pagosdetallefechacreacion) {
		this.pagosdetallefechacreacion = pagosdetallefechacreacion;
	}
	public Timestamp getPagosdetallefechamodificacion() {
		return pagosdetallefechamodificacion;
	}
	public void setPagosdetallefechamodificacion(Timestamp pagosdetallefechamodificacion) {
		this.pagosdetallefechamodificacion = pagosdetallefechamodificacion;
	}
	public Pagos getPagos() {
		return pagos;
	}
	public void setPagos(Pagos pagos) {
		this.pagos = pagos;
	}
	public Pagosdetalletipo getPagosdetalletipo() {
		return pagosdetalletipo;
	}
	public void setPagosdetalletipo(Pagosdetalletipo pagosdetalletipo) {
		this.pagosdetalletipo = pagosdetalletipo;
	}
	public Formapago getFormapago() {
		return formapago;
	}
	public void setFormapago(Formapago formapago) {
		this.formapago = formapago;
	}
	@Override
	public String toString() {
		return "Pagosdetalle [pagosdetalleid=" + pagosdetalleid + ", pagoid=" + pagoid + ", pagosdetalletipoid="
				+ pagosdetalletipoid + ", formapagoid=" + formapagoid + ", pagosdetalleactivo=" + pagosdetalleactivo
				+ ", pagosdetalleliquidado=" + pagosdetalleliquidado + ", pagosdetalleimporte=" + pagosdetalleimporte
				+ ", pagosdetalleimportependiente=" + pagosdetalleimportependiente + ", pagosdetallefactura="
				+ pagosdetallefactura + ", pagosdetallefechacreacion=" + pagosdetallefechacreacion
				+ ", pagosdetallefechamodificacion=" + pagosdetallefechamodificacion + ", pagos=" + pagos
				+ ", pagosdetalletipo=" + pagosdetalletipo + ", formapago=" + formapago + "]";
	}
}
