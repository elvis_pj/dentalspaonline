package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Tratamientodetalle {
	private int tratamientodetalleid;
	private int tratamientoid;
	private boolean tratamientodetalleactivo;
	private String tratamientodetallenombre;
	private String tratamientodetallecomentarios;
	private Timestamp tratamientodetallefechacreacion;
	private Timestamp tratamientodetallefechamodificacion;
	private Tratamientos tratamientos;
	public int getTratamientodetalleid() {
		return tratamientodetalleid;
	}
	public void setTratamientodetalleid(int tratamientodetalleid) {
		this.tratamientodetalleid = tratamientodetalleid;
	}
	public int getTratamientoid() {
		return tratamientoid;
	}
	public void setTratamientoid(int tratamientoid) {
		this.tratamientoid = tratamientoid;
	}
	public boolean isTratamientodetalleactivo() {
		return tratamientodetalleactivo;
	}
	public void setTratamientodetalleactivo(boolean tratamientodetalleactivo) {
		this.tratamientodetalleactivo = tratamientodetalleactivo;
	}
	public String getTratamientodetallenombre() {
		return tratamientodetallenombre;
	}
	public void setTratamientodetallenombre(String tratamientodetallenombre) {
		this.tratamientodetallenombre = tratamientodetallenombre;
	}
	public String getTratamientodetallecomentarios() {
		return tratamientodetallecomentarios;
	}
	public void setTratamientodetallecomentarios(String tratamientodetallecomentarios) {
		this.tratamientodetallecomentarios = tratamientodetallecomentarios;
	}
	public Timestamp getTratamientodetallefechacreacion() {
		return tratamientodetallefechacreacion;
	}
	public void setTratamientodetallefechacreacion(Timestamp tratamientodetallefechacreacion) {
		this.tratamientodetallefechacreacion = tratamientodetallefechacreacion;
	}
	public Timestamp getTratamientodetallefechamodificacion() {
		return tratamientodetallefechamodificacion;
	}
	public void setTratamientodetallefechamodificacion(Timestamp tratamientodetallefechamodificacion) {
		this.tratamientodetallefechamodificacion = tratamientodetallefechamodificacion;
	}
	public Tratamientos getTratamientos() {
		return tratamientos;
	}
	public void setTratamientos(Tratamientos tratamientos) {
		this.tratamientos = tratamientos;
	}
	@Override
	public String toString() {
		return "Tratamientodetalle [tratamientodetalleid=" + tratamientodetalleid + ", tratamientoid=" + tratamientoid
				+ ", tratamientodetalleactivo=" + tratamientodetalleactivo + ", tratamientodetallenombre="
				+ tratamientodetallenombre + ", tratamientodetallecomentarios=" + tratamientodetallecomentarios
				+ ", tratamientodetallefechacreacion=" + tratamientodetallefechacreacion
				+ ", tratamientodetallefechamodificacion=" + tratamientodetallefechamodificacion + "]";
	}
}
