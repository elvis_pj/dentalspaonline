package com.online.dentalspa.model;

import java.sql.Timestamp;
import java.util.ArrayList;

public class Tratamientos {
	private int tratamientoid;
	private int pacienteid;
	private int tipotratamientoid;
	private String tratamientocomentariosiniciales;
	private String tratamientoplan;
	private boolean tratamientoactivo;
	private boolean tratamientoterminado;
	private Timestamp tratamientofechacreacion;
	private Timestamp tratamientofechamodificacion;
	private Tipotratamiento tipotratamiento;
	private Odontogramas odontogramas;
	private ArrayList<Tratamientodetalle> tratamientosdetalle;
	private Pagos pagos;
	public int getTratamientoid() {
		return tratamientoid;
	}
	public void setTratamientoid(int tratamientoid) {
		this.tratamientoid = tratamientoid;
	}
	public int getPacienteid() {
		return pacienteid;
	}
	public void setPacienteid(int pacienteid) {
		this.pacienteid = pacienteid;
	}
	public int getTipotratamientoid() {
		return tipotratamientoid;
	}
	public void setTipotratamientoid(int tipotratamientoid) {
		this.tipotratamientoid = tipotratamientoid;
	}
	public String getTratamientocomentariosiniciales() {
		return tratamientocomentariosiniciales;
	}
	public void setTratamientocomentariosiniciales(String tratamientocomentariosiniciales) {
		this.tratamientocomentariosiniciales = tratamientocomentariosiniciales;
	}
	public String getTratamientoplan() {
		return tratamientoplan;
	}
	public void setTratamientoplan(String tratamientoplan) {
		this.tratamientoplan = tratamientoplan;
	}
	public boolean isTratamientoactivo() {
		return tratamientoactivo;
	}
	public void setTratamientoactivo(boolean tratamientoactivo) {
		this.tratamientoactivo = tratamientoactivo;
	}
	public boolean isTratamientoterminado() {
		return tratamientoterminado;
	}
	public void setTratamientoterminado(boolean tratamientoterminado) {
		this.tratamientoterminado = tratamientoterminado;
	}
	public Timestamp getTratamientofechacreacion() {
		return tratamientofechacreacion;
	}
	public void setTratamientofechacreacion(Timestamp tratamientofechacreacion) {
		this.tratamientofechacreacion = tratamientofechacreacion;
	}
	public Timestamp getTratamientofechamodificacion() {
		return tratamientofechamodificacion;
	}
	public void setTratamientofechamodificacion(Timestamp tratamientofechamodificacion) {
		this.tratamientofechamodificacion = tratamientofechamodificacion;
	}
	public Tipotratamiento getTipotratamiento() {
		return tipotratamiento;
	}
	public void setTipotratamiento(Tipotratamiento tipotratamiento) {
		this.tipotratamiento = tipotratamiento;
	}
	public Odontogramas getOdontogramas() {
		return odontogramas;
	}
	public void setOdontogramas(Odontogramas odontogramas) {
		this.odontogramas = odontogramas;
	}
	public ArrayList<Tratamientodetalle> getTratamientosdetalle() {
		return tratamientosdetalle;
	}
	public void setTratamientosdetalle(ArrayList<Tratamientodetalle> tratamientosdetalle) {
		this.tratamientosdetalle = tratamientosdetalle;
	}
	public Pagos getPagos() {
		return pagos;
	}
	public void setPagos(Pagos pagos) {
		this.pagos = pagos;
	}
	@Override
	public String toString() {
		return "Tratamientos [tratamientoid=" + tratamientoid + ", pacienteid=" + pacienteid + ", tipotratamientoid="
				+ tipotratamientoid + ", tratamientocomentariosiniciales=" + tratamientocomentariosiniciales
				+ ", tratamientoplan=" + tratamientoplan + ", tratamientoactivo=" + tratamientoactivo
				+ ", tratamientoterminado=" + tratamientoterminado + ", tratamientofechacreacion="
				+ tratamientofechacreacion + ", tratamientofechamodificacion=" + tratamientofechamodificacion + "]";
	}
}
