package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Examendental {
	private int examendentalid;
	private int pacienteid;
	private boolean examendentalactivo;
	private String examendentalhigiene;
	private String examendentalencia;
	private String examendentalpisoboca;
	private String examendentallabios;
	private String examendentalcarillos;
	private String examendentalbruxismo;
	private String examendentalchasquidos;
	private String examendentallengua;
	private String examendentalpaladar;
	private String examendentalsupernumerarios;
	private String examendentalsangrado;
	private String examendentalplacasarro;
	private String examendentalalteraciones;
	private String examendentalnota;
	private Timestamp examendentalfechacreacion;
	private Timestamp examendentalfechamodificacion;
	private Pacientes paciente;
	public int getExamendentalid() {
		return examendentalid;
	}
	public void setExamendentalid(int examendentalid) {
		this.examendentalid = examendentalid;
	}
	public int getPacienteid() {
		return pacienteid;
	}
	public void setPacienteid(int pacienteid) {
		this.pacienteid = pacienteid;
	}
	public boolean isExamendentalactivo() {
		return examendentalactivo;
	}
	public void setExamendentalactivo(boolean examendentalactivo) {
		this.examendentalactivo = examendentalactivo;
	}
	public String getExamendentalhigiene() {
		return examendentalhigiene;
	}
	public void setExamendentalhigiene(String examendentalhigiene) {
		this.examendentalhigiene = examendentalhigiene;
	}
	public String getExamendentalencia() {
		return examendentalencia;
	}
	public void setExamendentalencia(String examendentalencia) {
		this.examendentalencia = examendentalencia;
	}
	public String getExamendentalpisoboca() {
		return examendentalpisoboca;
	}
	public void setExamendentalpisoboca(String examendentalpisoboca) {
		this.examendentalpisoboca = examendentalpisoboca;
	}
	public String getExamendentallabios() {
		return examendentallabios;
	}
	public void setExamendentallabios(String examendentallabios) {
		this.examendentallabios = examendentallabios;
	}
	public String getExamendentalcarillos() {
		return examendentalcarillos;
	}
	public void setExamendentalcarillos(String examendentalcarillos) {
		this.examendentalcarillos = examendentalcarillos;
	}
	public String getExamendentalbruxismo() {
		return examendentalbruxismo;
	}
	public void setExamendentalbruxismo(String examendentalbruxismo) {
		this.examendentalbruxismo = examendentalbruxismo;
	}
	public String getExamendentalchasquidos() {
		return examendentalchasquidos;
	}
	public void setExamendentalchasquidos(String examendentalchasquidos) {
		this.examendentalchasquidos = examendentalchasquidos;
	}
	public String getExamendentallengua() {
		return examendentallengua;
	}
	public void setExamendentallengua(String examendentallengua) {
		this.examendentallengua = examendentallengua;
	}
	public String getExamendentalpaladar() {
		return examendentalpaladar;
	}
	public void setExamendentalpaladar(String examendentalpaladar) {
		this.examendentalpaladar = examendentalpaladar;
	}
	public String getExamendentalsupernumerarios() {
		return examendentalsupernumerarios;
	}
	public void setExamendentalsupernumerarios(String examendentalsupernumerarios) {
		this.examendentalsupernumerarios = examendentalsupernumerarios;
	}
	public String getExamendentalsangrado() {
		return examendentalsangrado;
	}
	public void setExamendentalsangrado(String examendentalsangrado) {
		this.examendentalsangrado = examendentalsangrado;
	}
	public String getExamendentalplacasarro() {
		return examendentalplacasarro;
	}
	public void setExamendentalplacasarro(String examendentalplacasarro) {
		this.examendentalplacasarro = examendentalplacasarro;
	}
	public String getExamendentalalteraciones() {
		return examendentalalteraciones;
	}
	public void setExamendentalalteraciones(String examendentalalteraciones) {
		this.examendentalalteraciones = examendentalalteraciones;
	}
	public String getExamendentalnota() {
		return examendentalnota;
	}
	public void setExamendentalnota(String examendentalnota) {
		this.examendentalnota = examendentalnota;
	}
	public Timestamp getExamendentalfechacreacion() {
		return examendentalfechacreacion;
	}
	public void setExamendentalfechacreacion(Timestamp examendentalfechacreacion) {
		this.examendentalfechacreacion = examendentalfechacreacion;
	}
	public Timestamp getExamendentalfechamodificacion() {
		return examendentalfechamodificacion;
	}
	public void setExamendentalfechamodificacion(Timestamp examendentalfechamodificacion) {
		this.examendentalfechamodificacion = examendentalfechamodificacion;
	}
	public Pacientes getPaciente() {
		return paciente;
	}
	public void setPaciente(Pacientes paciente) {
		this.paciente = paciente;
	}
	@Override
	public String toString() {
		return "Examendental [examendentalid=" + examendentalid + ", pacienteid=" + pacienteid + ", examendentalactivo="
				+ examendentalactivo + ", examendentalhigiene=" + examendentalhigiene + ", examendentalencia="
				+ examendentalencia + ", examendentalpisoboca=" + examendentalpisoboca + ", examendentallabios="
				+ examendentallabios + ", examendentalcarillos=" + examendentalcarillos + ", examendentalbruxismo="
				+ examendentalbruxismo + ", examendentalchasquidos=" + examendentalchasquidos
				+ ", examendentallengua=" + examendentallengua + ", examendentalpaladar=" + examendentalpaladar
				+ ", examendentalsupernumerarios=" + examendentalsupernumerarios + ", examendentalsangrado="
				+ examendentalsangrado + ", examendentalplacasarro=" + examendentalplacasarro
				+ ", examendentalalteraciones=" + examendentalalteraciones + ", examendentalnota=" + examendentalnota
				+ ", examendentalfechacreacion=" + examendentalfechacreacion + ", examendentalfechamodificacion="
				+ examendentalfechamodificacion + "]";
	}
}
