package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Odontogramas {
	private int odontogramaid;
	private int tratamientoid;
	private boolean odontogramaactivo;
	private String odontogramacomentarios;
	private Timestamp odontogramafechacreacion;
	private Timestamp odontogramafechamodificacion;
	private String odontograma_11;
	private String odontograma_12;
	private String odontograma_13;
	private String odontograma_14;
	private String odontograma_15;
	private String odontograma_16;
	private String odontograma_17;
	private String odontograma_18;
	private String odontograma_21;
	private String odontograma_22;
	private String odontograma_23;
	private String odontograma_24;
	private String odontograma_25;
	private String odontograma_26;
	private String odontograma_27;
	private String odontograma_28;
	private String odontograma_31;
	private String odontograma_32;
	private String odontograma_33;
	private String odontograma_34;
	private String odontograma_35;
	private String odontograma_36;
	private String odontograma_37;
	private String odontograma_38;
	private String odontograma_41;
	private String odontograma_42;
	private String odontograma_43;
	private String odontograma_44;
	private String odontograma_45;
	private String odontograma_46;
	private String odontograma_47;
	private String odontograma_48;
	private Tratamientos tratamientos;
	public int getOdontogramaid() {
		return odontogramaid;
	}
	public void setOdontogramaid(int odontogramaid) {
		this.odontogramaid = odontogramaid;
	}
	public int getTratamientoid() {
		return tratamientoid;
	}
	public void setTratamientoid(int tratamientoid) {
		this.tratamientoid = tratamientoid;
	}
	public boolean isOdontogramaactivo() {
		return odontogramaactivo;
	}
	public void setOdontogramaactivo(boolean odontogramaactivo) {
		this.odontogramaactivo = odontogramaactivo;
	}
	public String getOdontogramacomentarios() {
		return odontogramacomentarios;
	}
	public void setOdontogramacomentarios(String odontogramacomentarios) {
		this.odontogramacomentarios = odontogramacomentarios;
	}
	public Timestamp getOdontogramafechacreacion() {
		return odontogramafechacreacion;
	}
	public void setOdontogramafechacreacion(Timestamp odontogramafechacreacion) {
		this.odontogramafechacreacion = odontogramafechacreacion;
	}
	public Timestamp getOdontogramafechamodificacion() {
		return odontogramafechamodificacion;
	}
	public void setOdontogramafechamodificacion(Timestamp odontogramafechamodificacion) {
		this.odontogramafechamodificacion = odontogramafechamodificacion;
	}
	public String getOdontograma_11() {
		return odontograma_11;
	}
	public void setOdontograma_11(String odontograma_11) {
		this.odontograma_11 = odontograma_11;
	}
	public String getOdontograma_12() {
		return odontograma_12;
	}
	public void setOdontograma_12(String odontograma_12) {
		this.odontograma_12 = odontograma_12;
	}
	public String getOdontograma_13() {
		return odontograma_13;
	}
	public void setOdontograma_13(String odontograma_13) {
		this.odontograma_13 = odontograma_13;
	}
	public String getOdontograma_14() {
		return odontograma_14;
	}
	public void setOdontograma_14(String odontograma_14) {
		this.odontograma_14 = odontograma_14;
	}
	public String getOdontograma_15() {
		return odontograma_15;
	}
	public void setOdontograma_15(String odontograma_15) {
		this.odontograma_15 = odontograma_15;
	}
	public String getOdontograma_16() {
		return odontograma_16;
	}
	public void setOdontograma_16(String odontograma_16) {
		this.odontograma_16 = odontograma_16;
	}
	public String getOdontograma_17() {
		return odontograma_17;
	}
	public void setOdontograma_17(String odontograma_17) {
		this.odontograma_17 = odontograma_17;
	}
	public String getOdontograma_18() {
		return odontograma_18;
	}
	public void setOdontograma_18(String odontograma_18) {
		this.odontograma_18 = odontograma_18;
	}
	public String getOdontograma_21() {
		return odontograma_21;
	}
	public void setOdontograma_21(String odontograma_21) {
		this.odontograma_21 = odontograma_21;
	}
	public String getOdontograma_22() {
		return odontograma_22;
	}
	public void setOdontograma_22(String odontograma_22) {
		this.odontograma_22 = odontograma_22;
	}
	public String getOdontograma_23() {
		return odontograma_23;
	}
	public void setOdontograma_23(String odontograma_23) {
		this.odontograma_23 = odontograma_23;
	}
	public String getOdontograma_24() {
		return odontograma_24;
	}
	public void setOdontograma_24(String odontograma_24) {
		this.odontograma_24 = odontograma_24;
	}
	public String getOdontograma_25() {
		return odontograma_25;
	}
	public void setOdontograma_25(String odontograma_25) {
		this.odontograma_25 = odontograma_25;
	}
	public String getOdontograma_26() {
		return odontograma_26;
	}
	public void setOdontograma_26(String odontograma_26) {
		this.odontograma_26 = odontograma_26;
	}
	public String getOdontograma_27() {
		return odontograma_27;
	}
	public void setOdontograma_27(String odontograma_27) {
		this.odontograma_27 = odontograma_27;
	}
	public String getOdontograma_28() {
		return odontograma_28;
	}
	public void setOdontograma_28(String odontograma_28) {
		this.odontograma_28 = odontograma_28;
	}
	public String getOdontograma_31() {
		return odontograma_31;
	}
	public void setOdontograma_31(String odontograma_31) {
		this.odontograma_31 = odontograma_31;
	}
	public String getOdontograma_32() {
		return odontograma_32;
	}
	public void setOdontograma_32(String odontograma_32) {
		this.odontograma_32 = odontograma_32;
	}
	public String getOdontograma_33() {
		return odontograma_33;
	}
	public void setOdontograma_33(String odontograma_33) {
		this.odontograma_33 = odontograma_33;
	}
	public String getOdontograma_34() {
		return odontograma_34;
	}
	public void setOdontograma_34(String odontograma_34) {
		this.odontograma_34 = odontograma_34;
	}
	public String getOdontograma_35() {
		return odontograma_35;
	}
	public void setOdontograma_35(String odontograma_35) {
		this.odontograma_35 = odontograma_35;
	}
	public String getOdontograma_36() {
		return odontograma_36;
	}
	public void setOdontograma_36(String odontograma_36) {
		this.odontograma_36 = odontograma_36;
	}
	public String getOdontograma_37() {
		return odontograma_37;
	}
	public void setOdontograma_37(String odontograma_37) {
		this.odontograma_37 = odontograma_37;
	}
	public String getOdontograma_38() {
		return odontograma_38;
	}
	public void setOdontograma_38(String odontograma_38) {
		this.odontograma_38 = odontograma_38;
	}
	public String getOdontograma_41() {
		return odontograma_41;
	}
	public void setOdontograma_41(String odontograma_41) {
		this.odontograma_41 = odontograma_41;
	}
	public String getOdontograma_42() {
		return odontograma_42;
	}
	public void setOdontograma_42(String odontograma_42) {
		this.odontograma_42 = odontograma_42;
	}
	public String getOdontograma_43() {
		return odontograma_43;
	}
	public void setOdontograma_43(String odontograma_43) {
		this.odontograma_43 = odontograma_43;
	}
	public String getOdontograma_44() {
		return odontograma_44;
	}
	public void setOdontograma_44(String odontograma_44) {
		this.odontograma_44 = odontograma_44;
	}
	public String getOdontograma_45() {
		return odontograma_45;
	}
	public void setOdontograma_45(String odontograma_45) {
		this.odontograma_45 = odontograma_45;
	}
	public String getOdontograma_46() {
		return odontograma_46;
	}
	public void setOdontograma_46(String odontograma_46) {
		this.odontograma_46 = odontograma_46;
	}
	public String getOdontograma_47() {
		return odontograma_47;
	}
	public void setOdontograma_47(String odontograma_47) {
		this.odontograma_47 = odontograma_47;
	}
	public String getOdontograma_48() {
		return odontograma_48;
	}
	public void setOdontograma_48(String odontograma_48) {
		this.odontograma_48 = odontograma_48;
	}
	public Tratamientos getTratamientos() {
		return tratamientos;
	}
	public void setTratamientos(Tratamientos tratamientos) {
		this.tratamientos = tratamientos;
	}
	@Override
	public String toString() {
		return "Odontogramas [odontogramaid=" + odontogramaid + ", tratamientoid=" + tratamientoid + ", odontogramaactivo="
				+ odontogramaactivo + ", odontogramacomentarios=" + odontogramacomentarios
				+ ", odontogramafechacreacion=" + odontogramafechacreacion + ", odontogramafechamodificacion="
				+ odontogramafechamodificacion + ", odontograma_11=" + odontograma_11 + ", odontograma_12="
				+ odontograma_12 + ", odontograma_13=" + odontograma_13 + ", odontograma_14=" + odontograma_14
				+ ", odontograma_15=" + odontograma_15 + ", odontograma_16=" + odontograma_16 + ", odontograma_17="
				+ odontograma_17 + ", odontograma_18=" + odontograma_18 + ", odontograma_21=" + odontograma_21
				+ ", odontograma_22=" + odontograma_22 + ", odontograma_23=" + odontograma_23 + ", odontograma_24="
				+ odontograma_24 + ", odontograma_25=" + odontograma_25 + ", odontograma_26=" + odontograma_26
				+ ", odontograma_27=" + odontograma_27 + ", odontograma_28=" + odontograma_28 + ", odontograma_31="
				+ odontograma_31 + ", odontograma_32=" + odontograma_32 + ", odontograma_33=" + odontograma_33
				+ ", odontograma_34=" + odontograma_34 + ", odontograma_35=" + odontograma_35 + ", odontograma_36="
				+ odontograma_36 + ", odontograma_37=" + odontograma_37 + ", odontograma_38=" + odontograma_38
				+ ", odontograma_41=" + odontograma_41 + ", odontograma_42=" + odontograma_42 + ", odontograma_43="
				+ odontograma_43 + ", odontograma_44=" + odontograma_44 + ", odontograma_45=" + odontograma_45
				+ ", odontograma_46=" + odontograma_46 + ", odontograma_47=" + odontograma_47 + ", odontograma_48="
				+ odontograma_48 + "]";
	}
}
