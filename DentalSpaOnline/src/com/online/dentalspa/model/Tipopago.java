package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Tipopago {
	private int tipopagoid;
	private boolean tipopagoactivo;
	private String tipopagonombre;
	private String tipopagodescripcion;
	private Timestamp tipopagofechacreacion;
	private Timestamp tipopagofechamodificacion;
	public int getTipopagoid() {
		return tipopagoid;
	}
	public void setTipopagoid(int tipopagoid) {
		this.tipopagoid = tipopagoid;
	}
	public boolean isTipopagoactivo() {
		return tipopagoactivo;
	}
	public void setTipopagoactivo(boolean tipopagoactivo) {
		this.tipopagoactivo = tipopagoactivo;
	}
	public String getTipopagonombre() {
		return tipopagonombre;
	}
	public void setTipopagonombre(String tipopagonombre) {
		this.tipopagonombre = tipopagonombre;
	}
	public String getTipopagodescripcion() {
		return tipopagodescripcion;
	}
	public void setTipopagodescripcion(String tipopagodescripcion) {
		this.tipopagodescripcion = tipopagodescripcion;
	}
	public Timestamp getTipopagofechacreacion() {
		return tipopagofechacreacion;
	}
	public void setTipopagofechacreacion(Timestamp tipopagofechacreacion) {
		this.tipopagofechacreacion = tipopagofechacreacion;
	}
	public Timestamp getTipopagofechamodificacion() {
		return tipopagofechamodificacion;
	}
	public void setTipopagofechamodificacion(Timestamp tipopagofechamodificacion) {
		this.tipopagofechamodificacion = tipopagofechamodificacion;
	}
	@Override
	public String toString() {
		return "Tipopago [tipopagoid=" + tipopagoid + ", tipopagoactivo=" + tipopagoactivo + ", tipopagonombre="
				+ tipopagonombre + ", tipopagodescripcion=" + tipopagodescripcion + ", tipopagofechacreacion="
				+ tipopagofechacreacion + ", tipopagofechamodificacion=" + tipopagofechamodificacion + "]";
	}
}
