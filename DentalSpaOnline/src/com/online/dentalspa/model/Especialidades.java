package com.online.dentalspa.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Especialidades implements Serializable {
	private static final long serialVersionUID = 3023273795161658877L;
	private int especialidadid;
	private boolean especialidadactivo;
	private String especialidadnombre;
	private String especialidaddescripcion;
	private Timestamp especialidadfechacreacion;
	private Timestamp especialidadfechamodificacion;
	public int getEspecialidadid() {
		return especialidadid;
	}
	public void setEspecialidadid(int especialidadid) {
		this.especialidadid = especialidadid;
	}
	public boolean isEspecialidadactivo() {
		return especialidadactivo;
	}
	public void setEspecialidadactivo(boolean especialidadactivo) {
		this.especialidadactivo = especialidadactivo;
	}
	public String getEspecialidadnombre() {
		return especialidadnombre;
	}
	public void setEspecialidadnombre(String especialidadnombre) {
		this.especialidadnombre = especialidadnombre;
	}
	public String getEspecialidaddescripcion() {
		return especialidaddescripcion;
	}
	public void setEspecialidaddescripcion(String especialidaddescripcion) {
		this.especialidaddescripcion = especialidaddescripcion;
	}
	public Timestamp getEspecialidadfechacreacion() {
		return especialidadfechacreacion;
	}
	public void setEspecialidadfechacreacion(Timestamp especialidadfechacreacion) {
		this.especialidadfechacreacion = especialidadfechacreacion;
	}
	public Timestamp getEspecialidadfechamodificacion() {
		return especialidadfechamodificacion;
	}
	public void setEspecialidadfechamodificacion(Timestamp especialidadfechamodificacion) {
		this.especialidadfechamodificacion = especialidadfechamodificacion;
	}
	@Override
	public String toString() {
		return "Especialidades [especialidadid=" + especialidadid + ", especialidadactivo=" + especialidadactivo
				+ ", especialidadnombre=" + especialidadnombre + ", especialidaddescripcion=" + especialidaddescripcion
				+ ", especialidadfechacreacion=" + especialidadfechacreacion + ", especialidadfechamodificacion="
				+ especialidadfechamodificacion + "]";
	}
}
