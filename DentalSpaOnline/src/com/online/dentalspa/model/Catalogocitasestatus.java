package com.online.dentalspa.model;

import java.sql.Timestamp;

public class Catalogocitasestatus {
	private int citasestatusid;
	private boolean citasestatusactivo;
	private String citasestatusnombre;
	private String citasestatusdescripcion;
	private Timestamp citasestatusfechacreacion;
	private Timestamp citasestatusfechamodificacion;
	public int getCitasestatusid() {
		return citasestatusid;
	}
	public void setCitasestatusid(int citasestatusid) {
		this.citasestatusid = citasestatusid;
	}
	public boolean isCitasestatusactivo() {
		return citasestatusactivo;
	}
	public void setCitasestatusactivo(boolean citasestatusactivo) {
		this.citasestatusactivo = citasestatusactivo;
	}
	public String getCitasestatusnombre() {
		return citasestatusnombre;
	}
	public void setCitasestatusnombre(String citasestatusnombre) {
		this.citasestatusnombre = citasestatusnombre;
	}
	public String getCitasestatusdescripcion() {
		return citasestatusdescripcion;
	}
	public void setCitasestatusdescripcion(String citasestatusdescripcion) {
		this.citasestatusdescripcion = citasestatusdescripcion;
	}
	public Timestamp getCitasestatusfechacreacion() {
		return citasestatusfechacreacion;
	}
	public void setCitasestatusfechacreacion(Timestamp citasestatusfechacreacion) {
		this.citasestatusfechacreacion = citasestatusfechacreacion;
	}
	public Timestamp getCitasestatusfechamodificacion() {
		return citasestatusfechamodificacion;
	}
	public void setCitasestatusfechamodificacion(Timestamp citasestatusfechamodificacion) {
		this.citasestatusfechamodificacion = citasestatusfechamodificacion;
	}
	@Override
	public String toString() {
		return "Catalogocitasestatus [citasestatusid=" + citasestatusid + ", citasestatusactivo=" + citasestatusactivo
				+ ", citasestatusnombre=" + citasestatusnombre + ", citasestatusdescripcion=" + citasestatusdescripcion
				+ ", citasestatusfechacreacion=" + citasestatusfechacreacion + ", citasestatusfechamodificacion="
				+ citasestatusfechamodificacion + "]";
	}
}
