package com.online.dentalspa.dao;

import java.util.ArrayList;

import com.online.dentalspa.model.Tratamientos;

public interface TratamientosDao {

	public String getDetailError();

	public ArrayList<Tratamientos> getListTratamientos(int pacienteid);

	public ArrayList<Tratamientos> getListTratamientosPagos(int pacienteid);

	public boolean createTratamiento(Tratamientos tratamiento);
}
