package com.online.dentalspa.dao;

import com.online.dentalspa.model.Examendental;

public interface ExamendentalDao {

	public String getDetailError();

	public boolean crearExamendental(Examendental examendental);
}
