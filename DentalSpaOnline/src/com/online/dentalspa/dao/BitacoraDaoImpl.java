package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Bitacora;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class BitacoraDaoImpl implements BitacoraDao {
	private Logger log = Logger.getLogger(BitacoraDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean updateBitacora(Bitacora bitacora) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getSPUpdateBitacora());
			preparedStatement.setInt(1, bitacora.getPlataformaid());
			preparedStatement.setInt(2, bitacora.getProcesoid());
			preparedStatement.setInt(3, bitacora.getUsuarioid());
			preparedStatement.setString(4, bitacora.getBitacorapeticion());
			preparedStatement.setString(5, bitacora.getBitacorarespuesta());
			preparedStatement.setString(6, bitacora.getBitacoramensaje());
			preparedStatement.setString(7, bitacora.getBitacoraerror());
			preparedStatement.setString(8, bitacora.getBitacoraip());
			preparedStatement.setTimestamp(9, bitacora.getBitacorafecha());
			preparedStatement.setInt(10, bitacora.getBitacoraid());
			if((resultSet = preparedStatement.executeQuery()).next())
			{
//				log.info("ResponseSP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
					return true;
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del SP:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta del sp_create_bitacora";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al actualizar en bitacor",ex);
			DetailError="Se genero un error al actualizar en bitacora, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public boolean createBitacora(Bitacora bitacora) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreateBitacora());
			preparedStatement.setInt(1, bitacora.getPlataformaid());
			preparedStatement.setInt(2, bitacora.getProcesoid());
			preparedStatement.setInt(3, bitacora.getUsuarioid());
			preparedStatement.setString(4, bitacora.getBitacorapeticion());
			preparedStatement.setString(5, bitacora.getBitacorarespuesta());
			preparedStatement.setString(6, bitacora.getBitacoramensaje());
			preparedStatement.setString(7, bitacora.getBitacoraerror());
			preparedStatement.setString(8, bitacora.getBitacoraip());
			preparedStatement.setTimestamp(9, bitacora.getBitacorafecha());
			if((resultSet = preparedStatement.executeQuery()).next())
			{
//				log.info("ResponseSP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					bitacora.setBitacoraid(resultSet.getInt("rs_id"));
					return true;
				}
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del SP:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta del sp_create_bitacora";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar en bitacor",ex);
			DetailError="Se genero un error al registrar en bitacora, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

}
