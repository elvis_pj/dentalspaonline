package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.online.dentalspa.model.Pagos;
import com.online.dentalspa.model.Pagosdetalle;
import com.online.dentalspa.model.Tratamientos;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class TratamientosDaoImpl implements TratamientosDao {
	private Logger log = Logger.getLogger(TratamientosDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean createTratamiento(Tratamientos tratamiento) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreateTratamiento());
			preparedStatement.setInt(1, tratamiento.getPacienteid());
			preparedStatement.setInt(2, tratamiento.getTipotratamientoid());
			preparedStatement.setString(3, tratamiento.getTratamientocomentariosiniciales());
			preparedStatement.setString(4, tratamiento.getTratamientoplan());
			preparedStatement.setBoolean(5, tratamiento.isTratamientoactivo());
			preparedStatement.setBoolean(6, tratamiento.isTratamientoterminado());
			preparedStatement.setTimestamp(7, tratamiento.getTratamientofechacreacion());
			preparedStatement.setTimestamp(8, tratamiento.getTratamientofechamodificacion());
			preparedStatement.setString(9, new Gson().toJson(tratamiento.getOdontogramas()));
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					tratamiento.setTratamientoid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el Tratamiento: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public ArrayList<Tratamientos> getListTratamientosPagos(int pacienteid) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Tratamientos> tratamientos=null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getViewTratamientosPagos());
			preparedStatement.setInt(1, pacienteid);
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				Pagosdetalle pagosdetalle=null;
				ArrayList<Pagosdetalle> listapagosdetalle=new ArrayList<Pagosdetalle>();
				tratamientos = new ArrayList<Tratamientos>();
				
				Tratamientos tratamiento = utils.getObjectTratamientos(resultSet);
				tratamiento.setTipotratamiento(utils.getObjectTipotratamiento(resultSet));
				Pagos pagos = utils.getObjectPagos(resultSet);
				pagos.setTipopago(utils.getObjectTipopago(resultSet));
				do{
					if(tratamiento.getTratamientoid()!=resultSet.getInt("tratamientoid"))
					{
						pagos.setPagosdetalle(listapagosdetalle);
						tratamiento.setPagos(pagos);
						tratamientos.add(tratamiento);
						
						tratamiento = utils.getObjectTratamientos(resultSet);
						tratamiento.setTipotratamiento(utils.getObjectTipotratamiento(resultSet));
						pagos = utils.getObjectPagos(resultSet);
						pagos.setTipopago(utils.getObjectTipopago(resultSet));
						listapagosdetalle=new ArrayList<Pagosdetalle>();
					}
					pagosdetalle = utils.getObjectPagosdetalle(resultSet);
					pagosdetalle.setFormapago(utils.getObjectFormapago(resultSet));
					pagosdetalle.setPagosdetalletipo(utils.getObjectPagosdetalletipo(resultSet));
					listapagosdetalle.add(pagosdetalle);
				}while(resultSet.next());

				pagos.setPagosdetalle(listapagosdetalle);
				tratamiento.setPagos(pagos);
				tratamientos.add(tratamiento);
				
				for(Tratamientos tra: tratamientos)
				{
					log.info(tra.toString());
					if(tra.getPagos()!=null)
					{
						log.info("\t Pago :: "+tra.getPagos()!=null ? tra.getPagos().toString() : "");
						for(Pagosdetalle det: tra.getPagos().getPagosdetalle())
						{
							log.info("\t\t :: "+det.toString());
						}
					}
				}
			}
			else
			{
				log.warn("No se encontraron coincidencias con el parametro de busqueda :: "+preparedStatement.toString());
				DetailError="No se encontraron tratamientos";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al buscarlos Tratamientos: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return tratamientos;
	}

	@Override
	public ArrayList<Tratamientos> getListTratamientos(int pacienteid) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Tratamientos> tratamientos=null;
		TratamientodetalleDao tratamientodetalleDao=null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getListaTratamientos());
			preparedStatement.setInt(1, pacienteid);
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				tratamientodetalleDao = new TratamientodetalleDaoImpl();
				tratamientos = new ArrayList<Tratamientos>();
				do {
					Tratamientos tratamiento = utils.getObjectTratamientos(resultSet);
					tratamiento.setTipotratamiento(utils.getObjectTipotratamiento(resultSet));
					tratamiento.setOdontogramas(utils.getObjectOdontograma(resultSet));
					tratamiento.setTratamientosdetalle(tratamientodetalleDao.getListaTratamientosdetalle(tratamiento.getTratamientoid(), connection));
					tratamientos.add(tratamiento);
				}while(resultSet.next());
			}
			else
			{
				log.warn("No se encontraron coincidencias con el parametro de busqueda :: "+preparedStatement.toString());
				DetailError="No se encontraron tratamientos";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al buscarlos Tratamientos: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return tratamientos;
	}

}
