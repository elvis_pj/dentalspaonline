package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Formapago;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class FormapagoDaoImpl implements FormapagoDao {
	private Logger log = Logger.getLogger(FormapagoDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;

	@Override
	public ArrayList<Formapago> getFormapago() {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Formapago> tipopago = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getListaFormapago());
			if((resultSet = preparedStatement.executeQuery()).next())
			{
				tipopago = new ArrayList<Formapago>();
				do {
					tipopago.add(utils.getObjectFormapago(resultSet));
				}while(resultSet.next());
			}
			else
			{
				log.error("No se recuperaron registros de Formapago :: "+preparedStatement.toString());
				DetailError = "No se recuperaron registros de forma de pago";
			}
		}catch(Exception ex) {
			log.error("Se genero un error al recuperar la lista de Formapago",ex);
			DetailError="Se genero un error al recuperar la lista de Forma de pago:: "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return tipopago;
	}
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

}
