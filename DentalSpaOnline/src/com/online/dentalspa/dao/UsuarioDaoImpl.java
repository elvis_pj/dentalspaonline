package com.online.dentalspa.dao;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Permisos;
import com.online.dentalspa.model.Procesos;
import com.online.dentalspa.model.Usuario;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class UsuarioDaoImpl implements UsuarioDao {
	private Logger log = Logger.getLogger(UsuarioDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;

	@Override
	public void mostrarImagen(Usuario usuario, HttpServletResponse response) {
		OutputStream outputStream=null;
		BufferedInputStream bufferedInputStream=null;
		BufferedOutputStream bufferedOutputStream=null;
		response.setContentType("image/*");
		try {
			log.info("inputstream:: "+usuario.getUsuarioimage().available());
			outputStream= response.getOutputStream();
			bufferedInputStream=new BufferedInputStream(usuario.getUsuarioimage());
			bufferedOutputStream=new BufferedOutputStream(outputStream);
			int i=0;
			while((i=bufferedInputStream.read())!=-1) {
				System.out.println("leyendo");
				bufferedOutputStream.write(i);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean updateUsuario(Usuario usuario) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPUpdateUsuario());
			preparedStatement.setInt(1, usuario.getUsuarioid());
			preparedStatement.setInt(2, usuario.getPerfilid());
			preparedStatement.setBoolean(3, usuario.isUsuarioactivo());
			preparedStatement.setString(4, usuario.getUsuariocorreo());
			preparedStatement.setString(5, usuario.getUsuariopwd());
			preparedStatement.setString(6, usuario.getUsuarionombre());
			preparedStatement.setString(7, usuario.getUsuarioapellidopaterno());
			preparedStatement.setString(8, usuario.getUsuarioapellidomaterno());
			preparedStatement.setTimestamp(9, usuario.getUsuariofechacreacion());
			preparedStatement.setTimestamp(10, usuario.getUsuariofechamodificacion());
			preparedStatement.setTimestamp(11, usuario.getUsuarioultimoacceso());
			preparedStatement.setString(12, usuario.getUsuariokey());
			preparedStatement.setBinaryStream(13, usuario.getUsuarioimage(), usuario.getUsuarioimage().available());
			log.info("Image["+usuario.getUsuarioimage().available()+"] :: "+preparedStatement.toString());
			if((resultSet = preparedStatement.executeQuery()).next()) 
			{
				log.info("Responses SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					return true;
				}
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al actualizar el usuario: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public boolean CambiaContrasenia(Usuario usuario, String usuariopwd_old, String usuariopwd_new) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPCambiarContrasenia());
			preparedStatement.setInt(1, usuario.getUsuarioid());
			preparedStatement.setString(2, usuariopwd_old);
			preparedStatement.setString(3, usuariopwd_new);

			if((resultSet = preparedStatement.executeQuery()).next()) 
			{
				log.info("Responses SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					usuario.setUsuariopwd(resultSet.getString("rs_usuariopwd"));
					connection.commit();
					return true;
				}
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al cambiar la contrasenia: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public Usuario validateUsuario(Usuario usuario, int intentos) {
		log.info("Valida usuario ["+usuario.getUsuariocorreo()+"]");
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getSPvalidateUsuario());
			preparedStatement.setString(1, usuario.getUsuariocorreo());
			preparedStatement.setString(2, usuario.getUsuariopwd());
			preparedStatement.setInt(3, intentos);
			if((resultSet = preparedStatement.executeQuery()).next()) 
			{
				log.info("Responses SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
					return getUsuario(resultSet.getInt("rs_id"));
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al validar el usuario: "+ex.getMessage(),ex);
			if(ex.getMessage().indexOf("P0001")>=0 && ex.getMessage().indexOf("Contrasenia incorrecta")>=0)
			{
				DetailError="Contraseña incorrecta";
			}
			else
				DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return null;
	}

	@Override
	public Usuario getUsuario(int Usuarioid) {
		Usuario usuario=null;
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try {
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getInfoUsuario());
			preparedStatement.setInt(1, Usuarioid);
			if((resultSet = preparedStatement.executeQuery()).next())
			{
				usuario = utils.getObjectUsuario(resultSet);
				usuario.setPerfil(utils.getObjectPerfil(resultSet));
				usuario.getPerfil().setPermisos(new ArrayList<Permisos>());
				do {
					Permisos permiso = utils.getObjectPermisos(resultSet);
					Procesos proceso = utils.getObjectProcesos(resultSet);
					proceso.setPlataforma(utils.getObjectPlataforma(resultSet));
					permiso.setProcesos(proceso);
					usuario.getPerfil().getPermisos().add(permiso);
				}while(resultSet.next());
				return usuario;
			}
			else {
				log.error("No se encontro informacion del usuarioid:: "+preparedStatement.toString());
				DetailError="No se encontro informacion del usuario";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al buscar el usuario: "+ex.getMessage(),ex);
			DetailError="Se genero un error al buscar el usuario. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return null;
	}

	@Override
	public String getDetailError() {
		return DetailError;
	}
}

