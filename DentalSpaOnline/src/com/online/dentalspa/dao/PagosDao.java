package com.online.dentalspa.dao;

import com.online.dentalspa.model.Pagos;

public interface PagosDao {

	public String getDetailError();

	public boolean createPago(Pagos pago);
}
