package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Pagosdetalle;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class PagosdetalleDaoImpl implements PagosdetalleDao {
	private Logger log = Logger.getLogger(PagosDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean createPagosdetalle(Pagosdetalle pagosdetalle) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreatePagosdetalle());
			preparedStatement.setInt(1, pagosdetalle.getPagoid());
			preparedStatement.setString(2, pagosdetalle.getPagosdetalletipoid());
			preparedStatement.setInt(3, pagosdetalle.getFormapagoid());
			preparedStatement.setBoolean(4, pagosdetalle.isPagosdetalleactivo());
			preparedStatement.setBoolean(5, pagosdetalle.isPagosdetalleliquidado());
			preparedStatement.setDouble(6, pagosdetalle.getPagosdetalleimportependiente());
			preparedStatement.setDouble(7, pagosdetalle.getPagosdetalleimporte());
			preparedStatement.setString(8, pagosdetalle.getPagosdetallefactura());
			preparedStatement.setTimestamp(9, pagosdetalle.getPagosdetallefechacreacion());
			preparedStatement.setTimestamp(10, pagosdetalle.getPagosdetallefechamodificacion());
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					pagosdetalle.setPagosdetalleid(resultSet.getInt("rs_id"));
					pagosdetalle.setPagosdetalleimportependiente(resultSet.getDouble("rs_pagosdetalleimportependiente"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(SQLException ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el Pagosdetalle",ex);
			if(ex.getSQLState().equals("P0001"))
				DetailError=ex.getLocalizedMessage();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el Pagosdetalle: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

}
