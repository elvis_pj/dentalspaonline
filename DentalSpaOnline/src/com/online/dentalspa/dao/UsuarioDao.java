package com.online.dentalspa.dao;

import javax.servlet.http.HttpServletResponse;

import com.online.dentalspa.model.Usuario;

public interface UsuarioDao {
	
	public boolean updateUsuario(Usuario usuario);

	public Usuario validateUsuario(Usuario usuario, int intentos);

	public boolean CambiaContrasenia(Usuario usuario, String usuariopwd_old, String usuariopwd_new);

	public String getDetailError();

	public void mostrarImagen(Usuario usuario, HttpServletResponse response);

	public Usuario getUsuario(int Usuarioid);
}
