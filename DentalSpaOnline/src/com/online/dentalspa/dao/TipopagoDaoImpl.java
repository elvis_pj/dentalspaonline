package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Tipopago;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class TipopagoDaoImpl implements TipopagoDao {
	private Logger log = Logger.getLogger(TipopagoDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;

	@Override
	public ArrayList<Tipopago> getTipopago() {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Tipopago> tipopago = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getListaTipopago());
			if((resultSet = preparedStatement.executeQuery()).next())
			{
				tipopago = new ArrayList<Tipopago>();
				do {
					tipopago.add(utils.getObjectTipopago(resultSet));
				}while(resultSet.next());
			}
			else
			{
				log.error("No se recuperaron registros de Tipopago :: "+preparedStatement.toString());
				DetailError = "No se recuperaron registros de Tipos de pago ";
			}
		}catch(Exception ex) {
			log.error("Se genero un error al recuperar la lista de Tipopago",ex);
			DetailError="Se genero un error al recuperar la lista de Tipos de pago:: "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return tipopago;
	}
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

}
