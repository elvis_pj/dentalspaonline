package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Parentescos;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class ParentescosDaoImpl implements ParentescosDao {
	private Logger log = Logger.getLogger(ParentescosDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;

	@Override
	public ArrayList<Parentescos> getListaParentescos() {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Parentescos> parentescos=null;
		try {
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getListaParentescos());
			if((resultSet = preparedStatement.executeQuery()).next())
			{
				parentescos = new ArrayList<Parentescos>();
				do {
					parentescos.add(utils.getObjectParentescos(resultSet));
				}while(resultSet.next());
			}
			else
			{
				log.error("No se recuperaron registros de Parentescos :: "+preparedStatement.toString());
				DetailError = "No se recuperaron registros de Parentescos";
			}
		}catch(Exception ex) {
			log.error("Se genero un error al recuperar la lista de Parentescos",ex);
			DetailError="Se genero un error al recuperar la lista de Parentescos:: "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return parentescos;
	}
	
	@Override
	public String getDetailError() {
		return DetailError;
	}
}
