package com.online.dentalspa.dao;

import com.online.dentalspa.model.Pagosdetalle;

public interface PagosdetalleDao {

	public String getDetailError();

	public boolean createPagosdetalle(Pagosdetalle pagosdetalle);
}
