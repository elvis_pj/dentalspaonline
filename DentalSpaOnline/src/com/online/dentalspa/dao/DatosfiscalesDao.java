package com.online.dentalspa.dao;

import com.online.dentalspa.model.Datosfiscales;

public interface DatosfiscalesDao {

	public String getDetailError();

	public boolean updateDatosfiscales(Datosfiscales datosfiscales);
}
