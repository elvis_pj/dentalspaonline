package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Tratamientodetalle;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class TratamientodetalleDaoImpl implements TratamientodetalleDao {
	private Logger log = Logger.getLogger(TratamientosDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean createTratamientodetalle(Tratamientodetalle tratamientodetalle) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreateTratamientodetalle());
			preparedStatement.setInt(1, tratamientodetalle.getTratamientoid());
			preparedStatement.setBoolean(2, tratamientodetalle.isTratamientodetalleactivo());
			preparedStatement.setString(3, tratamientodetalle.getTratamientodetallenombre());
			preparedStatement.setString(4, tratamientodetalle.getTratamientodetallecomentarios());
			preparedStatement.setTimestamp(5, tratamientodetalle.getTratamientodetallefechacreacion());
			preparedStatement.setTimestamp(6, tratamientodetalle.getTratamientodetallefechamodificacion());
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					tratamientodetalle.setTratamientodetalleid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el Tratamiento: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public ArrayList<Tratamientodetalle> getListaTratamientosdetalle(int tratamientoid, Connection connection) {
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Tratamientodetalle> tratamientos=null;
		try
		{
			preparedStatement = connection.prepareStatement(scripts.getListaTratamientosdetalle());
			preparedStatement.setInt(1, tratamientoid);
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				tratamientos = new ArrayList<Tratamientodetalle>();
				do {
					tratamientos.add(utils.getObjectTratamientodetalle(resultSet));
				}while(resultSet.next());
			}
			else
			{
				log.warn("No se encontraron coincidencias con el parametro de busqueda :: "+preparedStatement.toString());
				DetailError="No se encontraron tratamientos detalle";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al buscarlos Tratamientos detalle: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(null, preparedStatement, resultSet);
		}
		return tratamientos;
	}

}
