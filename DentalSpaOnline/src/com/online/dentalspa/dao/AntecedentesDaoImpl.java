package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Antecedentes;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class AntecedentesDaoImpl implements AntecedentesDao {
	private Logger log = Logger.getLogger(AntecedentesDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean updateAntecedentes(Antecedentes antecedentes) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPupdateAntecedentes());
			preparedStatement.setInt(1, antecedentes.getPacienteid());
			preparedStatement.setBoolean(2, antecedentes.isAntecedenteactivo());
			preparedStatement.setString(3, antecedentes.getAntecedentetratamientomedico());
			preparedStatement.setString(4, antecedentes.getAntecedenteembarazo());
			preparedStatement.setString(5, antecedentes.getAntecedentemedicamentos());
			preparedStatement.setString(6, antecedentes.getAntecedentegsaq());
			preparedStatement.setString(7, antecedentes.getAntecedentealergias());
			preparedStatement.setString(8, antecedentes.getAntecedenteetsvih());
			preparedStatement.setString(9, antecedentes.getAntecedentecoagulacion());
			preparedStatement.setString(10, antecedentes.getAntecedentecancer());
			preparedStatement.setString(11, antecedentes.getAntecedenteccrn());
			preparedStatement.setString(12, antecedentes.getAntecedentepdtf());
			preparedStatement.setTimestamp(13, antecedentes.getAntecedentefechacreacion());
			preparedStatement.setTimestamp(14, antecedentes.getAntecedentefechamodificacion());
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					antecedentes.setAntecedentelid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al actulizar el paciente: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

}
