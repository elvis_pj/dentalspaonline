package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Tipotratamiento;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class TipotratamientoDaoImpl implements TipotratamientoDao {
	private Logger log = Logger.getLogger(TipotratamientoDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean delete(int tipotratamientoid) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPdeleteTipotratamiento());
			preparedStatement.setInt(1, tipotratamientoid);
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(SQLException ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el Tipotratamiento: "+ex.getMessage(),ex);
			if(ex.getSQLState().equals("P0001"))
				DetailError=ex.getLocalizedMessage();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el Tipotratamiento: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public boolean create(Tipotratamiento tipotratamiento) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreateTipotratamiento());
			preparedStatement.setBoolean(1, tipotratamiento.isTipotratamientoactivo());
			preparedStatement.setString(2, tipotratamiento.getTipotratamientonombre());
			preparedStatement.setString(3, tipotratamiento.getTipotratamientodescripcion());
			preparedStatement.setTimestamp(4, tipotratamiento.getTipotratamientofechacreacion());
			preparedStatement.setTimestamp(5, tipotratamiento.getTipotratamientofechamodifcacion());
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					tipotratamiento.setTipotratamientoid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(SQLException ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el Tipotratamiento: "+ex.getMessage(),ex);
			if(ex.getSQLState().equals("P0001"))
				DetailError=ex.getLocalizedMessage();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el Tipotratamiento: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public ArrayList<Tipotratamiento> getListaTratamientos() {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Tipotratamiento> tipotratamientos=null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getListaTiposTratamientos());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				tipotratamientos = new ArrayList<Tipotratamiento>();
				do {
					tipotratamientos.add(utils.getObjectTipotratamiento(resultSet));
				}while(resultSet.next());
			}
			else
			{
				log.warn("No se encontraron coincidencias con el parametro de busqueda :: "+preparedStatement.toString());
				DetailError="No se encontraron tipos de tratamientos";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al buscar los tipos de tratamientos: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return tipotratamientos;
	}

}
