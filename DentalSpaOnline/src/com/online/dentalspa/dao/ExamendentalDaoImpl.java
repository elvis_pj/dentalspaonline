package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Examendental;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class ExamendentalDaoImpl implements ExamendentalDao {
	private Logger log = Logger.getLogger(ExamendentalDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean crearExamendental(Examendental examendental) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreateExamendental());
			preparedStatement.setInt(1, examendental.getPacienteid());
			preparedStatement.setBoolean(2, examendental.isExamendentalactivo());
			preparedStatement.setString(3, examendental.getExamendentalhigiene());
			preparedStatement.setString(4, examendental.getExamendentalencia());
			preparedStatement.setString(5, examendental.getExamendentalpisoboca());
			preparedStatement.setString(6, examendental.getExamendentallabios());
			preparedStatement.setString(7, examendental.getExamendentalcarillos());
			preparedStatement.setString(8, examendental.getExamendentalbruxismo());
			preparedStatement.setString(9, examendental.getExamendentalchasquidos());
			preparedStatement.setString(10, examendental.getExamendentallengua());
			preparedStatement.setString(11, examendental.getExamendentalpaladar());
			preparedStatement.setString(12, examendental.getExamendentalsupernumerarios());
			preparedStatement.setString(13, examendental.getExamendentalsangrado());
			preparedStatement.setString(14, examendental.getExamendentalplacasarro());
			preparedStatement.setString(15, examendental.getExamendentalalteraciones());
			preparedStatement.setString(16, examendental.getExamendentalnota());
			preparedStatement.setTimestamp(17, examendental.getExamendentalfechacreacion());
			preparedStatement.setTimestamp(18, examendental.getExamendentalfechamodificacion());
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					examendental.setExamendentalid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el examen dental: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

}
