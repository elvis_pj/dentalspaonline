package com.online.dentalspa.dao;

import java.util.ArrayList;

import com.online.dentalspa.model.Citas;

public interface CitasDao {

	public String getDetailError();

	public boolean createCita(Citas cita);

	public ArrayList<Citas> getListaCitas();
}
