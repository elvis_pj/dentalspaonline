package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Pacientes;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class PacientesDaoImpl implements PacientesDao {
	private Logger log = Logger.getLogger(PacientesDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public Pacientes getPaciente(int pacienteid) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		Pacientes paciente=null;
		ContactosemergenciaDao contactosemergenciaDao = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getInfoPacientes());
			preparedStatement.setInt(1, pacienteid);
			preparedStatement.setString(2, "");
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				contactosemergenciaDao = new ContactosemergenciaDaoImpl();
				paciente=utils.getObjectPacientes(resultSet);
				paciente.setDatosfiscales(utils.getObjectDatosfiscales(resultSet));
				paciente.setUsuario(utils.getObjectUsuario(resultSet));
				paciente.setAntecedentes(utils.getObjectAntecedentes(resultSet));
				paciente.setExamendental(utils.getObjectExamendental(resultSet));
				paciente.setContactosemergencia(contactosemergenciaDao.getListContactosemergencia(paciente.getPacienteid(), connection));
			}
			else
			{
				log.warn("No se encontraron coincidencias con el parametro de busqueda");
				DetailError="No se encontraron coincidencias con el parametro de busqueda";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al buscar el paciente: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return paciente;
	}

	@Override
	public boolean updatePaciente(Pacientes paciente) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPupdatePaciente());
			preparedStatement.setInt(1, paciente.getPacienteid());
			preparedStatement.setInt(2, paciente.getUsuarioid());
			preparedStatement.setInt(3, paciente.getDatosfiscalid());
			preparedStatement.setBoolean(4, paciente.isPacienteactivo());
			preparedStatement.setString(5, paciente.getPacientenombre());
			preparedStatement.setString(6, paciente.getPacienteapellidopaterno());
			preparedStatement.setString(7, paciente.getPacienteapellidomaterno());
			preparedStatement.setString(8, paciente.getPacientesexo());
			preparedStatement.setInt(9, paciente.getPacienteedad());
			preparedStatement.setString(10, paciente.getPacienteestadocivil());
			preparedStatement.setString(11, paciente.getPacientelugarnacimiento());
			preparedStatement.setDate(12, paciente.getPacientefechanacimiento());
			preparedStatement.setString(13, paciente.getPacientecurp());
			preparedStatement.setString(14, paciente.getPacientedomicilio());
			preparedStatement.setString(15, paciente.getPacienteciudad());
			preparedStatement.setString(16, paciente.getPacientecodigopostal());
			preparedStatement.setInt(17, paciente.getEstadoid());
			preparedStatement.setString(18, paciente.getPacientecorreo());
			preparedStatement.setString(19, paciente.getPacientetelefono());
			preparedStatement.setString(20, paciente.getPacientecelular());
			preparedStatement.setTimestamp(21, paciente.getPacientefechacreacion());
			preparedStatement.setTimestamp(22, paciente.getPacientefechamodificacion());
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					paciente.setPacienteid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al actualizar el paciente: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public ArrayList<Pacientes> doBuscarPaciente(String parametroBusqueda) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Pacientes> pacientes=null;
		ContactosemergenciaDao contactosemergenciaDao = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getInfoPacientes());
			preparedStatement.setInt(1, (utils.isDigito(parametroBusqueda) ? Integer.parseInt(parametroBusqueda) : -1));
			preparedStatement.setString(2, "%"+parametroBusqueda+"%");
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				contactosemergenciaDao = new ContactosemergenciaDaoImpl();
				pacientes = new ArrayList<Pacientes>();
				do {
					Pacientes paciente=utils.getObjectPacientes(resultSet);
					paciente.setDatosfiscales(utils.getObjectDatosfiscales(resultSet));
					paciente.setUsuario(utils.getObjectUsuario(resultSet));
					paciente.setAntecedentes(utils.getObjectAntecedentes(resultSet));
					paciente.setExamendental(utils.getObjectExamendental(resultSet));
					paciente.setContactosemergencia(contactosemergenciaDao.getListContactosemergencia(paciente.getPacienteid(), connection));
					pacientes.add(paciente);
				}while(resultSet.next());
			}
			else
			{
				log.warn("No se encontraron coincidencias con el parametro de busqueda");
				DetailError="No se encontraron coincidencias con el parametro de busqueda";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al buscarlos pacientes: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return pacientes;
	}

	@Override
	public boolean createPaciente(Pacientes paciente) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreatePaciente());
			preparedStatement.setInt(1, paciente.getUsuarioid());
			preparedStatement.setInt(2, paciente.getDatosfiscalid());
			preparedStatement.setBoolean(3, paciente.isPacienteactivo());
			preparedStatement.setString(4, paciente.getPacientenombre());
			preparedStatement.setString(5, paciente.getPacienteapellidopaterno());
			preparedStatement.setString(6, paciente.getPacienteapellidomaterno());
			preparedStatement.setString(7, paciente.getPacientesexo());
			preparedStatement.setInt(8, paciente.getPacienteedad());
			preparedStatement.setString(9, paciente.getPacienteestadocivil());
			preparedStatement.setString(10, paciente.getPacientelugarnacimiento());
			preparedStatement.setDate(11, paciente.getPacientefechanacimiento());
			preparedStatement.setString(12, paciente.getPacientecurp());
			preparedStatement.setString(13, paciente.getPacientedomicilio());
			preparedStatement.setString(14, paciente.getPacienteciudad());
			preparedStatement.setString(15, paciente.getPacientecodigopostal());
			preparedStatement.setInt(16, paciente.getEstadoid());
			preparedStatement.setString(17, paciente.getPacientecorreo());
			preparedStatement.setString(18, paciente.getPacientetelefono());
			preparedStatement.setString(19, paciente.getPacientecelular());
			preparedStatement.setTimestamp(20, paciente.getPacientefechacreacion());
			preparedStatement.setTimestamp(21, paciente.getPacientefechamodificacion());log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					paciente.setPacienteid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al crear el nuevo paciente: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}
}
