package com.online.dentalspa.dao;

import com.online.dentalspa.model.Antecedentes;

public interface AntecedentesDao {

	public String getDetailError();

	public boolean updateAntecedentes(Antecedentes antecedentes);
}
