package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Contactosemergencia;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class ContactosemergenciaDaoImpl implements ContactosemergenciaDao {
	private Logger log = Logger.getLogger(ContactosemergenciaDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;

	@Override
	public boolean elminarContactoEmergencia(int pacienteid, int contactosemergenciaid) {
		Connection connection=null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;

		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getEliminarContactoEmergencia());
			preparedStatement.setBoolean(1, false);
			preparedStatement.setInt(2, pacienteid);
			preparedStatement.setInt(3, contactosemergenciaid);
			if(preparedStatement.executeUpdate()==1)
			{
				connection.commit();
				return true;
			}
			else
			{
				log.info("No se logro eliminar el contacto:: "+preparedStatement.toString());
				DetailError="No se logro eliminar el contacto";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al eliminar el contacto de emergencia: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public boolean agregaContactoEmergencia(Contactosemergencia contacto) {
		Connection connection=null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;

		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreateContactoEmergencia());
			preparedStatement.setInt(1, contacto.getPacienteid());
			preparedStatement.setInt(2, contacto.getParentescoid());
			preparedStatement.setString(3, contacto.getContactosemergencianombre());
			preparedStatement.setString(4, contacto.getContactosemergenciatelefono());
			preparedStatement.setString(5, contacto.getContactosemergenciacelular());
			preparedStatement.setString(6, contacto.getContactosemergenciacorreo());
			preparedStatement.setTimestamp(7, contacto.getContactosemergenciafechacreacion());
			preparedStatement.setTimestamp(8, contacto.getContactosemergenciafechamodificacion());
			preparedStatement.setBoolean(9, contacto.isContactosemergenciaactivo());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					contacto.setContactosemergenciaid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el contactos de emergencia: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public ArrayList<Contactosemergencia> getListContactosemergencia(int pacienteid, Connection connection) {
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Contactosemergencia> lista=null;

		try
		{
			preparedStatement = connection.prepareStatement(scripts.getContactosemergencia());
			preparedStatement.setInt(1, pacienteid);
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				lista = new ArrayList<Contactosemergencia>();
				do {
					Contactosemergencia contacto = utils.getObjectContactosemergencia(resultSet);
					contacto.setParentescos(utils.getObjectParentescos(resultSet));
					lista.add(contacto);
				}while(resultSet.next());
			}
			else
			{
				log.warn("No se encontraron contactos de emergencia");
				DetailError="No se encontraron contactos de emergencia";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al buscar los contactos de emergencia: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(null, preparedStatement, resultSet);
		}
		return lista;
	}
	
	@Override
	public String getDetailError() {
		return DetailError;
	}
}
