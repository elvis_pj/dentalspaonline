package com.online.dentalspa.dao;

import java.util.ArrayList;

import com.online.dentalspa.model.Tipopago;

public interface TipopagoDao {

	public ArrayList<Tipopago> getTipopago();
	
	public String getDetailError();
 
}
