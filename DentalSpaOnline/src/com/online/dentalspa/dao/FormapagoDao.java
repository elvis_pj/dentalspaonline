package com.online.dentalspa.dao;

import java.util.ArrayList;
import com.online.dentalspa.model.Formapago;

public interface FormapagoDao {

	public ArrayList<Formapago> getFormapago();
	
	public String getDetailError();

}
