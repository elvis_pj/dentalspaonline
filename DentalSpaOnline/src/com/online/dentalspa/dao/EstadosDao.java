package com.online.dentalspa.dao;

import java.util.ArrayList;

import com.online.dentalspa.model.Estados;

public interface EstadosDao {

	public String getDetailError();

	public ArrayList<Estados> getEstados();

}
