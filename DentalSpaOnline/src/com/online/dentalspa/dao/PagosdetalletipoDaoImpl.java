package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Pagosdetalletipo;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class PagosdetalletipoDaoImpl implements PagosdetalletipoDao {
	private Logger log = Logger.getLogger(PagosdetalletipoDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public ArrayList<Pagosdetalletipo> getPagosdetalletipo() {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Pagosdetalletipo> pagosdetalletipo = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getListaPagosdetalletipo());
			if((resultSet = preparedStatement.executeQuery()).next())
			{
				pagosdetalletipo = new ArrayList<Pagosdetalletipo>();
				do {
					pagosdetalletipo.add(utils.getObjectPagosdetalletipo(resultSet));
				}while(resultSet.next());
			}
			else
			{
				log.error("No se recuperaron registros de Pagosdetalletipo :: "+preparedStatement.toString());
				DetailError = "No se recuperaron registros de Tipos de pago";
			}
		}catch(Exception ex) {
			log.error("Se genero un error al recuperar la lista de Pagosdetalletipo",ex);
			DetailError="Se genero un error al recuperar la lista de Tipos de pago:: "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return pagosdetalletipo;
	}
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

}
