package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Datosfiscales;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class DatosfiscalesDaoImpl implements DatosfiscalesDao {
	private Logger log = Logger.getLogger(DatosfiscalesDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean updateDatosfiscales(Datosfiscales datosfiscales) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPupdateDatosfiscales());
			preparedStatement.setInt(1, datosfiscales.getPaciente().getPacienteid());
			preparedStatement.setInt(2, datosfiscales.getDatofiscalid());
			preparedStatement.setString(3, datosfiscales.getDatofiscalnombre());
			preparedStatement.setString(4, datosfiscales.getDatofiscalrfc());
			preparedStatement.setString(5, datosfiscales.getDatofiscaldireccion());
			preparedStatement.setString(6, datosfiscales.getDatofiscalcorreo());
			preparedStatement.setTimestamp(7, datosfiscales.getDatofiscalfechacreacion());
			preparedStatement.setTimestamp(8, datosfiscales.getDatofiscalfechamodificacion());
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					datosfiscales.setDatofiscalid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al actulizar el paciente: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

}
