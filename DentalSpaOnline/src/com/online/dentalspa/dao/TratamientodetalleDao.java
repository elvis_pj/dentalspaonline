package com.online.dentalspa.dao;

import java.sql.Connection;
import java.util.ArrayList;

import com.online.dentalspa.model.Tratamientodetalle;

public interface TratamientodetalleDao {

	public String getDetailError();

	public ArrayList<Tratamientodetalle> getListaTratamientosdetalle(int tratamientoid, Connection connection);

	public boolean createTratamientodetalle(Tratamientodetalle tratamientodetalle);
}
