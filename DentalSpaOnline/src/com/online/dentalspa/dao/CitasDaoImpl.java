package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Citas;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class CitasDaoImpl implements CitasDao {
	private Logger log = Logger.getLogger(CitasDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public ArrayList<Citas> getListaCitas() {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Citas> listacitas=null;
		try
		{
			listacitas = new ArrayList<Citas>();
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getListaCitas());
			preparedStatement.setInt(1, 1);
			if((resultSet = preparedStatement.executeQuery()).next())
			{
				do {
					Citas citas = utils.getObjectCitas(resultSet);
					citas.setUsuario(utils.getObjectUsuario(resultSet));
					listacitas.add(citas);
				}while(resultSet.next());
			}
			else
			{
				log.error("No se encontraron citas :: "+preparedStatement.toString());
				DetailError="No se encontraron citas";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al recuperar las Citas: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return listacitas;
	}

	@Override
	public boolean createCita(Citas cita) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreateCita());
			preparedStatement.setInt(1, cita.getUsuarioid());
			preparedStatement.setInt(2, cita.getPacienteid());
			preparedStatement.setInt(3, cita.getCitaestatusid());
			preparedStatement.setString(4, cita.getCitanombre());
			preparedStatement.setString(5, cita.getCitacomentarios());
			preparedStatement.setTimestamp(6, cita.getCitafechainicio());
			preparedStatement.setTimestamp(7, cita.getCitafechafin());
			preparedStatement.setTimestamp(8, cita.getCitafechacreacion());
			preparedStatement.setTimestamp(9, cita.getCitafechamodificacion());
			preparedStatement.setInt(10, cita.getTratamientoid());
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					cita.setCitaid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el Citas: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

}
