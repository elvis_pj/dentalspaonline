package com.online.dentalspa.dao;

import java.util.ArrayList;

import com.online.dentalspa.model.Pacientes;

public interface PacientesDao {

	public String getDetailError();

	public boolean createPaciente(Pacientes paciente);

	public ArrayList<Pacientes> doBuscarPaciente(String string);

	public boolean updatePaciente(Pacientes paciente);

	public Pacientes getPaciente(int pacienteid);

}
