package com.online.dentalspa.dao;

import java.util.ArrayList;

import com.online.dentalspa.model.Parentescos;

public interface ParentescosDao {
	
	public String getDetailError();

	public ArrayList<Parentescos> getListaParentescos();
}
