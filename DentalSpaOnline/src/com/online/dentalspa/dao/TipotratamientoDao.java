package com.online.dentalspa.dao;

import java.util.ArrayList;

import com.online.dentalspa.model.Tipotratamiento;

public interface TipotratamientoDao {

	public String getDetailError();

	public ArrayList<Tipotratamiento> getListaTratamientos();

	public boolean create(Tipotratamiento tipotratamiento);

	public boolean delete(int tipotratamientoid);
}
