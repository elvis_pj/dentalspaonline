package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Pagos;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class PagosDaoImpl implements PagosDao {
	private Logger log = Logger.getLogger(PagosDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean createPago(Pagos pagos) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(scripts.getSPcreatePago());
			preparedStatement.setInt(1, pagos.getTratamientoid());
			preparedStatement.setInt(2, pagos.getTipopagoid());
			preparedStatement.setBoolean(3, pagos.isPagoactivo());
			preparedStatement.setBoolean(4, pagos.isPagosaldado());
			preparedStatement.setDouble(5, pagos.getPagoimportetotal());
			preparedStatement.setDouble(6, pagos.getPagoimportependiente());
			preparedStatement.setString(7, pagos.getPagocomentarios());
			preparedStatement.setTimestamp(8, pagos.getPagofechacreacion());
			preparedStatement.setTimestamp(9, pagos.getPagofechamodificacion());
			log.info(preparedStatement.toString());
			if((resultSet=preparedStatement.executeQuery()).next())
			{
				log.info("Respuesta del SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					pagos.setPagoid(resultSet.getInt("rs_id"));
					return true;
				}
				DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta, por favor intentelo nuevamente";
			}
			connection.rollback();
		}catch(SQLException ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el pago: "+ex.getMessage(),ex);
			if(ex.getSQLState().equals("P0001"))
				DetailError=ex.getLocalizedMessage();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar el pago: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

}
