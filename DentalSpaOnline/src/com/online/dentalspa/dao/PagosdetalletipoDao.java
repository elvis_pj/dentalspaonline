package com.online.dentalspa.dao;

import java.util.ArrayList;

import com.online.dentalspa.model.Pagosdetalletipo;

public interface PagosdetalletipoDao {

	public ArrayList<Pagosdetalletipo> getPagosdetalletipo();

	public String getDetailError();
}
