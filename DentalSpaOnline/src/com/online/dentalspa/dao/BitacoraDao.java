package com.online.dentalspa.dao;

import com.online.dentalspa.model.Bitacora;

public interface BitacoraDao {

	public String getDetailError();

	public boolean createBitacora(Bitacora bitacora);

	public boolean updateBitacora(Bitacora bitacora);

}
