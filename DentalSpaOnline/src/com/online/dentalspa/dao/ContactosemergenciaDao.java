package com.online.dentalspa.dao;

import java.sql.Connection;
import java.util.ArrayList;

import com.online.dentalspa.model.Contactosemergencia;

public interface ContactosemergenciaDao {
	
	public String getDetailError();

	public ArrayList<Contactosemergencia> getListContactosemergencia(int pacienteid, Connection connection);

	public boolean agregaContactoEmergencia(Contactosemergencia contacto);

	public boolean elminarContactoEmergencia(int pacienteid, int contactosemergenciaid);
}
