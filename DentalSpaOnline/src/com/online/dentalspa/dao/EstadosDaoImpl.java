package com.online.dentalspa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.online.dentalspa.model.Estados;
import com.online.dentalspa.utils.Scripts;
import com.online.dentalspa.utils.Utils;

public class EstadosDaoImpl implements EstadosDao {
	private Logger log = Logger.getLogger(EstadosDaoImpl.class);
	private Utils utils=new Utils();
	private Scripts scripts = new Scripts();
	private String DetailError;

	@Override
	public ArrayList<Estados> getEstados() {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		ArrayList<Estados> estados = null;
		try
		{
			connection = utils.getConnection("db");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(scripts.getListaEstados());
			if((resultSet = preparedStatement.executeQuery()).next())
			{
				estados = new ArrayList<Estados>();
				do {
					estados.add(utils.getObjectEstados(resultSet));
				}while(resultSet.next());
			}
			else
			{
				log.error("No se recuperaron registros de estados :: "+preparedStatement.toString());
				DetailError = "No se recuperaron registros de estados";
			}
		}catch(Exception ex) {
			log.error("Se genero un error al recuperar la lista de estados",ex);
			DetailError="Se genero un error al recuperar la lista de estados:: "+ex.getMessage();
			return null;
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return estados;
	}
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

}
