package com.online.dentalspa.controller;

import java.sql.Timestamp;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.online.dentalspa.dao.CitasDao;
import com.online.dentalspa.dao.CitasDaoImpl;
import com.online.dentalspa.model.AjaxServiceResponse;
import com.online.dentalspa.model.Bitacora;
import com.online.dentalspa.model.Citas;
import com.online.dentalspa.utils.Utils;

@Controller
@RequestMapping("/citas")
public class CitasController {
	private Logger log = Logger.getLogger(CitasController.class);
	private Utils utils=new Utils();
	
	@RequestMapping("/")
	public String pacientes(Model theModel, HttpServletRequest request, 
			@RequestParam(value="view_mensaje_jsp", required=false) String view_mensaje_jsp, 
			@RequestParam(value="view_error_jsp", required=false) String view_error_jsp)
	{
		log.info("Inicia citas");
		Bitacora bitacora = utils.isSessionActive(request);
		if(bitacora==null)
		{
			return "redirect:/log/out?mensaje="+utils.getDetailError();
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		theModel.addAttribute("view_mensaje_jsp", view_mensaje_jsp);
		theModel.addAttribute("view_error_jsp", view_error_jsp);
		return "citas";
	}

	@RequestMapping("listaCitas")
	public @ResponseBody String listaCitas(HttpServletRequest request, Model theModel)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			CitasDao citasDao = new CitasDaoImpl();
			ArrayList<Citas> listaCitas = citasDao.getListaCitas();
			if(listaCitas!=null)
			{
				response.set_State("true");
				response.set_Message("Lista de citas");
				response.set_ObjectJson(new Gson().toJson(listaCitas));
			}
			else
			{
				response.set_State("false");
				response.set_Message(citasDao.getDetailError());
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion("");
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("crear")
	public @ResponseBody String guardarCambios(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			
			switch(jsonObject.get("proceso").getAsString())
			{
				case "citas":
					if(crearCita(bitacora, jsonObject.get("ParametersRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se guardo la informacion exitosamente");
					}
				break;
				default:
					response.set_State("false");
					response.set_Message("Tipo de formulario no soportado.");
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	private boolean crearCita(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		CitasDao citasDao = new CitasDaoImpl();
		Citas cita = getParametersRequestCitas(parametersRequest, bitacora);
		if(!citasDao.createCita(cita))
		{
			response.set_State("false");
			response.set_Message(citasDao.getDetailError());
			return false;
		}
		return true;
	}

	private Citas getParametersRequestCitas(String parametersRequest, Bitacora bitacora)throws Exception {
		Citas cita=null;
		try {
			JsonObject jsonObject = new Gson().fromJson(parametersRequest, JsonObject.class);
			cita= new Citas();
			cita.setCitaid(jsonObject.get("rq_citaid")!=null ? jsonObject.get("rq_citaid").getAsInt() : -1);
			cita.setUsuarioid(bitacora.getUsuarioid());
			cita.setPacienteid(jsonObject.get("rq_pacienteid")!=null ? jsonObject.get("rq_pacienteid").getAsInt() : -1);
			cita.setCitaestatusid(jsonObject.get("rq_citaestatusid")!=null ? jsonObject.get("rq_citaestatusid").getAsInt() : 1);
			cita.setCitanombre(jsonObject.get("rq_citanombre").getAsString());
			cita.setCitacomentarios(jsonObject.get("rq_citacomentarios").getAsString());
			cita.setCitafechainicio(Timestamp.valueOf(jsonObject.get("rq_citafechainicio").getAsString()));
			cita.setCitafechafin(Timestamp.valueOf(jsonObject.get("rq_citafechafin").getAsString()));
			cita.setCitafechacreacion(bitacora.getBitacorafecha());
			cita.setCitafechamodificacion(bitacora.getBitacorafecha());
			cita.setTratamientoid(jsonObject.get("rq_tratamientoid")!=null ? jsonObject.get("rq_tratamientoid").getAsInt() : -1);
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Se genero un error al recuperar la informacion del formulario:"+ex.getMessage());
		}
		return cita;
	}

}
