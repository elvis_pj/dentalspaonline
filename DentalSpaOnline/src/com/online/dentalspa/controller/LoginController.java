package com.online.dentalspa.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.online.dentalspa.dao.UsuarioDao;
import com.online.dentalspa.dao.UsuarioDaoImpl;
import com.online.dentalspa.model.Usuario;
import com.online.dentalspa.utils.Utils;

@Controller
@RequestMapping("/log")
public class LoginController {
	private Logger log = Logger.getLogger(LoginController.class);
	
	@RequestMapping("/in")
	public String logIn(Model theModel, HttpServletRequest request)
	{
		log.info("Inicia login");
		request.getSession().invalidate();
		theModel.addAttribute("usuario", new Usuario());
		theModel.addAttribute("view_mensaje_jsp","");
		theModel.addAttribute("view_error_jsp","");
		return "login";
	}
	
	@RequestMapping("/out")
	public String logout(Model theModel, HttpServletRequest request, @RequestParam("mensaje") String mensaje)
	{
		log.info("LogOut ["+mensaje+"]");
		Usuario user = (Usuario) request.getSession().getAttribute("usuariosession");
		if(user!=null)
		{
			log.info("Finaliza Sesion ["+user.getUsuariocorreo()+"]");
			request.getSession().removeAttribute("usuariosession");
			request.getSession().invalidate();
		}
		theModel.addAttribute("usuario", new Usuario());
		theModel.addAttribute("view_mensaje_jsp",mensaje);
		theModel.addAttribute("view_error_jsp","");
		return "login";
	}
	
	@RequestMapping("/loginProcess")
	public String viewLoginProcess(@ModelAttribute("usuario") Usuario usuario,HttpServletRequest request, Model theModel)
	{
		log.info("Login Process");
		String vista="login",mensaje="",error="";
		int numeroIntentos=0;
		if(usuario!=null)
		{
			log.info(usuario.toString());
			HttpSession mySession = (HttpSession) request.getSession(true);
			if(mySession.getAttribute("intentos_erroneos")!=null)
				numeroIntentos = (int) mySession.getAttribute("intentos_erroneos");
			UsuarioDao usuarioDao = new UsuarioDaoImpl();
			usuario = usuarioDao.validateUsuario(usuario,numeroIntentos);
			if(usuario!=null)
			{
				request.getSession().removeAttribute("intentos_erroneos");
				mySession.setAttribute("usuariosession", usuario);
				mySession.setAttribute("globalPath", Utils.pathFiles);
				vista="redirect:/dashboard/";
				mensaje="Bienvenido "+usuario.getUsuarionombre();
			}
			else
			{
				error=usuarioDao.getDetailError();
				mySession.setAttribute("intentos_erroneos", (error.equals("Contraseña incorrecta") ? numeroIntentos+1 : numeroIntentos));
			}
		}
		else
		{
			error="No se logro recuperar la informacion del formulario, por favor intentelo nuevamente.";
		}

		theModel.addAttribute("view_mensaje_jsp",mensaje);
		theModel.addAttribute("view_error_jsp",error);
		return vista;
	}
}
