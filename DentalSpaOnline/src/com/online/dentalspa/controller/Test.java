package com.online.dentalspa.controller;

import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/test")
public class Test {
	private Logger log = Logger.getLogger(Test.class);

	@RequestMapping("/")
	public String logIn(Model theModel, HttpServletRequest request) throws Exception
	{
		String cadena="Elvis";
		log.info("entro in ...: "+cadena);
		String encript = encript(cadena);
		log.info("encript["+encript+"]");
		String rest = decrypt(encript); 
		log.info("Resultado :: "+rest);
		return "test";
	}
	
	@RequestMapping("/testForm")
	public String testForm(Model theModel, HttpServletRequest request,
			@RequestParam("fileUpload") MultipartFile file)
	{
		log.info("entro");
		log.info("recibio el archivo.");
		log.info("Content type" + file.getContentType());
		log.info("Name" + file.getName());
		log.info("Original Filename" + file.getOriginalFilename());
		return "test";
	}
	
//	public byte[] encrypt(byte[] data, IvParameterSpec zeroIv, SecretKeySpec keySpec) {
//		  try {
//			  log.info("time cost on [aes encrypt]: data length=" + data.length);
//		    Cipher cipher = Cipher.getInstance(KEY_ALGORITHM_PADDING);
//		    cipher.init(Cipher.ENCRYPT_MODE, keySpec, zeroIv);
//		    return cipher.doFinal(data);
//		  } catch (Exception e) {
//		    LOGGER.error("AES encrypt ex, iv={}, key={}",
//		        Arrays.toString(zeroIv.getIV()),
//		        Arrays.toString(keySpec.getEncoded()), e);
//		    throw new CryptoException("AES encrypt ex", e);
//		  } finally {
//		    Profiler.release();
//		  }
//		}
	
	private String ENCRYPT_KEY="clave-compartida-no-reveloar-nunca";

	private String encript(String valueToEnc) throws Exception {	
//		Key aesKey = new SecretKeySpec(ENCRYPT_KEY.getBytes(), "AES");
//
//		Cipher cipher = Cipher.getInstance("AES");
//		cipher.init(Cipher.ENCRYPT_MODE, aesKey);
//
//		byte[] encrypted = cipher.doFinal(text.getBytes());
//			
//		return new String(Base64.getEncoder().encode(encrypted));
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);

		KeySpec spec = new PBEKeySpec("password".toCharArray(), salt, 65536, 256); // AES-256
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] key = f.generateSecret(spec).getEncoded();
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");

		byte[] ivBytes = new byte[16];
		random.nextBytes(ivBytes);
		IvParameterSpec iv = new IvParameterSpec(ivBytes);

		Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		c.init(Cipher.ENCRYPT_MODE, keySpec, iv);
		byte[] encValue = c.doFinal(valueToEnc.getBytes());

		byte[] finalCiphertext = new byte[encValue.length+2*16];
		System.arraycopy(ivBytes, 0, finalCiphertext, 0, 16);
		System.arraycopy(salt, 0, finalCiphertext, 16, 16);
		System.arraycopy(encValue, 0, finalCiphertext, 32, encValue.length);

		return new String(finalCiphertext);
		}

	private String decrypt(String encrypted) throws Exception {
		byte[] encryptedBytes=Base64.getDecoder().decode(encrypted.replace("\n", "")); //Base64.decode( encrypted.replace("\n", "") );
			
		Key aesKey = new SecretKeySpec(ENCRYPT_KEY.getBytes(), "AES");

		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, aesKey);

		String decrypted = new String(cipher.doFinal(encryptedBytes));
	        
		return decrypted;
		}
	
}
