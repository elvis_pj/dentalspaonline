package com.online.dentalspa.controller;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.online.dentalspa.dao.UsuarioDao;
import com.online.dentalspa.dao.UsuarioDaoImpl;
import com.online.dentalspa.model.AjaxServiceResponse;
import com.online.dentalspa.model.Bitacora;
import com.online.dentalspa.model.Usuario;
import com.online.dentalspa.utils.Utils;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {
	private Logger log = Logger.getLogger(DashboardController.class);
	private Utils utils=new Utils();

	@RequestMapping("/")
	public String view(HttpServletRequest request, Model theModel, 
			@RequestParam(value="view_mensaje_jsp", required=false) String view_mensaje_jsp, 
			@RequestParam(value="view_error_jsp", required=false) String view_error_jsp)
	{
		log.info("Inicia");
		Bitacora bitacora = utils.isSessionActive(request);
		if(bitacora==null)
		{
			return "redirect:/log/out?mensaje="+utils.getDetailError();
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		theModel.addAttribute("view_mensaje_jsp", view_mensaje_jsp);
		theModel.addAttribute("view_error_jsp", view_error_jsp);
		
		return "dashboard";
	}
	
	@RequestMapping("/perfil/")
	public String viewPerfil(HttpServletRequest request, Model theModel, 
			@RequestParam(value="view_mensaje_jsp", required=false) String view_mensaje_jsp, 
			@RequestParam(value="view_error_jsp", required=false) String view_error_jsp)
	{
		log.info("Inicia");
		Bitacora bitacora = utils.isSessionActive(request);
		if(bitacora==null)
		{
			return "redirect:/log/out?mensaje="+utils.getDetailError();
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		theModel.addAttribute("view_mensaje_jsp", view_mensaje_jsp);
		theModel.addAttribute("view_error_jsp", view_error_jsp);
		
		return "perfil";
	}

	@RequestMapping(value="/perfil/cambiaFotoPerfil", method=RequestMethod.POST)
	public @ResponseBody String cambiaFotoPerfil(HttpServletRequest request, Model theModel,
			@RequestParam("customFileProfilePicture") MultipartFile file) {
		log.info("Cambia Foto de perfil.");
		AjaxServiceResponse response = new AjaxServiceResponse();
		UsuarioDao usuarioDao = null;
		try
		{
			if(!utils.sessionActive(request))
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			Usuario usuario = (Usuario) request.getSession().getAttribute("usuariosession");
			usuario.setUsuarioimage(file.getInputStream());
			usuario.setUsuariofechamodificacion(new Timestamp(System.currentTimeMillis()));
			usuarioDao = new UsuarioDaoImpl();
			if(!usuarioDao.updateUsuario(usuario))
			{
				response.set_State("false");
				response.set_Message("No se logro actualizar la imagen. "+usuarioDao.getDetailError());
			}
			else
			{
				usuario=usuarioDao.getUsuario(usuario.getUsuarioid());
				response.set_State("true");
				response.set_Message("Foto Actualizada");
				request.getSession().setAttribute("usuariosession", usuario);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			log.info(new Gson().toJson(response));
		}
		return new Gson().toJson(response);
	}

	@RequestMapping(value="/perfil/obtieneFotoPerfil", method=RequestMethod.GET)
	public void getImage(HttpServletRequest request, HttpServletResponse response) {
		try {
			Usuario usuario =null;
			if(!utils.sessionActive(request))
				return;
			response.setContentType("image/*");
			usuario = (Usuario) request.getSession().getAttribute("usuariosession");
			utils.recuperaImagen(usuario.getUsuarioimage(), response);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	@RequestMapping("/perfil/cambiaContrasenia")
	public @ResponseBody String cambiaContrasenia(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest) {
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		UsuarioDao usuarioDao=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			usuarioDao = new UsuarioDaoImpl();

			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			Usuario usuario = (Usuario) request.getSession().getAttribute("usuariosession");
			if(usuarioDao.CambiaContrasenia(usuario, jsonObject.get("rq_usuariopwd_old").getAsString(), jsonObject.get("rq_usuariopwd_new").getAsString()))
			{
				request.getSession().setAttribute("usuariosession",usuario);
				response.set_State("true");
				response.set_Message("Contrasenia actualizada");
			}
			else
			{
				response.set_State("false");
				response.set_Message(usuarioDao.getDetailError());
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}log.info(new Gson().toJson(response));
		}
		return new Gson().toJson(response);
	}
}
