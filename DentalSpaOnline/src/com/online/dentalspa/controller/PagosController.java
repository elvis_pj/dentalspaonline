package com.online.dentalspa.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.online.dentalspa.dao.PagosDao;
import com.online.dentalspa.dao.PagosDaoImpl;
import com.online.dentalspa.dao.PagosdetalleDao;
import com.online.dentalspa.dao.PagosdetalleDaoImpl;
import com.online.dentalspa.dao.TratamientosDao;
import com.online.dentalspa.dao.TratamientosDaoImpl;
import com.online.dentalspa.model.AjaxServiceResponse;
import com.online.dentalspa.model.Bitacora;
import com.online.dentalspa.model.Pagos;
import com.online.dentalspa.model.Pagosdetalle;
import com.online.dentalspa.model.Tratamientos;
import com.online.dentalspa.utils.Utils;

@Controller
@RequestMapping("/pagos")
public class PagosController {
	private Logger log = Logger.getLogger(PagosController.class);
	private Utils utils=new Utils();
	
	@RequestMapping("/")
	public String pacientes(Model theModel, HttpServletRequest request,  
			@RequestParam(value="rq_paciente", required=false) String pacienteid, 
			@RequestParam(value="view_mensaje_jsp", required=false) String view_mensaje_jsp, 
			@RequestParam(value="view_error_jsp", required=false) String view_error_jsp)
	{
		log.info("Inicia pagos");
		Bitacora bitacora = utils.isSessionActive(request);
		if(bitacora==null)
		{
			return "redirect:/log/out?mensaje="+utils.getDetailError();
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		theModel.addAttribute("view_mensaje_jsp", view_mensaje_jsp);
		theModel.addAttribute("view_error_jsp", view_error_jsp);
		theModel.addAttribute("rq_pacienteid", pacienteid);
		return "pagos";
	}

	@RequestMapping("guardarPago")
	public @ResponseBody String guardarPago(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			
			switch(jsonObject.get("proceso").getAsString())
			{
				case "costoTratamiento":
					if(guardarCostoTretamiento(bitacora, jsonObject.get("ParametersRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se guardo la informacion exitosamente");
					}
				break;
				case "detallePago":
					if(guardarDetallePago(bitacora, jsonObject.get("ParametersRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se guardo la informacion exitosamente");
					}
				break;
				default:
					response.set_State("false");
					response.set_Message("Tipo de formulario no soportado.");
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
			log.info(response.toString());
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("listaPagosTratamientos")
	public @ResponseBody String listaPagosTratamientos(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			TratamientosDao tratamientosDao = new TratamientosDaoImpl();
			ArrayList<Tratamientos> lista = tratamientosDao.getListTratamientosPagos(jsonObject.get("rq_pacienteid").getAsInt());
			if(lista!=null && lista.size()>0)
			{
				response.set_State("true");
				response.set_Message("Busqueda exitosa");
				response.set_ObjectJson(new Gson().toJson(lista));
			}
			else
			{
				response.set_State("false");
				response.set_Message(tratamientosDao.getDetailError());
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}
	
	private boolean guardarCostoTretamiento(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		PagosDao pagosDao = new PagosDaoImpl();
		Pagos pagos = new Gson().fromJson(parametersRequest, Pagos.class);
		pagos.setPagoactivo(true);
		pagos.setPagoimportependiente(pagos.getPagoimportetotal());
		pagos.setPagofechacreacion(bitacora.getBitacorafecha());
		pagos.setPagofechamodificacion(bitacora.getBitacorafecha());
		log.info(pagos.toString());
		if(!pagosDao.createPago(pagos))
		{
			response.set_State("false");
			response.set_Message(pagosDao.getDetailError());
			return false;
		}
		response.set_ObjectJson(new Gson().toJson(pagos));
		return true;
	}

	private boolean guardarDetallePago(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		PagosdetalleDao pagosdetalleDao = new PagosdetalleDaoImpl();
		Pagosdetalle pagosdetalle = new Gson().fromJson(parametersRequest, Pagosdetalle.class);
		pagosdetalle.setPagosdetalleactivo(true);
		pagosdetalle.setPagosdetalleliquidado(false);
		pagosdetalle.setPagosdetallefactura("");
		pagosdetalle.setPagosdetallefechacreacion(bitacora.getBitacorafecha());
		pagosdetalle.setPagosdetallefechamodificacion(bitacora.getBitacorafecha());
		pagosdetalle.setPagosdetalleimportependiente(pagosdetalle.getPagosdetalleimporte());
		log.info(pagosdetalle.toString());
		if(!pagosdetalleDao.createPagosdetalle(pagosdetalle))
		{
			response.set_State("false");
			response.set_Message(pagosdetalleDao.getDetailError());
			return false;
		}
		response.set_ObjectJson(new Gson().toJson(pagosdetalle));
		return true;
	}

}
