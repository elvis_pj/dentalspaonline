package com.online.dentalspa.controller;

import java.sql.Date;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.online.dentalspa.dao.AntecedentesDao;
import com.online.dentalspa.dao.AntecedentesDaoImpl;
import com.online.dentalspa.dao.ContactosemergenciaDao;
import com.online.dentalspa.dao.ContactosemergenciaDaoImpl;
import com.online.dentalspa.dao.DatosfiscalesDao;
import com.online.dentalspa.dao.DatosfiscalesDaoImpl;
import com.online.dentalspa.dao.PacientesDao;
import com.online.dentalspa.dao.PacientesDaoImpl;
import com.online.dentalspa.model.AjaxServiceResponse;
import com.online.dentalspa.model.Antecedentes;
import com.online.dentalspa.model.Bitacora;
import com.online.dentalspa.model.Contactosemergencia;
import com.online.dentalspa.model.Datosfiscales;
import com.online.dentalspa.model.Pacientes;
import com.online.dentalspa.utils.Utils;

@Controller
@RequestMapping("/pacientes")
public class PacientesController {
	private Logger log = Logger.getLogger(PacientesController.class);
	private Utils utils=new Utils();
	
	@RequestMapping("/")
	public String pacientes(Model theModel, HttpServletRequest request, 
			@RequestParam(value="view_mensaje_jsp", required=false) String view_mensaje_jsp, 
			@RequestParam(value="view_error_jsp", required=false) String view_error_jsp)
	{
		log.info("Inicia pacientes");
		Bitacora bitacora = utils.isSessionActive(request);
		if(bitacora==null)
		{
			return "redirect:/log/out?mensaje="+utils.getDetailError();
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		theModel.addAttribute("view_mensaje_jsp", view_mensaje_jsp);
		theModel.addAttribute("view_error_jsp", view_error_jsp);
		return "pacientes";
	}

	@RequestMapping("agregaContactoEmergencia")
	public @ResponseBody String agregaContactoEmergencia(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		ContactosemergenciaDao contactosemergenciaDao = null;
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			Contactosemergencia contacto = getParametersRequestContactoEmergencia(ParametersRequest);
			contacto.setContactosemergenciafechacreacion(bitacora.getBitacorafecha());
			contacto.setContactosemergenciafechamodificacion(bitacora.getBitacorafecha());
			contactosemergenciaDao = new ContactosemergenciaDaoImpl();
			if(!contactosemergenciaDao.agregaContactoEmergencia(contacto))
			{
				response.set_State("false");
				response.set_Message(contactosemergenciaDao.getDetailError());
			}
			else {
				response.set_State("true");
				response.set_Message("Se creo exitosamente el contacto "+contacto.getContactosemergenciaid()+" "+contacto.getContactosemergencianombre());
				response.set_ObjectJson(new Gson().toJson(contacto));
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("elminaContactoEmergencia")
	public @ResponseBody String elminaContactoEmergencia(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		ContactosemergenciaDao contactosemergenciaDao = null;
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}

			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			contactosemergenciaDao = new ContactosemergenciaDaoImpl();
			if(!contactosemergenciaDao.elminarContactoEmergencia(jsonObject.get("rq_pacienteid").getAsInt(), 
					jsonObject.get("rq_contactosemergenciaid").getAsInt()))
			{
				response.set_State("false");
				response.set_Message(contactosemergenciaDao.getDetailError());
			}
			else {
				response.set_State("true");
				response.set_Message("Se elmino exitosamente el contacto");
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("altaPaciente")
	public @ResponseBody String alta(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		PacientesDao pacienteDao = new PacientesDaoImpl();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			Pacientes paciente = getParametersRequestPacientes(ParametersRequest);
			paciente.setPacientefechacreacion(bitacora.getBitacorafecha());
			paciente.setPacientefechamodificacion(bitacora.getBitacorafecha());
			
			if(!pacienteDao.createPaciente(paciente))
			{
				response.set_State("false");
				response.set_Message(pacienteDao.getDetailError());
			}
			else {
				paciente = pacienteDao.getPaciente(paciente.getPacienteid());
				response.set_State("true");
				response.set_Message("Se creo exitosamente el paciente "+paciente.getPacienteid());
				response.set_ObjectJson(new Gson().toJson(paciente));
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("buscarPaciente")
	public @ResponseBody String editar(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora = null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			PacientesDao pacientesDao = new PacientesDaoImpl();
			ArrayList<Pacientes> pacientes = pacientesDao.doBuscarPaciente(jsonObject.get("rq_parametroBusqueda").getAsString());
			if(pacientes!=null)
			{
				response.set_State("true");
				response.set_Message("Peticion procesada exitosamente");
				response.set_ObjectJson(new Gson().toJson(pacientes));
			}
			else
			{
				response.set_State("false");
				response.set_Message(pacientesDao.getDetailError());
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			log.info(response.toString());
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("guardarCambios")
	public @ResponseBody String guardarCambios(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			
			switch(jsonObject.get("proceso").getAsString())
			{
				case "PERSONAL":
					if(doCambioPersonal(bitacora, jsonObject.get("ParametersRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se actualizo la informacion exitosamente");
					}
				break;
				case "DATOSFISCALES":
					if(doCambioDatosFiscales(bitacora, jsonObject.get("ParametersRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se actualizo la informacion exitosamente");
					}
				break;
				case "ANTECEDENTES":
					if(doCambioAntecedentes(bitacora, jsonObject.get("ParametersRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se actualizo la informacion exitosamente");
					}
				break;
				default:
					response.set_State("false");
					response.set_Message("Tipo de formulario no soportado.");
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	private boolean doCambioPersonal(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		PacientesDao pacientesDao = new PacientesDaoImpl();
		Pacientes paciente = getParametersRequestPacientes(parametersRequest);
		paciente.setPacientefechamodificacion(bitacora.getBitacorafecha());
		log.info(paciente.toString());
		
		if(!pacientesDao.updatePaciente(paciente))
		{
			response.set_State("false");
			response.set_Message(pacientesDao.getDetailError());
			return false;
		}
		return true;
	}

	private boolean doCambioDatosFiscales(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		DatosfiscalesDao datosfiscalesDao = new DatosfiscalesDaoImpl();
		Datosfiscales datosfiscales = getParametersRequestDatosfiscales(parametersRequest);
		datosfiscales.setDatofiscalfechacreacion(bitacora.getBitacorafecha());
		datosfiscales.setDatofiscalfechamodificacion(bitacora.getBitacorafecha());
		log.info(datosfiscales.toString());
		
		if(!datosfiscalesDao.updateDatosfiscales(datosfiscales))
		{
			response.set_State("false");
			response.set_Message(datosfiscalesDao.getDetailError());
			return false;
		}
		return true;
	}

	private boolean doCambioAntecedentes(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		AntecedentesDao antecedentesDao = new AntecedentesDaoImpl();
		Antecedentes antecedentes = getParametersRequestAntecedentes(parametersRequest);
		antecedentes.setAntecedenteactivo(true);
		antecedentes.setAntecedentefechacreacion(bitacora.getBitacorafecha());
		antecedentes.setAntecedentefechamodificacion(bitacora.getBitacorafecha());
		log.info(antecedentes.toString());
		
		if(!antecedentesDao.updateAntecedentes(antecedentes))
		{
			response.set_State("false");
			response.set_Message(antecedentesDao.getDetailError());
			return false;
		}
		return true;
	}

	private Contactosemergencia getParametersRequestContactoEmergencia(String parametersRequest)throws Exception {
		Contactosemergencia contacto=null;
		try {
			JsonObject jsonObject = new Gson().fromJson(parametersRequest, JsonObject.class);
			contacto = new Contactosemergencia();
			contacto.setPacienteid(jsonObject.get("rq_pacienteid").getAsInt());
			contacto.setParentescoid(jsonObject.get("rq_parentescoid").getAsInt());
			contacto.setContactosemergencianombre(jsonObject.get("rq_contactosemergencianombre").getAsString());
			contacto.setContactosemergenciatelefono(jsonObject.get("rq_contactosemergenciatelefono").getAsString());
			contacto.setContactosemergenciacelular(jsonObject.get("rq_contactosemergenciacelular").getAsString());
			contacto.setContactosemergenciacorreo(jsonObject.get("rq_contactosemergenciacorreo").getAsString());
			contacto.setContactosemergenciaactivo(true);
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Se genero un error al recuperar la informacion del formulario: "+ex.getMessage());
		}
		return contacto;
	}

	private Pacientes getParametersRequestPacientes(String parametersRequest)throws Exception {
		Pacientes paciente=null;
		try {
			JsonObject jsonObject = new Gson().fromJson(parametersRequest, JsonObject.class);
			paciente = new Pacientes();
			paciente.setPacienteactivo(true);
			paciente.setPacienteid((jsonObject.get("rq_pacienteid")!=null ? jsonObject.get("rq_pacienteid").getAsInt() : -1));
			paciente.setPacientenombre(jsonObject.get("rq_pacientenombre").getAsString());
			paciente.setPacienteapellidopaterno(jsonObject.get("rq_pacienteapellidopaterno").getAsString());
			paciente.setPacienteapellidomaterno(jsonObject.get("rq_pacienteapellidomaterno").getAsString());
			paciente.setPacientesexo(jsonObject.get("rq_pacientesexo").getAsString());
			paciente.setPacienteedad(jsonObject.get("rq_pacienteedad").getAsInt());
			paciente.setPacienteestadocivil(jsonObject.get("rq_pacienteestadocivil").getAsString());
			paciente.setPacientelugarnacimiento(jsonObject.get("rq_pacientelugarnacimiento").getAsString());
			paciente.setPacientefechanacimiento(Date.valueOf(jsonObject.get("rq_pacientefechanacimiento").getAsString()));
			paciente.setPacientedomicilio(jsonObject.get("rq_pacientedomicilio").getAsString());
			paciente.setPacienteciudad(jsonObject.get("rq_pacienteciudad").getAsString());
			paciente.setPacientecodigopostal(jsonObject.get("rq_pacientecodigopostal").getAsString());
			paciente.setEstadoid(jsonObject.get("rq_estadoid").getAsInt());
			paciente.setPacientecorreo(jsonObject.get("rq_pacientecorreo").getAsString());
			paciente.setPacientetelefono(jsonObject.get("rq_pacientetelefono").getAsString());
			paciente.setPacientecelular(jsonObject.get("rq_pacientecelular").getAsString());
			paciente.setPacientecurp(jsonObject.get("rq_pacientecurp").getAsString());
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Se genero un error al recuperar la informacion del formulario: "+ex.getMessage());
		}
		return paciente;
	}

	private Datosfiscales getParametersRequestDatosfiscales(String parametersRequest)throws Exception {
		Datosfiscales datosfiscales = null;
		Pacientes paciente = null;
		try {
			JsonObject jsonObject = new Gson().fromJson(parametersRequest, JsonObject.class);
			paciente= new Pacientes();
			datosfiscales = new Datosfiscales();
			datosfiscales.setDatofiscalid(jsonObject.get("rq_datofiscalid").getAsInt());
			datosfiscales.setDatofiscalnombre(jsonObject.get("rq_datofiscalnombre").getAsString());
			datosfiscales.setDatofiscalrfc(jsonObject.get("rq_datofiscalrfc").getAsString());
			datosfiscales.setDatofiscaldireccion(jsonObject.get("rq_datofiscaldireccion").getAsString());
			datosfiscales.setDatofiscalcorreo(jsonObject.get("rq_datofiscalcorreo").getAsString());
			paciente.setPacienteid(jsonObject.get("rq_pacienteid").getAsInt());
			datosfiscales.setPaciente(paciente);
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Se genero un error al recuperar la informacion del formulario: "+ex.getMessage());
		}
		return datosfiscales;
	}

	private Antecedentes getParametersRequestAntecedentes(String parametersRequest)throws Exception {
		Antecedentes antecedentes = null;
		try {
			JsonObject jsonObject = new Gson().fromJson(parametersRequest, JsonObject.class);
			antecedentes = new Antecedentes();
			antecedentes.setPacienteid(jsonObject.get("rq_pacienteid").getAsInt());
			antecedentes.setAntecedentetratamientomedico(jsonObject.get("rq_antecedentetratamientomedico").getAsString());
			antecedentes.setAntecedenteembarazo(jsonObject.get("rq_antecedenteembarazo").getAsString());
			antecedentes.setAntecedentemedicamentos(jsonObject.get("rq_antecedentemedicamentos").getAsString());
			antecedentes.setAntecedentegsaq(jsonObject.get("rq_antecedentegsaq").getAsString());
			antecedentes.setAntecedentealergias(jsonObject.get("rq_antecedentealergias").getAsString());
			antecedentes.setAntecedenteetsvih(jsonObject.get("rq_antecedenteetsvih").getAsString());
			antecedentes.setAntecedentecoagulacion(jsonObject.get("rq_antecedentecoagulacion").getAsString());
			antecedentes.setAntecedentecancer(jsonObject.get("rq_antecedentecancer").getAsString());
			antecedentes.setAntecedenteccrn(jsonObject.get("rq_antecedenteccrn").getAsString());
			antecedentes.setAntecedentepdtf(jsonObject.get("rq_antecedentepdtf").getAsString());
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Se genero un error al recuperar la informacion del formulario: "+ex.getMessage());
		}
		return antecedentes;
	}
}
