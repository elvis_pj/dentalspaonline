package com.online.dentalspa.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.online.dentalspa.dao.EstadosDao;
import com.online.dentalspa.dao.EstadosDaoImpl;
import com.online.dentalspa.dao.FormapagoDao;
import com.online.dentalspa.dao.FormapagoDaoImpl;
import com.online.dentalspa.dao.PagosdetalletipoDao;
import com.online.dentalspa.dao.PagosdetalletipoDaoImpl;
import com.online.dentalspa.dao.ParentescosDao;
import com.online.dentalspa.dao.ParentescosDaoImpl;
import com.online.dentalspa.dao.TipopagoDao;
import com.online.dentalspa.dao.TipopagoDaoImpl;
import com.online.dentalspa.dao.TipotratamientoDao;
import com.online.dentalspa.dao.TipotratamientoDaoImpl;
import com.online.dentalspa.model.AjaxServiceResponse;
import com.online.dentalspa.model.Bitacora;
import com.online.dentalspa.model.Tipotratamiento;
import com.online.dentalspa.utils.Utils;

@Controller
@RequestMapping("/catalogos")
public class CatalogosController {
	private Logger log = Logger.getLogger(CatalogosController.class);
	private Utils utils=new Utils();
	
	@RequestMapping("/")
	public String catalogos(Model theModel, HttpServletRequest request, 
			@RequestParam(value="view_mensaje_jsp", required=false) String view_mensaje_jsp, 
			@RequestParam(value="view_error_jsp", required=false) String view_error_jsp)
	{
		log.info("Inicia catalogos");
		Bitacora bitacora = utils.isSessionActive(request);
		if(bitacora==null)
		{
			return "redirect:/log/out?mensaje="+utils.getDetailError();
		}
		Map<String, String> listaCatalogos=new HashMap<String, String>();
		listaCatalogos.put("tipotratamiento", "Tratamientos");
		theModel.addAttribute("listaCatalogos", listaCatalogos);
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		theModel.addAttribute("view_mensaje_jsp", view_mensaje_jsp);
		theModel.addAttribute("view_error_jsp", view_error_jsp);
		return "catalogos";
	}

	@RequestMapping("/eliminarCatalogo")
	public @ResponseBody String eliminarCatalogo(HttpServletRequest request,
			@RequestParam String ParametersRequest) {
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}

			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			switch(jsonObject.get("tipoCatalogo").getAsString())
			{
				case "tipotratamiento":
					if(deleteTipoTratamiento(jsonObject.get("id").getAsInt(), response))
					{
						response.set_State("true");
						response.set_Message("Se guardo la informacion exitosamente");
					}
				break;
				default:
					log.warn("El tipo de catalogo no esta soportado :: ["+jsonObject.get("tipoCatalogo").getAsString()+"]");
					response.set_State("false");
					response.set_Message("El tipo de catalogo no esta soportado");
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("/guardarCatalogo")
	public @ResponseBody String guardarCatalogo(HttpServletRequest request,
			@RequestParam String ParametersRequest) {
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}

			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			switch(jsonObject.get("tipoCatalogo").getAsString())
			{
				case "tipotratamiento":
					if(addTipoTratamiento(bitacora, jsonObject.get("jsonRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se guardo la informacion exitosamente");
					}
				break;
				default:
					log.warn("El tipo de catalogo no esta soportado :: ["+jsonObject.get("tipoCatalogo").getAsString()+"]");
					response.set_State("false");
					response.set_Message("El tipo de catalogo no esta soportado");
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("/tipotratamiento")
	public @ResponseBody String catalogoTipotratamiento(HttpServletRequest request) {
		AjaxServiceResponse response = new AjaxServiceResponse();
		TipotratamientoDao tipotratamientoDao = new TipotratamientoDaoImpl();
		try
		{
			if(!utils.sessionActive(request))
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			response.set_State("true");
			response.set_Message("Peticion procesada exitosamente");
			response.set_ObjectJson(new Gson().toJson(tipotratamientoDao.getListaTratamientos()));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			log.info(response.toString());
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("/formapago")
	public @ResponseBody String catalogoFormapago(HttpServletRequest request) {
		AjaxServiceResponse response = new AjaxServiceResponse();
		FormapagoDao dormapagoDao = new FormapagoDaoImpl();
		try
		{
			if(!utils.sessionActive(request))
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			response.set_State("true");
			response.set_Message("Peticion procesada exitosamente");
			response.set_ObjectJson(new Gson().toJson(dormapagoDao.getFormapago()));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			log.info(response.toString());
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("/pagosdetalletipo")
	public @ResponseBody String catalogoPagosdetalletipo(HttpServletRequest request) {
		AjaxServiceResponse response = new AjaxServiceResponse();
		PagosdetalletipoDao pagosdetalletipoDao = new PagosdetalletipoDaoImpl();
		try
		{
			if(!utils.sessionActive(request))
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			response.set_State("true");
			response.set_Message("Peticion procesada exitosamente");
			response.set_ObjectJson(new Gson().toJson(pagosdetalletipoDao.getPagosdetalletipo()));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			log.info(response.toString());
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("/tipopago")
	public @ResponseBody String catalogoTipopago(HttpServletRequest request) {
		AjaxServiceResponse response = new AjaxServiceResponse();
		TipopagoDao tipopagoDao = new TipopagoDaoImpl();
		try
		{
			if(!utils.sessionActive(request))
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			response.set_State("true");
			response.set_Message("Peticion procesada exitosamente");
			response.set_ObjectJson(new Gson().toJson(tipopagoDao.getTipopago()));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			log.info(response.toString());
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("/estados")
	public @ResponseBody String catalogoEstados(HttpServletRequest request) {
		AjaxServiceResponse response = new AjaxServiceResponse();
		EstadosDao estadosDao = new EstadosDaoImpl();
		try
		{
			if(!utils.sessionActive(request))
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			response.set_State("true");
			response.set_Message("Peticion procesada exitosamente");
			response.set_ObjectJson(new Gson().toJson(estadosDao.getEstados()));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			log.info(response.toString());
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("/parentescos")
	public @ResponseBody String catalogoParentescos(HttpServletRequest request) {
		AjaxServiceResponse response = new AjaxServiceResponse();
		ParentescosDao parentescosDao = new ParentescosDaoImpl();
		try
		{
			if(!utils.sessionActive(request))
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			response.set_State("true");
			response.set_Message("Peticion procesada exitosamente");
			response.set_ObjectJson(new Gson().toJson(parentescosDao.getListaParentescos()));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			log.info(response.toString());
		}
		return new Gson().toJson(response);
	}

	private boolean deleteTipoTratamiento(int tipotratamientoid, AjaxServiceResponse response)throws Exception {
		TipotratamientoDao tipotratamientoDao = new TipotratamientoDaoImpl();
		if(!tipotratamientoDao.delete(tipotratamientoid))
		{
			response.set_State("false");
			response.set_Message(tipotratamientoDao.getDetailError());
			return false;
		}
		return true;
	}

	private boolean addTipoTratamiento(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		TipotratamientoDao tipotratamientoDao = new TipotratamientoDaoImpl();
		Tipotratamiento tipotratamiento = new Gson().fromJson(parametersRequest, Tipotratamiento.class);
		tipotratamiento.setTipotratamientoactivo(true);
		tipotratamiento.setTipotratamientofechacreacion(bitacora.getBitacorafecha());
		tipotratamiento.setTipotratamientofechamodifcacion(bitacora.getBitacorafecha());
		if(!tipotratamientoDao.create(tipotratamiento))
		{
			response.set_State("false");
			response.set_Message(tipotratamientoDao.getDetailError());
			return false;
		}
		return true;
	}
}
