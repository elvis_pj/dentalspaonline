package com.online.dentalspa.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.online.dentalspa.model.Bitacora;
import com.online.dentalspa.utils.Utils;

@Controller
@RequestMapping("/recetas")
public class RecetasController {
	private Logger log = Logger.getLogger(RecetasController.class);
	private Utils utils=new Utils();
	
	@RequestMapping("/")
	public String pacientes(Model theModel, HttpServletRequest request, 
			@RequestParam(value="view_mensaje_jsp", required=false) String view_mensaje_jsp, 
			@RequestParam(value="view_error_jsp", required=false) String view_error_jsp)
	{
		log.info("Inicia recetas");
		Bitacora bitacora = utils.isSessionActive(request);
		if(bitacora==null)
		{
			return "redirect:/log/out?mensaje="+utils.getDetailError();
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		theModel.addAttribute("view_mensaje_jsp", view_mensaje_jsp);
		theModel.addAttribute("view_error_jsp", view_error_jsp);
		return "recetas";
	}

}
