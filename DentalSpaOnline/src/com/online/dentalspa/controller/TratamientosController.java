package com.online.dentalspa.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.online.dentalspa.dao.ExamendentalDao;
import com.online.dentalspa.dao.ExamendentalDaoImpl;
import com.online.dentalspa.dao.PacientesDao;
import com.online.dentalspa.dao.PacientesDaoImpl;
import com.online.dentalspa.dao.TipotratamientoDao;
import com.online.dentalspa.dao.TipotratamientoDaoImpl;
import com.online.dentalspa.dao.TratamientodetalleDao;
import com.online.dentalspa.dao.TratamientodetalleDaoImpl;
import com.online.dentalspa.dao.TratamientosDao;
import com.online.dentalspa.dao.TratamientosDaoImpl;
import com.online.dentalspa.model.AjaxServiceResponse;
import com.online.dentalspa.model.Bitacora;
import com.online.dentalspa.model.Examendental;
import com.online.dentalspa.model.Odontogramas;
import com.online.dentalspa.model.Pacientes;
import com.online.dentalspa.model.Tratamientodetalle;
import com.online.dentalspa.model.Tratamientos;
import com.online.dentalspa.utils.Utils;

@Controller
@RequestMapping("/tratamientos")
public class TratamientosController {
	private Logger log = Logger.getLogger(TratamientosController.class);
	private Utils utils=new Utils();
	
	@RequestMapping("/")
	public String pacientes(Model theModel, HttpServletRequest request, 
			@RequestParam(value="rq_paciente", required=false) String pacienteid, 
			@RequestParam(value="view_mensaje_jsp", required=false) String view_mensaje_jsp, 
			@RequestParam(value="view_error_jsp", required=false) String view_error_jsp)
	{
		log.info("Inicia tratamientos");
		Bitacora bitacora = utils.isSessionActive(request);
		if(bitacora==null)
		{
			return "redirect:/log/out?mensaje="+utils.getDetailError();
		}
		TipotratamientoDao tipotratamientoDao = new TipotratamientoDaoImpl();
		TratamientosDao tratamientosDao = null;
		PacientesDao pacientesdao =null;
		Pacientes paciente = null;
		if(pacienteid!=null && utils.isDigito(pacienteid))
		{
			pacientesdao = new PacientesDaoImpl();
			paciente=pacientesdao.getPaciente(Integer.parseInt(pacienteid));
			if(paciente==null)
				view_error_jsp=pacientesdao.getDetailError();
			else
			{
				tratamientosDao = new TratamientosDaoImpl();
				paciente.setTratamientos(tratamientosDao.getListTratamientos(paciente.getPacienteid()));
//				if(paciente.getTratamientos()==null || paciente.getTratamientos().size()<1)
//					view_error_jsp=tratamientosDao.getDetailError();
				if(paciente.getExamendental()==null || paciente.getExamendental().getExamendentalid()<1)
					view_mensaje_jsp="Es necesario completar el examen dental antes de continuar con un tratamiento.";
			}
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		theModel.addAttribute("view_mensaje_jsp", view_mensaje_jsp);
		theModel.addAttribute("view_error_jsp", view_error_jsp);
		theModel.addAttribute("paciente", paciente);
		theModel.addAttribute("tipotratamientos", tipotratamientoDao.getListaTratamientos());
		return "tratamientos";
	}

	@RequestMapping("listaTratamientos")
	public @ResponseBody String listaTratamientos(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			TratamientosDao tratamientosDao = new TratamientosDaoImpl();
			ArrayList<Tratamientos> lista = tratamientosDao.getListTratamientos(jsonObject.get("rq_pacienteid").getAsInt());
			if(lista!=null && lista.size()>0)
			{
				response.set_State("true");
				response.set_Message("Se guardo la informacion exitosamente");
				response.set_ObjectJson(new Gson().toJson(lista));
			}
			else
			{
				response.set_State("false");
				response.set_Message(tratamientosDao.getDetailError());
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	@RequestMapping("crear")
	public @ResponseBody String guardarCambios(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest)
	{
		AjaxServiceResponse response = new AjaxServiceResponse();
		Bitacora bitacora=null;
		try
		{
			bitacora = utils.isSessionActive(request);
			if(bitacora==null)
			{
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			JsonObject jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			
			switch(jsonObject.get("proceso").getAsString())
			{
				case "examendental":
					if(crearExamenDental(bitacora, jsonObject.get("ParametersRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se guardo la informacion exitosamente");
					}
				break;
				case "tratamiento":
					if(crearTratamiento(bitacora, jsonObject.get("ParametersRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se guardo la informacion exitosamente");
					}
				break;
				case "tratamientodetalle":
					if(crearTratamientodetalle(bitacora, jsonObject.get("ParametersRequest").getAsString(), response))
					{
						response.set_State("true");
						response.set_Message("Se guardo la informacion exitosamente");
					}
				break;
				default:
					response.set_State("false");
					response.set_Message("Tipo de formulario no soportado.");
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Error al procesar la peticion",ex);
			response = new AjaxServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: "+ex.getMessage());
		}
		finally
		{
			if(bitacora!=null && bitacora.getBitacoraid()>0)
			{
				bitacora.setBitacorapeticion(ParametersRequest);
				bitacora.setBitacorarespuesta(new Gson().toJson(response));
				bitacora.setBitacoramensaje(response.get_Message());
				utils.updateBitacora(bitacora);
			}
		}
		return new Gson().toJson(response);
	}

	private boolean crearTratamientodetalle(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		JsonObject jsonObject = new Gson().fromJson(parametersRequest, JsonObject.class);
		log.info(jsonObject.get("rq_fintratamiento").getAsBoolean()+" :: "+!jsonObject.get("rq_fintratamiento").getAsBoolean()+" >> "+new Gson().toJson(jsonObject));
		TratamientodetalleDao tratamientodetalleDao = new TratamientodetalleDaoImpl();
		Tratamientodetalle tratamientodetalle = new Tratamientodetalle();
		tratamientodetalle.setTratamientoid(jsonObject.get("rq_tratamientoid").getAsInt());
		tratamientodetalle.setTratamientodetalleactivo(jsonObject.get("rq_fintratamiento")!=null ? (!jsonObject.get("rq_fintratamiento").getAsBoolean()) : true);
		tratamientodetalle.setTratamientodetallenombre(jsonObject.get("rq_tratamientodetallenombre").getAsString()); 
		tratamientodetalle.setTratamientodetallecomentarios(jsonObject.get("rq_tratamientodetallecomentarios").getAsString());
		tratamientodetalle.setTratamientodetallefechacreacion(bitacora.getBitacorafecha());
		tratamientodetalle.setTratamientodetallefechamodificacion(bitacora.getBitacorafecha());
		log.info(tratamientodetalle.toString());
		if(!tratamientodetalleDao.createTratamientodetalle(tratamientodetalle))
		{
			response.set_State("false");
			response.set_Message(tratamientodetalleDao.getDetailError());
			return false;
		}
		return true;
	}

	private boolean crearTratamiento(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		TratamientosDao tratamientosDao = new TratamientosDaoImpl();
		Tratamientos tratamiento = getParametersRequestTratamientos(parametersRequest, bitacora);
		log.info(tratamiento.toString());
		if(!tratamientosDao.createTratamiento(tratamiento))
		{
			response.set_State("false");
			response.set_Message(tratamientosDao.getDetailError());
			return false;
		}
		return true;
	}

	private boolean crearExamenDental(Bitacora bitacora, String parametersRequest, AjaxServiceResponse response)throws Exception {
		ExamendentalDao examendentalDao = new ExamendentalDaoImpl();
		Examendental examendental = getParametersRequestExamendental(parametersRequest);
		examendental.setExamendentalactivo(true);
		examendental.setExamendentalfechacreacion(bitacora.getBitacorafecha());
		examendental.setExamendentalfechamodificacion(bitacora.getBitacorafecha());
		log.info(examendental.toString());
		if(!examendentalDao.crearExamendental(examendental))
		{
			response.set_State("false");
			response.set_Message(examendentalDao.getDetailError());
			return false;
		}
		return true;
	}

	private Tratamientos getParametersRequestTratamientos(String parametersRequest, Bitacora bitacora)throws Exception {
		Tratamientos tratamiento = null;
		Odontogramas odontograma = null;
		try {
			JsonObject jsonObject = new Gson().fromJson(parametersRequest, JsonObject.class);
			tratamiento = new Tratamientos();
			tratamiento.setPacienteid(jsonObject.get("rq_pacienteid").getAsInt());
			tratamiento.setTipotratamientoid(jsonObject.get("rq_tipotratamientoid").getAsInt());
			tratamiento.setTratamientocomentariosiniciales(jsonObject.get("rq_tratamientocomentariosiniciales")!=null ? jsonObject.get("rq_tratamientocomentariosiniciales").getAsString() : "");
			tratamiento.setTratamientoplan(jsonObject.get("rq_tratamientoplan")!=null ? jsonObject.get("rq_tratamientoplan").getAsString() : "");
			tratamiento.setTratamientoactivo(jsonObject.get("rq_tratamientoactivo")!=null ? jsonObject.get("rq_tratamientoactivo").getAsBoolean() : true);
			tratamiento.setTratamientoterminado(jsonObject.get("rq_tratamientoterminado")!=null ? jsonObject.get("rq_tratamientoterminado").getAsBoolean() : false);
			tratamiento.setTratamientofechacreacion(bitacora.getBitacorafecha());
			tratamiento.setTratamientofechamodificacion(bitacora.getBitacorafecha());
			
			odontograma = new Odontogramas();
			odontograma.setOdontogramaactivo(jsonObject.get("rq_odontogramaactivo")!=null ? jsonObject.get("rq_odontogramaactivo").getAsBoolean() : true);
			odontograma.setOdontogramacomentarios(jsonObject.get("rq_odontogramacomentarios")!=null ? jsonObject.get("rq_odontogramacomentarios").getAsString() : "");
			odontograma.setOdontogramafechacreacion(bitacora.getBitacorafecha());
			odontograma.setOdontogramafechamodificacion(bitacora.getBitacorafecha());
			odontograma.setOdontograma_11(jsonObject.get("rq_odontograma_11")!=null ? jsonObject.get("rq_odontograma_11").getAsString() : "");
			odontograma.setOdontograma_12(jsonObject.get("rq_odontograma_12")!=null ? jsonObject.get("rq_odontograma_12").getAsString() : "");
			odontograma.setOdontograma_13(jsonObject.get("rq_odontograma_13")!=null ? jsonObject.get("rq_odontograma_13").getAsString() : "");
			odontograma.setOdontograma_14(jsonObject.get("rq_odontograma_14")!=null ? jsonObject.get("rq_odontograma_14").getAsString() : "");
			odontograma.setOdontograma_15(jsonObject.get("rq_odontograma_15")!=null ? jsonObject.get("rq_odontograma_15").getAsString() : "");
			odontograma.setOdontograma_16(jsonObject.get("rq_odontograma_16")!=null ? jsonObject.get("rq_odontograma_16").getAsString() : "");
			odontograma.setOdontograma_17(jsonObject.get("rq_odontograma_17")!=null ? jsonObject.get("rq_odontograma_17").getAsString() : "");
			odontograma.setOdontograma_18(jsonObject.get("rq_odontograma_18")!=null ? jsonObject.get("rq_odontograma_18").getAsString() : "");
			odontograma.setOdontograma_21(jsonObject.get("rq_odontograma_21")!=null ? jsonObject.get("rq_odontograma_21").getAsString() : "");
			odontograma.setOdontograma_22(jsonObject.get("rq_odontograma_22")!=null ? jsonObject.get("rq_odontograma_22").getAsString() : "");
			odontograma.setOdontograma_23(jsonObject.get("rq_odontograma_23")!=null ? jsonObject.get("rq_odontograma_23").getAsString() : "");
			odontograma.setOdontograma_24(jsonObject.get("rq_odontograma_24")!=null ? jsonObject.get("rq_odontograma_24").getAsString() : "");
			odontograma.setOdontograma_25(jsonObject.get("rq_odontograma_25")!=null ? jsonObject.get("rq_odontograma_25").getAsString() : "");
			odontograma.setOdontograma_26(jsonObject.get("rq_odontograma_26")!=null ? jsonObject.get("rq_odontograma_26").getAsString() : "");
			odontograma.setOdontograma_27(jsonObject.get("rq_odontograma_27")!=null ? jsonObject.get("rq_odontograma_27").getAsString() : "");
			odontograma.setOdontograma_28(jsonObject.get("rq_odontograma_28")!=null ? jsonObject.get("rq_odontograma_28").getAsString() : "");
			odontograma.setOdontograma_31(jsonObject.get("rq_odontograma_31")!=null ? jsonObject.get("rq_odontograma_31").getAsString() : "");
			odontograma.setOdontograma_32(jsonObject.get("rq_odontograma_32")!=null ? jsonObject.get("rq_odontograma_32").getAsString() : "");
			odontograma.setOdontograma_33(jsonObject.get("rq_odontograma_33")!=null ? jsonObject.get("rq_odontograma_33").getAsString() : "");
			odontograma.setOdontograma_34(jsonObject.get("rq_odontograma_34")!=null ? jsonObject.get("rq_odontograma_34").getAsString() : "");
			odontograma.setOdontograma_35(jsonObject.get("rq_odontograma_35")!=null ? jsonObject.get("rq_odontograma_35").getAsString() : "");
			odontograma.setOdontograma_36(jsonObject.get("rq_odontograma_36")!=null ? jsonObject.get("rq_odontograma_36").getAsString() : "");
			odontograma.setOdontograma_37(jsonObject.get("rq_odontograma_37")!=null ? jsonObject.get("rq_odontograma_37").getAsString() : "");
			odontograma.setOdontograma_38(jsonObject.get("rq_odontograma_38")!=null ? jsonObject.get("rq_odontograma_38").getAsString() : "");
			odontograma.setOdontograma_41(jsonObject.get("rq_odontograma_41")!=null ? jsonObject.get("rq_odontograma_41").getAsString() : "");
			odontograma.setOdontograma_42(jsonObject.get("rq_odontograma_42")!=null ? jsonObject.get("rq_odontograma_42").getAsString() : "");
			odontograma.setOdontograma_43(jsonObject.get("rq_odontograma_43")!=null ? jsonObject.get("rq_odontograma_43").getAsString() : "");
			odontograma.setOdontograma_44(jsonObject.get("rq_odontograma_44")!=null ? jsonObject.get("rq_odontograma_44").getAsString() : "");
			odontograma.setOdontograma_45(jsonObject.get("rq_odontograma_45")!=null ? jsonObject.get("rq_odontograma_45").getAsString() : "");
			odontograma.setOdontograma_46(jsonObject.get("rq_odontograma_46")!=null ? jsonObject.get("rq_odontograma_46").getAsString() : "");
			odontograma.setOdontograma_47(jsonObject.get("rq_odontograma_47")!=null ? jsonObject.get("rq_odontograma_47").getAsString() : "");
			odontograma.setOdontograma_48(jsonObject.get("rq_odontograma_48")!=null ? jsonObject.get("rq_odontograma_48").getAsString() : "");
			tratamiento.setOdontogramas(odontograma);
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Se genero un error al recuperar la informacion del formulario: "+ex.getMessage());
		}
		return tratamiento;
	}

	private Examendental getParametersRequestExamendental(String parametersRequest)throws Exception {
		Examendental examendental = null;
		try {
			examendental = new Examendental();
			JsonObject jsonObject = new Gson().fromJson(parametersRequest, JsonObject.class);
			examendental.setPacienteid(jsonObject.get("rq_pacienteid").getAsInt());
			examendental.setExamendentalhigiene(jsonObject.get("rq_examendentalhigiene").getAsString());
			examendental.setExamendentalencia(jsonObject.get("rq_examendentalencia").getAsString());
			examendental.setExamendentalpisoboca(jsonObject.get("rq_examendentalpisoboca").getAsString());
			examendental.setExamendentallabios(jsonObject.get("rq_examendentallabios").getAsString());
			examendental.setExamendentalcarillos(jsonObject.get("rq_examendentalcarillos").getAsString());
			examendental.setExamendentalbruxismo(jsonObject.get("rq_examendentalbruxismo").getAsString());
			examendental.setExamendentalchasquidos(jsonObject.get("rq_examendentalchasquidos").getAsString());
			examendental.setExamendentallengua(jsonObject.get("rq_examendentallengua").getAsString());
			examendental.setExamendentalpaladar(jsonObject.get("rq_examendentalpaladar").getAsString());
			examendental.setExamendentalsupernumerarios(jsonObject.get("rq_examendentalsupernumerarios").getAsString());
			examendental.setExamendentalsangrado(jsonObject.get("rq_examendentalsangrado").getAsString());
			examendental.setExamendentalplacasarro(jsonObject.get("rq_examendentalplacasarro").getAsString());
			examendental.setExamendentalalteraciones(jsonObject.get("rq_examendentalalteraciones").getAsString());
			examendental.setExamendentalnota(jsonObject.get("rq_examendentalnota").getAsString());
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Se genero un error al recuperar la informacion del formulario: "+ex.getMessage());
		}
		return examendental;
	}
}
