<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
	<head>
<!-- *************************************************** -->
		<jsp:include page="templates/header.jsp" />
<!-- *************************************************** -->
	</head>
<body id="page-top">
	<!-- Page Wrapper -->
	<div id="wrapper">
<!-- *************************************************** -->
		<jsp:include page="templates/menu.jsp" />
<!-- *************************************************** -->
    	<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
<!-- *************************************************** -->
				<jsp:include page="templates/topbar.jsp" />
<!-- *************************************************** -->
				<!-- ****************************** Begin Page Content ****************************** -->
				<div class="container-fluid">
					<h1 class="h3 mb-4 text-gray-800">${permiso_active.procesos.procesonombre}</h1>
					<div class="row" id="container_opciones">
						<div class="col-md-12">
							<div class="row">
								<c:forEach items="${usuariosession.perfil.permisos}" var="permiso">
									<c:if test="${permiso_active.procesos.procesoid==permiso.procesos.procesopadreid && permiso.procesos.procesoprincipal=='f'}">
										<div class="col">
							              <div class="card mb-4 py-3 border-left-info">
							                <div class="card-body">
							                ${permiso.procesos.procesonombre} 
							                  <button type="button" class="btn btn-sm btn-info" onclick="showForm(${permiso.procesos.procesoid})">Ir</button>
							                </div>
							              </div>
										</div>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</div>
					<div class="row" id="container_form"><!-- CONTENEDOR DE VISTAS -->
					
					</div><!-- CONTENEDOR DE VISTAS -->
				</div>
				<!-- ******************************* End Page Content ******************************* -->
			</div>
			<!-- End of Main Content -->
<!-- *************************************************** -->
			<jsp:include page="templates/footer.jsp" />
<!-- *************************************************** -->
		</div>
		<!-- End of Content Wrapper -->
	</div>
<!-- End of Page Wrapper -->
<!-- *************************************************** -->
	<jsp:include page="templates/footer_ln.jsp" />
	<script src="${pageContext.request.contextPath}/resources/js/pacientes.js"></script>
<!-- *************************************************** -->
</body>
</html>