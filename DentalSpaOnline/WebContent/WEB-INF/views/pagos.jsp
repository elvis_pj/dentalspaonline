<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html lang="en">
	<head>
<!-- *************************************************** -->
		<jsp:include page="templates/header.jsp" />
<!-- *************************************************** -->
	</head>
<body id="page-top">
	<!-- Page Wrapper -->
	<div id="wrapper">
<!-- *************************************************** -->
		<jsp:include page="templates/menu.jsp" />
<!-- *************************************************** -->
    	<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
<!-- ***************************************************  -->
				<jsp:include page="templates/topbar.jsp" />
<!-- ***************************************************
				<!-- ****************************** Begin Page Content ****************************** -->
				<c:if test="${rq_pacienteid!=null && rq_pacienteid!=''}">
					<script type="text/javascript">
						document.getElementById("in-search").value=${rq_pacienteid};
						document.getElementById("btn-search").click();
					</script>
				</c:if>
				<div class="container-fluid">
					<h1 class="h3 mb-4 text-gray-800">${permiso_active.procesos.procesonombre}</h1>
					<hr>
					<div class="row">
						<div class='col-md-12'>
							<div class='row'>
								<div class='col-md-1'></div>
								<div class='col-md-10' id="container_search_result"></div>
								<div class='col-md-1'></div>
							</div>
						</div>
					</div>
					<div class='row'>
						<div class="col">
							<h3 style="text-align:center;">Informacion del Paciente</h3>
						</div>
					</div>
					<div class='row'>
		                <div class='col-md-4'>
		                      <div class='form-group'>
		                            <label>Nombre Completo</label>
		                            <input type='text' class='form-control' id='info_pacienteid' value='${paciente.pacientenombre} ${paciente.pacienteapellidopaterno} ${paciente.pacienteapellidomaterno}' disabled>
		                      </div>
		                </div>
		                <div class='col-md-3'>
		                      <div class='form-group'>
		                            <label>Sexo</label>
		                            <input type='text' class='form-control' id='info_pacientesexo' value="${paciente.pacientesexo!=null ? paciente.pacientesexo.equals('H') ? 'Hombre' : 'Mujer' : ''}" disabled>
		                      </div>
		                </div>
		                <div class='col-md-3'>
		                      <div class='form-group'>
		                            <label>Edad</label>
		                            <input type='text' class='form-control' id='info_pacienteedad' value='${paciente.pacienteedad}' disabled>
		                      </div>
		                </div>
					</div>
					<hr>
					<div class='row'>
						<div class="col">
							<div class="card shadow mb-4">
								<div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">Lista de pagos por tratamiento</h6>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered" id="datatable_tratamientos" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>Tratamiento</th>
													<th>Fecha inicio</th>
													<th>Ultima Visita</th>
													<th>Importe</th>
													<th>Estatus</th>
													<th></th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>Tratamiento</th>
													<th>Fecha inicio</th>
													<th>Ultima Visita</th>
													<th>Importe</th>
													<th>Estatus</th>
													<th></th>
												</tr>
											</tfoot>
											<tbody>
												<c:forEach items="${paciente.tratamientos}" var="tratamiento">
													<tr>
													  <td>${tratamiento.tipotratamiento.tipotratamientonombre}</td>
													  <td>${tratamiento.tratamientofechacreacion}</td>
													  <td>${tratamiento.tratamientofechamodificacion}</td>
													  <td>${tratamiento.pagos.pagoimportetotal}</td>
													  <td>${tratamiento.tratamientoterminado ? 'Terminado' : 'Activo' } - ${tratamiento.pagos.pagoid>0 && tratamiento.pagos.pagosaldado ? 'Pagado' : 'Pendiente'}</td>
													  <td><input type="button" class="btn btn-info" value="Cargar" onclick=""/></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr> <!-- INFORMACION DE LOS PAGOS -->
					<div class='row'>
						<div class="col">
							<h3 style="text-align:center;">Costo del tratamiento</h3>
						</div>
					</div>
					<div class='row'>
						<div class="col-md-3">
							<div class="form-group">
								<label>Tratamiento</label>
								<input type="text" class="form-control" id="tratamientonombre" value="" disabled/>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Fecha Inicio</label>
								<input type="text" class="form-control" id="tratamientofechacreacion" value="" disabled/>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Ultima Visita</label>
								<input type="text" class="form-control" id="tratamientofechamodificacion" value="" disabled/>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Estatus</label>
								<input type="text" class="form-control" id="pagoestatus" value="" disabled/>
							</div>
						</div>
					</div>
					<div class='row'>
						<div class="col-md-3">
							<div class="form-group pago">
								<input type="hidden" id="pacienteid" value=""/>
								<input type="hidden" id="tratamientoid" value=""/>
								<label>Plan de pago</label>
								<select class="form-control" id="tipopagoid" placeholder="Plan de pago" disabled>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group pago">
								<label>Costo del tratamiento</label>
								<input type="text" class="form-control" id=pagoimportetotal value="" placeholder="Costo del tratamiento" onchange="validaCampo('DOUBLE',this);" disabled/>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group pago">
								<label>Comentarios</label>
								<textarea class="form-control" id="pagocomentarios" style="resize:none" cols="4" placeholder="Comentarios" onchange="validaCampo('NULO',this);" disabled></textarea>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group" id="container_btn_pago">
								<input type="button" class="btn btn-info btn-block" id="btn_agrega_pago" value="Guardar" onclick="addCostoTratamiento(this);" disabled/>
							</div>
						</div>
					</div>
					<div class='row'>
						<div class="col">
							<h3 style="text-align:center;">Lista de pagos</h3>
						</div>
					</div>
					<div class='row'>
						<div class="col">
							<div class="card shadow mb-4">
								<div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">Lista de pagos</h6>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered" id="datatable_lista_pagos" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>Fecha</th>
													<th>Importe</th>
													<th>Tipo Pago</th>
													<th>Forma Pago</th>
													<th>Saldo Pendiente</th>
													<th></th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>Fecha</th>
													<th>Importe</th>
													<th>Tipo Pago</th>
													<th>Forma Pago</th>
													<th>Saldo Pendiente</th>
													<th></th>
												</tr>
											</tfoot>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<!-- ******************************* End Page Content ******************************* -->
			</div>
			<!-- End of Main Content -->

<!-- *************************************************** -->
			<jsp:include page="templates/footer.jsp" />
<!-- *************************************************** -->
		</div>
		<!-- End of Content Wrapper -->
	</div>
<!-- End of Page Wrapper -->
<!-- *************************************************** -->
	<jsp:include page="templates/footer_ln.jsp" />
	<script src="${pageContext.request.contextPath}/resources/js/pagos.js"></script>
<!-- *************************************************** -->
</body>
</html>