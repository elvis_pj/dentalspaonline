<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
	<head>
<!-- *************************************************** -->
		<jsp:include page="templates/header.jsp" />
		
		<link href="${pageContext.request.contextPath}/resources/css/dashboard.css" rel="stylesheet">
		
		<style>
		
		  body {
		    padding: 0;
		    font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
		    font-size: 14px;
		  }
		
		  #calendar {
		    max-width: 900px;
		    margin: 0 auto;
		  }
		
		</style>
<!-- *************************************************** -->
	</head>
<body id="page-top">
	<!-- Page Wrapper -->
	<div id="wrapper">
<!-- *************************************************** -->
		<jsp:include page="templates/menu.jsp" />
<!-- *************************************************** -->
    	<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
<!-- *************************************************** -->
				<jsp:include page="templates/topbar.jsp" />
<!-- *************************************************** -->
				<!-- ****************************** Begin Page Content ****************************** -->
				<div class="container-fluid">
					<h1 class="h3 mb-4 text-gray-800">${procesonombre}</h1>
					<!-- <div id='calendar'></div> -->
					<hr>
						<div class="row">
							<div class="col-md-12">
								<c:set var="count" value="${0}" />
								<c:set var="style_img" value=""/>
								<c:forEach items="${usuariosession.perfil.permisos}" var="permiso">
									<c:if test="${permiso.procesos.procesoprincipal}">
										<c:if test="${count % 3 == 0 }">
											<c:out value="${'<div class=\"row\">'}" escapeXml="false" />
										</c:if>
										<c:set var="style_img" value="${style_img}
											.backgroud_${fn:toLowerCase(permiso.procesos.procesonombre)}
											{ 
												background: url(${pageContext.request.contextPath}/resources/img/icon_${fn:toLowerCase(permiso.procesos.procesonombre)}.png); 
												background-position: center; 
												background-repeat: no-repeat; 
												background-size: cover;
												width: 100%;
												height: 100%;
											}">
										</c:set>
										<div class="col-md-4">
											<div class="flip-card">
												<div class="flip-card-inner">
												    <div class="flip-card-front backgroud_${fn:toLowerCase(permiso.procesos.procesonombre)}">
														<h2 style="color:white;">${permiso.procesos.procesonombre}</h2>
												    </div>
												    <div class="flip-card-back">
														<h2>${permiso.procesos.procesonombre}</h2> 
														<p>${permiso.procesos.procesodescripcion}</p> 
														<a href="${pageContext.request.contextPath}${permiso.procesos.procesopath}" class="btn btn-danger btn-block">IR . . . !</a>
												    </div>
												</div>
											</div>
										</div>
										<c:set var="count" value="${count+1}" />
										<c:if test="${count % 3 == 0 }">
											<c:out value="${'</div>'}" escapeXml="false" />
										</c:if>
									</c:if>
								</c:forEach>
								<c:if test="${count % 3 != 0 }">
									<c:out value="${'</div>'}" escapeXml="false" />
								</c:if>
							</div>
						</div>
					<hr>
				</div>
				<c:set var="style_img" value="${'<style>'} ${style_img} ${'</style>'} " />
				<c:out value="${style_img}" escapeXml="false" ></c:out>
				<!-- ******************************* End Page Content ******************************* -->
			</div>
			<!-- End of Main Content -->
<!-- *************************************************** -->
			<jsp:include page="templates/footer.jsp" />
<!-- *************************************************** -->
		</div>
		<!-- End of Content Wrapper -->
	</div>
<!-- End of Page Wrapper -->
<!-- *************************************************** -->
	<jsp:include page="templates/footer_ln.jsp" />
	<script src='${pageContext.request.contextPath}/resources/calendar/packages/core/main.js'></script>
	<script src='${pageContext.request.contextPath}/resources/calendar/packages/list/main.js'></script>
	<script src='${pageContext.request.contextPath}/resources/js/dashboard.js'></script>
<!-- *************************************************** -->
</body>
</html>