
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">�Listo para salir?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">�</span>
          </button>
        </div>
        <div class="modal-body">Seleccione "Cerrar sesi�n" a continuaci�n si est� listo para finalizar su sesi�n actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="${pageContext.request.contextPath}/log/out?mensaje=Session+finalizada">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="${pageContext.request.contextPath}/resources/theme/vendor/jquery/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="${pageContext.request.contextPath}/resources/theme/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="${pageContext.request.contextPath}/resources/theme/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="${pageContext.request.contextPath}/resources/theme/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level plugins -->
  <script src="${pageContext.request.contextPath}/resources/theme/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/theme/vendor/datatables/dataTables.bootstrap4.min.js"></script>
  
  <!-- SweetAlert -->
  <script src="${pageContext.request.contextPath}/resources/alert/sweetalert2.all.min.js"></script>

  <!-- Function Utils -->
  <script src="${pageContext.request.contextPath}/resources/js/utils.js?idRandom=${pageContext.session.id}"></script>
  <!-- Page level custom scripts 
  <script src="${pageContext.request.contextPath}/resources/theme/js/demo/chart-area-demo.js"></script>
  <script src="${pageContext.request.contextPath}/resources/theme/js/demo/chart-pie-demo.js"></script>
  -->