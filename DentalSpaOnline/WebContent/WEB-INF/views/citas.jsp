<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
<!-- *************************************************** -->
		<jsp:include page="templates/header.jsp" />
<!-- *************************************************** -->
    
    <!-- Calendar -->
	<link href='${pageContext.request.contextPath}/resources/calendar/packages/core/main.css' rel='stylesheet' />
	<link href='${pageContext.request.contextPath}/resources/calendar/packages/daygrid/main.css' rel='stylesheet' />
	<link href='${pageContext.request.contextPath}/resources/calendar/packages/timegrid/main.css' rel='stylesheet' />
	<link href='${pageContext.request.contextPath}/resources/calendar/packages/list/main.css' rel='stylesheet' />
    
	<style>
	  #calendar {
	    max-width: 900px;
	    margin: 0 auto;
	  }
	</style>
	</head>
<body id="page-top">
	<!-- Page Wrapper -->
	<div id="wrapper">
<!-- *************************************************** -->
		<jsp:include page="templates/menu.jsp" />
<!-- *************************************************** -->
    	<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
<!-- ***************************************************  -->
				<jsp:include page="templates/topbar.jsp" />
<!-- ***************************************************
				<!-- ****************************** Begin Page Content ****************************** -->
				<div class="container-fluid">
					<h1 class="h3 mb-4 text-gray-800">${permiso_active.procesos.procesonombre}</h1>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group citas">
								<label>Nombre</label>
								<input type="text" class="form-control" id="citanombre" placeholder="Nombre" onchange="validaCampo('NULO',this);" />
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group citas">
								<label>Comentarios</label>
								<input type="text" class="form-control" id="citacomentarios" placeholder="Comentarios" onchange="validaCampo('NULO',this);" />
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group citas">
								<label>Fecha Hora Inicio</label>
								<input type="datetime-local" class="form-control" id="citafechainicio" placeholder="Fecha Hora Inicio" onchange="validaCampo('NULO',this);" />
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group citas">
								<label>Fecha Hora Fin</label>
								<input type="datetime-local" class="form-control" id="citafechafin" placeholder="Fecha Hora Fin" onchange="validaCampo('NULO',this);" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"></div>
						<div class="col-md-4">
							<input type="button" class="btn btn-info btn-block" id="btn-add-cita" value="Agregar Cita" onclick="guardar('citas',this)"/>
						</div>
						<div class="col-md-4"></div>
					</div>
					<hr>
					 <div id='calendar'></div>
				</div>
				<!-- ******************************* End Page Content ******************************* -->
			</div>
			<!-- End of Main Content -->

<!-- *************************************************** -->
			<jsp:include page="templates/footer.jsp" />
<!-- *************************************************** -->
		</div>
		<!-- End of Content Wrapper -->
	</div>
<!-- End of Page Wrapper -->
<!-- *************************************************** -->
	<jsp:include page="templates/footer_ln.jsp" />
    <!-- Calendar -->
	<script src='${pageContext.request.contextPath}/resources/calendar/packages/core/main.js'></script>
	<script src='${pageContext.request.contextPath}/resources/calendar/packages/interaction/main.js'></script>
	<script src='${pageContext.request.contextPath}/resources/calendar/packages/daygrid/main.js'></script>
	<script src='${pageContext.request.contextPath}/resources/calendar/packages/timegrid/main.js'></script>
	<script src='${pageContext.request.contextPath}/resources/calendar/packages/list/main.js'></script>
	<script src='${pageContext.request.contextPath}/resources/js/citas.js'></script>
<!-- *************************************************** -->
</body>
</html>