<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
<!-- *************************************************** -->
		<jsp:include page="templates/header.jsp" />
<!-- *************************************************** -->
	</head>
<body id="page-top">
	<!-- Page Wrapper -->
	<div id="wrapper">
<!-- *************************************************** -->
		<jsp:include page="templates/menu.jsp" />
<!-- *************************************************** -->
    	<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
<!-- ***************************************************  -->
				<jsp:include page="templates/topbar.jsp" />
<!-- ***************************************************
				<!-- ****************************** Begin Page Content ****************************** -->
				<div class="container-fluid">
					<h1 class="h3 mb-4 text-gray-800">${permiso_active.procesos.procesonombre}</h1>
					<hr>
				</div>
				<!-- ******************************* End Page Content ******************************* -->
			</div>
			<!-- End of Main Content -->

<!-- *************************************************** -->
			<jsp:include page="templates/footer.jsp" />
<!-- *************************************************** -->
		</div>
		<!-- End of Content Wrapper -->
	</div>
<!-- End of Page Wrapper -->
<!-- *************************************************** -->
	<jsp:include page="templates/footer_ln.jsp" />
	<script src='${pageContext.request.contextPath}/resources/js/recetas.js'></script>
<!-- *************************************************** -->
</body>
</html>