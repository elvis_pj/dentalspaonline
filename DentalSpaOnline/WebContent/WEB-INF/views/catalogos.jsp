<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html lang="en">
	<head>
<!-- *************************************************** -->
		<jsp:include page="templates/header.jsp" />
<!-- *************************************************** -->
	</head>
<body id="page-top">
	<!-- Page Wrapper -->
	<div id="wrapper">
<!-- *************************************************** -->
		<jsp:include page="templates/menu.jsp" />
<!-- *************************************************** -->
    	<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
<!-- *************************************************** -->
				<jsp:include page="templates/topbar.jsp" />
<!-- *************************************************** -->
				<!-- ****************************** Begin Page Content ****************************** -->
				<div class="container-fluid">
					<h1 class="h3 mb-4 text-gray-800">${procesonombre}</h1>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>Lista de catalogos</label>
								<select class="form-control" id="tipo_catalogo">
									<option value="">Seleccionar</option>
									<c:forEach items="${listaCatalogos}" var="catalogo">
										<option value="${catalogo.key}">${catalogo.value}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group catalogo">
										<label>Nombre</label>
										<input type="text" class="form-control" id="nombre" onchange="validaCampo('NULO',this);">
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group catalogo">
										<label>Descripcion</label>
										<textarea class="form-control" id="descripcion" onchange="validaCampo('NULO',this);" style="resize:none;"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<input type="button" class="btn btn-info btn-block" id="btn-add-catalogo" value="Agregar"/>
								</div>
								<div class="col-md-4"></div>
							</div>
						</div>
					</div>
					<hr>
					<div class='row'>
						<div class="col">
							<div class="card shadow mb-4">
								<div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">Lista del catalogo</h6>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered" id="datatable_lista" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>Nombre</th>
													<th>Descripcion</th>
													<th>Fecha Creacion</th>
													<th>Estatus</th>
													<th></th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>Nombre</th>
													<th>Descripcion</th>
													<th>Fecha Creacion</th>
													<th>Estatus</th>
													<th></th>
												</tr>
											</tfoot>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ******************************* End Page Content ******************************* -->
			</div>
			<!-- End of Main Content -->

<!-- *************************************************** -->
			<jsp:include page="templates/footer.jsp" />
<!-- *************************************************** -->
		</div>
		<!-- End of Content Wrapper -->
	</div>
<!-- End of Page Wrapper -->
<!-- *************************************************** -->
	<jsp:include page="templates/footer_ln.jsp" />
	<script src="${pageContext.request.contextPath}/resources/js/catalogos.js"></script>
<!-- *************************************************** -->
</body>
</html>