<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html lang="en">
	<head>
<!-- *************************************************** -->
		<jsp:include page="templates/header.jsp" />
		<link href="${pageContext.request.contextPath}/resources/css/base.css?idRandom=${pageContext.session.id}" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- <script type="text/javascript" src="${pageContext.request.contextPath}/resources/canvas/html2canvas.js"></script>-->
	<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
	<style type="text/css">
		.custom-file-control :: after{
			content: "Select file...";
		}
		
		.custom-file-control :: before{
			content: "Click me";
		}
		
		.custom-file-control : lang(es) :: after{
			content: "Seccionar archivo...";
		}
		
		.custom-file-control : lang(es) :: before{
			content: "Seccionar";
		}
		
		.custom-file-control.selected : lang(es) :: after{
			content: "" !important;
		}
		
		.custom-file{
			content: hidden;
		}
		
		.custom-file-control{
			white-space: nowrap;
		}
	</style>
<!-- *************************************************** -->
	</head>
<body id="page-top">
	<!-- Page Wrapper -->
	<div id="wrapper">
<!-- *************************************************** -->
		<jsp:include page="templates/menu.jsp" />
<!-- *************************************************** -->
    	<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
<!-- ***************************************************  -->
				<jsp:include page="templates/topbar.jsp" />
<!-- ***************************************************
				<!-- ****************************** Begin Page Content ****************************** -->
				<div class="container-fluid">
					<h1 class="h3 mb-4 text-gray-800">${permiso_active.procesos.procesonombre}</h1>
					<div class="row">
						<div class='col-md-12'>
							<div class='row'>
								<div class='col-md-1'></div>
								<div class='col-md-10' id="container_search_result"></div>
								<div class='col-md-1'></div>
							</div>
							<hr>
							<div class='row'>
								<div class="col">
									<h3 style="text-align:center;">Informacion del Paciente</h3>
								</div>
							</div>
							<div class='row'>
				                <div class='col-md-4'>
				                      <div class='form-group'>
				                            <label>Nombre Completo</label>
				                            <input type='text' class='form-control' id='info_pacienteid' value='${paciente.pacientenombre} ${paciente.pacienteapellidopaterno} ${paciente.pacienteapellidomaterno}' disabled>
				                      </div>
				                </div>
				                <div class='col-md-3'>
				                      <div class='form-group'>
				                            <label>Sexo</label>
				                            <input type='text' class='form-control' id='info_pacientesexo' value="${paciente.pacientesexo!=null ? paciente.pacientesexo.equals('H') ? 'Hombre' : 'Mujer' : ''}" disabled>
				                      </div>
				                </div>
				                <div class='col-md-3'>
				                      <div class='form-group'>
				                            <label>Edad</label>
				                            <input type='text' class='form-control' id='info_pacienteedad' value='${paciente.pacienteedad}' disabled>
				                      </div>
				                </div>
							</div>
							<hr>
							
							<!-- INICIA FORM EXAMEN DENTAL -->
							<div id="accordion_examendental">
								<div class="card">
								
									<div class="card-header" id="headingOne" style="text-align:center;">
										<div class='row'>
											<div class="col">
												<!-- <h3 style="text-align:center;">Examen Dental</h3> -->
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapseExamenDental" aria-expanded="true" aria-controls="collapseExamenDental">
													Examen Dental
												</button>
											</div>
										</div>
									</div>
									
									<div id="collapseExamenDental" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion_examendental">
										<div class="card-body">
											<div class='row'>
								                <div class='col-md-12'>
													<div class='row'>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Higiene Bucal</label>
										                            <input type="hidden" id="pacienteid" value="${paciente.pacienteid}" />
										                            <input type='text' class='form-control' id='examendentalhigiene' placeholder="Higiene Bucal" value="${paciente.examendental.examendentalhigiene}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Encia</label>
										                            <input type='text' class='form-control' id='examendentalencia' placeholder="Encia" value="${paciente.examendental.examendentalencia}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Piso de boca</label>
										                            <input type='text' class='form-control' id='examendentalpisoboca' placeholder="Piso de boca" value="${paciente.examendental.examendentalpisoboca}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Labios</label>
										                            <input type='text' class='form-control' id='examendentallabios' placeholder="Labios" value="${paciente.examendental.examendentallabios}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
													</div>
													<div class='row'>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Carillos</label>
										                            <input type='text' class='form-control' id='examendentalcarillos' placeholder="Carillos" value="${paciente.examendental.examendentalcarillos}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Bruxismo</label>
										                            <input type='text' class='form-control' id='examendentalbruxismo' placeholder="Bruxismo" value="${paciente.examendental.examendentalbruxismo}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Chasquidos en ATM</label>
										                            <input type='text' class='form-control' id='examendentalchasquidos' placeholder="Chasquidos en ATM" value="${paciente.examendental.examendentalchasquidos}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Lengua</label>
										                            <input type='text' class='form-control' id='examendentallengua' placeholder="Lengua" value="${paciente.examendental.examendentallengua}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
													</div>
													<div class='row'>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Paladar</label>
										                            <input type='text' class='form-control' id='examendentalpaladar' placeholder="Paladar" value="${paciente.examendental.examendentalpaladar}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Supernumerarios</label>
										                            <input type='text' class='form-control' id='examendentalsupernumerarios' placeholder="Supernumerarios" value="${paciente.examendental.examendentalsupernumerarios}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Sangrado</label>
										                            <input type='text' class='form-control' id='examendentalsangrado' placeholder="Sangrado" value="${paciente.examendental.examendentalsangrado}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Placa/Sarro</label>
										                            <input type='text' class='form-control' id='examendentalplacasarro' placeholder="Placa/Sarro" value="${paciente.examendental.examendentalplacasarro}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
													</div>
													<div class='row'>
										                <div class='col-md-3'>
										                      <div class='form-group examendental'>
										                            <label>Alteraciones</label>
										                            <input type='text' class='form-control' id='examendentalalteraciones' placeholder="Alteraciones" value="${paciente.examendental.examendentalalteraciones}" onchange="validaCampo('NULO',this);"/>
										                      </div>
										                </div>
										                <div class='col-md-6'>
										                      <div class='form-group examendental'>
										                            <label>Notas</label>
																	<textarea class="form-control" id="examendentalnota" placeholder="Notas" rows="3" onchange="validaCampo('NULO',this);" style="resize:none;">${paciente.examendental.examendentalnota}</textarea>
										                      </div>
										                </div>
										                <div class='col-md-3'>
										                </div>
													</div>
													<c:set var="isDisabled" value=""/>
													<c:if test="${paciente.examendental.examendentalid>0}">
														<c:set var="isDisabled" value="none"/>
													</c:if>
													<c:if test="${paciente==null || paciente.pacienteid<1}">
														<c:set var="isDisabled" value="none"/>
													</c:if>
													<div class='row'>
										                <div class='col-md-4'>
										                </div>
										                <div class='col-md-4'>
										                	<input type="button" class="btn btn-primary btn-block" id="btn-guardar-examendental" value="Guardar Examen Dental" onclick="guardar('examendental',this);" style="display: <c:out value="${isDisabled}"/>;"/>
										                </div>
										                <div class='col-md-4'>
										                </div>
													</div>
								                </div>
											</div>
										</div>
									</div>
							
								</div>
							</div>
							<hr>
							<!-- TERMINA FORM EXAMEN DENTAL -->
							
							<!-- INICIAL LISTA DE TRATAMIENTOS ACTUALES -->
							<div id="accordion_listatratamientos">
								<div class="card">
								
									<div class="card-header" id="headingOne" style="text-align:center;">
										<div class='row'>
											<div class="col">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapseListatratamientos" aria-expanded="true" aria-controls="collapseListatratamientos">
													Lista de tratamientos del paciente
												</button>
											</div>
										</div>
									</div>
									
									<div id="collapseListatratamientos" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion_listatratamientos">
										<div class="card-body">
											<div class='row'>
												<div class="col">
													<div class="card shadow mb-4">
														<div class="card-header py-3">
															<h6 class="m-0 font-weight-bold text-primary">Tratamientos</h6>
														</div>
														<div class="card-body">
															<div class="table-responsive">
																<table class="table table-bordered" id="datatable_tratamientos" width="100%" cellspacing="0">
																	<thead>
																		<tr>
																			<th>Tratamiento</th>
																			<th>Estatus</th>
																			<th>Fecha inicio</th>
																			<th>Ultima Visita</th>
																			<th></th>
																		</tr>
																	</thead>
																	<tfoot>
																		<tr>
																			<th>Tratamiento</th>
																			<th>Estatus</th>
																			<th>Fecha inicio</th>
																			<th>Ultima Visita</th>
																			<th></th>
																		</tr>
																	</tfoot>
																	<tbody>
																		<c:forEach items="${paciente.tratamientos}" var="tratamiento">
																			<tr>
																			  <td>${tratamiento.tipotratamiento.tipotratamientonombre}</td>
																			  <td>${tratamiento.tratamientoterminado ? 'Terminado' : 'Activo' }</td>
																			  <td>${tratamiento.tratamientofechacreacion}</td>
																			  <td>${tratamiento.tratamientofechamodificacion}</td>
																			  <td><input type="button" class="btn btn-info" value="Cargar" onclick=""/></td>
																			</tr>
																		</c:forEach>
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								</div>
							</div>
							<hr>
							<!-- FINALIZA LISTA DE TRATAMIENTOS ACTUALES -->
							
							<div class='row'>
								<div class="col">
									<h3 style="text-align:center;" id="titulo-tipovisita">Agregar tratamiento</h3>
								</div>
							</div>
							<div class='row'>
								<div class="col-md-3">
									<div class="form-group tratamiento">
										<label>Tratamiento</label>
										<input type="hidden" id="pacienteid" value="${paciente.pacienteid}" />
										<select class="form-control tratamiento" id="tipotratamientoid" placeholder="Tipo Tratamiento">
											<option value="">Tipo tratamientos</option>
											<c:forEach items="${tipotratamientos}" var="tipo">
												<option value="${tipo.tipotratamientoid}">${tipo.tipotratamientonombre}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group tratamiento">
										<label>Plan</label>
										<input type="text" class="form-control" id="tratamientoplan" placeholder="Plan" onchange="" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group tratamiento">
										<label>Comentarios</label>
										<textarea class="form-control" id="tratamientocomentariosiniciales" placeholder="Comentarios" rows="5" style="resize:none;"></textarea>
									</div>
								</div>
							</div>
							<!-- AQUI -->
							<div id="container_view">
								<!-- AQUI -->
								<!-- 	ODONTROGRAMA	 -->
								<div class="row">
									<div class="col" id="odontograma-div">
										<div class="card shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Odontograma</h6>
											</div>
											<br>
							                <div class="row" id="controls">
							                    <div class="col" style="text-align:center;">
					                                <div class="btn-group" data-toggle="buttons">
					                                    <label id="caries" class="btn btn-warning active">
					                                        <input type="radio" name="options" id="option1" autocomplete="off" checked>Caries
					                                    </label>
					                                    <label id="restauracion" class="btn btn-primary">
					                                        <input type="radio" name="options" id="option2" autocomplete="off"> Restauracion
					                                    </label>
					                                    <label id="ausencia" class="btn bg-gray-600 text-white">
					                                        <input type="radio" name="options" id="option3" autocomplete="off"> Ausencia
					                                    </label>
					                                    <label id="extraer" class="btn btn-danger">
					                                        <input type="radio" name="options" id="option4" autocomplete="off"> Extraccion
					                                    </label>
					                                    <label id="r.desajustada" class="btn btn-info">
					                                        <input type="radio" name="options" id="option5" autocomplete="off"> R. Desajustada
					                                    </label>
					                                    <label id="limpiar" class="btn btn-success">
					                                        <input type="radio" name="options" id="option7" autocomplete="off"> Limpiar
					                                    </label>
					                                </div>
							                    </div>
							                </div>
							                <hr>
							                <div class="row">
							                	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="tr">
							                    </div>
							                	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="tl">
							                    </div>
							                </div>
							                <div class="row">
							                	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="tlr">
							                    </div>
							                	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="tll">
							                    </div>
							                </div>
							                <div class="row">
							                	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="blr">
							                    </div>
							                	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="bll">
							                    </div>
							                </div>
							                <div class="row">
							                	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="br">
							                    </div>
							                	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="bl">
							                    </div>
							                </div>
							                <hr>
							                <div class="row">
							                    <div class="col">
													Caries = <div style="height: 20px; width:20px; display:inline-block;" class="click-caries"></div>
							                   	</div>
							                    <div class="col">
													Restauracion = <div style="height: 20px; width:20px; display:inline-block;" class="click-restauracion"></div>
												</div>
							                    <div class="col">
													Ausencia = <div style="height: 20px; width:20px; display:inline-block;" class="click-ausencia"></div>
												</div>
							                    <div class="col">
													Extracción = <div style="height: 20px; width:20px; display:inline-block;" class="click-extraer"></div>
							                    </div>
							                    <div class="col">
													R. Desajustada = <div style="height: 20px; width:20px; display:inline-block;" class="click-r-desajustada"></div>
												</div>
							                </div>
							                <hr>
										</div>
									</div>
								</div>
								<!-- 	ODONTROGRAMA	 -->
								<hr>
								<div class='row'>
									<div class="col">
										<h3 style="text-align:center;">Detalle del tratamiento</h3>
										<div class="row">
											<div class="col">
												<!-- Basic Card Example -->
												<div class="card shadow mb-4">
													<div class="card-header py-3">
														<h6 class="m-0 font-weight-bold text-primary">Detalle del tratamiento</h6>
													</div>
													<div class="card-body" id="container_detalle_tratamiento">
														<!-- 
														<div class="card mb-4 py-3 border-left-primary">
															<div class="card-body">.border-left-primary</div>
														</div>
														-->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<div class='row'>
									<div class="col">
										<h3 style="text-align:center;">Evidencias</h3>
										<div class="row">
											<div class="col">
												<!-- Basic Card Example -->
												<div class="card shadow mb-4">
													<div class="card-header py-3">
														<h6 class="m-0 font-weight-bold text-primary">Evidencias <input type="button" class="btn btn-info" onclick="addEvidencia()" value="+" /></h6>
													</div>
													<div class="card-body" id="container_evidencias">
														<!-- 
														<div class="card mb-4 py-3 border-left-primary">
															<div class="card-body">.border-left-primary</div>
														</div>
														-->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<c:set var="isDisabled" value=""/>
								<c:if test="${paciente==null || paciente.pacienteid<1 || paciente.examendental.examendentalid<1}">
									<c:set var="isDisabled" value="none"/>
								</c:if>
								<div class="row">
					                <div class='col-md-4'>
					                </div>
					                <div class='col-md-4'>
					                	<input type="submit" class="btn btn-primary btn-block" id="btn-guardar-tratamiento" value="Guardar Tratamiento" style="display: <c:out value="${isDisabled}"/>;"/>
					                </div>
					                <div class='col-md-4'>
					                </div>
								</div>
								<hr>
							<!-- AQUI -->
							</div>
							<!-- AQUI -->
						</div>
					</div>
				</div>
				<!-- ******************************* End Page Content ******************************* -->
			</div>
			<!-- End of Main Content -->
<!-- *************************************************** -->
			<jsp:include page="templates/footer.jsp" />
<!-- *************************************************** -->
		</div>
		<!-- End of Content Wrapper -->
	</div>
<!-- End of Page Wrapper -->
<!-- *************************************************** -->
	<jsp:include page="templates/footer_ln.jsp" />
	<script src="${pageContext.request.contextPath}/resources/js/tratamientos.js"></script>
									
	<c:if test="${paciente.pacienteid>0 && paciente.examendental.examendentalid>0}">
		<script type="text/javascript">
			disabledMasive("examendental", true);
		</script>
	</c:if>
<!-- *************************************************** -->
</body>
</html>