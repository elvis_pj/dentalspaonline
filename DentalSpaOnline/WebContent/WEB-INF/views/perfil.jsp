<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html lang="en">
	<head>
<!-- *************************************************** -->
		<jsp:include page="templates/header.jsp" />
<!-- *************************************************** -->
	</head>
<body id="page-top">
	<!-- Page Wrapper -->
	<div id="wrapper">
<!-- *************************************************** -->
		<jsp:include page="templates/menu.jsp" />
<!-- *************************************************** -->
    	<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
<!-- *************************************************** -->
				<jsp:include page="templates/topbar.jsp" />
<!-- *************************************************** -->
				<!-- ****************************** Begin Page Content ****************************** -->
				<div class="container-fluid">
					<h1 class="h3 mb-4 text-gray-800">${permiso_active.procesos.procesonombre}</h1>
					<form action="#" method="post" enctype="multipart/form-data" id="form_update_profile_picture">
						<div class="row">
			                <div class="col-md-3">
								<div class="row" style="text-align:center;">
					                <div class="col-md">
										<img src="${pageContext.request.contextPath}/dashboard/perfil/obtieneFotoPerfil" class="rounded-circle" width="150" height="150" alt="Perfil"id="preview"/>
									</div>
								</div>
								<div class="row">
					                <div class="col-md">
										<div class="custom-file">
											<input type="file" accept="image/*;capture=camera" class="custom-file-input" id="customFileProfilePicture" name="customFileProfilePicture" lang="es">
											<label class="custom-file-label" for="customFileProfilePicture">Actualizar Foto Perfil</label>
										</div>
									</div>
								</div>
							    <div class="row" style="text-align:center;">
					                <div class="col-md">
					                	<input type="submit" class="btn btn-info" id="btn-upload-profilepicture" value="Guardar Foto"/>
					                </div>
								</div>
							    <div class="row">
								    <div class="col-md-12">
								    	<div class="form-group">
								    		<div class="progress">
									     	<div id="progressBar1" class="progress-bar progress-bar-success" role="progressbar"
									        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
									        <div id="alertMsg1" style="color: red;font-size: 12px;"></div>
									    	</div>
								    	</div>
								    </div>																    																    
								</div>
			                </div>
			                <div class="col-md-9">
								<div class="row">
					                <div class="col-md">
					                    <div class="form-group">
					                        <label for="usuarionombre">Nombre</label>
					                        <input type="text" class="form-control" value="${usuariosession.usuarionombre}" readonly>
					                    </div>
					                </div>
					                <div class="col-md">
					                    <div class="form-group">
					                        <label for="perfil.perfilnombre">Perfil</label>
					                        <input type="text" class="form-control" value="${usuariosession.perfil.perfilnombre}" readonly>
					                    </div>
					                </div>
								</div>
								<div class="row">
					                <div class="col-md">
					                    <div class="form-group">
					                        <label for="usuarioactivo">Estatus</label>
					                        <input type="text" class="form-control" value="${usuariosession.usuarioactivo ? 'Activo' : 'Inactivo'}" readonly>
					                    </div>
					                </div>
					                <div class="col-md">
					                    <div class="form-group">
					                        <label for="usuariofechaultimoacceso">Fecha Ultimo Acceso</label>
					                        <input type="text" class="form-control" value="<fmt:formatDate pattern="yyyy-MM-dd HH:mm:dd" value="${usuariosession.usuarioultimoacceso}" />" readonly>
					                    </div>
					                </div>
								</div>
								<div class="row">
					                <div class="col-md-4"></div>
					                <div class="col-md-4">
					                    <div class="form-group">
					                		<input type="button" class="btn btn-primary btn-block" value="Cambiar contrasenia" id="btn-show-form-cambiocontraenia"/>
					                	</div>
					                </div>
					                <div class="col-md-4"></div>
								</div>
							</div>
						</div>
					</form>
			        <div class="row" id="form-cambiocontrasenia" style="display:none;">
					    <div class="col-md-3"></div>
					    <div class="col-md-6">
					        <form id="form-cambiocontrasenia">
					            <div class="form-group">
					                <label>Usuario</label>
					                <input type="text" id="usuariocorreo" class="form-control" value="${usuariosession.usuariocorreo}" readonly/>
					            </div>
					            <div class="form-group">
					                <label>Contraseña actual</label>
					                <input class="form-control" placeholder="Contraseña Actual" id="usuariopwd_old" type="password" required>
					            </div>
					            <div class="form-group">
					                <label>Nueva Contraseña</label> 
					                <input class="form-control" placeholder="Contraseña Nueva" id="usuariopwd_new" type="password" onchange="validaCampo('PASS',this);" required>
					            </div>
					            <div class="form-group">
					                <label>Confirmar Nueva Contraseña</label> 
					                <input class="form-control" placeholder="Confirma Contraseña" id="usuariopwd_new_conf" type="password" onchange="validaCampo('PASS',this);" required>
					            </div>
					            <div style="text-align:center;">
						            <input type="button" class="btn btn-success" id="button_restablecer_contrasenia" value="Guardar"/>
						            <input type="button" class="btn btn-danger" id="button_cancelar_restablecer_contrasenia" value="Cancelar"/>
								</div>
					        </form>
					    </div>                    
					    <div class="col-md-3"></div>
					</div>
				</div>
				<!-- ******************************* End Page Content ******************************* -->
			</div>
			<!-- End of Main Content -->
<!-- *************************************************** -->
			<jsp:include page="templates/footer.jsp" />
<!-- *************************************************** -->
		</div>
		<!-- End of Content Wrapper -->
	</div>
<!-- End of Page Wrapper -->
<!-- *************************************************** -->
	<jsp:include page="templates/footer_ln.jsp" />
	<script src="${pageContext.request.contextPath}/resources/js/perfil.js?idRandom=${pageContext.session.id}"></script>
<!-- *************************************************** -->
</body>
</html>