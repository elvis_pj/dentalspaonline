<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="${pageContext.request.contextPath}/resources/img/clinica_favicon.ico">

  <title>Login</title>

  <!-- Custom fonts for this template
  <link href="${pageContext.request.contextPath}/resources/theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="${pageContext.request.contextPath}/resources/theme/css/sb-admin-2.min.css?version=10" rel="stylesheet">

  <script src="${pageContext.request.contextPath}/resources/js/login.js"></script>
  <style type="text/css">
	.div_login{
		background-color: rgba(0,0,0,.5);
        border-radius: 2rem;
	}
  </style>
</head>

<body class="bg-gradient-login" onload="mostrarMensajes('${view_mensaje_jsp}','${view_error_jsp}');">
	<div id="containerMostrarMensajes"></div>
	<div class="container" style="padding: 2.5%; padding-top: 10%;">
	    <div class="row">
	        <div class="col-xl-12 col-lg-12 col-md-12">
	            <div class="row">
	                <div class="col-lg-3"></div>
	                <div class="col-lg-6">
	                    <div class="p-5 div_login">
	                        <div class="text-center">
	                            <h1 class="h4 text-gray-100 mb-4">Clinica Aguero Cardoso</h1>
	                        </div>
	                        <form:form action="loginProcess" modelAttribute="usuario" method="POST">
	                            <div class="form-group">
	                                <form:input class="form-control form-control-user" path="usuariocorreo" aria-describedby="emailHelp" placeholder="Enter Email Address..." required="true" />
	                            </div>
	                            <div class="form-group">
	                                <form:password class="form-control form-control-user" path="usuariopwd" placeholder="Password" required="true" />
	                            </div>
	                            <button type="submit" class="btn btn-primary btn-user btn-block">Ingresar</button>
	                        </form:form>
	                        <hr>
	                        <div class="text-center">
	                            <a class="small text-gray-100" href="#" onclick="alert('No disponible');">Forgot Password?</a>
	                        </div>
	                        <div class="text-center">
	                            <a class="small text-gray-100" href="#" onclick="alert('No disponible');">Create an Account!</a>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-lg-3"></div>
	            </div>
	        </div>
	    </div>
	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="${pageContext.request.contextPath}/resources/theme/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	
	<!-- Core plugin JavaScript-->
	<script src="${pageContext.request.contextPath}/resources/theme/vendor/jquery-easing/jquery.easing.min.js"></script>
	
	<!-- Custom scripts for all pages-->
	<script src="${pageContext.request.contextPath}/resources/theme/js/sb-admin-2.min.js"></script>
	
	<!-- SweetAlert -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>
