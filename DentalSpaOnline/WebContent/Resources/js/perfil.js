

$(document).ready(function()
{
	$("#btn-show-form-cambiocontraenia").click(function(){
		$("#form-cambiocontrasenia").toggle(true);
	});
	
	$("#button_restablecer_contrasenia").click(function(){
		var jsonRequest=validaFormularioRestaura();
		if(jsonRequest!=undefined && jsonRequest!=null)
		{
			doCambiaContrsenia(jsonRequest, this);
		}
	});
	
	$("#button_cancelar_restablecer_contrasenia").click(function(){
		$("#form-cambiocontrasenia").toggle(false);
	});
	
	$("#form_update_profile_picture").submit(function(event){
		event.preventDefault();
	    var formData = new FormData(this);
		console.log("Guarda imagen :: "+formData.id);
	    console.log(formData);
		doCambiaFotoPerfil(formData, document.getElementById("btn-upload-profilepicture"));
	});
});

function validaFormularioRestaura()
{
	var error="";
	var in_usuariopwd_old=$("#usuariopwd_old").val();
	var in_usuariopwd_new=$("#usuariopwd_new").val();
	var in_usuariopwd_new_conf=$("#usuariopwd_new_conf").val();

	if(in_usuariopwd_old==undefined || in_usuariopwd_old==null || in_usuariopwd_old.trim().length<1)
	{
		error+="<li>Es necesario indicar la contrasenia actual";
		in_usuariopwd_old="";
	}
	if(in_usuariopwd_new==undefined || in_usuariopwd_new==null || in_usuariopwd_new.trim().length<1)
	{
		error+="<li>No indico la contrasenia nueva";
		in_usuariopwd_new="";
	}
	if(in_usuariopwd_new_conf==undefined || in_usuariopwd_new_conf==null || in_usuariopwd_new_conf.trim().length<1)
	{
		error+="<li>Hace falta indicar la confirmacion de la contrasenia nueva";
	}
	else
	{
		if(in_usuariopwd_new!=in_usuariopwd_new_conf)
		{
			error+="<li>No coinciden las contrasenias";
		}
	}
	if(in_usuariopwd_old==in_usuariopwd_new)
	{
		error+="<li>La nueva contrasenia es igual a la actual";
	}
	if(error=="")
	{
		return {
			"rq_usuariopwd_old" : in_usuariopwd_old,
			"rq_usuariopwd_new" : in_usuariopwd_new};
	}
	mostrarError(error);
	return null;
}

function doCambiaFotoPerfil(dataRequest, objectElement)
{
	$.ajax(
	{
		url : 'cambiaFotoPerfil',
		type : 'POST',
		data:dataRequest,
		enctype: 'multipart/form-data',
		dataType : 'json',
		cache : false,
		contentType : false,
		processData : false,
	    xhr: function(){
	    	var xhr = $.ajaxSettings.xhr() ;
	    	xhr.upload.onprogress = function(event){
				var perc = Math.round((event.loaded / event.total) * 100);
				$("#progressBar1").text(perc + "%");
				$("#progressBar1").css("width",perc + "%");
	    	};
	    	return xhr ;
	    },
		beforeSend : function( xhr ) {
    		$("#progressBar1").css("width","0%");
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
            }
			console.log("Request Service...");
			$(objectElement).prop("disabled", true);
		},
		success : function(jsonResponse)
		{
			console.log(JSON.stringify(jsonResponse));
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
				if(jsonResponse._State=="true")
				{
					mostrarSuccess(jsonResponse._Message);
					$("#preview").attr("src",$("#preview").attr("src")+"?up=1");
					$("#img_profile_picture").attr("src",$("#img_profile_picture").attr("src")+"?up=1");
					
				}else
				{
					mostrarError(jsonResponse._Message);
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [doCambiaFotoPerfil]");
			$(objectElement).prop('disabled', false);
	    }
	});
}

function doCambiaContrsenia(jsonRequest, objectElement)
{
	$.ajax(
	{
        async: true,
		url : 'cambiaContrasenia',
		type : 'POST',
		dataType : 'json',
		data:{	"ParametersRequest": JSON.stringify(jsonRequest)
		},
		beforeSend : function( xhr ) {
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
            }
			console.log("Request Service...");
			$(objectElement).prop('disabled', true);
			ShowLoadBar();
		},
		success : function(jsonResponse)
		{
			console.log(JSON.stringify(jsonResponse));
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
//				console.log("valida:: "+jsonResponse._State);
				if(jsonResponse._State=="true")
				{
					mostrarSuccess(jsonResponse._Message);
					
				}else
				{
					mostrarError(jsonResponse._Message);
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [CambiaContrasenia]");
			$(objectElement).prop('disabled', false);
			HiddenLoadBar()
	    }
	});
}