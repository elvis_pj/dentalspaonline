var listaCitas=[];

$(document).ready(function(){
	$('#in-search').keydown(function(event) {
		if (event.keyCode || event.which === 13) {
			
		}
	})
 });
 
 function cargarListaCitas()
 {
  	$.ajax(
  	{
        async: true,
  		url : '/citas/listaCitas',
  		type : 'POST',
  		dataType : 'json',
  		success : function(jsonResponse)
  		{
  			if(jsonResponse._State=='REDIRECT')
  			{
  				console.log("Error en la peticion:: "+jsonResponse._Message);
  				mostrarError(jsonResponse._Message);
  				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
  			}
  			else
  			{
  				if(jsonResponse._State=="true")
  				{
  					var citas=JSON.parse(jsonResponse._ObjectJson);
  					listaCitas = [];
  					for(var i=0; i<citas.length; i++)
  					{
  						listaCitas.push({
  							title: citas[i].citanombre+" :: "+citas[i].citacomentarios,
  							start: new Date(citas[i].citafechainicio).toJSON(), 
  							end: new Date(citas[i].citafechafin).toJSON()
  						});
  					}
  					console.log("ListaCitas :: "+JSON.stringify(listaCitas));
  					cargarCalendario();
  				}
  				else
  				{
  					mostrarError(jsonResponse._Message);
  				}
  			}
  		},
  	    error : function(jqXHR, exception)
  	    {
  	    	console.log("["+exception.statusText+"] ", exception);
  	    	console.log("info: "+ exception.statusText);
  	    	alert(getErrorMessage(jqXHR, exception));
  	    },
  	    complete : function()
  	    {
  	    	console.log("Completed [ListaParentescos]");
  	    }
  	});
  }

  function cargarCalendario()
  {
	  $("#calendar").html("");
      var calendarEl = document.getElementById('calendar');

      var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'list' ],

        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'listDay,listWeek,dayGridMonth'
        },

        // customize the button names,
        // otherwise they'd all just say "list"
        views: {
          listDay: { buttonText: 'list day' },
          listWeek: { buttonText: 'list week' }
        },

        defaultView: 'listWeek',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: listaCitas
      });

      calendar.render();
  }