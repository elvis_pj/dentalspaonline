/*
 * js para ayudar a valida cualquier tipo de campo
 * 
 */
var loader = "<br><br><br><h3 style='text-align: -webkit-center;'>Resolviendo peticion,<br> Espere por favor</h3><br><br><br><div class='loader'></div>";

function validaCampo(tipo,objeto)
{
	console.log("Valida["+objeto.id+"] Tipo["+tipo+"] Value["+objeto.value+"]");
	switch(tipo)
	{
		case "INTEGER":
			var ExpresionRegularNumeros=/^\d*$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularNumeros.test(objeto.value))
			{
				console.log("El campo solo recibe numeros. "+objeto.value);
				mostrarError("El campo solo recibe numeros. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "DOUBLE":
			var ExpresionRegularDouble=/^\d*\.\d*$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularDouble.test(objeto.value))
			{
				console.log("El campo recibe numeros con punto decimal. "+objeto.value);
				mostrarError("El campo recibe numeros con punto decimal. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "STRING":
			var ExpresionRegularLetras=/^[a-zA-Z\s]*$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularLetras.test(objeto.value))
			{
				console.log("El campo solo recibe letras, por favor omite caracteres especiales. "+objeto.value);
				mostrarError("El campo solo recibe letras, por favor omite caracteres especiales. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "EMAIL":
			var ExpresionRegularEmail=/^.[\w-_\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularEmail.test(objeto.value))
			{
				console.log("El correo capturado es incorrecto. "+objeto.value);
				mostrarError("El correo capturado es incorrecto. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "CURP":
			var ExpresionRegularCURP = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;			
			if(objeto.value!=undefined && objeto.value!="")
			{
				if(!ExpresionRegularCURP.test(objeto.value.toUpperCase()))
				{
					console.log("CURP capturado es incorrecto. "+objeto.value);
					mostrarError("La CURP que se capturaro es incorrecto. Por favor valida tu informacion e intentalo nuevamente.");
					objeto.value="";
					return false;
				}
				else
					objeto.value=objeto.value.toUpperCase();
			}
		break;
		case "RFC":
			var ExpresionRegularRFC=/^([A-ZÑ\x26]{3}|[A-Z][AEIOU][A-Z]{2})\d{2}((01|03|05|07|08|10|12)(0[1-9]|[12]\d|3[01])|02(0[1-9]|[12]\d)|(04|06|09|11)(0[1-9]|[12]\d|30))[A-Z0-9]{2}[0-9A]$/;
			if(objeto.value!=undefined && objeto.value!="")
			{
				if(!ExpresionRegularRFC.test(objeto.value.toUpperCase()))
				{
					console.log("RFC capturado es incorrecto. "+objeto.value);
					mostrarError("El RFC que se capturaro es incorrecto. Por favor valida tu informacion e intentalo nuevamente.");
					objeto.value="";
					return false;
				}
				else
					objeto.value=objeto.value.toUpperCase();
			}
		break;
		case "CADENA":
			var ExpresionRegularLetras=/^[a-zA-Z0-9\s]*$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularLetras.test(objeto.value))
			{
				console.log("El campo solo recibe letras, por favor omite caracteres especiales. "+objeto.value);
				mostrarError("El campo solo recibe letras, por favor omite caracteres especiales. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "NULO":
			if(objeto.value!=undefined && (objeto.value==" " || objeto.value.trim().length==0))
			{
				console.log("El campo esta vacio, por favor completa este campo. "+objeto.value);
				mostrarError("El campo esta vacio, por favor completa este campo. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "PASS":
			var logerror="";
			var expresionCompletaSimple=/^([A-Za-z\d]|[^ ]){4,15}$/;
			var lestrass=/^.*[A-Za-z].*$/;
			var numeross=/^.*\d.*$/;
			var espaciosblancoo=/^.* .*$/;
			var caracteresEspecialess=/^.*[$@!%*?&].*$/;
			if(!expresionCompletaSimple.test(objeto.value))
			{
				console.log("Password no supera las validaciones ["+objeto.value+"]");
				logerror="La contrasenia capturada no cumple con los requisitos minimos de complejidad.<br>";

				if(objeto.value.length<4 || objeto.value.length>15)
					logerror+=">Debe tener entre 4 y 15 caracteres de largo<br>";

				if(objeto.value.search(lestrass))
					logerror+=">Se recomienda incluir almenos una letra<br>";

				if(objeto.value.search(numeross))
					logerror+=">Se recomienda incluir almenos un numero<br>";

				if(objeto.value.search(caracteresEspecialess))
					logerror+=">Los caracteres especiales permitidos son [$@!%*?&]<br>";

				if(!objeto.value.search(espaciosblancoo))
					logerror+=">No es permitido incluir espacios en blanco<br>";
				logerror+="Por favor verifica tu contrasenia e ingresala nuevamente";
				console.log(logerror);
				mostrarError(logerror);
				objeto.value="";
				return false;
			}
		break;
		case "_PASS":
			var logerror="";
			var expresionCompleta=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@!%*?&])([A-Za-z\d]|[^ ]){8,15}$/;
			var lestrasMayusculas=/^.*[A-Z].*$/;
			var lestrasMinisuculas=/^.*[a-z].*$/;
			var caracteresEspeciales=/^.*[$@!%*?&].*$/;
			var numeros=/^.*\d.*$/;
			var espaciosblanco=/^.* .*$/;
			if(!expresionCompleta.test(objeto.value))
			{
				console.log("Password no supera las validaciones ["+objeto.value+"]");
				logerror="La contrasenia capturada no cumple con los requisitos minimos de complejidad.<br>";

				if(objeto.value.length<7 || objeto.value.length>15)
					logerror+=">Debe tener entre 8 y 15 caracteres de largo<br>";

				if(objeto.value.search(lestrasMayusculas))
					logerror+=">Debe tener al menos una letra mayuscula<br>";

				if(objeto.value.search(lestrasMinisuculas))
					logerror+=">Debe tener al menos una letra minuscula<br>";

				if(objeto.value.search(numeros))
					logerror+=">Debe tener al menos un numero<br>";

				if(objeto.value.search(caracteresEspeciales))
					logerror+=">Debe tener al menos un caracter especial [$@!%*?&]<br>";

				if(!objeto.value.search(espaciosblanco))
					logerror+=">No es permitido incluir espacios en blanco<br>";
				logerror+="Por favor verifica tu contrasenia e ingresala nuevamente";
				console.log(logerror);
				mostrarError(logerror);
				objeto.value="";
				return false;
			}
		break;
		case "IMSS":
			
		break;
		case "INF":
			
		break;
		case "TEL":
			var ExpresionRegularTelefono=/^[0-9\s]{10}$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularTelefono.test(objeto.value))
			{
				console.log("El numero de telefono debe contener solo 10 numeros. "+objeto.value);
				mostrarError("El numero de telefono o celular debe contener solo 10 numeros. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "CP":
			var ExpresionRegularCP=/^[0-9\s]{5}$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularCP.test(objeto.value))
			{
				console.log("El coigo postal debe contener 5 numeros. "+objeto.value);
				mostrarError("El coigo postal debe contener 5 numeros. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		default:
			console.log("El tipo de dato no esta soportado para validacion["+tipo+"]");
			alert("El tipo de dato no esta soportado para validacion["+tipo+"]");
			return false;
	}
	return true;
}

function isInt(value) {
    var patt = new RegExp('^[^?\.?\s]*[0-9]$');
    var res = patt.test(value);
    return res;
}

function mostrarError(error)
{
//	swal({
//		  title: "Operacion Erronea!",
//		  text: error,
//		  icon: "error",
//		  dangerMode: true ,
//	});
	Swal.fire({
		  icon: 'error',
		  title: 'Operacion Erronea!',
		  text: error
//		  footer: '<a href>Why do I have this issue?</a>'
		});
}

function mostrarInfo(info)
{
//	swal({
//		  title: "Info",
//		  text: info,
//		  icon: "info",
//		  dangerMode: true ,
//	});
	Swal.fire({
		  icon: 'info',
		  title: 'Info!',
		  text: info
//		  footer: '<a href>Why do I have this issue?</a>'
		});
}

function mostrarSuccess(success)
{
//	swal({
//		  title: "Operacion Exitosa!",
//		  text: success,
//		  icon: "success",
//		  dangerMode: true ,
//	});
	Swal.fire({
		  icon: 'success',
		  title: 'Operacion Exitosa!',
		  text: success
//		  footer: '<a href>Why do I have this issue?</a>'
		});
}

function confirmarMovimiento(id_movimiento, arrayParam, ObjectElement)
{
	Swal.fire({
		  title: 'Estas seguro?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si, eliminar.'
		}).then((result) => {
		  if (result.value) {
			  executeFunction(id_movimiento, arrayParam, ObjectElement);
		  }
		});
}

function executeFunction(id_movimiento, arrayParam, ObjectElement)
{
	switch(parseInt(id_movimiento))
	{
		case 1:
			addEliminar(ObjectElement, arrayParam[0], arrayParam[1]);
		break;
		default:
			mostrarError("El id del movimiento no ests soportado");
	}
}

function validaForm(idForm) {
	var error = '';
	$("div[class='form-group "+idForm+"']").children().each(function() {
		if (this.type != undefined)
		{
			if (this.value == undefined || this.value == null
					|| String(this.value).trim().length < 1) 
			{
				console.log("Value:: ["+this.value+"] id:: ["+this.id+"] type:: "+this.type);
				error += "<br> Es necesario completar " + $(this).attr("placeholder")+"\n";
			}
		}
	});
	return error;
}

function getJsonForm(idForm) {
	var jsonForm = {};
	
	$("div[class='form-group "+idForm+"']").children().each(function() {
		if (this.type != undefined){
			if (this.value != undefined || this.value != null
					|| String(this.value).trim().length > 0) {
//				console.log("Value:: ["+this.value+"] id:: ["+this.id+"] type:: "+this.type);
				jsonForm[this.id]=String(this.value).trim();
			}
		}
	});
	return jsonForm;
}

function disabledMasive(form, state)
{
	$("div[class='form-group "+form+"']").children().each(function () {
		if(this.type!=undefined)
		{
			if(this.type!="hidden")
				$(getTipoElemento(this.type)+"[id='"+this.id+"']").prop("disabled", state);
		}
	});
}

function limpiarValores(tipoFormulario)
{
	$("div[class='form-group "+tipoFormulario+"']").children().each(function () {
		if(this.type!=undefined)
		{
			if(this.type!="hidden")
				$(getTipoElemento(this.type)+"[id='"+this.id+"']").val("");
		}
	});
}

function buscaPaciente(parametersRequest, objectElement, idcontainer)
{
	$.ajax(
	{
        async: true,
		url : '/pacientes/buscarPaciente',
		type : 'POST',
		dataType : 'json',
		data:{	"ParametersRequest": JSON.stringify(parametersRequest)
		},
		beforeSend : function( xhr ) {
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
            }
			console.log("Request Service...");
			$(objectElement).prop('disabled', true);
			ShowLoadBar();
		},
		success : function(jsonResponse)
		{
//			console.log(JSON.stringify(jsonResponse));
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
				if(jsonResponse._State=="true")
				{
					var listaResultados = JSON.parse(jsonResponse._ObjectJson);

					$("#"+idcontainer).html(""
							+"<div class='card shadow mb-4'>"
							+"	<div class='card-header py-3'>"
							+"		<h6 class='m-0 font-weight-bold text-primary'>Resultados de la busqueda</h6>"
							+"	</div>"
							+"	<div class='card-body'>"
							+"		<div class='table-responsive'>"
							+"			<div class='table-responsive'>"
							+"				<table class='table table-bordered' id='Utils_DataTable' width='100%' cellspacing='0'>"
							+"					<thead>"
							+"						<th>Nombre</th>"
							+"						<th>Apellido Paterno</th>"
							+"						<th>Apellido Materno</th>"
							+"						<th>Edad</th>"
							+"						<th>Fecha Registro</th>"
							+"						<th></th>"
							+"					</thead>"
							+"					<tfoot>"
							+"						<th>Nombre</th>"
							+"						<th>Apellido Paterno</th>"
							+"						<th>Apellido Materno</th>"
							+"						<th>Edad</th>"
							+"						<th>Fecha Registro</th>"
							+"						<th></th>"
							+"					</tfoot>"
							+"					<tbody id='tbody_container_result_search'>"
							+"					</tbody>"
							+"				</table>"
							+"			</div>"
							+"		</div>"
							+"	</div>"
							+"</div>");
					var Utils_DataTable=$("#Utils_DataTable").DataTable();
					
					for(var i=0; i<listaResultados.length; i++)
					{
						Utils_DataTable.row.add(
								[ 
									listaResultados[i].pacientenombre,
									listaResultados[i].pacienteapellidopaterno,
									listaResultados[i].pacienteapellidomaterno,
									listaResultados[i].pacienteedad,
									new Date(listaResultados[i].pacientefechacreacion).toLocaleString(),
									"<input type='button' class='btn btn-info' value='Carga Info' onclick='loadInfo("+listaResultados[i].pacienteid+")'>"
									+"<div id='cont_info_paciente_id-"+listaResultados[i].pacienteid+"' data-json_paciente='"+JSON.stringify(listaResultados[i])+"'></div>"
								]).draw();
					}
				}else
				{
					mostrarError(jsonResponse._Message);
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [InformacionServer]");
			$(objectElement).prop('disabled', false);
			HiddenLoadBar()
	    }
	});
}

function getTipoElemento(cadena)
{
	if(cadena=="text" || cadena=="date" || cadena=="datetime-local" 
		|| cadena=="checkbox" || cadena=="button" || cadena=="hidden"
			 || cadena=="file")
		return "input";
	if(cadena=="select-one")
		return "select";
	return cadena;
}

//This function is used to get error message for all ajax calls
function getErrorMessage(jqXHR, exception) {
	 var msg = '';
	 if (jqXHR.status === 0) {
	     msg = 'Not connect.\n Verify Network.';
	 } else if (jqXHR.status == 404) {
	     msg = 'Requested page not found. [404]';
	 } else if (jqXHR.status == 500) {
	     msg = 'Internal Server Error [500].';
	 } else if (exception === 'parsererror') {
		 msg = 'Requested JSON parse failed.';
	 } else if (exception === 'timeout') {
		 msg = 'Time out error.';
	 } else if (exception === 'abort') {
		 msg = 'Ajax request aborted.';
	 } else {
	     msg = 'Uncaught Error.\n' + jqXHR.responseText;
	 }
	 return msg;
}

function ShowLoadBar() {
       $("#data").html("");
       $("#data").show();
       $("#idCarga").html(loader);
       $("#idCarga").show();
}

function HiddenLoadBar() {
       $("#idCarga").html("");
       $("#idCarga").show();
}

// ************************++++******************************************


/* VALIDAMOS LA INFORMACION CAPTURADA PARA CALCULAR LA CURP
     * generaCurp()
     * Función principal que genera el CURP.
     * @param {object} param - Objeto que tiene los parámetros necesarios para poder generar el curp,
     * @param boolean search - La clave omonima y digito verificador como guiones bajos,
     * Las propiedasdes del objeto param son:
     * @param {string} nombre - Nombre(s).
     * @param {string} apellido_paterno - Apellido materno.
     * @param {string} apellido_materno - Apellido materno.
     * En caso de haber conjunciones en los apellidos, estas deben ir aqui.
     * @param {string} sexo - Sexo. H para hombre, M para mujer.
     * @param {string} estado - Estado, usando las abreviaturas oficiales.
     * @param {array} fecha_nacimiento - Arreglo con [ día, mes, año ], cada uno como numero.
     * Por default es 0 si la fecha de nacimiento es menor o igual a 1999, o A, si es igual o mayor a 2000.
     * */
var getCURP = function (param, search) {
    /**
     * filtraInconvenientes()
     * Filtra palabras altisonantes en los primeros 4 caracteres del CURP
     * @param {string} str - Los primeros 4 caracteres del CURP
     */
    function filtraInconvenientes(str) {
        var inconvenientes = ['BACA', 'LOCO', 'BUEI', 'BUEY', 'MAME', 'CACA', 'MAMO',
            'CACO', 'MEAR', 'CAGA', 'MEAS', 'CAGO', 'MEON', 'CAKA', 'MIAR', 'CAKO', 'MION',
            'COGE', 'MOCO', 'COGI', 'MOKO', 'COJA', 'MULA', 'COJE', 'MULO', 'COJI', 'NACA',
            'COJO', 'NACO', 'COLA', 'PEDA', 'CULO', 'PEDO', 'FALO', 'PENE', 'FETO', 'PIPI',
            'GETA', 'PITO', 'GUEI', 'POPO', 'GUEY', 'PUTA', 'JETA', 'PUTO', 'JOTO', 'QULO',
            'KACA', 'RATA', 'KACO', 'ROBA', 'KAGA', 'ROBE', 'KAGO', 'ROBO', 'KAKA', 'RUIN',
            'KAKO', 'SENO', 'KOGE', 'TETA', 'KOGI', 'VACA', 'KOJA', 'VAGA', 'KOJE', 'VAGO',
            'KOJI', 'VAKA', 'KOJO', 'VUEI', 'KOLA', 'VUEY', 'KULO', 'WUEI', 'LILO', 'WUEY',
            'LOCA'];

        if (inconvenientes.indexOf(str) > -1) {
            str = str.replace(/^(\w)\w/, '$1X');
        }
        return str;
    }

    /**
     * ajustaCompuesto()
     * Cuando el nombre o los apellidos son compuestos y tienen
     * proposiciones, contracciones o conjunciones, se deben eliminar esas palabras
     * a la hora de calcular el CURP.
     * @param {string} str - String donde se eliminarán las partes que lo hacen compuesto
     */
    function ajustaCompuesto(str) {
        var compuestos = [/\bDA\b/, /\bDAS\b/, /\bDE\b/, /\bDEL\b/, /\bDER\b/, /\bDI\b/,
            /\bDIE\b/, /\bDD\b/, /\bEL\b/, /\bLA\b/, /\bLOS\b/, /\bLAS\b/, /\bLE\b/,
            /\bLES\b/, /\bMAC\b/, /\bMC\b/, /\bVAN\b/, /\bVON\b/, /\bY\b/];

        compuestos.forEach(function (compuesto) {
            if (compuesto.test(str)) {
                str = str.replace(compuesto, '');
            }
        });

        return str;
    }

    /**
     * zeropad()
     * Rellena con ceros un string, para que quede de un ancho determinado.
     * @param {number} ancho - Ancho deseado.
     * @param {number} num - Numero que sera procesado.
     */
    function zeropad(ancho, num) {
        var pad = Array.apply(0, Array.call(0, ancho)).map(function () {
            return 0;
        }).join('');

        return (pad + num).replace(new RegExp('^.*([0-9]{' + ancho + '})$'), '$1');
    }

    var pad = zeropad.bind(null, 2);

    /**
     * primerConsonante()
     * Saca la primer consonante interna del string, y la devuelve.
     * Si no hay una consonante interna, devuelve X.
     * @param {string} str - String del cual se va a sacar la primer consonante.
     */
    function primerConsonante(str) {
        str = str.trim().substring(1).replace(/[AEIOU]/ig, '').substring(0, 1);
        return (str === '') ? 'X' : str;
    }

    /**
     * filtraCaracteres()
     * Filtra convirtiendo todos los caracteres no alfabeticos a X.
     * @param {string} str - String el cual sera convertido.
     */
    function filtraCaracteres(str) {
        return str.toUpperCase().replace(/[\d_\-\.\/\\,]/g, 'X');
    }

    /**
     * estadoValido()
     * Valida si el estado esta en la lista de estados, de acuerdo a la RENAPO.
     * @param {string} str - String con el estado.
     */
    function estadoValido(estadoid) {
        var estado=new Map();
        estado.set(1,"AS");
        estado.set(2,"BC");
        estado.set(3,"BS");
        estado.set(4,"CC");
        estado.set(5,"CL");
        estado.set(6,"CM");
        estado.set(7,"CS");
        estado.set(8,"CH");
        estado.set(9,"DF");
        estado.set(10,"DG");
        estado.set(11,"GT");
        estado.set(12,"GR");
        estado.set(13,"HG");
        estado.set(14,"JC");
        estado.set(15,"MC");
        estado.set(16,"MN");
        estado.set(17,"MS");
        estado.set(18,"NT");
        estado.set(19,"NL");
        estado.set(20,"OC");
        estado.set(21,"PL");
        estado.set(22,"QT");
        estado.set(23,"QR");
        estado.set(24,"SP");
        estado.set(25,"SL");
        estado.set(26,"SR");
        estado.set(27,"TC");
        estado.set(28,"TS");
        estado.set(29,"TL");
        estado.set(30,"VZ");
        estado.set(31,"YN");
        estado.set(32,"ZS");
        return (estado.get(estadoid));
    }

    /**
     * normalizaString()
     * Elimina los acentos, eñes y diéresis que pudiera tener el nombre.
     * @param {string} str - String con el nombre o los apellidos.
     */
    function normalizaString(str) {
        var origen, destino, salida;
        origen = ['Ã', 'À', 'Á', 'Ä', 'Â', 'È', 'É', 'Ë', 'Ê', 'Ì', 'Í', 'Ï', 'Î',
            'Ò', 'Ó', 'Ö', 'Ô', 'Ù', 'Ú', 'Ü', 'Û', 'ã', 'à', 'á', 'ä', 'â',
            'è', 'é', 'ë', 'ê', 'ì', 'í', 'ï', 'î', 'ò', 'ó', 'ö', 'ô', 'ù',
            'ú', 'ü', 'û', 'Ñ', 'ñ', 'Ç', 'ç'];
        destino = ['A', 'A', 'A', 'A', 'A', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I',
            'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'a', 'a', 'a', 'a', 'a',
            'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'u',
            'u', 'u', 'u', 'n', 'n', 'c', 'c'];
        str = str.split('');
        salida = str.map(function (char) {
            var pos = origen.indexOf(char);
            return (pos > -1) ? destino[pos] : char;
        });

        return salida.join('');
    }

    /**
     * agregaDigitoVerificador()
     * Agrega el dígito que se usa para validar el CURP.
     * @param {string} curp_str - String que contiene los primeros 17 caracteres del CURP.
     */
    function agregaDigitoVerificador(curp_str) {
        // Convierte el CURP en un arreglo
        var curp, caracteres, curpNumerico, suma, digito;

        curp = curp_str.substring(0, 17).toUpperCase().split('');
        caracteres = [
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E',
            'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S',
            'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        ];
        // Convierte el curp a un arreglo de números, usando la posición de cada
        // carácter, dentro del arreglo `caracteres`.
        curpNumerico = curp.map(function (caracter) {
            return caracteres.indexOf(caracter);
        });
        suma = curpNumerico.reduce(function (prev, valor, indice) {
            return prev + (valor * (18 - indice));
        }, 0);
        digito = (10 - (suma % 10));
        if (digito === 10) {
            digito = 0;
        }
        return curp_str + digito;
    }
    
    function getArrayFechaNacimiento(param)
    {
    	if(!(String(param.fecha_nacimiento[0]).length===2 || String(param.fecha_nacimiento[0]).length===4) && 
    			String(param.fecha_nacimiento[1]).length!=2 && String(param.fecha_nacimiento[2]).length!=2)
    	{
    		return "ERROR_FechaNacimiento";
    	}
    	var ArrayFecha=[
    			String(param.fecha_nacimiento[0].substring(param.fecha_nacimiento[0].length-2, param.fecha_nacimiento[0].length)),
    			String(param.fecha_nacimiento[1]),
    			String(param.fecha_nacimiento[2])
    			];
    	return ArrayFecha;
    }

    var inicial_nombre, vocal_apellido, posicion_1_4, posicion_14_16, curp;
    param.nombre = ajustaCompuesto(normalizaString(param.nombre.toUpperCase())).trim();
    param.apellido_paterno = ajustaCompuesto(normalizaString(param.apellido_paterno.toUpperCase())).trim();
    param.apellido_materno = ajustaCompuesto(normalizaString(param.apellido_materno.toUpperCase())).trim();
    param.estado=estadoValido(parseInt(param.estado));
    // La inicial del primer nombre, o, si tiene mas de 1 nombre Y el primer
    // nombre es uno de la lista de nombres comunes, la inicial del segundo nombre
    inicial_nombre = (function (nombre) {
        var comunes, nombres, primerNombreEsComun;
        comunes = ['MARIA', 'MA', 'MA.', 'JOSE', 'J', 'J.'];
        nombres = nombre.toUpperCase().trim().split(/\s+/);
        primerNombreEsComun = (nombres.length > 1 && comunes.indexOf(nombres[0]) > -1);

        if (primerNombreEsComun) {
            return nombres[1].substring(0, 1);
        }
        if (!primerNombreEsComun) {
            return nombres[0].substring(0, 1);
        }
    }(param.nombre));
    vocal_apellido = param.apellido_paterno.trim().substring(1).replace(/[^AEIOU]/g, '').substring(0, 1);
    vocal_apellido = (vocal_apellido === '') ? 'X' : vocal_apellido;
    posicion_1_4 = [
        param.apellido_paterno.substring(0, 1),
        vocal_apellido,
        param.apellido_materno.substring(0, 1),
        inicial_nombre
    ].join('');
    posicion_1_4 = filtraInconvenientes(filtraCaracteres(posicion_1_4));
    posicion_14_16 = [
        primerConsonante(param.apellido_paterno),
        primerConsonante(param.apellido_materno),
        primerConsonante(param.nombre)
    ].join('');
    var fecha_nacimiento = getArrayFechaNacimiento(param);// param.fecha_nacimiento.split('-');
    param.sexo = param.sexo || '_';
    curp = [
        posicion_1_4,
        fecha_nacimiento[0],
        fecha_nacimiento[1],
        fecha_nacimiento[2],
        param.sexo.toUpperCase(),
        param.estado.toUpperCase(),
        posicion_14_16
    ];
    if (!param.fecha_nacimiento || search !== true) {
        curp.push('__');
        return curp.join('');
    }
    var date_fecha = new Date(param.fecha_nacimiento[0],param.fecha_nacimiento[1],param.fecha_nacimiento[2]);
    curp.push((date_fecha.getYear() > 1999 ? 'A' : 0));
    return agregaDigitoVerificador(curp.join(''));
};