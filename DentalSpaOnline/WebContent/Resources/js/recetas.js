
$(document).ready(function()
{
	$("#btn-search").click(function(){
		var parametroBusqueda = $("#in-search").val();
		if(parametroBusqueda!=undefined && parametroBusqueda!=null && parametroBusqueda.trim().length>0)
		{
			parametroBusqueda={"rq_parametroBusqueda":parametroBusqueda};
			buscaPaciente(parametroBusqueda, this, "container_search_result");
			$("#in-search").val("");
		}
		else
		{
			mostrarError("Indique un parametro de busqueda");
		}
	});
});