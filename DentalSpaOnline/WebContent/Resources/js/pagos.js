var listaTipoPago=null;
var listaPagosdetalletipo=null;
var listaFormapago=null;
var datatable_tratamientos;
var datatable_lista_pagos;

$(document).ready(function()
{
	$("#btn-search").click(function(){
		var parametroBusqueda = $("#in-search").val();
		if(parametroBusqueda!=undefined && parametroBusqueda!=null && parametroBusqueda.trim().length>0)
		{
			parametroBusqueda={"rq_parametroBusqueda":parametroBusqueda};
			buscaPaciente(parametroBusqueda, this, "container_search_result");
			$("#in-search").val("");
		}
		else
		{
			mostrarError("Indique un parametro de busqueda");
		}
	});

	datatable_tratamientos = $("#datatable_tratamientos").DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );

	datatable_lista_pagos = $("#datatable_lista_pagos").DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
	
	getTipoPago();
	getPagosdetalletipo();
	getFormapago();
});

function addCostoTratamiento(objectElement)
{
	console.log("Add costo del tratamiento");
	var jsonRequest=getJsonResquest("form-group pago");
	if(jsonRequest!=undefined && jsonRequest!=null)
	{
		jsonRequest={proceso: "costoTratamiento", ParametersRequest: JSON.stringify(jsonRequest)};
		console.log(JSON.stringify(jsonRequest));
		$.ajax(
		{
	        async: true,
			url : "guardarPago",
			type : "POST",
			dataType : "json",
			data:{	"ParametersRequest": JSON.stringify(jsonRequest)
			},
			beforeSend : function( xhr ) {
				if (xhr && xhr.overrideMimeType) {
					xhr.overrideMimeType("application/j-son;charset=UTF-8");
	            }
				console.log("Request Service...");
				$(objectElement).prop("disabled", true);
			},
			success : function(jsonResponse)
			{
//				console.log(JSON.stringify(jsonResponse));
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
//					console.log("valida:: "+jsonResponse._State);
					if(jsonResponse._State=="true")
					{
						mostrarSuccess(jsonResponse._Message);
						var pagoNew = JSON.parse(jsonResponse._ObjectJson);
						
						addDetallePago(pagoNew.pagoid);
						
						$(objectElement).remove();
						$("#tipopagoid").prop("disabled", true);
						$("#pagoimportetotal").prop("disabled", true);
						$("#pagocomentarios").prop("disabled", true);
					}else
					{
						mostrarError(jsonResponse._Message);
						$(objectElement).prop("disabled", false);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
				$(objectElement).prop("disabled", false);
		    },
		    complete : function()
		    {
		    	console.log("Completed [InformacionServer]");
		    }
		});
	}
}

function addPagoDetalle(objectElement, id)
{
	console.log("Add Pago Detalle :: "+id);
	var jsonRequest=getJsonResquest(id);
	if(jsonRequest!=undefined && jsonRequest!=null)
	{
		jsonRequest={proceso: "detallePago", ParametersRequest: JSON.stringify(jsonRequest)};
		console.log(JSON.stringify(jsonRequest));
		$.ajax(
		{
	        async: true,
			url : "guardarPago",
			type : "POST",
			dataType : "json",
			data:{	"ParametersRequest": JSON.stringify(jsonRequest)
			},
			beforeSend : function( xhr ) {
				if (xhr && xhr.overrideMimeType) {
					xhr.overrideMimeType("application/j-son;charset=UTF-8");
	            }
				console.log("Request Service...");
				$(objectElement).prop("disabled", true);
			},
			success : function(jsonResponse)
			{
//				console.log(JSON.stringify(jsonResponse));
				if(jsonResponse._State=="REDIRECT")
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = "/log/out?mensaje="+jsonResponse._Message;
				}
				else
				{
//					console.log("valida:: "+jsonResponse._State);
					if(jsonResponse._State=="true")
					{
						mostrarSuccess(jsonResponse._Message);
						var newpagosdetalle = JSON.parse(jsonResponse._ObjectJson);
						$("#"+id+"-pagosdetalleimportependiente").val("$ "+newpagosdetalle.pagosdetalleimportependiente.toFixed(2));
						$("#pagoimportependiente").val("$ "+newpagosdetalle.pagosdetalleimportependiente.toFixed(2));
						$("div[class*='"+id+"']").children().each(function () {
						    console.log("Value:: ["+this.value+"] id:: ["+this.id+"] type:: "+this.type);
						    $(getTipoElemento(this.type)+"[id='"+this.id+"']").prop("disabled", true);
						    $(getTipoElemento(this.type)+"[id='"+this.id+"']").prop( "id", "" );
						});
						
						if(newpagosdetalle.pagosdetalleimportependiente==0.0)
							mostrarInfo("El tratamiento ha sido pagado en su totalidad.");
						else
							addDetallePago(newpagosdetalle.pagoid);
						
						$(objectElement).remove();
					}else
					{
						mostrarError(jsonResponse._Message);
						$(objectElement).prop("disabled", false);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
				$(objectElement).prop("disabled", false);
		    },
		    complete : function()
		    {
		    	console.log("Completed [InformacionServer]");
		    }
		});
	}
}

function getJsonResquest(idForm)
{
	var error="";
	var jsonRequest={};
	var exists=false;
	$("div[class='"+idForm+"']").children().each(function () {
		if(this.type!=undefined)
		{
//		    console.log("Value:: ["+this.value+"] id:: ["+this.id+"] type:: "+this.type);
			var valueObject=null;
			switch(this.type)
			{
				case "checkbox":
					valueObject=$(getTipoElemento(this.type)+"[id='"+this.id+"']").is(":checked");
				break;
				default:
					valueObject=$(getTipoElemento(this.type)+"[id='"+this.id+"']").val();
			}
			
		    if(valueObject==undefined || valueObject==null || String(valueObject).trim().length<1)
		    {
//		    	console.log("Val error:: [id='"+this.id+"'] :: "+valueObject);
		    	error+="\n > Es necesario completar "+$(getTipoElemento(this.type)+"[id='"+this.id+"']").attr("placeholder");
		    }
		    else
		    {
		    	jsonRequest[this.id]=valueObject;
		    	exists=true;
		    }
		}
	
	});
	if(error!="")
	{
		console.log(error);
		mostrarError(error);
	}
	else
	{
		if(exists)
			return jsonRequest;
	}
	return null;
}

function loadPagos(idtratamiento)
{
	datatable_lista_pagos.clear().draw();
	var tratamiento = $("#info_tramiento_id-"+idtratamiento).data("json_tratamiento");
	var v_pagoestatus= (tratamiento.pagos.pagoid>0 && tratamiento.pagos.pagosaldado ? "Pagado" : "Pendiente");
	$("#pacienteid").val(tratamiento.pacienteid);
	$("#tratamientoid").val(tratamiento.tratamientoid);
	$("#tratamientonombre").val(tratamiento.tipotratamiento.tipotratamientonombre);
	$("#tratamientofechacreacion").val(tratamiento.tratamientofechacreacion);
	$("#tratamientofechamodificacion").val(tratamiento.tratamientofechamodificacion);
	$("#pagoestatus").val((tratamiento.tratamientoterminado ? "Terminado" : "Activo") +" - " + v_pagoestatus);

	$("#tipopagoid").prop("disabled", true);
	$("#pagoimportetotal").prop("disabled", true);
	$("#pagocomentarios").prop("disabled", true);
	$('#tipopagoid option:selected').removeAttr('selected');
	$("#pagoimportetotal").val("");
	$("#pagocomentarios").val("");
	$("#container_btn_pago").html("<input type='button' class='btn btn-info btn-block' id='btn_agrega_pago' value='Guardar' onclick='addCostoTratamiento(this);' disabled/>");
	
	if(tratamiento.pagos!=undefined && tratamiento.pagos.pagoid!=undefined && tratamiento.pagos.pagoid>0)
	{	
		$("#tipopagoid option[value='"+tratamiento.pagos.tipopagoid+"']").attr("selected", true);
		$("#pagoimportetotal").val(tratamiento.pagos.pagoimportetotal);
		$("#pagocomentarios").val(tratamiento.pagos.pagocomentarios);
		$("#container_btn_pago").html("<label>Importe Restante</label><input type='text' class='form-control' id='pagoimportependiente' value='$ "+tratamiento.pagos.pagoimportependiente+"' disabled/>");
		
		if(tratamiento.pagos.pagosdetalle!=undefined && tratamiento.pagos.pagosdetalle!=null 
				&& tratamiento.pagos.pagosdetalle.length>0)
		{
			for(var i=0; i<tratamiento.pagos.pagosdetalle.length; i++)
			{
				if(tratamiento.pagos.pagosdetalle[i].pagosdetalleid!=undefined && tratamiento.pagos.pagosdetalle[i].pagosdetalleid>0)
				{
					datatable_lista_pagos.row.add(
						[
							new Date(tratamiento.pagos.pagosdetalle[i].pagosdetallefechacreacion).toLocaleString(),
							"$ "+tratamiento.pagos.pagosdetalle[i].pagosdetalleimporte.toFixed(2),
							tratamiento.pagos.pagosdetalle[i].pagosdetalletipo.pagosdetalletiponombre,
							tratamiento.pagos.pagosdetalle[i].formapago.formapagonombre+"</option></select>",
							"$ "+tratamiento.pagos.pagosdetalle[i].pagosdetalleimportependiente.toFixed(2),
							""
						]).draw();
				}
			}
		}

		if(v_pagoestatus=="Pagado")
		{
			mostrarInfo("El tratamiento ya ha sido pagado");
		}
		else
		{
			
			addDetallePago(tratamiento.pagos.pagoid);
		}
	}
	else
	{
		$("#tipopagoid").prop("disabled", false);
		$("#pagoimportetotal").prop("disabled", false);
		$("#pagocomentarios").prop("disabled", false);
		$("#btn_agrega_pago").prop("disabled", false);
	}
	mostrarSuccess("Informacion del tratamiento cargado");
}

function loadInfo(idpaciente)
{
	if(idpaciente==undefined || idpaciente==null || idpaciente=="" || !isInt(idpaciente))
	{
		mostrarError("No se recupero el id del paciente");
		return;
	}

	var paciente = $("#cont_info_paciente_id-"+idpaciente).data("json_paciente");
	datatable_lista_pagos.clear().draw();
    datatable_tratamientos.clear().draw();
	
//	INFORMACION DEL PACIENTE
	$("#info_pacienteid").val(paciente.pacientenombre+" "+paciente.pacienteapellidopaterno+" "+paciente.pacienteapellidomaterno);
	$("#info_pacientesexo").val((paciente.pacientesexo=="H" ? "Hombre" : "Mujer"));
	$("#info_pacienteedad").val(paciente.pacienteedad);
	
	$.ajax(
	{
        async: true,
		url : 'listaPagosTratamientos',
		type : 'POST',
		dataType : 'json',
		data:{	"ParametersRequest": "{rq_pacienteid : "+paciente.pacienteid+"}"
		},
		beforeSend : function( xhr ) {
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
            }
			console.log("Request Service...");
		},
		success : function(jsonResponse)
		{
//					console.log(JSON.stringify(jsonResponse));
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
//				console.log("valida:: "+jsonResponse._State);
				if(jsonResponse._State=="true")
				{
					listaTratamientos = JSON.parse(jsonResponse._ObjectJson);
					if(listaTratamientos!=undefined && listaTratamientos!=null && listaTratamientos.length>0)
					{
						for(var i=0; i<listaTratamientos.length; i++)
						{
							datatable_tratamientos.row.add(
								[ 
									listaTratamientos[i].tipotratamiento.tipotratamientonombre, 
									new Date(listaTratamientos[i].tratamientofechacreacion).toLocaleString(),
									new Date(listaTratamientos[i].tratamientofechamodificacion).toLocaleString(),
									"$ "+listaTratamientos[i].pagos.pagoimportetotal.toFixed(2),
									((listaTratamientos[i].tratamientoterminado ? "Terminado" : "Activo")+" - "+(listaTratamientos[i].pagos.pagoid>0 && listaTratamientos[i].pagos.pagosaldado ? "Pagado" : "Pendiente")),
									"<input type='button' class='btn btn-info' value='Cargar' onclick='loadPagos("+listaTratamientos[i].tratamientoid+")'/>"
									+"<div id='info_tramiento_id-"+listaTratamientos[i].tratamientoid+"' data-json_tratamiento='"+JSON.stringify(listaTratamientos[i])+"'></div>"
								]).draw();
						}
					}
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [Request]");
	    }
	});
	$("#container_search_result").html("");
	mostrarSuccess("Informacion del paciente cargado");
}

function addDetallePago(pagoid)
{
	var id="newPago-"+new Date().getTime();
	datatable_lista_pagos.row.add(
			[
				"<input type='text' id='' class='form-control' value='"+(new Date().toLocaleString())+"' disabled/>",
				"<div class='"+id+"'><input type='hidden' id='pagoid' value='"+pagoid+"' placeholer='Pago id'><input type='text' id='pagosdetalleimporte' class='form-control' placeholer='Importe'onchange='validaCampo(\"DOUBLE\",this);''/></div>",
				"<div class='"+id+"'><select id='pagosdetalletipoid' class='form-control' placeholer='Tipo de pago'></select></div>",
				"<div class='"+id+"'><select id='formapagoid' class='form-control' placeholer='Forma Pago'></select></div>",
				"<input id='"+id+"-pagosdetalleimportependiente' type='text' class='form-control' value='0.0' placeholer='Saldo pendiente' disabled/>",
				"<input id='btn_"+id+"' type='button' class='btn btn-success' value='[+]' onclick='addPagoDetalle(this,\""+id+"\")'/></div>"
			]).draw();
	
	doPrintListaPagosdetalletipo();
	doPrintListaFormapago();
}

function getTipoPago()
{
	if(listaTipoPago==undefined || listaTipoPago==null || listaTipoPago.length<0)
	{
		$.ajax(
		{
	        async: true,
			url : '/catalogos/tipopago',
			type : 'POST',
			dataType : 'json',
			success : function(jsonResponse)
			{
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
					if(jsonResponse._State=="true")
					{
						listaTipoPago=JSON.parse(jsonResponse._ObjectJson);
						doPrintListTipopago();
					}else
					{
						mostrarError(jsonResponse._Message);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [InformacionServer]");
		    }
		});
	}
	else
	{
		doPrintListTipopago();
	}
}

function getPagosdetalletipo()
{
	if(listaPagosdetalletipo==undefined || listaPagosdetalletipo==null || listaPagosdetalletipo.length<0)
	{
		$.ajax(
		{
	        async: true,
			url : '/catalogos/pagosdetalletipo',
			type : 'POST',
			dataType : 'json',
			success : function(jsonResponse)
			{
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
					if(jsonResponse._State=="true")
					{
						listaPagosdetalletipo=JSON.parse(jsonResponse._ObjectJson);
						doPrintListaPagosdetalletipo();
					}else
					{
						mostrarError(jsonResponse._Message);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [InformacionServer]");
		    }
		});
	}
	else
	{
		doPrintListaPagosdetalletipo();
	}
}

function getFormapago()
{
	if(listaFormapago==undefined || listaFormapago==null || listaFormapago.length<0)
	{
		$.ajax(
		{
	        async: true,
			url : '/catalogos/formapago',
			type : 'POST',
			dataType : 'json',
			success : function(jsonResponse)
			{
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
					if(jsonResponse._State=="true")
					{
						listaFormapago=JSON.parse(jsonResponse._ObjectJson);
						doPrintListaFormapago();
					}else
					{
						mostrarError(jsonResponse._Message);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [InformacionServer]");
		    }
		});
	}
	else
	{
		doPrintListaFormapago();
	}
}

function doPrintListTipopago()
{
	$("select[id='tipopagoid']").empty();
	if(listaTipoPago!=undefined && listaTipoPago!=null && listaTipoPago.length>0)
	{
		$("#tipopagoid").append(new Option('Selecciona plan de pago', -1, true, true));
		for(var i=0; i<listaTipoPago.length; i++)
		{
			$("#tipopagoid").append(new Option(listaTipoPago[i].tipopagonombre, listaTipoPago[i].tipopagoid, false, false));
		}
	}
	else
	{
		mostrarError("Error al recuperar los tipos de pago¡");
	}
}

function doPrintListaPagosdetalletipo()
{
	$("select[id='pagosdetalletipoid']").empty();
	if(listaPagosdetalletipo!=undefined && listaPagosdetalletipo!=null && listaPagosdetalletipo.length>0)
	{
		$("#pagosdetalletipoid").append(new Option('Selecciona tipo de pago', -1, true, true));
		for(var i=0; i<listaPagosdetalletipo.length; i++)
		{
			$("#pagosdetalletipoid").append(new Option(listaPagosdetalletipo[i].pagosdetalletiponombre, listaPagosdetalletipo[i].pagosdetalletipoid, false, false));
		}
	}
	else
	{
		mostrarError("Error al recuperar los tipos de pagos");
	}
}

function doPrintListaFormapago()
{
	$("select[id='formapagoid']").empty();
	if(listaFormapago!=undefined && listaFormapago!=null && listaFormapago.length>0)
	{
		$("#formapagoid").append(new Option('Selecciona forma de pago', -1, true, true));
		for(var i=0; i<listaFormapago.length; i++)
		{
			$("#formapagoid").append(new Option(listaFormapago[i].formapagonombre, listaFormapago[i].formapagoid, false, false));
		}
	}
	else
	{
		mostrarError("Error al recuperar las forma de pago");
	}
}