var listaCitas=[];

document.addEventListener('DOMContentLoaded', function() {
	cargarListaCitas();
});

$(document).ready(function(){

	$("#citafechainicio").change(function() {
		console.log("Copia valor");
		console.log(">> "+$( this ).val());
		if($( this ).val()!=undefined && $( this ).val()!=null && $( this ).val().trim().length>0)
		{
			$("#citafechafin").val($( this ).val().trim());
		}
	});
});

function guardar(tipoFormulario, objectElement)
{
	var error="";
	var jsonRequest={};
	$("div[class='form-group "+tipoFormulario+"']").children().each(function () {
		if(this.type!=undefined)
		{
			var valueObject = $(getTipoElemento(this.type)+"[id='"+this.id+"']").val();
		    if(valueObject==undefined || valueObject==null || valueObject.trim().length<1)
		    {
		    	error+="\n > Es necesario completar "+$(getTipoElemento(this.type)+"[id='"+this.id+"']").attr("placeholder");
		    }
		    else
		    {
		    	if(this.type=="datetime-local")
		    	{
		    		valueObject=valueObject.split("T")[0]+" "+new Date(valueObject).toLocaleTimeString();
		    	}
		    	jsonRequest["rq_"+this.id]=valueObject;
		    }
		}
	});
	if(error!="")
	{
		console.log(error);
		mostrarError(error);
	}
	else
	{	
		jsonRequest={proceso: tipoFormulario, ParametersRequest: JSON.stringify(jsonRequest)};

		console.log("Json_PETICION:: "+JSON.stringify(jsonRequest));
		guardarCambios(jsonRequest, objectElement);
		limpiarValores(tipoFormulario);
	}
}

function guardarCambios(jsonParameter, objectElement)
{
	$.ajax(
	{
        async: true,
		url : 'crear',
		type : 'POST',
		dataType : 'json',
		data:{	"ParametersRequest": JSON.stringify(jsonParameter)
		},
		beforeSend : function( xhr ) {
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
            }
			console.log("Request Service...");
			$(objectElement).prop('disabled', true);
			ShowLoadBar();
		},
		success : function(jsonResponse)
		{
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
				if(jsonResponse._State=="true")
				{
					mostrarSuccess(jsonResponse._Message);
					cargarListaCitas();
				}else
				{
					mostrarError(jsonResponse._Message);
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [InformacionServer]");
			$(objectElement).prop('disabled', false);
			HiddenLoadBar()
	    }
	});
}

function cargarListaCitas()
{
	$.ajax(
	{
        async: true,
		url : 'listaCitas',
		type : 'POST',
		dataType : 'json',
		success : function(jsonResponse)
		{
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
				if(jsonResponse._State=="true")
				{
					var citas=JSON.parse(jsonResponse._ObjectJson);
					listaCitas = [];
					for(var i=0; i<citas.length; i++)
					{
						listaCitas.push({
							title: citas[i].citanombre+" :: "+citas[i].citacomentarios, 
							url : '#', 
							start: new Date(citas[i].citafechainicio).toJSON(), 
							end: new Date(citas[i].citafechafin).toJSON()
						});
					}
					console.log("ListaCitas :: "+JSON.stringify(listaCitas));
					cargarCalendario();
				}
				else
				{
					mostrarError(jsonResponse._Message);
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [ListaParentescos]");
	    }
	});
}

function cargarCalendario()
{
	  $("#calendar").html("");
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'dayGrid', 'list', 'timeGrid' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
      },
//	      defaultDate: '2020-02-12',
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectMirror: true,
      select: function(arg) {
        var title = prompt('Event Title:');
        if (title) {
          calendar.addEvent({
            title: title,
            start: arg.start,
            end: arg.end,
            allDay: arg.allDay
          })
        }
        calendar.unselect()
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: listaCitas,
//    	  [
//        {
//          title: 'All Day Event',
//          start: '2020-02-01'
//        },
//        {
//          title: 'Long Event',
//          start: '2020-05-07',
//          end: '2020-05-10'
//        },
//        {
//          groupId: 999,
//          title: 'Repeating Event',
//          start: '2020-05-09T16:00:00'
//        },
//        {
//          groupId: 999,
//          title: 'Repeating Event',
//          start: '2020-05-16T16:00:00'
//        },
//        {
//          title: 'Conference',
//          start: '2020-05-11',
//          end: '2020-05-13'
//        },
//        {
//          title: 'Meeting',
//          start: '2020-05-12T10:30:00',
//          end: '2020-05-12T12:30:00'
//        },
//        {
//          title: 'Lunch',
//          start: '2020-05-12T12:00:00'
//        },
//        {
//          title: 'Meeting',
//          start: '2020-05-12T14:30:00'
//        },
//        {
//          title: 'Happy Hour',
//          start: '2020-05-12T17:30:00'
//        },
//        {
//          title: 'Dinner',
//          start: '2020-05-12T20:00:00'
//        },
//        {
//          title: 'Birthday Party',
//          start: '2020-05-13T07:00:00'
//        },
//        {
//          title: 'Click for Google',
//          url: 'http://google.com/',
//          start: '2020-05-28'
//        }
//      ]
    });

    calendar.render();
}

