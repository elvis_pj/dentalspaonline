function mostrarMensajes(mensaje,error)
{
	if(error!=undefined && error!=null && error.trim().length>0)
	{
		mostrarInfo(error);
	}
	else
	{
		if(mensaje!=undefined && mensaje!=null && mensaje.trim().length>0)
		{
			mostrarInfo(mensaje);
		}
	}
}


function mostrarInfo(info)
{
	swal({
		  title: "Info",
		  text: info,
		  icon: "info",
		  dangerMode: true ,
	});
}