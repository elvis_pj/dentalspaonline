var listaEstados=null;
var listaParentescos=null;

$(document).ready(function()
{
	$("#btn-search").click(function(){
		var parametroBusqueda = $("#in-search").val();
		if(parametroBusqueda!=undefined && parametroBusqueda!=null && parametroBusqueda.trim().length>0)
		{
			parametroBusqueda={"rq_parametroBusqueda":parametroBusqueda};
			$("#container_opciones").toggle(false);
			$("#container_form").html(viewBuscarPaciente());
			buscaPaciente(parametroBusqueda, this, "container_table_result_search");
//			showInfoPaciente(JSON.parse(responseJson));
			$("#in-search").val("");
		}
		else
		{
			mostrarError("Indique un parametro de busqueda");
		}
	});

	getEstados();
	getParentescos();
});

function closeInfo()
{
	$("#container_form").html(viewBuscarPaciente());
}

function saveInfo(form, objectElement)
{
	var error="";
	var jsonRequest={};
	$("div[class='form-group input-edit-paciente-"+form+"']").children().each(function () {
		if(this.type!=undefined)
		{
//		    console.log("Value:: "+this.value);
//		    console.log("id:: "+this.id);
//		    console.log("type:: "+this.type);
//		    $(getTipoElemento(this.type)+"[id='"+this.id+"']").prop("disabled", state);
			
			var valueObject = $(getTipoElemento(this.type)+"[id='"+this.id+"']").val();
		    if(valueObject==undefined || valueObject==null || valueObject.trim().length<1)
		    {
//		    	console.log("Val error:: [id='"+this.id+"'] :: "+valueObject);
		    	error+="\n > Es necesario completar "+$(getTipoElemento(this.type)+"[id='"+this.id+"']").attr("placeholder");
		    }
		    else
		    {
		    	jsonRequest["rq_"+this.id]=valueObject;
//		    	if(jsonRequest!="")
//		    	    jsonRequest+=",";
//		    	jsonRequest+='"rq_'+this.id+'" : "'+valueObject+'" ';
		    }
		}
	});
	if(error!="")
	{
		console.log(error);
		mostrarError(error);
	}
	else
	{
//		jsonRequest = '{'+jsonRequest+'}';
//		console.log("Json_Armado_1:: "+jsonRequest);
//		jsonRequest = JSON.parse(jsonRequest);
//		console.log("Json_Armado:: "+JSON.stringify(jsonRequest));
		if(form=="PERSONAL")
		{
			jsonRequest["rq_pacientecurp"]=validaCamposCURP(jsonRequest.rq_pacientenombre,
																jsonRequest.rq_pacienteapellidopaterno,
																jsonRequest.rq_pacienteapellidomaterno,
																jsonRequest.rq_estadoid,
																jsonRequest.rq_pacientefechanacimiento,
																jsonRequest.rq_pacientesexo);
			console.log("CURP AGREGADA:: "+JSON.stringify(jsonRequest));
		}
		jsonRequest={proceso: form, ParametersRequest: JSON.stringify(jsonRequest)};

		console.log("Json_PETICION:: "+JSON.stringify(jsonRequest));
		guardarCambios(jsonRequest, objectElement);
		disabledMasive(form, true);
	}
}

function doCancelaBuscarPaciente()
{
	$("#container_form").html("");
	$('#container_opciones').toggle(true);
}

function doAltaPaciente(objectElement)
{
	var jsonRequest = validaFormularioAlta();
	if(jsonRequest!=null && jsonRequest!="")
	{
		altaPaciente(jsonRequest, objectElement);
	}
}

function doSearchPaciente(objectElement)
{
	var parametroBusqueda = $("#ed_serach_paciente").val();
	if(parametroBusqueda!=undefined && parametroBusqueda!=null && parametroBusqueda.trim().length>0)
	{
		parametroBusqueda={"rq_parametroBusqueda":parametroBusqueda};
		buscaPaciente(parametroBusqueda, objectElement, "container_table_result_search");
//		showInfoPaciente(JSON.parse(responseJson));
	}
}

function doCancelaAltaPaciente()
{
	$("#container_form").html(""); 
	$('#container_opciones').toggle(true);
	$("#pacientenombre").val("");
	$("#pacienteapellidopaterno").val("");
	$("#pacienteapellidomaterno").val("");
	$("#pacienteedad").val("");
	$("#pacientesexo").val("");
	$("#pacienteestadocivil").val("");
	$("#pacientefechanacimiento").val("");
	$("#pacientelugarnacimiento").val("");
	$("#pacientedomicilio").val("");
	$("#pacienteciudad").val("");
	$("#pacientecodigopostal").val("");
	$("#pacientetelefono").val("");
	$("#pacientecelular").val("");
	$("#pacientecorreo").val("");
	doPrintListEstados();
}

function habilitaEdicion(form)
{
	disabledMasive(form,false);
	$("#btn-save-editar-info-"+form).prop("disabled", false);
}

function disabledMasive(form, state)
{
	$("div[class='form-group input-edit-paciente-"+form+"']").children().each(function () {
		if(this.type!=undefined)
		{
//		    console.log("Value:: "+this.value);
//		    console.log("id:: "+this.id);
//		    console.log("type:: "+this.type);
			if(this.type!="hidden")
				$(getTipoElemento(this.type)+"[id='"+this.id+"']").prop("disabled", state);
		}
	});
}

function showForm(idForm)
{
	switch(parseInt(idForm))
	{
		case 4:
			$("#container_form").html(viewAltaPaciente());
			doListaEstadosAlta();
		break;
		case 5:
			$("#container_form").html(viewBuscarPaciente());
			getParentescos();
		break;
	}
	$('#container_opciones').toggle(false);
}

function guardarCambios(jsonParameter, objectElement)
{
	$.ajax(
	{
        async: true,
		url : 'guardarCambios',
		type : 'POST',
		dataType : 'json',
		data:{	"ParametersRequest": JSON.stringify(jsonParameter)
		},
		beforeSend : function( xhr ) {
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
            }
			console.log("Request Service...");
			$(objectElement).prop('disabled', true);
			ShowLoadBar();
		},
		success : function(jsonResponse)
		{
//			console.log(JSON.stringify(jsonResponse));
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
//				console.log("valida:: "+jsonResponse._State);
				if(jsonResponse._State=="true")
				{
					mostrarSuccess(jsonResponse._Message);
				}else
				{
					mostrarError(jsonResponse._Message);
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [InformacionServer]");
			$(objectElement).prop('disabled', false);
			HiddenLoadBar()
	    }
	});
}

function altaPaciente(jsonParameter, objectElement)
{
	$.ajax(
	{
        async: true,
		url : 'altaPaciente',
		type : 'POST',
		dataType : 'json',
		data:{	"ParametersRequest": JSON.stringify(jsonParameter)
		},
		beforeSend : function( xhr ) {
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
            }
			console.log("Request Service...");
			$(objectElement).prop('disabled', true);
			ShowLoadBar();
		},
		success : function(jsonResponse)
		{
//			console.log(JSON.stringify(jsonResponse));
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
//				console.log("valida:: "+jsonResponse._State);
				if(jsonResponse._State=="true")
				{
					var paciente_nuevo=JSON.parse(jsonResponse._ObjectJson);
					mostrarSuccess("Se registro exitosamente el paciente "+paciente_nuevo.pacientenombre+" con el id "+paciente_nuevo.pacienteid);

					$("#container_form").html(viewInformacionTabsPeciente());
					$("#nav-informacionpersonal").html(showInfoPaciente(paciente_nuevo));

					$("#pacientesexo option[value='"+paciente_nuevo.pacientesexo+"']").attr("selected", true);
					$("#pacienteestadocivil option[value='"+paciente_nuevo.pacienteestadocivil+"']").attr("selected", true);
					$("#estadoid option[value='"+paciente_nuevo.estadoid+"']").attr("selected", true);
					
					$("#nav-datosfiscales").html(showInfoDatosFiscales(paciente_nuevo));
					$("#nav-antecedentes").html(showInfoAntecedentes(paciente_nuevo));
					$("#nav-contactoemergencia").html(showInfoContactosEmergencia(paciente_nuevo));
					
				}else
				{
					mostrarError(jsonResponse._Message);
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [InformacionServer]");
			$(objectElement).prop('disabled', false);
			HiddenLoadBar()
	    }
	});
}

//function buscaPaciente(parametersRequest, objectElement, oo)
//{
//	$.ajax(
//	{
//        async: true,
//		url : 'buscarPaciente',
//		type : 'POST',
//		dataType : 'json',
//		data:{	"ParametersRequest": JSON.stringify(parametersRequest)
//		},
//		beforeSend : function( xhr ) {
//			if (xhr && xhr.overrideMimeType) {
//				xhr.overrideMimeType("application/j-son;charset=UTF-8");
//            }
//			console.log("Request Service...");
//			$(objectElement).prop('disabled', true);
//			ShowLoadBar();
//		},
//		success : function(jsonResponse)
//		{
////			console.log(JSON.stringify(jsonResponse));
//			if(jsonResponse._State=='REDIRECT')
//			{
//				console.log("Error en la peticion:: "+jsonResponse._Message);
//				mostrarError(jsonResponse._Message);
//				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
//			}
//			else
//			{
//				if(jsonResponse._State=="true")
//				{
////					showInfoPaciente(JSON.parse(jsonResponse._ObjectJson));
//					var listaResultados = JSON.parse(jsonResponse._ObjectJson);
//
//					$("#container_table_result_search").html(""
//							+"<div class='card shadow mb-4'>"
//							+"	<div class='card-header py-3'>"
//							+"		<h6 class='m-0 font-weight-bold text-primary'>Resultados de la busqueda</h6>"
//							+"	</div>"
//							+"	<div class='card-body'>"
//							+"		<div class='table-responsive'>"
//							+"			<div class='table-responsive'>"
//							+"				<table class='table table-bordered' id='dataTable' width='100%' cellspacing='0'>"
//							+"					<thead>"
//							+"						<th>Nombre</th>"
//							+"						<th>Apellido Paterno</th>"
//							+"						<th>Apellido Materno</th>"
//							+"						<th>Edad</th>"
//							+"						<th>Fecha Registro</th>"
//							+"						<th></th>"
//							+"					</thead>"
//							+"					<tfoot>"
//							+"						<th>Nombre</th>"
//							+"						<th>Apellido Paterno</th>"
//							+"						<th>Apellido Materno</th>"
//							+"						<th>Edad</th>"
//							+"						<th>Fecha Registro</th>"
//							+"						<th></th>"
//							+"					</tfoot>"
//							+"					<tbody id='tbody_container_result_search'>"
//							+"					</tbody>"
//							+"				</table>"
//							+"			</div>"
//							+"		</div>"
//							+"	</div>"
//							+"</div>");
//					
//					for(var i=0; i<listaResultados.length; i++)
//					{
////						console.log(i+".- "+JSON.stringify(listaResultados[i]));
//						$("#tbody_container_result_search").append(""
//								+"<tr>"
//								+"	<td>"+listaResultados[i].pacientenombre+"</td>"
//								+"	<td>"+listaResultados[i].pacienteapellidopaterno+"</td>"
//								+"	<td>"+listaResultados[i].pacienteapellidomaterno+"</td>"
//								+"	<td>"+listaResultados[i].pacienteedad+"</td>"
//								+"	<td>"+listaResultados[i].pacientefechacreacion+"</td>"
//								+"	<td>"
//								+"		<input type='button' class='btn btn-info' value='Detalle' onclick='loadInfo("+listaResultados[i].pacienteid+")'>"
//								+"		<div id='cont_info_paciente_id-"+listaResultados[i].pacienteid+"' data-json_paciente='"+JSON.stringify(listaResultados[i])+"'></div>"
//								+"	</td>"
//								+"</tr>");
//					}
//					$('#dataTable').DataTable();
//				}else
//				{
//					mostrarError(jsonResponse._Message);
//				}
//			}
//		},
//	    error : function(jqXHR, exception)
//	    {
//	    	console.log("["+exception.statusText+"] ", exception);
//	    	console.log("info: "+ exception.statusText);
//	    	alert(getErrorMessage(jqXHR, exception));
//	    },
//	    complete : function()
//	    {
//	    	console.log("Completed [InformacionServer]");
//			$(objectElement).prop('disabled', false);
//			HiddenLoadBar()
//	    }
//	});
//}

function addContactoEmergencia(objectElement, id)
{
	var parametersRequest = validaFormAddContactoEmergencia();
	if(parametersRequest!=undefined && parametersRequest!=null)
	{
		$.ajax(
		{
	        async: true,
			url : 'agregaContactoEmergencia',
			type : 'POST',
			dataType : 'json',
			data:{	"ParametersRequest": JSON.stringify(parametersRequest)
			},
			beforeSend : function( xhr ) {
				if (xhr && xhr.overrideMimeType) {
					xhr.overrideMimeType("application/j-son;charset=UTF-8");
	            }
				console.log("Request Service...");
				$(objectElement).prop('disabled', true);
				ShowLoadBar();
			},
			success : function(jsonResponse)
			{
//					console.log(JSON.stringify(jsonResponse));
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
//						console.log("valida:: "+jsonResponse._State);
					if(jsonResponse._State=="true")
					{
						var contacto_nuevo=JSON.parse(jsonResponse._ObjectJson);
						mostrarSuccess("Se registro exitosamente el contacto "+contacto_nuevo.contactosemergencianombre);
						$("#contactosemergenciaid-"+id).remove();
						$("#tbody_container_result_contactoemergencia").append(""
								+"<tr id='contactosemergenciaid-"+contacto_nuevo.contactosemergenciaid+"'>"
								+"	<td>"+contacto_nuevo.contactosemergencianombre+"</td>"
								+"	<td>"+getNombreParentesco(contacto_nuevo.parentescoid)+"</td>"
								+"	<td>"+contacto_nuevo.contactosemergenciatelefono+"</td>"
								+"	<td>"+contacto_nuevo.contactosemergenciacelular+"</td>"
								+"	<td>"+contacto_nuevo.contactosemergenciacorreo+"</td>"
								+"	<td>"
								+"		<input type='button' class='btn btn-danger' id='btn-delete-row-"+contacto_nuevo.contactosemergenciaid+"' value='[x]' onclick='deleteContactoEmergencia(this, "+contacto_nuevo.contactosemergenciaid+");'>"
								+"		<input type='button' class='btn btn-success' id='btn-add-new-row' value='[+]' onclick='addNewTrContactoEmergencia(this);'>"
								+"		<div id='cont_info_contactosemergenciaid_id-"+contacto_nuevo.contactosemergenciaid+"' data-json_contactosemergencia='"+JSON.stringify(contacto_nuevo)+"'></div>"
								+"	</td>"
								+"</tr>");
					}else
					{
						mostrarError(jsonResponse._Message);
						$(objectElement).prop('disabled', false);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [InformacionServer]");
				HiddenLoadBar()
		    }
		});
	}
}

function deleteContactoEmergencia(objectElement, id)
{
	var rq_pacienteid = $("#new_contacto_pacienteid").val();
	if(id!=undefined && id!=null && String(id).trim().length>0
			&& rq_pacienteid!=undefined && rq_pacienteid!=null && rq_pacienteid.trim().length>0)
	{
		$.ajax(
		{
	        async: true,
			url : 'elminaContactoEmergencia',
			type : 'POST',
			dataType : 'json',
			data:{	"ParametersRequest": "{rq_pacienteid: "+rq_pacienteid+", rq_contactosemergenciaid:"+id+"}"
			},
			beforeSend : function( xhr ) {
				if (xhr && xhr.overrideMimeType) {
					xhr.overrideMimeType("application/j-son;charset=UTF-8");
	            }
				console.log("Request Service...");
				$(objectElement).prop('disabled', true);
				ShowLoadBar();
			},
			success : function(jsonResponse)
			{
//					console.log(JSON.stringify(jsonResponse));
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
//						console.log("valida:: "+jsonResponse._State);
					if(jsonResponse._State=="true")
					{
						mostrarSuccess("Se elmino exitosamente el contacto");
						$("#contactosemergenciaid-"+id).remove();
					}else
					{
						mostrarError(jsonResponse._Message);
						$(objectElement).prop('disabled', false);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [InformacionServer]");
				HiddenLoadBar()
		    }
		});
	}
	else
	{
		mostrarError("No se recupero el id del contacto, por favor actualiza tu ventana e intentalo nuevamente.");
	}
}

function validaFormularioAlta()
{
	var error="";
	var in_pacientenombre=$("#pacientenombre").val();
	var in_pacienteapellidopaterno=$("#pacienteapellidopaterno").val();
	var in_pacienteapellidomaterno=$("#pacienteapellidomaterno").val();
	var in_pacienteedad=$("#pacienteedad").val();
	var in_pacientesexo=$("#pacientesexo").val();
	var in_pacienteestadocivil=$("#pacienteestadocivil").val();
	var in_pacientefechanacimiento=$("#pacientefechanacimiento").val();
	var in_pacientelugarnacimiento=$("#pacientelugarnacimiento").val();
	var in_pacientedomicilio=$("#pacientedomicilio").val();
	var in_pacienteciudad=$("#pacienteciudad").val();
	var in_pacientecodigopostal=$("#pacientecodigopostal").val();
	var in_pacientetelefono=$("#pacientetelefono").val();
	var in_pacientecelular=$("#pacientecelular").val();
	var in_pacientecorreo=$("#pacientecorreo").val();
	var in_estadoid=$("#estadoid").val();
	var in_pacientecurp=validaCamposCURP(in_pacientenombre,in_pacienteapellidopaterno,in_pacienteapellidomaterno,in_estadoid,in_pacientefechanacimiento,in_pacientesexo);

	if(in_pacientenombre==undefined || in_pacientenombre==null || in_pacientenombre.trim().length<1)
	{
	  error+="\n > Es necesario indicar el nombre";
	}
	if(in_pacienteapellidopaterno==undefined || in_pacienteapellidopaterno==null || in_pacienteapellidopaterno.trim().length<1)
	{
	  error+="\n > Es necesario indicar el apellido paterno";
	}
	if(in_pacienteapellidomaterno==undefined || in_pacienteapellidomaterno==null || in_pacienteapellidomaterno.trim().length<1)
	{
	  error+="\n > Es necesario indicar el apellido materno";
	}
	if(in_pacienteedad==undefined || in_pacienteedad==null || in_pacienteedad.trim().length<1)
	{
	  error+="\n > Es necesario indicar la edad";
	}
	if(in_pacientesexo==undefined || in_pacientesexo==null || in_pacientesexo.trim().length<1)
	{
	  error+="\n > Es necesario indicar el sexo";
	}
	if(in_pacienteestadocivil==undefined || in_pacienteestadocivil==null || in_pacienteestadocivil.trim().length<1)
	{
	  error+="\n > Es necesario indicar el estadocivil";
	}
	if(in_pacientefechanacimiento==undefined || in_pacientefechanacimiento==null || in_pacientefechanacimiento.trim().length<1)
	{
	  error+="\n > Es necesario indicar la fecha de nacimiento";
	}
	if(in_pacientelugarnacimiento==undefined || in_pacientelugarnacimiento==null || in_pacientelugarnacimiento.trim().length<1)
	{
	  error+="\n > Es necesario indicar el lugar de nacimiento";
	}
	if(in_pacientedomicilio==undefined || in_pacientedomicilio==null || in_pacientedomicilio.trim().length<1)
	{
	  error+="\n > Es necesario indicar el domicilio";
	}
	if(in_pacienteciudad==undefined || in_pacienteciudad==null || in_pacienteciudad.trim().length<1)
	{
	  error+="\n > Es necesario indicar el ciudad";
	}
	if(in_pacientecodigopostal==undefined || in_pacientecodigopostal==null || in_pacientecodigopostal.trim().length<1)
	{
	  error+="\n > Es necesario indicar el codigo postal";
	}
	if(in_pacientetelefono==undefined || in_pacientetelefono==null || in_pacientetelefono.trim().length<1)
	{
	  error+="\n > Es necesario indicar el telefono";
	}
	if(in_pacientecelular==undefined || in_pacientecelular==null || in_pacientecelular.trim().length<1)
	{
	  error+="\n > Es necesario indicar el celular";
	}
	if(in_pacientecorreo==undefined || in_pacientecorreo==null || in_pacientecorreo.trim().length<1)
	{
	  error+="\n > Es necesario indicar el correo";
	}
	if(in_estadoid==undefined || in_estadoid.trim().length<1 || parseInt(in_estadoid)<1)
	{
		error+="\n > Es necesario indicar el estado del domicilio";
	}
	if(in_pacientecurp==undefined || in_pacientecurp==null || in_pacientecurp.trim().length<1)
	{
		error+="\n > No se logro determinar la curp con los datos proporcionados";	
	}
	
	if(error!="")
	{
		mostrarError(error);
	}
	else
	{
		return getJsonAltaPaciente(-1, in_pacientenombre,in_pacienteapellidopaterno,in_pacienteapellidomaterno,in_pacienteedad,
				in_pacientesexo,in_pacienteestadocivil,in_pacientefechanacimiento,in_pacientelugarnacimiento,in_pacientedomicilio,
				in_pacienteciudad,in_pacientecodigopostal,in_pacientetelefono,in_pacientecelular,in_pacientecorreo, in_estadoid, in_pacientecurp);
	}
}

function validaFormAddContactoEmergencia()
{
	var error="";
	var in_pacienteid=$("#new_contacto_pacienteid").val();
	var in_parentescoid=$("#new_parentescoid").val();
	var in_contactosemergencianombre=$("#new_contactosemergencianombre").val();
	var in_contactosemergenciatelefono=$("#new_contactosemergenciatelefono").val();
	var in_contactosemergenciacelular=$("#new_contactosemergenciacelular").val();
	var in_contactosemergenciacorreo=$("#new_contactosemergenciacorreo").val();
	if(in_pacienteid==undefined || in_pacienteid==null || in_pacienteid.trim().length<1)
	{
		error+="\n > Es necesario indicar el pacienteid";
	}
	if(in_parentescoid==undefined || in_parentescoid==null || in_parentescoid.trim().length<1)
	{
		error+="\n > Es necesario indicar el parentesco";
	}
	if(in_contactosemergencianombre==undefined || in_contactosemergencianombre==null || in_contactosemergencianombre.trim().length<1)
	{
		error+="\n > Es necesario indicar el nombre del contacto";
	}
	if(in_contactosemergenciatelefono==undefined || in_contactosemergenciatelefono==null || in_contactosemergenciatelefono.trim().length<1)
	{
		error+="\n > Es necesario indicar el telefono del contacto";
	}
	if(in_contactosemergenciacelular==undefined || in_contactosemergenciacelular==null || in_contactosemergenciacelular.trim().length<1)
	{
		error+="\n > Es necesario indicar el celular del contacto";
	}
	if(in_contactosemergenciacorreo==undefined || in_contactosemergenciacorreo==null || in_contactosemergenciacorreo.trim().length<1)
	{
		error+="\n > Es necesario indicar el correo del contacto";
	}
	if(error!="")
	{
		mostrarError(error);
		return null;
	}
	else
	{
		return getJsonContactoEmergencia(in_pacienteid,in_parentescoid,in_contactosemergencianombre,in_contactosemergenciatelefono,in_contactosemergenciacelular,in_contactosemergenciacorreo);
	}
}

function loadInfo(pacienteid)
{
	if(pacienteid!=undefined && pacienteid!=null && pacienteid!="" && isInt(pacienteid))
	{
		var linkInfo = $("#cont_info_paciente_id-"+pacienteid);
//		console.log("GetInfo :: "+JSON.stringify(linkInfo.data("json_paciente")));
		var jsonPaciente = linkInfo.data("json_paciente");
		if(jsonPaciente!=undefined && jsonPaciente!=null)
		{
			$("#container_form").html(viewInformacionTabsPeciente());
			$("#nav-informacionpersonal").html(showInfoPaciente(jsonPaciente));
			$("#pacientesexo option[value='"+jsonPaciente.pacientesexo+"']").attr("selected", true);
			$("#pacienteestadocivil option[value='"+jsonPaciente.pacienteestadocivil+"']").attr("selected", true);
			$("#estadoid option[value='"+jsonPaciente.estadoid+"']").attr("selected", true);
			$("#nav-datosfiscales").html(showInfoDatosFiscales(jsonPaciente));
			$("#nav-antecedentes").html(showInfoAntecedentes(jsonPaciente));
			$("#nav-contactoemergencia").html(showInfoContactosEmergencia(jsonPaciente));
		}
		else
		{
			mostrarError("No se logro cargar la informacion del paciente, actualice su ventana e intentelo nuevamente.");
		}
	}
}

function viewAltaPaciente()
{
	return ""
	+"<h4>Alta de paciente</h4>"
	+"<div class='col-md-12'>" 
	+"	<form id='form_alta'>"
	+"		<div class='row'>"
	+"			<div class='col-md-4'>"
	+"				<div class='form-group'>"
	+"					<label>Nombre</label>"
	+"					<input type='text' class='form-control' id='pacientenombre' placeholder='Nombre' onchange='validaCampo(\"NULO\",this);'>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-4'>"
	+"				<div class='form-group'>"
	+"					<label>Apellido paterno</label>"
	+"					<input type='text' class='form-control' id='pacienteapellidopaterno' placeholder='Apellido Paterno' onchange='validaCampo(\"NULO\",this);'>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-4'>"
	+"				<div class='form-group'>"
	+"					<label>Apellido materno</label>"
	+"					<input type='text' class='form-control' id='pacienteapellidomaterno' placeholder='Apellido Materno' onchange='validaCampo(\"NULO\",this);'>"
	+"				</div>"
	+"			</div>"
	+"		</div>"
	+"		<div class='row'>"
	+"			<div class='col-md-2'>"
	+"				<div class='form-group'>"
	+"					<label>Edad</label>"
	+"					<input type='text' class='form-control' id='pacienteedad' placeholder='Edad' onchange='validaCampo(\"INTEGER\",this);'>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Sexo</label>"
	+"					<select class='form-control' id='pacientesexo'>"
	+"						<option value=''>Sexo</option>"
	+"						<option value='H'>Hombre</option>"
	+"						<option value='M'>Mujer</option>"
	+"					</select>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Estado Civil</label>"
	+"					<select class='form-control' id='pacienteestadocivil'>"
	+"						<option value=''>Estado Civil</option>"
	+"						<option value='S'>Soltero</option>"
	+"						<option value='C'>Casado</option>"
	+"						<option value='D'>Divorsiado</option>"
	+"						<option value='V'>Viudo</option>"
	+"					</select>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-4'>"
	+"				<div class='form-group'>"
	+"					<label>Fecha Nacimiento</label>"
	+"					<input type='date' class='form-control' id='pacientefechanacimiento' onchange='validaCampo(\"NULO\",this);'>"
	+"				</div>"
	+"			</div>"
	+"		</div>"
	+"		<div class='row'>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Lugar Nacimiento</label>"
	+"					<input type='text' class='form-control' id='pacientelugarnacimiento' placeholder='Lugar Nacimiento' onchange='validaCampo(\"NULO\",this);'>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Domicilio</label>"
	+"					<input type='text' class='form-control' id='pacientedomicilio' placeholder='Domicilio' onchange='validaCampo(\"NULO\",this);'>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Ciudad</label>"
	+"					<input type='text' class='form-control' id='pacienteciudad' placeholder='Ciudad' onchange='validaCampo(\"NULO\",this);'>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Codigo Postal</label>"
	+"					<input type='text' class='form-control' id='pacientecodigopostal' placeholder='Codigo Postal' onchange='validaCampo(\"CP\",this);'>"
	+"				</div>"
	+"			</div>"
	+"		</div>"
	+"		<div class='row'>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Estado</label>"
	+"					<select class='form-control' id='estadoid'>"
	+"						<option value=''>Estados</option>"
	+"					</select>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Telefono</label>"
	+"					<input type='text' class='form-control' id='pacientetelefono' placeholder='Telefono Casa' onchange='validaCampo(\"TEL\",this);'>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Celular</label>"
	+"					<input type='text' class='form-control' id='pacientecelular' placeholder='Celular' onchange='validaCampo(\"TEL\",this);'>"
	+"				</div>"
	+"			</div>"
	+"			<div class='col-md-3'>"
	+"				<div class='form-group'>"
	+"					<label>Correo electronico</label>"
	+"					<input type='text' class='form-control' id='pacientecorreo' placeholder='Email' onchange='validaCampo(\"EMAIL\",this);'>"
	+"				</div>"
	+"			</div>"
	+"		</div>"
	+"		<div class='row'>"
	+"			<div class='col-md-3'></div>"
	+"			<div class='col-md-3'>"
	+"				<button type='button' class='btn	 btn-sm btn-success' id='btn-alta-paciente' onclick='doAltaPaciente(this)'>Agregar Paciente</button>"
	+"			</div>"
	+"			<div class='col-md-3'>"
	+"				<button type='button' class='btn btn-sm btn-danger' id='btn-alta-paciente_cancelar' onclick='doCancelaAltaPaciente(this)'>Cancelar Alta</button>"
	+"			</div>"
	+"			<div class='col-md-3'></div>"
	+"		</div>"
	+"	</form>"
	+"</div>";
}

function viewBuscarPaciente()
{
	return ""
	+"<div class='col-md-12'>"
	+"<div class='row'>"
	+"	<div class='col-md-4'></div>"
	+"	<div class='col-md-4'>"
	+"		<div class='form-group'>"
	+"			<label>Buscar Paciente</label>"
	+"			<input type='text' class='form-control' id='ed_serach_paciente' placeholder='ID / Nombre / Correo' onchange='validaCampo(\"NULO\",this);'>"
	+"		</div>"
	+"	</div>"
	+"	<div class='col-md-4'></div>"
	+"</div>"
	+"<div class='row'>"
	+"	<div class='col-md-4'></div>"
	+"	<div class='col-md-4'>"
	+"		<div class='form-group'>"
	+"			<button type='button' class='btn btn-info btn-sm' id='btn_search_paciente' onclick='doSearchPaciente(this)'>Buscar</button>"
	+"			<button type='button' class='btn btn-danger btn-sm' id='btn_search_paciente_regresar' onclick='doCancelaBuscarPaciente()'>Regresar</button>"
	+"		</div>"
	+"	</div>"
	+"	<div class='col-md-4'></div>"
	+"</div>"
	+"<div class='row'>"
	+"	<div class='col-md-1'></div>"
	+"	<div class='col-md-10' id='container_table_result_search'>"
	+"	</div>"
	+"	<div class='col-md-1'></div>"
	+"</div>"
	+"</div>";
}

function viewInformacionTabsPeciente()
{
	return ""
	+"	<div class='col-md-12'>"
	+"		<nav>"
	+"				<div class='nav nav-tabs' id='nav-tab' role='tablist'>"
	+"						<a class='nav-item nav-link active' id='nav-informacionpersonal-tab' data-toggle='tab' href='#nav-informacionpersonal' role='tab' aria-controls='nav-datosfiscales' aria-selected='true'>Informacion Personal</a>"
	+"						<a class='nav-item nav-link' id='nav-datosfiscales-tab' data-toggle='tab' href='#nav-datosfiscales' role='tab' aria-controls='nav-datosfiscales' aria-selected='false'>Datos Fiscales</a>"
	+"						<a class='nav-item nav-link' id='nav-antecedentes-tab' data-toggle='tab' href='#nav-antecedentes' role='tab' aria-controls='nav-antecedentes' aria-selected='false'>Antecedentes</a>"
	+"						<a class='nav-item nav-link' id='nav-contactoemergencia-tab' data-toggle='tab' href='#nav-contactoemergencia' role='tab' aria-controls='nav-contactoemergencia' aria-selected='false'>Contactos Emergencia</a>"
	+"				</div>"
	+"		</nav>"
	+"		<div class='tab-content' id='nav-tabContent'>"
	+"			<div class='tab-pane fade show active' id='nav-informacionpersonal' role='tabpanel' aria-labelledby='nav-informacionpersonal-tab'>"
	+"				<!-- Formulario para editar informacion -->"
	+"			</div>"
	+"			<div class='tab-pane fade' id='nav-datosfiscales' role='tabpanel' aria-labelledby='nav-datosfiscales-tab'>"
	+"				<!-- Datos Fiscales -->"
	+"			</div>"
	+"			<div class='tab-pane fade' id='nav-antecedentes' role='tabpanel' aria-labelledby='nav-antecedentes-tab'>"
	+"				<!-- Antecedentes -->"
	+"			</div>"
	+"			<div class='tab-pane fade' id='nav-contactoemergencia' role='tabpanel' aria-labelledby='nav-contactoemergencia-tab'>"
	+"				<!-- Contactos de emergencia -->"
	+"			</div>"
	+"		</div>"
	+"	</div>";
}

function showInfoPaciente(jsonPaciente)
{
    return ""
    +"<form id='form_edit_paciente'>"
    +"  <div class='row'>"
    +"      <div class='col-md-2'>"
    +"          <div class='form-group'>"
    +"              <label>ID Paciente</label>"
    +"              <input type='text' class='form-control' value='"+jsonPaciente.pacienteid+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-4'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Nombre</label>"
    +"				<input type='hidden' id='pacienteid' value='"+jsonPaciente.pacienteid+"'/>"
    +"              <input type='text' class='form-control' id='pacientenombre' placeholder='Nombre' onchange='validaCampo(\"NULO\",this);' value='"+jsonPaciente.pacientenombre+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Apellido paterno</label>"
    +"              <input type='text' class='form-control' id='pacienteapellidopaterno' placeholder='Apellido Paterno' onchange='validaCampo(\"NULO\",this);' value='"+jsonPaciente.pacienteapellidopaterno+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Apellido materno</label>"
    +"              <input type='text' class='form-control' id='pacienteapellidomaterno' placeholder='Apellido Materno' onchange='validaCampo(\"NULO\",this);' value='"+jsonPaciente.pacienteapellidomaterno+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"  </div>"
    +"  <div class='row'>"
    +"      <div class='col-md-2'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Edad</label>"
    +"              <input type='text' class='form-control' id='pacienteedad' placeholder='Edad' onchange='validaCampo(\"INTEGER\",this);' value='"+jsonPaciente.pacienteedad+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Sexo</label>"
    +"              <select class='form-control' id='pacientesexo' placeholder='Sexo' value='"+jsonPaciente.pacientesexo+"' disabled>"
    +"                  <option value=''>Sexo</option>"
    +"                  <option value='H'>Hombre</option>"
    +"                  <option value='M'>Mujer</option>"
    +"              </select>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Estado Civil</label>"
    +"              <select class='form-control' id='pacienteestadocivil' placeholder='Estado Civil' value='"+jsonPaciente.pacienteestadocivil+"' disabled>"
    +"                  <option value=''>Estado Civil</option>"
    +"                  <option value='S'>Soltero</option>"
    +"                  <option value='C'>Casado</option>"
    +"                  <option value='D'>Divorsiado</option>"
    +"                  <option value='V'>Viudo</option>"
    +"              </select>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-4'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Fecha Nacimiento</label>"
    +"              <input type='date' class='form-control' id='pacientefechanacimiento' placeholder='Fecha Nacimiento' onchange='validaCampo(\"NULO\",this);' value='"+jsonPaciente.pacientefechanacimiento+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"  </div>"
    +"  <div class='row'>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Lugar Nacimiento</label>"
    +"              <input type='text' class='form-control' id='pacientelugarnacimiento' placeholder='Lugar Nacimiento' onchange='validaCampo(\"NULO\",this);' value='"+jsonPaciente.pacientelugarnacimiento+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Domicilio</label>"
    +"              <input type='text' class='form-control' id='pacientedomicilio' placeholder='Domicilio' onchange='validaCampo(\"NULO\",this);' value='"+jsonPaciente.pacientedomicilio+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Ciudad</label>"
    +"              <input type='text' class='form-control' id='pacienteciudad' placeholder='Ciudad' onchange='validaCampo(\"NULO\",this);' value='"+jsonPaciente.pacienteciudad+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Codigo Postal</label>"
    +"              <input type='text' class='form-control' id='pacientecodigopostal' placeholder='Codigo Postal' onchange='validaCampo(\"CP\",this);' value='"+jsonPaciente.pacientecodigopostal+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"  </div>"
    +"  <div class='row'>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Estado</label>"
    +"              <select class='form-control' id='estadoid' placeholder='Estado' disabled>"
    +"                  <option value=''>Estados</option>"
    + getHtmlEstados(jsonPaciente.estadoid)
    +"              </select>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Telefono</label>"
    +"              <input type='text' class='form-control' id='pacientetelefono' placeholder='Telefono Casa' onchange='validaCampo(\"TEL\",this);' value='"+jsonPaciente.pacientetelefono+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Celular</label>"
    +"              <input type='text' class='form-control' id='pacientecelular' placeholder='Celular' onchange='validaCampo(\"TEL\",this);' value='"+jsonPaciente.pacientecelular+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <div class='form-group input-edit-paciente-PERSONAL'>"
    +"              <label>Correo electronico</label>"
    +"              <input type='text' class='form-control' id='pacientecorreo' placeholder='Email' onchange='validaCampo(\"EMAIL\",this);' value='"+jsonPaciente.pacientecorreo+"' disabled>"
    +"          </div>"
    +"      </div>"
    +"  </div>"
    +"  <div class='row'>"
    +"		<div class='col-md-3'>"
    +"			<button type='button' class='btn btn-sm btn-warning btn-block' id='btn-load-tratamientos' onclick='cargarTratamientos("+jsonPaciente.pacienteid+");'>Tratamientos</button>"
    +"		</div>"
    +"      <div class='col-md-3'>"
    +"          <button type='button' class='btn btn-sm btn-primary btn-block' id='btn-show-editar' onclick='habilitaEdicion(\"PERSONAL\");'>Editar Paciente</button>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <button type='button' class='btn btn-sm btn-success btn-block' id='btn-save-editar-info-PERSONAL' onclick='saveInfo(\"PERSONAL\", this);' disabled>Guardar</button>"
    +"      </div>"
    +"      <div class='col-md-3'>"
    +"          <button type='button' class='btn btn-sm btn-danger btn-block' id='btn-cerrar-info-paciente' onclick='closeInfo();'>Regresar</button>"
    +"      </div>"
    +"      <div class='col-md-3'></div>"
    +"  </div>"
    +"</form>";
}

function showInfoDatosFiscales(jsonPaciente)
{
    return ""
    +"<div class='col-md-12'>" 
    +"    <form id='form_datosfiscales'>"
    +"          <div class='row'>"
    +"                <div class='col-md-6'>"
    +"                      <div class='form-group input-edit-paciente-DATOSFISCALES'>"
    +"                            <label>Nombre Completo</label>"
    +"							<input type='hidden' id='datofiscalid' value='"+jsonPaciente.datosfiscales.datofiscalid+"'/>"
    +"							<input type='hidden' id='pacienteid' value='"+jsonPaciente.pacienteid+"'/>"
    +"                            <input type='text' class='form-control' id='datofiscalnombre' value='"+jsonPaciente.datosfiscales.datofiscalnombre+"' placeholder='Nombre completo' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-3'>"
    +"                      <div class='form-group input-edit-paciente-DATOSFISCALES'>"
    +"                            <label>RFC</label>"
    +"                            <input type='text' class='form-control' id='datofiscalrfc' value='"+jsonPaciente.datosfiscales.datofiscalrfc+"' placeholder='RFC' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-3'>"
    +"                      <div class='form-group'>"
    +"                            <label>Ultima Modificacion</label>"
    +"                            <input type='text' class='form-control' id='datofiscalfechamodificacion' value='"+jsonPaciente.datosfiscales.datofiscalfechamodificacion+"' placeholder='Fecha' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"          </div>"
    +"          <div class='row'>"
    +"                <div class='col-md-8'>"
    +"                      <div class='form-group input-edit-paciente-DATOSFISCALES'>"
    +"                            <label>Direccion</label>"
    +"                            <input type='text' class='form-control' id='datofiscaldireccion' value='"+jsonPaciente.datosfiscales.datofiscaldireccion+"' placeholder='Direccion' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-DATOSFISCALES'>"
    +"                            <label>Correo Electronico</label>"
    +"                            <input type='text' class='form-control' id='datofiscalcorreo' value='"+jsonPaciente.datosfiscales.datofiscalcorreo+"' placeholder='Correo' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"          </div>"
    +"          <div class='row'>"
    +"                <div class='col-md-3'></div>"
    +"                <div class='col-md-3'>"
    +"                      <button type='button' class='btn btn-sm btn-success' id='btn-show-editar' onclick='habilitaEdicion(\"DATOSFISCALES\");'>Editar Paciente</button>"
    +"                </div>"
    +"                <div class='col-md-3'>"
    +"                      <button type='button' class='btn btn-sm btn-success' id='btn-save-editar-info-DATOSFISCALES' onclick='saveInfo(\"DATOSFISCALES\", this);' disabled>Guardar</button>"
    +"                </div>"
    +"                <div class='col-md-3'>"
    +"                      <button type='button' class='btn btn-sm btn-danger' id='btn-cerrar-info-paciente' onclick='closeInfo();'>Regresar</button>"
    +"                </div>"
    +"                <div class='col-md-3'></div>"
    +"          </div>"
    +"    </form>"
    +"</div>";
}

function showInfoAntecedentes(jsonPaciente)
{
    return ""
    +"<div class='col-md-12'>" 
    +"    <form id='form_datosfiscales'>"
    +"          <div class='row'>"
    +"                <div class='col-md-6'>"
    +"                      <div class='form-group'>"
    +"                            <label>Nombre Completo</label>"
    +"                            <input type='text' class='form-control' id='' value='"
    			+jsonPaciente.pacientenombre+" "+jsonPaciente.pacienteapellidopaterno+" "+jsonPaciente.pacienteapellidomaterno+"' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-3'>"
    +"                      <div class='form-group'>"
    +"                            <label>Sexo</label>"
    +"                            <input type='text' class='form-control' id='' value='"+(jsonPaciente.pacientesexo=="H" ? "Hombre" : "Mujer")+"' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-3'>"
    +"                      <div class='form-group'>"
    +"                            <label>Edad</label>"
    +"                            <input type='text' class='form-control' id='' value='"+jsonPaciente.pacienteedad+"' disabled>"
    +"                      </div>"
    +"                </div>"
    +"          </div>"
    +"          <div class='row'>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>Esta en tretamiento medico?</label>"
    +"								<input type='hidden' id='pacienteid' value='"+jsonPaciente.pacienteid+"'/>"
    +"								<input type='hidden' id='pacienteid' value='"+jsonPaciente.antecedentes.antecedenteid+"'/>"
    +"                            <input type='text' class='form-control' id='antecedentetratamientomedico' value='"+jsonPaciente.antecedentes.antecedentetratamientomedico+"' placeholder='Esta en tratamiento medico' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>Esta embarazada?</label>"
    +"                            <input type='text' class='form-control' id='antecedenteembarazo' value='"+jsonPaciente.antecedentes.antecedenteembarazo+"' placeholder='Esta embarazada' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>Toma algun medicamento</label>"
    +"                            <input type='text' class='form-control' id='antecedentemedicamentos' value='"+jsonPaciente.antecedentes.antecedentemedicamentos+"' placeholder='Toma medicamentos' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"          </div>"
    +"          <div class='row'>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>Gastrointestinales, sinusitis, asma, quirurgicos</label>"
    +"                            <input type='text' class='form-control' id='antecedentegsaq' value='"+jsonPaciente.antecedentes.antecedentegsaq+"' placeholder='Gastrointestinales, sinusitis, asma, quirurgicos' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>Es alergico algun medicamento o substancia?</label>"
    +"                            <input type='text' class='form-control' id='antecedentealergias' value='"+jsonPaciente.antecedentes.antecedentealergias+"' placeholer='Es alergico' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>ETS-VIH</label>"
    +"                            <input type='text' class='form-control' id='antecedenteetsvih' value='"+jsonPaciente.antecedentes.antecedenteetsvih+"' placeholer='ETS-VIH' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"          </div>"
    +"          <div class='row'>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>Tiene problemas de coagulacion?</label>"
    +"                            <input type='text' class='form-control' id='antecedentecoagulacion' value='"+jsonPaciente.antecedentes.antecedentecoagulacion+"' placeholer='Problemas de coagulacion' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>Padece cancer?</label>"
    +"                            <input type='text' class='form-control' id='antecedentecancer' value='"+jsonPaciente.antecedentes.antecedentecancer+"' placeholer='Cancer' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>Cardiacos, circulatorios, respiratorios y/o neurologicos</label>"
    +"                            <input type='text' class='form-control' id='antecedenteccrn' value='"+jsonPaciente.antecedentes.antecedenteccrn+"' placeholer='Cardiacos, circulatorios, respiratorios y/o neurologicos' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"          </div>"
    +"          <div class='row'>"
    +"                <div class='col-md-4'>"
    +"                      <div class='form-group input-edit-paciente-ANTECEDENTES'>"
    +"                            <label>Presion alterial, diabeticos, traumaticos y/o fiebre reumatica</label>"
    +"                            <input type='text' class='form-control' id='antecedentepdtf' value='"+jsonPaciente.antecedentes.antecedentepdtf+"' placeholer='Presion alterial, diabeticos, traumaticos y/o fiebre reumatica' onchange='validaCampo(\"NULO\",this);' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-4'>"
    +"                </div>"
    +"                <div class='col-md-4'>"
    +"                </div>"
    +"          </div>"
    +"          <div class='row'>"
    +"                <div class='col-md'>"
    +"                      <button type='button' class='btn btn-sm btn-success' id='btn-show-editar' onclick='habilitaEdicion(\"ANTECEDENTES\");'>Editar Paciente</button>"
    +"                </div>"
    +"                <div class='col-md'>"
    +"                      <button type='button' class='btn btn-sm btn-success' id='btn-save-editar-info-ANTECEDENTES' onclick='saveInfo(\"ANTECEDENTES\", this);' disabled>Guardar</button>"
    +"                </div>"
    +"                <div class='col-md'>"
    +"                      <button type='button' class='btn btn-sm btn-danger' id='btn-cerrar-info-paciente' onclick='closeInfo();'>Regresar</button>"
    +"                </div>"
    +"          </div>"
    +"    </form>"
    +"</div>";
}

function showInfoContactosEmergencia(jsonPaciente)
{
    var html= ""
    +"<div class='col-md-12'>" 
    +"    <form id='form_datosfiscales'>"
    +"		<input type='hidden' id='new_contacto_pacienteid' value='"+jsonPaciente.pacienteid+"'>"
    +"          <div class='row'>"
    +"                <div class='col-md-6'>"
    +"                      <div class='form-group'>"
    +"                            <label>Nombre Completo</label>"
    +"                            <input type='text' class='form-control' id='' value='"
    			+jsonPaciente.pacientenombre+" "+jsonPaciente.pacienteapellidopaterno+" "+jsonPaciente.pacienteapellidomaterno+"' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-3'>"
    +"                      <div class='form-group'>"
    +"                            <label>Sexo</label>"
    +"                            <input type='text' class='form-control' id='' value='"+(jsonPaciente.pacientesexo=="H" ? "Hombre" : "Mujer")+"' disabled>"
    +"                      </div>"
    +"                </div>"
    +"                <div class='col-md-3'>"
    +"                      <div class='form-group'>"
    +"                            <label>Edad</label>"
    +"                            <input type='text' class='form-control' id='' value='"+jsonPaciente.pacienteedad+"' disabled>"
    +"                      </div>"
    +"                </div>"
    +"          </div>"
    +"          <div class='row'>"
	+"				<div class='card-body'>"
	+"					<div class='table-responsive'>"
	+"						<div class='table-responsive'>"
	+"							<table class='table table-bordered' id='dataTable' width='100%' cellspacing='0'>"
	+"								<thead>"
	+"									<th>Nombre</th>"
	+"									<th>Parentesco</th>"
	+"									<th>Telefono</th>"
	+"									<th>Celular</th>"
	+"									<th>Correo</th>"
	+"									<th></th>"
	+"								</thead>"
	+"								<tfoot>"
	+"									<th>Nombre</th>"
	+"									<th>Parentesco</th>"
	+"									<th>Telefono</th>"
	+"									<th>Celular</th>"
	+"									<th>Correo</th>"
	+"									<th></th>"
	+"								</tfoot>"
	+"								<tbody id='tbody_container_result_contactoemergencia'>";

	if(jsonPaciente.contactosemergencia!=undefined && jsonPaciente.contactosemergencia!=null)
	{
		var listaResultados =jsonPaciente.contactosemergencia;
		for(var i=0; i<listaResultados.length; i++)
		{
			html+=""
					+"<tr id='contactosemergenciaid-"+listaResultados[i].contactosemergenciaid+"'>"
					+"	<td>"+listaResultados[i].contactosemergencianombre+"</td>"
					+"	<td>"+listaResultados[i].parentescos.parentesconombre+"</td>"
					+"	<td>"+listaResultados[i].contactosemergenciatelefono+"</td>"
					+"	<td>"+listaResultados[i].contactosemergenciacelular+"</td>"
					+"	<td>"+listaResultados[i].contactosemergenciacorreo+"</td>"
					+"	<td>"
					+"		<input type='button' class='btn btn-danger' id='btn-delete-row-"+listaResultados[i].contactosemergenciaid+"' value='[x]' onclick='deleteContactoEmergencia(this, "+listaResultados[i].contactosemergenciaid+");'>"
//					+"		<input type='button' class='btn btn-success' id='btn-add-new-row' value='[+]' onclick='addNewTrContactoEmergencia(this);'>"
					+"		<div id='cont_info_contactosemergenciaid_id-"+listaResultados[i].contactosemergenciaid+"' data-json_contactosemergencia='"+JSON.stringify(listaResultados[i])+"'></div>"
					+"	</td>"
					+"</tr>";
		}
	}
//	else
//	{
		var id=Date.now();
//		console.log("No se encontraron contactos de emergencia");
	html+=""
		+"<tr id='contactosemergenciaid-"+id+"'>"
		+"	<td><input type='text' class='form-control' id='new_contactosemergencianombre' value='' placeholder='Nombre' onchange='validaCampo(\"NULO\",this);'/></td>"
		+"	<td>"
		+"		<select class='form-control' id='new_parentescoid' placeholder='Parentesco'>"
		+getHTMLListParentescos()
		+"		</select>"
		+"	</td>"
		+"	<td><input type='text' class='form-control' id='new_contactosemergenciatelefono' value='' placeholder='Telefono' onchange='validaCampo(\"TEL\",this);'/></td>"
		+"	<td><input type='text' class='form-control' id='new_contactosemergenciacelular' value='' placeholder='Celular' onchange='validaCampo(\"TEL\",this);'/></td>"
		+"	<td><input type='text' class='form-control' id='new_contactosemergenciacorreo' value='' placeholder='Correo' onchange='validaCampo(\"EMAIL\",this);'/></td>"
		+"	<td>"
		+"		<input type='button' class='btn btn-success' id='btn-new-contactoemergencia-"+id+"' value='[+]' onclick='addContactoEmergencia(this, "+id+")'>"
		+"	</td>"
		+"</tr>";
//	}
	
	html+=""

	+"								</tbody>"
	+"							</table>"
	+"						</div>"
	+"					</div>"
	+"				</div>"
	+"			</div>"
    +"          <div class='row'>"
    +"                <div class='col-md-4'></div>"
    +"                <div class='col-md-4'>"
    +"                      <button type='button' class='btn btn-sm btn-danger btn-block' id='btn-cerrar-info-paciente' onclick='closeInfo();'>Regresar</button>"
    +"                </div>"
    +"                <div class='col-md-4'></div>"
    +"          </div>"
    +"    </form>"
    +"</div>";
    
    return html;
}

function addNewTrContactoEmergencia(element)
{
	var id=Date.now();
	$("#tbody_container_result_contactoemergencia").append(""
			+"<tr id='contactosemergenciaid-"+id+"'>"
			+"	<td><input type='text' class='form-control' id='new_contactosemergencianombre' value='' placeholder='Nombre' onchange='validaCampo(\"NULO\",this);'/></td>"
			+"	<td>"
			+"		<select class='form-control' id='new_parentescoid' placeholder='Parentesco'>"
			+getHTMLListParentescos()
			+"		</select>"
			+"	</td>"
			+"	<td><input type='text' class='form-control' id='new_contactosemergenciatelefono' value='' placeholder='Telefono' onchange='validaCampo(\"TEL\",this);'/></td>"
			+"	<td><input type='text' class='form-control' id='new_contactosemergenciacelular' value='' placeholder='Celular' onchange='validaCampo(\"TEL\",this);'/></td>"
			+"	<td><input type='text' class='form-control' id='new_contactosemergenciacorreo' value='' placeholder='Correo' onchange='validaCampo(\"EMAIL\",this);'/></td>"
			+"	<td>"
			+"		<input type='button' class='btn btn-success' id='btn-new-contactoemergencia-"+id+"' value='[+]' onclick='addContactoEmergencia(this, "+id+");'>"
			+"	</td>"
			+"</tr>");
	$("#"+element.id).remove();
	doPrintListParentescos();
}

//function doListaParentescos()
//{
//	if(listaParentescos!=undefined && listaParentescos!=null && listaParentescos.length>0)
//	{
//		doPrintListParentescos();
//	}
//	else
//	{
//		mostrarError("Error al recuperar los parentescos");
//	}
//}

function doPrintListParentescos()
{
	if(listaParentescos!=undefined && listaParentescos!=null && listaParentescos.length>0)
	{
		$("select[id='new_parentescoid']").empty();
		$("select[id='new_parentescoid']").append(new Option('Parentesco', "", true, true));
		for(var i=0; i<listaParentescos.length; i++)
		{
			$("select[id='new_parentescoid']").append(new Option(listaParentescos[i].parentesconombre, listaParentescos[i].parentescoid, false, false));
		}
	}
	else
	{
		mostrarError("Error al recuperar los parentescos");
	}
}

//function doHtmlListaParentescos()
//{
//	if(listaParentescos!=undefined && listaParentescos!=null && listaParentescos.length>0)
//	{
//		return getHTMLListParentescos();
//	}
//	else
//	{
//		alert("No se recuperaron la lista de parentescos");
//		return "";
//	}
//}

function getHTMLListParentescos()
{
	if(listaParentescos!=undefined && listaParentescos!=null && listaParentescos.length>0)
	{
		var html="";
		html+="<option value=''>Parentesco</option>";
		for(var i=0; i<listaParentescos.length; i++)
		{
			html+="<option value='"+listaParentescos[i].parentescoid+"'>"+listaParentescos[i].parentesconombre+"</option>";
		}
		return html;
	}
	else
	{
		console.log("Error al recuperar los parentescos");
		return "";
	}
}

function getNombreParentesco(parentescoid)
{
	if(listaParentescos!=undefined && listaParentescos!=null && listaParentescos.length>0)
	{
		for(var i=0; i<listaParentescos.length; i++)
		{
			if(parseInt(parentescoid)==listaParentescos[i].parentescoid)
			{
				return listaParentescos[i].parentesconombre;
			}
		}
	}
	return "S/D";
}

function getParentescos()
{
	if(listaParentescos==undefined || listaParentescos==null || listaParentescos.length<0)
	{
		$.ajax(
		{
	        async: true,
			url : '/catalogos/parentescos',
			type : 'POST',
			dataType : 'json',
			success : function(jsonResponse)
			{
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
					if(jsonResponse._State=="true")
					{
						listaParentescos=JSON.parse(jsonResponse._ObjectJson);
					}
					else
					{
						mostrarError(jsonResponse._Message);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [ListaParentescos]");
		    }
		});
	}
}

function getEstados()
{
	if(listaEstados==undefined || listaEstados==null || listaEstados.length<0)
	{
		$.ajax(
		{
	        async: true,
			url : '/catalogos/estados',
			type : 'POST',
			dataType : 'json',
			success : function(jsonResponse)
			{
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
					if(jsonResponse._State=="true")
					{
						listaEstados=JSON.parse(jsonResponse._ObjectJson);
					}else
					{
						mostrarError(jsonResponse._Message);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [InformacionServer]");
		    }
		});
	}
}

function doListaEstadosAlta()
{
	if(listaEstados==undefined || listaEstados==null || listaEstados.length<1)
	{
		mostrarError("No se descargo el catalogo de estados, por favor actualiza tu ventana e intentlo nuevamente.");
	}
	else
	{
		doPrintListEstados();
	}
}

function getHtmlEstados(estadoid)
{
	var html="<option value=''>Estado</option>";
	for(var i=0; i<listaEstados.length; i++)
	{
		html+="<option value='"+listaEstados[i].estadoid+"' "+(parseInt(estadoid)==listaEstados[i].estadoid ? "selected" : "")+">"+listaEstados[i].estadonombre+"</option>";
	}
	return html;
}

function doPrintListEstados()
{
	$("select[id='estadoid']").empty();
	if(listaEstados!=undefined && listaEstados!=null && listaEstados.length>0)
	{
		$("#estadoid").append(new Option('Selecciona un estado', -1, true, true));
		for(var i=0; i<listaEstados.length; i++)
		{
			$("#estadoid").append(new Option(listaEstados[i].estadonombre, listaEstados[i].estadoid, false, false));
		}
	}
	else
	{
		mostrarError("Error al recuperar los estados");
	}
}

function getJsonContactoEmergencia(in_pacienteid,in_parentescoid,in_contactosemergencianombre,in_contactosemergenciatelefono,in_contactosemergenciacelular,in_contactosemergenciacorreo)
{
	return {
		"rq_pacienteid" : in_pacienteid,
		"rq_parentescoid" : in_parentescoid,
		"rq_contactosemergencianombre" : in_contactosemergencianombre,
		"rq_contactosemergenciatelefono" : in_contactosemergenciatelefono,
		"rq_contactosemergenciacelular" : in_contactosemergenciacelular,
		"rq_contactosemergenciacorreo" : in_contactosemergenciacorreo
	};
}

function getJsonBuscarPaciente(in_pacienteid)
{
	return {
		"rq_pacienteid" : in_pacienteid,
		"rq_pacientenombre" : "",
		"rq_pacienteapellidopaterno" : "",
		"rq_pacienteapellidomaterno" : "",
		"rq_pacienteedad" : "",
		"rq_pacientesexo" : "",
		"rq_pacienteestadocivil" : "",
		"rq_pacientefechanacimiento" : "",
		"rq_pacientelugarnacimiento" : "",
		"rq_pacientedomicilio" : "",
		"rq_pacienteciudad" : "",
		"rq_pacientecodigopostal" : "",
		"rq_pacientetelefono" : "",
		"rq_pacientecelular" : "",
		"rq_pacientecorreo" : "",
		"rq_estadoid" : ""
	};
}

function getJsonAltaPaciente(in_pacienteid, in_pacientenombre,in_pacienteapellidopaterno,in_pacienteapellidomaterno,in_pacienteedad,
		in_pacientesexo,in_pacienteestadocivil,in_pacientefechanacimiento,in_pacientelugarnacimiento,in_pacientedomicilio,
		in_pacienteciudad,in_pacientecodigopostal,in_pacientetelefono,in_pacientecelular,in_pacientecorreo, in_estadoid, in_pacientecurp)
{
	return {
		"rq_pacienteid" : in_pacienteid,
		"rq_pacientenombre" : in_pacientenombre,
		"rq_pacienteapellidopaterno" : in_pacienteapellidopaterno,
		"rq_pacienteapellidomaterno" : in_pacienteapellidomaterno,
		"rq_pacienteedad" : in_pacienteedad,
		"rq_pacientesexo" : in_pacientesexo,
		"rq_pacienteestadocivil" : in_pacienteestadocivil,
		"rq_pacientefechanacimiento" : in_pacientefechanacimiento,
		"rq_pacientelugarnacimiento" : in_pacientelugarnacimiento,
		"rq_pacientedomicilio" : in_pacientedomicilio,
		"rq_pacienteciudad" : in_pacienteciudad,
		"rq_pacientecodigopostal" : in_pacientecodigopostal,
		"rq_pacientetelefono" : in_pacientetelefono,
		"rq_pacientecelular" : in_pacientecelular,
		"rq_pacientecorreo" : in_pacientecorreo,
		"rq_estadoid" : in_estadoid,
		"rq_pacientecurp" : in_pacientecurp
	};
}

function cargarTratamientos(pacienteid)
{
	window.location.href = '/tratamientos/?rq_paciente='+pacienteid;
}

function validaCamposCURP(curp_nombre, curp_apellido_paterno, curp_apellido_materno, curp_estado, curp_fecha_nacimiento, curp_sexo)
{
	if((curp_nombre!=undefined && curp_nombre!=null && curp_nombre.trim().length>2) &&
			(curp_apellido_paterno!=undefined && curp_apellido_paterno!=null && curp_apellido_paterno.trim().length>2) &&
				(curp_apellido_materno!=undefined && curp_apellido_materno!=null && curp_apellido_materno.trim().length>2) &&
					(curp_estado!=undefined && curp_estado!=null && parseInt(curp_estado)>0) &&
						(curp_fecha_nacimiento!=undefined && curp_fecha_nacimiento!=null && curp_fecha_nacimiento.trim().length>5) &&
							(curp_sexo!=undefined && curp_sexo!=null && curp_sexo.trim().length>0 && curp_sexo!="N")
			)
	{
		curp_fecha_nacimiento = curp_fecha_nacimiento.split("-");
		if(parseInt(curp_fecha_nacimiento[0])>1900)
		{
			var parametrosCURP={
					nombre : curp_nombre,
					apellido_paterno : curp_apellido_paterno,
					apellido_materno : curp_apellido_materno,
					estado : parseInt(curp_estado),
					fecha_nacimiento : curp_fecha_nacimiento,
					sexo : curp_sexo
				};
			console.log("Parametros: "+JSON.stringify(parametrosCURP));
			var CURP=getCURP(parametrosCURP,true);
			console.log("CURP:["+CURP+"]");
			if(CURP!="ERROR" && CURP.length==18)
			{
				return CURP;
			}
		}
	}
	return null;
}