var datatable_lista;
var listadoTipotratamiento=null;

$(document).ready(function()
{
	$("#btn-add-catalogo").click(function(){
		var tipocatalogo = $("#tipo_catalogo").val();
		var jsonRequest;
		console.log("Agregar al catalogo "+tipocatalogo);
		
		if(tipocatalogo!=undefined && tipocatalogo !=null && tipocatalogo.trim().length>0)
		{
			jsonRequest = getJsonResquest(tipocatalogo);
			if(jsonRequest!=undefined && jsonRequest!=null)
			{
				jsonRequest = { "tipoCatalogo": tipocatalogo.trim(), "jsonRequest": JSON.stringify(jsonRequest)};
				console.log("jsonRequest :: "+JSON.stringify(jsonRequest));
				addCatalogo(jsonRequest, this);
			}
		}
	});
	
	$("#tipo_catalogo").change(function(){
		var tipocatalogo = $("#tipo_catalogo").val();
		console.log("Carga informacion para catalogo ["+tipocatalogo+"]");
		if(tipocatalogo!=undefined && tipocatalogo !=null && tipocatalogo.trim().length>0)
		{
			datatable_lista.clear().draw();
			switch(tipocatalogo)
			{
				case "tipotratamiento":
					getTipotratamiento();
				break;
				default:
					mostrarError("Tipo de catalogo no soportado");
			}
		}
	});

	datatable_lista = $("#datatable_lista").DataTable();
});

function addEliminar(objectElement, tipoCatalogo, id)
{
	console.log("Add catalogo");
	var jsonRequest = { "tipoCatalogo" : tipoCatalogo, "id" : id};
	$.ajax(
	{
        async: true,
		url : "eliminarCatalogo",
		type : "POST",
		dataType : "json",
		data:{	"ParametersRequest": JSON.stringify(jsonRequest)
		},
		beforeSend : function( xhr ) {
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
            }
			console.log("Request Service...");
			$(objectElement).prop("disabled", true);
		},
		success : function(jsonResponse)
		{
//				console.log(JSON.stringify(jsonResponse));
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
//					console.log("valida:: "+jsonResponse._State);
				if(jsonResponse._State=="true")
				{
					mostrarSuccess(jsonResponse._Message);
					datatable_lista.clear().draw();
					listadoTipotratamiento=null;
					switch(jsonRequest.tipoCatalogo)
					{
						case "tipotratamiento":
							getTipotratamiento();
						break;
						default:
							mostrarError("Tipo de catalogo no soportado");
					}
				}else
				{
					mostrarError(jsonResponse._Message);
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [addCatalogo]");
			$(objectElement).prop("disabled", false);
	    }
	});
}

function addCatalogo(jsonRequest, objectElement)
{
	console.log("Add catalogo");
	if(jsonRequest!=undefined && jsonRequest!=null)
	{
		$.ajax(
		{
	        async: true,
			url : "guardarCatalogo",
			type : "POST",
			dataType : "json",
			data:{	"ParametersRequest": JSON.stringify(jsonRequest)
			},
			beforeSend : function( xhr ) {
				if (xhr && xhr.overrideMimeType) {
					xhr.overrideMimeType("application/j-son;charset=UTF-8");
	            }
				console.log("Request Service...");
				$(objectElement).prop("disabled", true);
			},
			success : function(jsonResponse)
			{
//				console.log(JSON.stringify(jsonResponse));
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
//					console.log("valida:: "+jsonResponse._State);
					if(jsonResponse._State=="true")
					{
						mostrarSuccess(jsonResponse._Message);
						datatable_lista.clear().draw();
						listadoTipotratamiento=null;
						switch(jsonRequest.tipoCatalogo)
						{
							case "tipotratamiento":
								getTipotratamiento();
							break;
							default:
								mostrarError("Tipo de catalogo no soportado");
						}
					}else
					{
						mostrarError(jsonResponse._Message);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [addCatalogo]");
				$(objectElement).prop("disabled", false);
		    }
		});
	}
}

function getJsonResquest(tipotratamiento)
{
	var error="";
	var jsonRequest={};
	var exists=false;
	$("div[class='form-group catalogo']").children().each(function()
	{
		if(this.type!=undefined)
		{
//		    console.log("Value:: ["+this.value+"] id:: ["+this.id+"] type:: "+this.type);
			var valueObject=null;
			switch(this.type)
			{
				case "checkbox":
					valueObject=$(getTipoElemento(this.type)+"[id='"+this.id+"']").is(":checked");
				break;
				default:
					valueObject=$(getTipoElemento(this.type)+"[id='"+this.id+"']").val();
			}
			
		    if(valueObject==undefined || valueObject==null || String(valueObject).trim().length<1)
		    {
		    	error+="\n > Es necesario completar "+$(getTipoElemento(this.type)+"[id='"+this.id+"']").attr("placeholder");
		    }
		    else
		    {
		    	jsonRequest[tipotratamiento+this.id]=valueObject;
		    	exists=true;
		    }
		}
	});
	if(error!="")
	{
		console.log(error);
		mostrarError(error);
	}
	else
	{
		if(exists)
			return jsonRequest;
	}
	return null;
}

function getTipotratamiento()
{
	if(listadoTipotratamiento==undefined || listadoTipotratamiento==null || listadoTipotratamiento.length<0)
	{
		$.ajax(
		{
	        async: true,
			url : '/catalogos/tipotratamiento',
			type : 'POST',
			dataType : 'json',
			success : function(jsonResponse)
			{
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
					if(jsonResponse._State=="true")
					{
						listadoTipotratamiento=JSON.parse(jsonResponse._ObjectJson);
						doPrintListaTipotratamiento();
					}else
					{
						mostrarError(jsonResponse._Message);
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [InformacionServer]");
		    }
		});
	}
	else
	{
		doPrintListaTipotratamiento();
	}
}

function doPrintListaTipotratamiento()
{
	if(listadoTipotratamiento!=undefined && listadoTipotratamiento!=null && listadoTipotratamiento.length>0)
	{
		for(var i=0; i<listadoTipotratamiento.length; i++)
		{
			datatable_lista.row.add(
					[
						listadoTipotratamiento[i].tipotratamientonombre,
						listadoTipotratamiento[i].tipotratamientodescripcion,
						new Date(listadoTipotratamiento[i].tipotratamientofechacreacion).toLocaleString(),
						(listadoTipotratamiento[i].tipotratamientoactivo ? "Activo" : "Inactivo"),
						"<input type='button' class='btn btn-danger' value='[x]' onclick='confirmarMovimiento(1, [\"tipotratamiento\",\""+listadoTipotratamiento[i].tipotratamientoid+"\"], this);'/>"
					]).draw();
		}
	}
	else
	{
		mostrarError("Error al recuperar los tipos de tratamientos");
	}
}
