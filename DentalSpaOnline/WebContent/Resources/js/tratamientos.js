var arrayTratamiento = new Map();
var arrayJsonPeticon = new Map();
var datatable_tratamientos;
var getCanvas;

$(document).ready(function()
{
	$("#btn-search").click(function(){
		var parametroBusqueda = $("#in-search").val();
		if(parametroBusqueda!=undefined && parametroBusqueda!=null && parametroBusqueda.trim().length>0)
		{
			parametroBusqueda={"rq_parametroBusqueda":parametroBusqueda};
			buscaPaciente(parametroBusqueda, this, "container_search_result");
			$("#in-search").val("");
		}
		else
		{
			mostrarError("Indique un parametro de busqueda");
		}
	});

	$("#btn-export-img-odontograma").on('click', function() { 
		console.log("export img");
		
		html2canvas(document.querySelector("#odontograma-div")).then(canvas => {
			getCanvas=canvas;
			$("#previewImage").html("");
			$("#previewImage").append(canvas);
			$("#previewImage").append("<div style='text-align:center;'><a href='#' id='btn-download' class='btn btn-info btn-lg' onclick='downloadImg();'>Descargar Imagen</a></div>");
		});
	}); 
	
	$("#btn-guardar-tratamiento").click(function(event){
		event.preventDefault();
		guardar("tratamiento", this);
	});
	
	datatable_tratamientos = $("#datatable_tratamientos").DataTable();

    createOdontogram();
    
    $(".click").click(function(event) {
        var control = $("#controls").children().find('.active').attr('id');
        console.log("Ctr["+control+"] >> "+$(this).attr('id'))
        if(control!="limpiar" && 
        		($(this).hasClass("click-caries") || $(this).hasClass("click-restauracion")
        				|| $(this).hasClass("click-ausencia") || $(this).hasClass("click-extraer")
        				 || $(this).hasClass("click-r-desajustada") || $(this).hasClass("click-r-desajustada-centro")))
        {
        	mostrarInfo("Este cuadrante ya cuenta con tratamiento");
        	return;
        }
        
        switch (control) {
            case "caries":
                $(this).addClass('click-caries');
                addArrayTratamiento($(this).attr('id'), 'CARIES');
                
                break;
            case "restauracion":
            	$(this).addClass('click-restauracion');
            	addArrayTratamiento($(this).attr('id'), 'RESTAURACION');

                break;
            case "ausencia":
                $(this).parent().children().each(function(index, el) {
                    if ($(el).hasClass("click")) {
                    		$(el).addClass("click-ausencia");
                    }
                });
            	addArrayTratamiento($(this).attr('id'), 'AUSENCIA');
                break;
            case "extraer":
                $(this).parent().children().each(function(index, el) {
                    if ($(el).hasClass("click")) {
                    	$(el).addClass("click-extraer");
                    }
                });
            	addArrayTratamiento($(this).attr('id'), 'EXTRACCION');
                break;
            case "r.desajustada":
                $(this).parent().children().each(function(index, el) {
                    if ($(el).hasClass("click") && $(el).hasClass('cuadro')) {
                    	$(el).addClass("click-r-desajustada");
                    	$(this).parent().children('.centro').addClass("click-r-desajustada-centro");
                    }
                });
            	addArrayTratamiento($(this).attr('id'), 'R. DESAJUSTADA');
                break;
            case "limpiar":
            	$(this).parent().children().each(function () {
            		console.log("ElementID["+this.id+"]");
            		$("div[id='"+this.id+"']").removeClass("click-extraer");
            		$("div[id='"+this.id+"']").removeClass("click-ausencia");
            		$("div[id='"+this.id+"']").removeClass("click-restauracion");
            		$("div[id='"+this.id+"']").removeClass("click-caries");
            		$("div[id='"+this.id+"']").removeClass("click-r-desajustada");
            		$(this).parent().children('.centro').removeClass("click-r-desajustada-centro");
            	});
            	delArrayTratamiento(this.id);
            break;
            default:
                console.log("not found");
        }
        return false;
    });
    return false;
});

function downloadImg()
{
	var imgageData = getCanvas.toDataURL("image/png"); 

	// Now browser starts downloading 
	// it instead of just showing it 
	var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream"); 

	$("#btn-download").attr("download", "odontograma.png").attr("href", newData); 
}

function loadInfo(idpaciente)
{
	if(idpaciente==undefined || idpaciente==null || pacienteid=="" || !isInt(idpaciente))
	{
		mostrarError("No se recupero el id del paciente");
		return;
	}
	limpiarValores("");
	limpiarValores("examendental");
	limpiarValores("tratamiento");
	$("#tbody_container_tratamientos").html("");
	disabledMasive("examendental", false);
	$("#btn-guardar-examendental").toggle(false);
	$("#btn-guardar-tratamiento").toggle(false);
    datatable_tratamientos.clear().draw();
	
	var paciente = $("#cont_info_paciente_id-"+idpaciente).data("json_paciente");
//	console.log("GetInfo :: "+JSON.stringify(paciente));
	if(paciente==undefined || paciente==null)
	{
		mostrarError("No se recupero la informacion del paciente");
		return;
	}
	
//	INFORMACION DEL PACIENTE
	$("#info_pacienteid").val(paciente.pacientenombre+" "+paciente.pacienteapellidopaterno+" "+paciente.pacienteapellidomaterno);
	$("#info_pacientesexo").val((paciente.pacientesexo=="H" ? "Hombre" : "Mujer"));
	$("#info_pacienteedad").val(paciente.pacienteedad);
	
//	EXAMEN DENTAL DEL PACIENTE
	$("#pacienteid").val(paciente.pacienteid);
	if(paciente.examendental!=undefined && paciente.examendental.examendentalid>0)
	{
        $("#examendentalhigiene").val(paciente.examendental.examendentalhigiene);
        $("#examendentalhigiene").prop("disabled", true);
        $("#examendentalencia").val(paciente.examendental.examendentalencia);
        $("#examendentalencia").prop("disabled", true);
        $("#examendentalpisoboca").val(paciente.examendental.examendentalpisoboca);
        $("#examendentalpisoboca").prop("disabled", true);
        $("#examendentallabios").val(paciente.examendental.examendentallabios);
        $("#examendentallabios").prop("disabled", true);
        $("#examendentalcarillos").val(paciente.examendental.examendentalcarillos);
        $("#examendentalcarillos").prop("disabled", true);
        $("#examendentalbruxismo").val(paciente.examendental.examendentalbruxismo);
        $("#examendentalbruxismo").prop("disabled", true);
        $("#examendentalchasquidos").val(paciente.examendental.examendentalchasquidos);
        $("#examendentalchasquidos").prop("disabled", true);
        $("#examendentallengua").val(paciente.examendental.examendentallengua);
        $("#examendentallengua").prop("disabled", true);
        $("#examendentalpaladar").val(paciente.examendental.examendentalpaladar);
        $("#examendentalpaladar").prop("disabled", true);
        $("#examendentalsupernumerarios").val(paciente.examendental.examendentalsupernumerarios);
        $("#examendentalsupernumerarios").prop("disabled", true);
        $("#examendentalsangrado").val(paciente.examendental.examendentalsangrado);
        $("#examendentalsangrado").prop("disabled", true);
        $("#examendentalplacasarro").val(paciente.examendental.examendentalplacasarro);
        $("#examendentalplacasarro").prop("disabled", true);
        $("#examendentalalteraciones").val(paciente.examendental.examendentalalteraciones);
        $("#examendentalalteraciones").prop("disabled", true);
        $("#examendentalnota").val(paciente.examendental.examendentalnota);
        $("#examendentalnota").prop("disabled", true);

    	$.ajax(
		{
	        async: true,
			url : 'listaTratamientos',
			type : 'POST',
			dataType : 'json',
			data:{	"ParametersRequest": "{rq_pacienteid : "+paciente.pacienteid+"}"
			},
			beforeSend : function( xhr ) {
				if (xhr && xhr.overrideMimeType) {
					xhr.overrideMimeType("application/j-son;charset=UTF-8");
	            }
				console.log("Request Service...");
			},
			success : function(jsonResponse)
			{
//    					console.log(JSON.stringify(jsonResponse));
				if(jsonResponse._State=='REDIRECT')
				{
					console.log("Error en la peticion:: "+jsonResponse._Message);
					mostrarError(jsonResponse._Message);
					window.location.href = '/log/out?mensaje='+jsonResponse._Message;
				}
				else
				{
//    				console.log("valida:: "+jsonResponse._State);
					if(jsonResponse._State=="true")
					{
						listaTratamientos = JSON.parse(jsonResponse._ObjectJson);
						if(listaTratamientos!=undefined && listaTratamientos!=null && listaTratamientos.length>0)
						{
							for(var i=0; i<listaTratamientos.length; i++)
							{
								datatable_tratamientos.row.add(
									[ 
										listaTratamientos[i].tipotratamiento.tipotratamientonombre, 
										(listaTratamientos[i].tratamientoterminado ? "Terminado" : "Activo"),
										new Date(listaTratamientos[i].tratamientofechacreacion).toLocaleString(),
										new Date(listaTratamientos[i].tratamientofechamodificacion).toLocaleString(),
										"<input type='button' class='btn btn-info' value='Cargar' onclick='loadTratamiento("+listaTratamientos[i].tratamientoid+")'/>"
										+"<div id='info_tramiento_id-"+listaTratamientos[i].tratamientoid+"' data-json_tratamiento='"+JSON.stringify(listaTratamientos[i])+"'></div>"
									]).draw();
							}
						}
					}
				}
			},
		    error : function(jqXHR, exception)
		    {
		    	console.log("["+exception.statusText+"] ", exception);
		    	console.log("info: "+ exception.statusText);
		    	alert(getErrorMessage(jqXHR, exception));
		    },
		    complete : function()
		    {
		    	console.log("Completed [Request]");
		    }
		});
        
//		HABILITA BOTONES
		$("#btn-guardar-tratamiento").toggle(true);
		mostrarSuccess("Informacion del paciente cargada");
	}
	else
	{
		$("#btn-guardar-examendental").toggle(true);
		mostrarSuccess("Es necesario que registren su examen dental");
	}
	$("#container_search_result").html("");
}

function loadTratamiento(tratamientoid)
{
	if(tratamientoid==undefined || tratamientoid==null || tratamientoid=="" || !isInt(tratamientoid))
	{
		mostrarError("No se recupero el id del tratamiento");
		return;
	}

	var tratamiento = $("#info_tramiento_id-"+tratamientoid).data("json_tratamiento");
	if(tratamiento==undefined || tratamiento==null)
	{
		mostrarError("No se recupero la informacion del tratamiento");
		return;
	}
	
	$("#tipotratamientoid option[value='"+tratamiento.tipotratamientoid+"']").attr("selected", true);
    $("#tipotratamientoid").prop("disabled", true);
    
    $("#tratamientoplan").val(tratamiento.tratamientoplan);
    $("#tratamientoplan").prop("disabled", true);
    
    $("#tratamientocomentariosiniciales").val(tratamiento.tratamientocomentariosiniciales);
    $("#tratamientocomentariosiniciales").prop("disabled", true);
    
    $("#titulo-tipovisita").text("Tratamiento")
	$("#container_view").html(""
			+"<div class='row'>"
			+"	<div class='col'>"
			+"		<h3 style='text-align:center;'>Odontograma</h3>"
			+"	</div>"
			+"</div>"
			+"<div class='row'>"
			+"	<div class='col-md-1'></div>"
			+"	<div class='col-md-10'>"
			
			+"      <div class='card shadow mb-4'>"
			+"              <div class='card-header py-3'>"
			+"                      <h6 class='m-0 font-weight-bold text-primary'>Odontograma</h6>"
			+"              </div>"
			+"              <div class='card-body' id='container_detalle_odontograma'>"
//			+"                      <div class='card mb-4 py-3 border-left-primary'>"
//			+"                              <div class='card-body'>.border-left-primary</div>"
//			+"                      </div>"
			+"              </div>"
			+"      </div>"
			
//            +"      <div class='card shadow mb-4'>"
//            +"            <div class='card-header py-3'>"
//            +"                    <h6 class='m-0 font-weight-bold text-primary'>Detalle</h6>"
//            +"            </div>"
//            +"            <div class='card-body'>"
//            +"                    <div class='table-responsive'>"
//            +"                            <div class='table-responsive'>"
//            +"                                    <table class='table table-bordered' id='dataTable_odontrograma' width='100%' cellspacing='0'>"
//            +"                                            <thead>"
//            +"                                                    <th>Diente</th>"
//            +"                                                    <th>Fecha Registro</th>"
//            +"                                                    <th colspan='4'>Comentarios</th>"
//            +"                                            </thead>"
//            +"                                            <tbody>"
//            +"                                            </tbody>"
//            +"                                    </table>"
//            +"                            </div>"
//            +"                    </div>"
//            +"            </div>"
//            +"      </div>"
            +"	</div>"
			+"	<div class='col-md-1'></div>"
			+"</div>"
			+"<div class='row'>"
			+"	<div class='col'>"
			+"		<h3 style='text-align:center;'>Visitas Anteriores</h3>"
			+"	</div>"
			+"</div>"
			+"<div class='row'>"
			+"	<div class='col-md-1'></div>"
			+"	<div class='col-md-10'>"
            +"      <div class='card shadow mb-4'>"
            +"            <div class='card-header py-3'>"
            +"                    <h6 class='m-0 font-weight-bold text-primary'>Ultimas visitas</h6>"
            +"            </div>"
            +"            <div class='card-body'>"
            +"                    <div class='table-responsive'>"
            +"                            <div class='table-responsive'>"
            +"                                    <table class='table table-bordered' id='dataTable_visitas_anteriores' width='100%' cellspacing='0'>"
            +"                                            <thead>"
            +"                                                    <th>Titulo</th>"
            +"                                                    <th>Fecha Visita</th>"
            +"                                                    <th colspan='2'>Comentarios</th>"
            +"                                            </thead>"
            +"                                            <tbody id='tbody_container_visitas_Anteriores'>"
            +"                                            </tbody>"
            +"                                    </table>"
            +"                            </div>"
            +"                    </div>"
            +"            </div>"
            +"      </div>"
            +"	</div>"
			+"	<div class='col-md-1'></div>"
			+"</div>"
			+"<div class='row'>"
			+"	<div class='col'>"
			+"		<h3 style='text-align:center;'>Agregar Visita</h3>"
			+"	</div>"
			+"</div>"
			+"<div class='row'>"
			+"	<div class='col-md-4'>"
			+"		<div class='row'>"
			+"			<div class='col'>"
			+"				<div class='form-group tratamientodetalle'>"
			+"					<label>Titulo de la visita</label>"
			+"					<input type='hidden' id='tratamientoid' value='"+tratamiento.tratamientoid+"' />"
			+"					<input type='hidden' id='pacienteid' value='"+tratamiento.pacienteid+"' />"
			+"					<input type='text' class='form-control' id='tratamientodetallenombre' placeholder='Titulo visita' onchange='validaCampo(\"NULO\",this);' />"
			+"				</div>"
			+"			</div>"
			+"		</div>"
			+"		<div class='row'>"
			+"			<div class='col'>"
			+"				<div class='form-group tratamientodetalle'>"
			+"					<input type='checkbox' class='form-check-input' id='fintratamiento' value='0'/>"
			+"					<label class='form-check-label' for='fintratamiento'>Fin de tratamiento</label>"
			+"				</div>"
			+"			</div>"
			+"		</div>"
			+"	</div>"
			+"	<div class='col-md-8'>"
			+"		<div class='form-group tratamientodetalle'>"
			+"			<label>Comentarios</label>"
			+"			<textarea class='form-control' id='tratamientodetallecomentarios' placeholder='Comentarios' onchange='validaCampo(\"NULO\",this);'></textarea>"
			+"		</div>"
			+"	</div>"
			+"</div>"
			+"<div class='row'>"
			+"	<div class='col-md-4'></div>"
			+"	<div class='col-md-4'>"
			+"		<input type='button' class='btn btn-info btn-block' value='Guardar visita' onclick='guardar(\"tratamientodetalle\",this);' "+(tratamiento.tratamientoterminado ? "disabled" : "")+">"
			+"	</div>"
			+"	<div class='col-md-4'></div>"
			+"</div>"
			+"<hr>"
			);
    
	
	 // Obteniendo todas las claves del JSON
	 for (var clave in tratamiento.odontogramas){
	   // Controlando que json realmente tenga esa propiedad
	   if (tratamiento.odontogramas.hasOwnProperty(clave) 
			   && String(clave).indexOf("_")>0 && String(tratamiento.odontogramas[clave]).trim().length>0) {
	     // Mostrando en pantalla la clave junto a su valor
		   console.log("La clave es " + clave+ " y el valor es " + tratamiento.odontogramas[clave]);
		   $("#container_detalle_odontograma").append(""
					+"<div class='card mb-4 py-3 border-left-primary'>"
					+"	<div class='card-body'>"
					+"		<h5>Diente "+String(clave).split("_")[1]+"</h5>"
					+		replaceAll("&", "<li>", String(tratamiento.odontogramas[clave]))
					+"	</div>"
					+"</div>");
	   }
	 }
    
    
    
    if(tratamiento.tratamientosdetalle!=undefined && tratamiento.tratamientosdetalle!=null && tratamiento.tratamientosdetalle.length>0)
    {
//    	if(dataTable_visitas_anteriores==undefined)
//    		dataTable_visitas_anteriores = $("#dataTable_citas_anteriores").DataTable();
    	console.log("tamanio :: "+tratamiento.tratamientosdetalle.length);
	    for(var i=0; i<tratamiento.tratamientosdetalle.length; i++)
	    {
//	    	console.log(" >> "+tratamiento.tratamientosdetalle[i].tratamientodetallenombre+" :: "
//	    	    	+new Date(tratamiento.tratamientosdetalle[i].tratamientodetallefechamodificacion).toLocaleString()+" :: "
//	    	    	+tratamiento.tratamientosdetalle[i].tratamientodetallecomentarios);
	    	$("#tbody_container_visitas_Anteriores").append(""
	    			+"<tr>"
	    			+"	<td>"+tratamiento.tratamientosdetalle[i].tratamientodetallenombre+"</td>" 
	    			+"	<td>"+new Date(tratamiento.tratamientosdetalle[i].tratamientodetallefechamodificacion).toLocaleString()+"</td>" 
	    			+"	<td>"+tratamiento.tratamientosdetalle[i].tratamientodetallecomentarios+"</td>"
	    			+"</tr>");
//	    	dataTable_visitas_anteriores.row.add(
//				[ 
//					tratamiento.tratamientosdetalle[i].tratamientodetallenombre, 
//					new Date(tratamiento.tratamientosdetalle[i].tratamientodetallefechamodificacion).toLocaleString(), 
//					tratamiento.tratamientosdetalle[i].tratamientodetallecomentarios
//				]).draw();
	    }
    }
	mostrarSuccess("Informacion del tratamiento cargada");
	
}

function delArrayTratamiento(idElement)
{
	idElement = idElement.substring(1, idElement.length);
	if(arrayTratamiento!=undefined && arrayTratamiento.size>0 && arrayTratamiento.get(idElement)!=undefined)
	{
		arrayTratamiento.delete(idElement);
		$("#container-diente-"+idElement).remove();
	}
	else
	{
		console.log("No existe para eliminacion");
	}
}

function addArrayTratamiento(idElement, tratamiento)
{
	var cuadrante = idElement.substring(0, 1);
	var numeroDiente = parseInt(idElement.substring(2, 3));
	var Numerocuadrante = parseInt(idElement.substring(1, 2));
	console.log("Numerocuadrante :: "+Numerocuadrante);
	cuadrante = cuadrante=="c" ? (numeroDiente<4 ? "INCISAL" : "OCLUSAL")//:"VESTIBULAR" 
			: cuadrante=="t" ? "VESTIBULAR"
					: cuadrante=="l" ? (Numerocuadrante=="2" || Numerocuadrante=="3" ? "MESIAL" : "DISTAL")
							: cuadrante=="r" ? (Numerocuadrante=="1" || Numerocuadrante=="4" ? "MESIAL" : "DISTAL")
									: cuadrante=="b" ? "PALANTINO O LINGUAL" 
											: "DESCONOCIDO ["+cuadrante+"]";
	idElement = idElement.substring(1, idElement.length);
	if(arrayTratamiento==undefined || arrayTratamiento==null || arrayTratamiento.size<1 || arrayTratamiento.get(idElement)==undefined)
	{
		arrayTratamiento.set(idElement, tratamiento.trim());
		$("#container_detalle_tratamiento").append(""
				+"<div class='card mb-4 py-3 border-left-primary' id='container-diente-"+idElement+"'>"
				+"	<div class='card-body' id='detalle-diente-"+idElement+"'>"
				+"		<label>Detalle del tratamiento en el diente "+idElement+"</label>"
				+"		<div id='lista-detalle-tratamiento-diente-"+idElement+"-"+tratamiento+"'>"
				+"			<div class='form-group tratamiento'>"
				+"				<label>"+tratamiento+" en cara "+cuadrante+"</label>"
				+"				<input type'text' class='form-control' id='odontograma_"+idElement+"-"+cuadrante+"-"+tratamiento+"' placeholder='Detalle del diente "+idElement+" en la cara "+cuadrante+" por tratamiento de "+tratamiento+"' onchange='validaCampo(\"NULO\",this);'/>"
				+"			</div>"
				+"		</div>"
				+"</div>");
	}
	else
	{
//		console.log(idElement+" >> "+arrayTratamiento.get(idElement)+" == "+tratamiento);
		if(arrayTratamiento.get(idElement)!=undefined && arrayTratamiento.get(idElement)==tratamiento)
		{
			$("div[id='lista-detalle-tratamiento-diente-"+idElement+"-"+tratamiento+"']").append(""
					+"			<div class='form-group tratamiento'>"
					+"				<label>"+tratamiento+" en cara "+cuadrante+"</label>"
					+"				<input type'text' class='form-control' id='odontograma_"+idElement+"-"+cuadrante+"-"+tratamiento+"' placeholder='Detalle del diente "+idElement+" en la cara "+cuadrante+" por tratamiento de "+tratamiento+"' onchange='validaCampo(\"NULO\",this);'/>"
					+"			</div>");
		}
		else
		{
			$("div[id='detalle-diente-"+idElement+"']").append(""
					+"		<div id='lista-detalle-tratamiento-diente-"+idElement+"-"+tratamiento+"'>"
					+"			<div class='form-group tratamiento'>"
					+"				<label>"+tratamiento+" en cara "+cuadrante+"</label>"
					+"				<input type'text' class='form-control' id='odontograma_"+idElement+"-"+cuadrante+"-"+tratamiento+"' placeholder='Detalle del diente "+idElement+" en la cara "+cuadrante+" por tratamiento de "+tratamiento+"' onchange='validaCampo(\"NULO\",this);'/>"
					+"			</div>"
					+"		</div>");
		}
		console.log("Ya la han agreado :: "+idElement);
	}
}

function guardar(tipoFormulario, objectElement)
{
	var error="";
	var jsonRequest={};
	$("div[class='form-group "+tipoFormulario+"']").children().each(function () {
		if(this.type!=undefined)
		{
//		    console.log("Value:: ["+this.value+"] id:: ["+this.id+"] type:: "+this.type);
			var valueObject=null;
			switch(this.type)
			{
				case "checkbox":
					valueObject=$(getTipoElemento(this.type)+"[id='"+this.id+"']").is(":checked");
				break;
				default:
					valueObject=$(getTipoElemento(this.type)+"[id='"+this.id+"']").val();
			}
			
		    if(valueObject==undefined || valueObject==null || String(valueObject).trim().length<1)
		    {
//		    	console.log("Val error:: [id='"+this.id+"'] :: "+valueObject);
		    	error+="\n > Es necesario completar "+$(getTipoElemento(this.type)+"[id='"+this.id+"']").attr("placeholder");
		    }
		    else
		    {
		    	if(tipoFormulario=="tratamiento" && this.id.split("-").length>2)
		    	{
		    		jsonRequest["rq_"+this.id.split("-")[0]]="Tratamiento en la cara "+this.id.split("-")[1]+" para tratar "+this.id.split("-")[2]+": "+valueObject;
		    		if(arrayJsonPeticon.size<1 || arrayJsonPeticon.get("rq_"+this.id.split("-")[0])==undefined)
		    			arrayJsonPeticon.set("rq_"+this.id.split("-")[0], "&Tratamiento en la cara "+this.id.split("-")[1]+" para tratar "+this.id.split("-")[2]+": "+valueObject);
		    		else
		    			arrayJsonPeticon.set("rq_"+this.id.split("-")[0], arrayJsonPeticon.get("rq_"+this.id.split("-")[0])+"&Tratamiento en la cara "+this.id.split("-")[1]+" para tratar "+this.id.split("-")[2]+": "+valueObject);
		    	}
	    		else
	    		{
			    	jsonRequest["rq_"+this.id]=valueObject;
	    		}
		    }
		}
	});
	if(error!="")
	{
		console.log(error);
		mostrarError(error);
	}
	else
	{
		if(tipoFormulario=="tratamiento")
    	{
			arrayJsonPeticon.forEach(function(value, key){
			    jsonRequest[key]=value;
			});
    	}
		
		jsonRequest={proceso: tipoFormulario, ParametersRequest: JSON.stringify(jsonRequest)};

		console.log("Json_PETICION:: "+JSON.stringify(jsonRequest));
		guardarCambios(jsonRequest, objectElement);
		disabledMasive(tipoFormulario, true);
	}
}

function guardarCambios(jsonParameter, objectElement)
{
	$.ajax(
	{
        async: true,
		url : 'crear',
		type : 'POST',
		dataType : 'json',
		data:{	"ParametersRequest": JSON.stringify(jsonParameter)
		},
		beforeSend : function( xhr ) {
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
            }
			console.log("Request Service...");
			$(objectElement).prop('disabled', true);
			ShowLoadBar();
		},
		success : function(jsonResponse)
		{
//			console.log(JSON.stringify(jsonResponse));
			if(jsonResponse._State=='REDIRECT')
			{
				console.log("Error en la peticion:: "+jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/log/out?mensaje='+jsonResponse._Message;
			}
			else
			{
//				console.log("valida:: "+jsonResponse._State);
				if(jsonResponse._State=="true")
				{
					mostrarSuccess(jsonResponse._Message);
					$("#btn-guardar-tratamiento").toggle();
					limpiarDetalleOdontograma();
				}else
				{
					mostrarError(jsonResponse._Message);
				}
			}
		},
	    error : function(jqXHR, exception)
	    {
	    	console.log("["+exception.statusText+"] ", exception);
	    	console.log("info: "+ exception.statusText);
	    	alert(getErrorMessage(jqXHR, exception));
	    },
	    complete : function()
	    {
	    	console.log("Completed [InformacionServer]");
			$(objectElement).css("display", "none");
			HiddenLoadBar()
	    }
	});
}

function createOdontogram() {
    var htmlLeft = "",
        htmlRight = "",
        a = 1;
    for (var i = 9 - 1; i >= 1; i--) {
        //Dientes Definitivos Cuandrante Derecho (Superior/Inferior)
        htmlRight += '<div data-name="value" id="dienteindex' + i + '" class="diente">' +
            '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-info">index' + i + '</span>' +
            '<div id="tindex' + i + '" class="cuadro click">' +
            '</div>' +
            '<div id="lindex' + i + '" class="cuadro izquierdo click">'+
            '</div>' +
            '<div id="bindex' + i + '" class="cuadro debajo click">' +
            '</div>' +
            '<div id="rindex' + i + '" class="cuadro derecha click click">' +
            '</div>' +
            '<div id="cindex' + i + '" class="centro click">' +
            '</div>' +
            '</div>';
        //Dientes Definitivos Cuandrante Izquierdo (Superior/Inferior)
        htmlLeft += 
        	'<div id="dienteindex' + a + '" class="diente">' +
            '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-info">index' + a + '</span>' +
            '<div id="tindex' + a + '" class="cuadro click">' +
            '</div>' +
            '<div id="lindex' + a + '" class="cuadro izquierdo click">' +
            '</div>' +
            '<div id="bindex' + a + '" class="cuadro debajo click">' +
            '</div>' +
            '<div id="rindex' + a + '" class="cuadro derecha click click">' +
            '</div>' +
            '<div id="cindex' + a + '" class="centro click">' +
            '</div>' +
            '</div>';
        
        a++;
    }
    $("#tr").html(replaceAll('index', '1', htmlRight));
    $("#tl").html(replaceAll('index', '2', htmlLeft));

    $("#bl").html(replaceAll('index', '3', htmlLeft));
    $("#br").html(replaceAll('index', '4', htmlRight));
}

function addEvidencia()
{
	var idRandom = new Date().getTime();
	$("#container_evidencias").append(""
				+"<div class='card mb-4 py-3 border-left-primary' id='container-evidencia-"+idRandom+"'>"
				+"	<div class='card-body'>"
				+"		<label>Adjuntar imagen</label>"
				+"		<div class='row'>"
				+"			<div class='col-md-11'>"
				+"				<div class='form-group tratamiento'>"
				+"					<label class='custom-file' id='customFile'>"
				+"					<input type='file' accept='image/*;capture=camera' class='custom-file-control' aria-describedby='fileHelp' id='file_"+idRandom+"' name='file_"+idRandom+"' lang='es'/>"
				+"					<span class='custom-file-control custom-control-file'></span>"
				+"					</label>"
				+"				</div>"
				+"			</div>"
				+"			<div class='col-md-1'>"
				+"				<input type='button' class='btn btn-danger' value='[X]' onclick='$(\"#container-evidencia-"+idRandom+"\").remove();'>"
				+"			</div>"
				+"		</div>"
				+"	</div>"
				+"</div>");
}

function limpiarDetalleOdontograma()
{
	arrayTratamiento.forEach(function(value, key){
		delArrayTratamiento("d"+key);
	});
}

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}